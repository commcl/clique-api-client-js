## Clique Client JS SDK

Clique Client JS SDK provides high-level tools for enterprise web developers to enhance their existing or build new web apps equipped with high quality, secure WebRTC-based collaboration and media processing applications. Just some basic knowledge of javascript and web development practices is required to take advantage of this technology.

For more info visit [Online Documentation](https://dev.cliqueapi.com/clique-api-client-js/CliqueSDK.html).