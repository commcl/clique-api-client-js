/*

This is a Javascript high-level library incapsulates Clique API and browser WebRTC functions
to provide an effective, easy to use way to equip your application with voice, screen 
sharing and chat functions.

Naming conventions:
in order, objects provides functions with camelCase names starts with a noun meaning the subject
following with the verb meaning the action.

Example: chatInvite - the function making "Invite" with the "chat"
PLEASE DO NOT PROVIDE FUNCTIONS WITH NAMES THAT ARE NOT FIT THIS CONVENTION

*/

(function (global, factory) {
    /*global define*/
    if (typeof define === 'function' && define.amd) {
        define(['./verto/verto', 'socket.io-client', 'peerjs'], factory);                 // AMD
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('./verto/verto'), require('socket.io-client'), require('peerjs')); // commonjs
    } else {
        global.CliqueClient = factory(global.Verto, global.io, global.Peer);                        // Browser
    }
}(this, function(Verto, io, Peer) {

    /*
    
    CliqueContact represents a contact information that is needed to invite a participant to the conference

    */

    class CliqueContact {

        constructor (data) {
            this._types = ['email', 'sms', 'call'];
            this.type = data.type;
            this.contact = data.contact || data.text;
            this.name = data.name;
        }
        
        validate () {
            switch (this.type) {
                case 'email':
                    return !!this.contact.match(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
                case 'sms':
                    return !!this.contact.match(/^(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/);
                case 'call':
                    return !!this.contact;
                default:
                    return false;
            }
        };

        toString () { return this.contact || ''; };
        isEmail () { return this.type === 'email'; };
        isSms () { return this.type === 'sms'; };
        isCall() { return this.type === 'call'; };
    }

    /*
    
    AjaxRequest is an internal function that is not exposed to library users.
    It represents a high-level interface on top of XMLHttpRequest and we
    use it to avoid soem third-party implmentation, such as jQuery

    */

    function AjaxRequest (request) {
        return new Promise(function(resolve, reject) {
            if (request.method == null) request.method = 'GET';

            var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
                if (xmlhttp.readyState == XMLHttpRequest.DONE ) {
                    if (xmlhttp.status >= 200 && xmlhttp.status < 400) return resolve(xmlhttp.response);

                    reject(xmlhttp.status);
                }
            };

            xmlhttp.responseType = request.dataType.toLowerCase() || '';
            var params = '';
            for (var key in request.data) {
                params = params + '&' + key + '=' + request.data[key];
            }
            params = params.replace('&','');

            xmlhttp.open(request.method, request.url + ( (request.method === 'GET' && params) ? '?'+params : ''), true);
            xmlhttp.setRequestHeader('X-Requested-With', 'XMLHttpRequest');

            if (request.bearer) xmlhttp.setRequestHeader('Authorization', 'Bearer ' + request.bearer);

            if (request.method !== 'GET') {
                xmlhttp.setRequestHeader('Content-Type', 'application/json');
                xmlhttp.send( JSON.stringify(request.data) );
            } else {
                xmlhttp.send();
            }
        });
    }

    /*
    
    CliqueAPI represents a JS wrapper to Clique RESTful API.
    It uses a temporary session token to authenticate. You should implement a session
    token issuement and updation using your application.

    CliqueAPI is not exposed to library users while its instance is available as a part of
    Conference and SimpleDial objects.

    */

    class CliqueAPI {
        constructor (options) {
            let self = this;
            self.sessionToken = options.sessionToken;
            self.apiEndpoint = options.baseURL + '/api/v2';
        }

        init() {
            let self = this;
            return new Promise(function(resolve, reject){
                if (self.user) return resolve();
    
                AjaxRequest({
                    url : self.apiEndpoint + '/users/?token=' + self.sessionToken,
                    dataType : 'JSON',
                    bearer: self.sessionToken
                })
                .then(function(data) {
                    if (!data.ok) return reject({code:'api-error', data: data});
                    self.user = data.result[0];
                    return resolve();
                })
                .catch(function(error) {
                    return reject({code:'http-error', data: error});
                });
            });
        }

        userUpdate (user_data) {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/users/'+self.user.uuid,
                method: 'PUT',
                dataType : 'JSON',
                bearer: self.sessionToken,
                data: user_data
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};

                self.user = data.user;
                return self.user;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        userFetch () {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/users/'+self.user.uuid,
                method: 'GET',
                dataType : 'JSON',
                bearer: self.sessionToken,
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};

                self.user = data.user;
                return self.user;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        conferenceCreate (options) {
            let self = this;
            var post_data = { creator_uuid: self.user.uuid, node_autoassign: true, settings: options && options.conference_settings  }
            if  ( options && options.p2p ) {
                post_data.node_autoassign = false;
                post_data.node_domain = 'p2p';
            }

            return AjaxRequest({
                url : self.apiEndpoint + '/conferences/',
                method: 'POST',
                dataType : 'JSON',
                bearer: self.sessionToken,
                data: post_data
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data.conference;
            });
        }


        conferenceFetch (id, startFlag) {
            let self = this;
            return AjaxRequest({
                url : self.apiEndpoint + '/conferences/' + id + (startFlag?'/start':''),
                method: startFlag?'POST':'GET',
                dataType : 'JSON',
                bearer: self.sessionToken,
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data.conference;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        conferenceUpdate(id, conference_data) {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/conferences/'+id,
                method: 'PUT',
                dataType : 'JSON',
                bearer: self.sessionToken,
                data: conference_data
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data.conference;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }
        
        conferenceUsers (id) {
            let self = this;
            
            return AjaxRequest({
                url : self.apiEndpoint + '/conferences/'+id+'/user-list-hash',
                method: 'GET',
                dataType : 'JSON',
                bearer: self.sessionToken
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data.result;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        conferenceSettings (id, settings) {
            let self = this;

            if (typeof settings === 'undefined')
                return AjaxRequest({
                    url : self.apiEndpoint + '/conferences/'+id+'/settings',
                    method: 'GET',
                    dataType : 'JSON',
                    bearer: self.sessionToken
                }).then(function(data) {
                    if (!data.ok) throw {code:'api-error', data: data};
                    return data.settings;
                }).catch(function(err) {
                    throw {code: 'http-error', data: err}
                });                
            else 
                return AjaxRequest({
                    url : self.apiEndpoint + '/conferences/'+id+'/settings',
                    method: 'PUT',
                    dataType : 'JSON',
                    bearer: self.sessionToken,
                    data: settings
                }).then(function(data) {
                    if (!data.ok) throw {code:'api-error', data: data};
                    return data.settings;
                }).catch(function(err) {
                    throw {code: 'http-error', data: err}
                });
        }

        conferenceInvite(id, inviteData) {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/invites/' + id + '/send',
                method: 'POST',
                dataType : 'JSON',
                bearer: self.sessionToken,
                data: inviteData
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data.failures;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        conferenceFind(params) {
            let self = this;

            var queryString = Object.keys(params).map(function(k) {
                return window.encodeURIComponent(k) + '=' + window.encodeURIComponent(params[k]);
            }).join('&');
            
            return AjaxRequest({
                url : self.apiEndpoint + '/conferences?' + queryString,
                method: 'GET',
                dataType : 'JSON',
                bearer: self.sessionToken
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data.result;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        recordingById(recordingId) {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/recording/' + recordingId,
                method: 'GET',
                dataType : 'JSON',
                bearer: self.sessionToken
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data.record;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        recordingBySessionId(sessionId) {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/recording/' + sessionId + '/session',
                method: 'GET',
                dataType : 'JSON',
                bearer: self.sessionToken
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data.record;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        phoneNumberAssign(id, data) {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/conferences/' + id + '/phone_number',
                method: 'POST',
                dataType : 'JSON',
                bearer: self.sessionToken,
                data: data
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data.phone_number;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        phoneNumberUpdate(id, data) {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/conferences/' + id + '/phone_number',
                method: 'PUT',
                dataType : 'JSON',
                bearer: self.sessionToken,
                data: data
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data.phone_number;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        phoneNumberRelease(id) {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/conferences/' + id + '/phone_number',
                method: 'DELETE',
                dataType : 'JSON',
                bearer: self.sessionToken
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return true;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        chatCreate(data = {}) {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/chats',
                method: 'POST',
                dataType : 'JSON',
                bearer: self.sessionToken,
                data: data,
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        chatJoin(chatId) {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/chats/' + chatId + '/join',
                method: 'POST',
                dataType : 'JSON',
                bearer: self.sessionToken,
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        chatLeave(chatId) {
            let self = this;

            return AjaxRequest({
                url : self.apiEndpoint + '/chats/' + chatId + '/leave',
                method: 'POST',
                dataType : 'JSON',
                bearer: self.sessionToken,
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        whitelistCreate(conference_id, allowed_users) {
            let self = this;
            return AjaxRequest({
                url : self.apiEndpoint + '/whitelists',
                method: 'POST',
                dataType : 'JSON',
                bearer: self.sessionToken,
                data: { conference_id: conference_id, allowed_users: allowed_users }
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        whitelistGet(conference_id) {
            let self = this;
            return AjaxRequest({
                url : self.apiEndpoint + '/whitelists/' + conference_id,
                method: 'GET',
                dataType : 'JSON',
                bearer: self.sessionToken
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        whitelistAddUsers(conference_id, users) {
            let self = this;
            return AjaxRequest({
                url : self.apiEndpoint + '/whitelists/' + conference_id + '/add_users',
                method: 'PUT',
                dataType : 'JSON',
                bearer: self.sessionToken,
                data: { users: users }
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        whitelistDelUsers(conference_id, users) {
            let self = this;
            return AjaxRequest({
                url : self.apiEndpoint + '/whitelists/' + conference_id + '/del_users',
                method: 'PUT',
                dataType : 'JSON',
                bearer: self.sessionToken,
                data: { users: users }
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }

        whitelistDelete(conference_id) {
            let self = this;
            return AjaxRequest({
                url : self.apiEndpoint + '/whitelists/' + conference_id,
                method: 'DELETE',
                dataType : 'JSON',
                bearer: self.sessionToken
            }).then(function(data) {
                if (!data.ok) throw {code:'api-error', data: data};
                return data;
            }).catch(function(err) {
                throw {code: 'http-error', data: err}
            });
        }
    }

    class CliqueAbstract {

        constructor(options) {
            if (typeof Verto === 'undefined') throw new Error('Verto is not available');
            if (typeof io === 'undefined') throw new Error('Socket.io is not available');
            if (typeof Peer === 'undefined') throw new Error('Peer.js is not available');
            if (!options || options && (!(options.base_url || options.baseURL) || !(options.session_token || options.sessionToken))) throw new Error('Invalid config params');

            let self = this;

            self.baseURL = options.base_url || options.baseURL;
            self.apiEndpoint = self.baseURL + '/api/v2';
            self.websocketEndpoint = self.baseURL;
            self.sessionToken = options.session_token || options.sessionToken;
            self.debug = options.debug || options.Debug || false;
            console.log('DEBUG is set to',self.debug);
            self.wssPort = options.wss_port || options.wssPort || '443';
            self.peerManager = options.peermanager || options.peerManager || self.baseURL + '/peermanager';
            self.iceServers = options.ice_servers || options.iceServers || [{url:'stun:stun.l.google.com:19302'}];
            self.musicOnHoldUrl = options.music_on_hold_url || options.musicOnHoldUrl || 
            'data:audio/mp3;base64,//uUZAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAASW5mbwAAAA8AAAHIAAKtgAADBQcLDQ8RFRcZHR8hIycpKy8xMzU5Oz1BQ0VHS01PUVVXWV1fYWNnaWtvcXN1eXt9gYOFh4uNj5GVl5mdn6Gjp6mrr7Gztbm7vcHDxcfLzc/R1dfZ3d/h4+fp6+/x8/X5+/0AAAA8TEFNRTMuOTlyAaoAAAAAAAAAABSAJAaZjgAAgAACrYA1hZEjAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//uUZAAAAgInR7njElQnoAnNBGMBi1UtOaekTZFCJSWw8wmwAICCik+lS5pBOjjHYFYuXPhBGieh8SmTm7wQuknJ6CFf35PwjSVPzoQQTdn/Po6JRyy5R3Pu5cc7/9kocKXCo4gkOWRxtpJLkzjws4WAaCinFBvU4o7lCf6Caz7smz+Od/3+7/7P/yBBLUaSSSSSvEzRhL3hcCxFGhBByCKxfDTVQ9AlNgntCjni2MJVA4C7UdmvmT/z7VCsw/LSciFBFkJJQqLVzqqJqCKAWqhMnrtor6fp//3/wOgtlaRkGE5H//lwGBLIXqE2FvF0ZSEDFFjbT7JGgUu6UZCI06DoBYPt64xZu0hl/cjZ93r27WQzZYnr55DkIEEE/MM53Z6u0+VJ0/TVv1R6v7fyH3X/COqKLegEhpQl01XgmMhgFaIcdaDT43g/DVkNqKfJLDgMZ7M5xX0aoLA6WliPREezueV3KHMkyeKwJF9FrcmzXsUz1NSU2CmRHghzTFsl//uUZCSAAuk8zmHnHaRNhHlZPMhWC1TZP6wkTfEMDCXw9Ikgzmq3z/5uU5LmmaDoCcLMXD9emNWACQgAAwOUxhciTjoFgO5SDqRiOczBRZyTIV1qMIBp5SB9vTfLI8K9YwOTeXmb1uLuomdqFjjScccFhC2li4aY/49D1uI3JoZekh2cUNFlRWNJFIgA1G+kNBnyeNnqHRhSEhVjyVHCZuimIDAWIGTc2kALiXxRWJ000fv/3kaimuZI0D2PcM8MQUyuvlRnbXVVKtiqcdaDP5326aGBiBlDm/0yhNehP/rBBpTBAIA4sGsELDTSapVRoA3DweaEYkNtEBO3fqeG2I7qy+j2P1ZRg8+6haCoRJRg9wpy41QpN/fr+ynwwDshWPyrlQkm42mkkQCqNxOg6DRDqHsPsxSDl/QB0j7MsdBLoaGnjy6YBiag2oebAmMiFi4pT7r9MpLJE9ZuohCo7zKCIqO9NXqrDA3tSrH/d6exVnL9WMClIuR23Zd2qxWc//uUZC4AAwlIUGnpE2xAQtm9PYNEC7k9Q6ekTbEOpSj0wJ3fI7//r/SERa2y0wknMFeiXSbRK09YAwTvFdbISo7yCGLbBaPDY+Q8ubSitLNi7n4SHuOqFVcW3HVr1zzLUSTF9mDYBk6iYSbkjSSaJBWJ+mhGCfiQKoRwwmc2iDrxPHpkhQlGDuOgKwJP7J0Imku1kKnU9U2Pi4Eg1GKTRnyK5i7lV6sSZ9Y76yozdD6cvyNt4PdlemmvbZcbcI0I3jVcIcIf9KKdsjaSjRBQOaHg+Gjx6AgOg1H1OiWLTs+QGE2Qfdy9C62WXKlgiVZlVRWTnPbdDm6HU/6dv///7ftSnveo4ibIPq+VJTbsiVUoINI1C7KMtBPj2HEbqNWz6eqotqHJRQsL5UiyMLNVeOEIfvDS0fXzUiCE46WrIfSILMGpmA2TQ8kMLPYFg6U3bFGkucxSkL/NLQVc/OIF1BVSoCzOFEMIvQsB8nKrCdk4aEbLJ4Ws7Y5oORvKrGaE//uUZDqAAqce0GHmRBxB4wl5PMNWCrj3NyekTxD8ByYo9gw4536R9hJHCh1OK5e0Ew2yk+6ZYti9O8ITNPTrbvFB6HQOUVSueqq8AS0LIePkfQupmEpSrkX4zFCbr8j204m0BIJi+O7JrUixZuBn4Zu++V0U0Eiuoac0HpNQ9Qbqt1ViTd4JA8nJfcf9OlnDCxlUocXSmbfsDOHQBrbBKRKoU4bAHA/YMYUZPD0bYnXFofCs+29UEry0OQS+ZzEs+bWaQdS4LEVvWuUd2ooXMzbuR9ZxHJHBw5SlJVVYkklJwFVItrAJeqzKDpgwWCIkA8IBiaDmNJ2OB0vQ2nR3WmhVYXcEpz12KNJoxBmrnSLQ0Jakmc721yOxkrUV9J8RRCRf/VVfMhOdRK8xU8PP/fZw6Ueb+F7mf79hIOPJf//3gG1iAIJNxyiU8DcOAPLGjSaGWGQNt3kGOFs1qVZju/2QJVWWhLbpZE7a1/20OMcBIowDgs5w5+j3mjstYokw//uUZFKAAzFHS9MMQfRBxQmtPMJEDMTtJUykb0EAhiiw95ieOXQKAIQAAAAJwD7BzI8GJGj1oCNLOmGAlcimxlozIIEpXAhEROBciKqEhYTZqTQDIodUbm5puMVenaSJAn5Lov7A1Euah85mOxRTkxMPfnmtPBmRZmVFEZlZC7wKyGvbgI6sCNHl//VrTktkkdVQwrUQK6IUIQOZAF5BYAGLS/6lWlKSRHCTAkeSaLiIBGgUC+0V17dwUJLDoudGkLRWKIb/7dOkytkbAqolJuRtVzCgNhrGqjV8QMk3O0fZbzDLgS9rWWIyAeo4y2fsZoGS1+U2O85TBIWSHQyKNIpJl8xyXlMr2PVXGiB1dQ7EYp7Nc7fa1bcgoEdaaD/6DYAAAjBBWLCnFJLYGkJYzD4RxsrVVZZ0CZXJ+r/h1jxmMmmvkYsAws5zaWyucDY0msxUkWQw8fX/z+ZUUWw2eWoOxVC9B+J6halRSKSdQBFrhpw+VkoqxvFqjW6ApEAx//uUZFkAApk7UGHmE9xGYblsPYMeCqCpNUwwZ9EeDCcw9gi0GXQnTE8yMVJxVLVqZcQlxOlymvwyIoIFYAJOqx9OsfWNv//34Z244l47cgKsfQtTg0l5GWo+MQ7//r0+lioPNZ71rArpbSWbEhNYlql2soQvLQlwc9/3OLraGXyzlpN1sxg7w61qkDdSTL2JGALWSF0gZRCt+lPJXhYC7NL0C7D6mEoWgQIcJABJKR58EmC6YBquBdAIgqShG6PYWGx0mWqTpaWN48SD3WG2C12AR0E4oISag22togaqOtMjWOzuYNyLJT9bVl0heqP7EQvsd3/KU2XDM4tg+C/X6v//R+wCFgwABFThZG+IubYDCmDIQDg8cswctT1I63SZRO0Va1paJgioiZK7ipa95kJNTselNBB8aBJX+77yTJ57pZJFDBcdsTSbSSeB7lZWLMtVvb1hy6gGhyiBiEgC4TQQzA9L8ChtyN17YeBcHJ0GZAhnqVFKceoVKFnD6Sm0//uUZGwAAuA6SusvGPBAYwl8PCKGCy0ZN6wwZ6FMIyVo8YnwkyPJcm2fvsf/E84Pl7dLOjctn5/5qXl/yQWsz/+gEggCEiS6C2I8EGTsLShycD/AthpM0eSkyLcoIuQYYuiWGJY6mZdIZi4cMTCNDPhKa+tLtyaEdsr2VXKRb9EJ3s1KWtq9WYqNMFaiWyzBgs4j9iYLBAJBJgZQuGXqXSVDJ0EIWuF+EeXU2xa/HAOBkSwIk7LkyaW6MkwpmiPKq9HqhJdRQpy22FggBRQUGjiZFQGuj2b7qrL69QZ/RZbI+oxQTMjAEmoPmGKBk5//9TUkktjaUSJQEKOw+0sCBJOwuLORqpx2UCRMGt0ZHWAbLVnatbTugRqOl22RarlY3dJTrXte6Iuvqm1H7/+zp/9d/fChHQg1SpJJRThIUv0whlL+OC+SnSfK92qvawJ17ACsmCNLUT1h47ZmF0xesU7yyR0L4GPWSNAQNhRIdKhZxRIsTILCSnxG+/yHcdWv//uUZHWAAuE4yrsJE1RFSOo9PSJVinRhM0wkzJEwjCUw8KZIgyXFDax6Bf//6wAAQqAAguBAQMQDGG0KSKWjUPOhBsD5/MpkJZ2xse3liW0kIywrur3d1ZY4HRJSDxw+QGi1IGJX1XCrhAJKhjQy/G2NkXlcrW5IOg3VAR0AAAAF4wapUHCBY0MFLupytLDDXLfF5BkQhszA8gRhUmdTwVWIFZHpLH27IFDJcFw3B1pSBWXbmuVuetaS2aE2TjDwGOJamyITx6PUR1iMsKh54euZnjZJn/q/ZpUuu1jkSjZTCMZCXh5FiE1Rxgh9FvH4Q92oH4QhcvFWnTspLgixRP+Ky3jMLD2/8Ym336OY3U2o/oi7ywbcxbDylI/5CSa2Qk6TQSFoqD4eLgH0AArOIYjoTaAuKpmgnfJeoKi5hCDoSQpvUqlpT6y7r1W/KesK7OqvwwIMKMl7YUIEZMMLBkmfJkLh/SkX6dOkbEX5dsYWZaHlGn1qHP14B5hAk2An//uUZIKAAvQgSdMpMuBQZPpdJegLipjFKywwaYEJjCWk9I0QYAADWEFP0uBlGiWNPuIC0LawRLhQUEm1FZHsWXUsvpltGKDxxh8iGHSf7Dx+xDG0GxQFAPLHiFNToj4XtryQtcwAngAACUn0Eh9OzZOIacQDQpTASRl7KoXFXaaUOCAgSCqUZH4qoDYrPMtwfDbbitNAum1baiMgEYqZHy5Rl+ZqWUuEs6KPqJbc97/+2D5RXm/mvIklCTkav/T/v5nLekkLgeh///2AAYAgCuBtOIjaiQExEMmQDpuIiYNlilk5+r1BPc0hZwg79QDcADtxQQgEwYbHKtRTA4kTcTUdaxiIz7TI8VZVLm84ACSjU0kiEUm6HLKVKVJbQWqdN+EvO96JjSDQcIwZju/AvEyAODmnjk4u7SGH+6e8d+32FoVgrINIttqAjI8IChQ4TGhQ2bU5lo2pxK1gFeOESCanRx170wlEYaf//7gATggQsDCL0WrNh8eZgksCoDk+//uUZI2AEzNKylMJG3BDo2lZPSMaC7h/N6wwylETIuWQ8Yno40Zgc0ISQ55V5p09YZpEEMRBwlQ6jorGfs/3V2z9e30RNKl2Yn+2n3d6f/lBHC7fUNVV6lUQSSlA3DKlMm+Seh2FrpTLcd+kBpehDgPw+Gy9UdLMPU+a5Kc9N41LSbJUwjJuwRUGPBgNQiSg9b2wucY+fDX+Xv+VKwvSf9p5H4Iwqyx/ezQ45/9IBEpAIjh9kwDSNZ0uGehIVHGKDpcYAqCHEfghox+dpHOXM5gzDhAfA9JHTgsgNRWlZlArV3ad/oRVy3QlDPWIZcAAYM3ZHQNIGiUSS1L6gIVCUv2BIeJigjOVJVyg3c5VQJYk0lNG7WzD7vNtOS98wACPrVLkTgsKBcJFohkpB4Lm5UYn5CM+QjnOfPLzPy2lPmgIxF2OE3PLaB33ngwVBkBLDDihaQ6hWhoqEzkKjpTumV9LDjUFT7uiOhpS27wrmSXlb8LHFhM7UtkjYYrc98aY//uUZJUAAr06zNMMGtRAo3l5PYMYC5jrJkywa8EIi+Wk8ZnIVu7KHegc3tFMXd7EyyoGCABQVTgQoeR1xRaaCo3vZM6dltX6fO3dqVl91ZsiXg5kePYDxleyzNijvAVT2SLatB53h1iXng2gVpRgeVV7qWA8gvnB/SA/iO80NehWRbL55Pl6LvPo8IhNMPPRatx83BzCigNYDAqgAqGd2WyKrsScJrL3XGmRQJtTaJSFL5u7L1fv5sV1CNRNvEcMKFpNJb/ELXNFNLwGRcWMMe86igD4mXeUW21aL3rdlAsQUvqkEhpt4KoAAHZZfH4NQFLNA4i9ERbxsluXxN/YlOPP8prFv8I9/L0CkohiAucxwCH4p59T5/aq3y6JwVCWy6ZK+Tf0/fDpdHIuncpKd53e6QVJ0RtYVZ//2dcemSyTRyUn3h2BIvI/iWHGPiA4GhpMiCzYuOjqLrw3Cx3d/qEKTSXi01VX3d/vU46fHiwbOskVNfUqFONNApNuTCQI//uUZKcAAw08SjMPE/BNZKlpYSNcC7EBOUwYUZDbg+owx6QO0XMP4b4SsTtVGUSco1wbJcz+7cX9kWqJuVvgjSEVOd4IFDw5N/4QIAsNR6M70EOUmlhmQ6lsyAvDug555x7l9Iv+flDMbtYbhkMdVikdqKuSfciMN//bU3pKGQRmQaoYAtJll9fFYT8fYYSGs9ZEXK3RCsZF782RgYnpRA4ln9LyFj4wyRiliUpfTUiuVaKlkKrValJ3uQSkk4AMxukUZ2iJF2JtOWc1mWxVl0ch+I1ZY41unvSnsjDw2sTMQJ9n3U7Jvgd27NJroESR1falZGs7GHWPXZvVW6W2Ivz5WsVJSG0R30mf9iAgcCJpAGGwc2ijH0PwuR2B1i8SB+PjZ9ckcbrCwWuy1PXwN+fNVxxRVA+OR4aPYeQHp0JkzImIW7U9aW0rU/eOH9VLkDEKBBLrSIBabkAdRPLjDDgTpdA6zgTZmjidq4zkNX2MYIovQIUUqWyRHrSNklW2//uUZLMAAwdIT2noHLQ9YwnMPCJ3CskbN0wYUVkPjGWk9iDgo/s3uDA9v34QJoM0AnZLelySlnijwjcRWnvpwegwaIHhCx0OLHzwxBUAAO0NAoJpQAgQSwWQUZeOwqcEARVCcalSxG+CC7OVVspKRN7dkNxhETf/SkCFqzoUfjQ4eXZWKbfr7soHUaQEHAABJKfJRI+pDw0toSMyF7HBY88L8AZPE54FJkPDqyVaGIETfMIZb+UJU7RHm3xrFFALPZOU90OdTmBWurnJUWREe6u8qabb167tRN9KQgSOgZx6YIMFCyDKP//9bTNdkjq6WJzgG6mzOZPtPEvdVRCfThQ4+PEvUWMGy7l3MFGWpEQ+Tt8S6u128e3p0Qa/uWVoijA1VlpAgkpJ0Z67QwSEPoQglJyoEfpfCxs5bIbmh5e096WxpJDC8ELCVrVj42u3pl2BWlHrxmiOZEedJS5I+TZgB5FaUiAu62QHgzOH8HgsB+baiIrcVFgo7/0fu7iw//uUZMSAArAnzOnpG9A+wvmdGYMMC9TpK0wwS8DiBekw8wmGSEgAIrh6EDMLiDEhOkebcywhLCVE3W0q6M1ITj0NKHDQubzTAcIIezF93eLs9KkW2+VyNnKqvongF6mAJJKUKsUL0l33ZCz5WFerB8IMUZeaONGjD/yyPxmBrMuvKHuZbpnKhhmnOv1f9ZaQiQOalvvTOzu5rZ2rnhnTVlOvY15f3WlfZu+CnZ/PFO2QwVX/9v9RBD2aIACcLwFOSoso7YaBX5g7BcB6y+i/QEwGHyhJWaxybujJV7dczffupaQTIS9FQudHcjUIllZ1zodf//6uqF2sUz7V+VjXAHWqRruJBEHCsLEQs8wkAvDEJYMhMsjMKiicnZWXJXt7+KYzcjN1HRnWHN1c4aUmwx//klll+55ean8XvlB5dQw5GaEZH2IbN2PpXygs3tNyw9tajIIP8tE4XBzpBDT0YFWwwJhslEBSyXwqEZfGSBzfLZFLDoNSIvWnM8fjEXFO//uUZNyAAuU3TNHmHLQ44Zl5MekEC0T/MUwkVJEqJGY09AmQDoRcuDcDdsMimZy3l0NezutHU0i+779W6a/mo9br0l31cjAxecSskpWRhkpEgGBahQDeLcPF05AMBjnISUsx8l/ThUoUZTDemPlfLVQv3NgJlBOur2p9XGeyCtMROp/DZcTnPGAs7DArcmnqyoyui6GUiMtK+peZrbf1+5p1Z//CRUBI66+71gMhqtpkttOA/yUhpqogaoEo0Sj9jCUw4GBIGF0LcW9AcSgwAg6Gw+KjoNGg7KGjZwssQiAVVSW1vuh5GjRR37LZ/QxTrUqqISbrRZJIIBkIuo6W4dY6ynG+LcD5HkWwDmGWyqNXZZNQtfn6PxykZYGInAaYIOUTsB+ZFc5yTLJvnmeXOuO0JuKd7kbyER0qC7jlwr6suvr3RHfsU7BU+W/kwC7bGECS0lQyk9H+hiiQ1CSSQwkx1HmgGdGKxkV8WVlvrMFYh1xoPnwE0EpSE/xesDMA//uUZO0AAtA7TknsGmxKyOmsMGJ9DCU5PaeYVPEZhqZ09KSYlgrGlbn7mUiIfZv6//yl7EXVcCAfQABAAAAAF5Jxxh2qm5gVAYjFIRba3nKbZoEAw1K24zTuT03Zh2ZkrFat+NvqFHo4mbTl/PC1rNQW5WtUpPqHhjfDSzbv3JbLIhS7WTCTW7IjdLJ3zP3u9EMSS3SJZgEUHb9KnPO4ht6O19WtRAKAAAEErrZWiXkImLB9eeAmCuW0DJIKUShMYpkQQlrhDbcY8d8+7cH1uuuxoDY5w4VHCEdEZlUwgaQRcQtn0izZBRWhopCIrsUo//+yLRbcbaTRIJiLzhsABhndVMWFXJoyynfHDaMnGBOpxfqpZFIvDFc4z1hYdxysZghwZRAKHDUDMAh4kNe0GoR2RaKxtJGVou01rovVurbU++RhyDfo5jLt1+3+RlwSf6wA8BAAHGsBtHAXUvMziaDoIAgQXB2QYHCzjyNEMPAMOvCLmh+5iA0lR1j73G2h//uUZPSAAtJHTunjFWxIaOmNPCOuDXlDJUwYVQE1i+UphJkY8UQlz4dJrZe15lcBtzjEHgZEKqk+XLCqAQFUkAkikm6WYLwSRGE5JqUSrF8fSqHQXl2ryMVkhgxXtJY0+t5IPMek6fd5BInwb2Ki61d66s84k3U3UrPCPurP2uut7U+0dVOhaIDjqWsrKhzxwuT//UQC/EwFZWgLWW0rwvIKdQxpBaiZCxmGXhKKp2kE+G1Ww0ZukRevFhHsaEM4RJ6KeyzDtkfNFluUcb938fN/f/8N/aUEY5gOOjIbSY/yBKlqjRSRIBjM2eKqqArpkSfb1JVpJqZBCofaMov9PHm0typw8hNhMm8YM7bpWT4k2qvF6qp3kecIEBHRQmlBCqzXgke9mYurzU7Nbuitf0qjqXrTf/ZaYO3+Qsd6vZ2/WBiBwgKZpIpJUFmPsBEDaPksS6iHmAqCIgAVTNY5UzY9hSRVJwtjhBChV3Xe7JJ5ophSAmUn/x52p9sW/ymk//uUZPSAAvZQT+sPEXxGgrlJPYM4CwT7N6ekTVExjCWw9A3BXumllbZx//59rmvaN8h8K7f2yNyKDQAMQAAIAeHnThpDgmQERwlMHRJII3yd9VwyF/5nUtXlATayAMp4qiACcQEJW16cfBuxEEFIDCo1HSA4BsBwhVMstzSmDRKWr6yndqfun6fTjKX5LwvX8/4fz755T3ckAiBZrUVgqV7FUJijeiUhNqNjXVgCoIwAuBDSTp5coM0Caix4sph0lkLQxNyuxRZpQfvvtH4bck5aQCVQwYJA3G1HBSJSABWyTbHPMmLjed1sYHN6rhlx87UWv9ikiQBTyFEDGDmNMaRMx8qImJwLYxk+/R6Gj6V7xBKei+DJFypaMf9OT8s7AMBhk1jtVASYWYJKLUv0CpnnpAt958uh78ISwxBhc4wQWhwlI/OrPjpBiEf1tikBa1oOiASgACUSuOwuBeCcYHw9qbFFW2QzQkIKkTSxDc5/3yA0BCzSgaLG2HCF6XgY//uUZP2AAwVOz2sJFDxUYplvPYhkTYkjJUykcIEsi+Xw9g0oCqFocKLEp1JMDEjIkHEkd0eeWm6kgXfKO6Lf//XbWAEoBJJJLgGCHfAg0NIZQSIar0kzU15rsbemgeHYw0rUwUgNF44GfGCzmVnRPqry6SqtGQTB9B6qg3WDlNSI9uGdyJezdIWZvwr89vX7Eyk8k1Q5/l5akRmRDQgQcESmpVVG/o/0oAIKrYJRSScAYBcJckGpgZemn5pMiGo8/VMpReJqV6Lb3B7E7xUQcwGtS/mRalCKtwoeILdC6VF6hKhz0a6RZj2Rw8w0yAF74TChsJBAwhUCZUCSSSnhOAqAHwGwPwTcg4vR/KwTMbhbmY41hvP9zeIo8kSkQco4wJMrcHmvfc5nN2Nb2qFEbAytchLe6JZHytvvo66UovXs+mykD0pc8mpZmYUokENCZxIPjiPObKxKrrHICSSlM1peRkAsQRxknEgQyONZ483G/4aKGisKeiDqIG2tJYym//uUZPUAAwkyztHmHDxLgZlaMgwADMUzLUwkbxE6EKX0t4w4HdUiVZaUMy2vqrtZJme1aWZ6b+lcIVLU9pFsHOx2orLXqfehFFtBYEBQAgj/YAMIABJAJ7GQeQCR2Slzx4dItDg1tCcp0yGBX9ga7BTl0jdonOSeFUz2/nTbDj0PjNGtkZjgQS3JJd2MRMYTRVVjWhSJSaXds8yNZs9rWaqX/yqvtdXY7ZEjpHvW40FwyATKbtTf5YeoWspSSSTpMSTBJUScSi67FSpQNiLX3mnRMWpSic9mduwe6vs6M6AhkdnOiMC76F2QuHZbWz0n2M7vqqdc3Q1aes5yUe2+fkd+dAagCQ5Ur4NBsfrVAADAADLQFYIRBGRow3IX51EpN9LvWFPvkof5MTnM472kwngzIDVGX4KBmCCQMZcwJYOFrCTAXQECEA5nF4VmliGYIuSg/AlEkx3Zj1x52tJ3o8LToUIj7FraW36tSU0k874vzzAixMAQAAchoDCAFKwD//uUZPIAAxRGS9HmFERTKNlXYYIcDPEBKUyYVRFQqCao9gkqgShPCwHQJkyE7QwmlI7o2FZDLCo+hiHuok7lSm/6zZ1rPpeWSrrcLOL6HMPuTcznujXdMkW2OI6RVJd8tSQvvzowSs4mOvsDVkHp7Gm1LGn/0sBJFsH+QslggZ/Ktr+SbOLsuTfKcLTZ6e+EkqPfQPbv9rr6Vs5C/TRjREEQlvf2Xd0Oo49VdhmqzdINWW/od0bt6v6NfIQjmSM1wfpwKBUYXCiSkQIHidRxKQGHhasagxCkuEhIsfVJhoOLLgdR0qZp9no3YiFbXpsrodLpRg6uyUav7NavtTL3/7dXX0v5ORoUoZbySHYoCgASAAeIClMRacjAfkRgAEgsQ2i5k6OHKONsmjW0GtHVjXHNwvSqXczW8SlPOvSlk/T6wFMnISXdBDco9EPuYZHm7dv8gqGRV0p6NU7Ql208jr4fv9HVHe6+4qvYpr2mu2t7+qu/ZhOtVCc0RJIN9NG2//uUZOcAAyU2yUMPGXZZaUlKPCPYinUhSYeYUTEpI2a1hgisXxRYWm0dTwIKgose+6EXK3XqZlGKbkCf8yjgIokCDhlqfOtxVamDOm3dPTj+73RddlRSSkjSaSJBOIWPAkxdBNQWLMQksbK3lxRxumFFPI3g0MBMntE01VMyD5OTM0elqXglnz7txQO8HmJ8onqnSWXdFDvtbdjeISwmiMu6o+yc5a2r9QxgUqDDVte5jzEYXIhfap1AAANDARgVGujxlh11AkCodhQHo4kwtDAJizrpTJS0aocQW7Z/TvDxmRt16gyv/sIOQKO8MDmLfKlnlzlBBLLib1P6vcCPRYNpnVrvRSgbs0Uoyk1MqdXgKLDEwuJrjQfRsHkoYCDJpY6ysFDYADRoQgzYRHEKgOc0OFSiBgZh451f8YlsPLtLxv1RUdqnZGjwIRgjRkONiQMiUlq7jeoLVWT5d1nj3wJF5yvhhl3TjCRc4fTpIo/z/MLSUP8pYUs0Rg8mxzJy//uUZOcAAzMrSjsvMXY9g1ndPSNHDHjpQaekT/EwEmVxhhhwiLRmm//NSn2v/1xA7MFw4oWPkYYCsoTCy+KWjiALhwOpSBAAEYSgMkAsAtGMhRzkukM8vbYlXNgf7SYm5sFCQYQBj09S1Do+FGLucovcTqMTrYf2AmJDEEIOC2ECZYWWlhmJxVdSmiZ3+at0Pax0EEHxvcgTBEUMgEgApJ8eFUOwOAi9zNU3FA39AwqZiqhS7RvBQIQgalU7Tv4SBLNcnCUQtvC81PhbmXFoaWzzHfZgVuf5+7gYQEiczZ/htQIGFZhad2m2PygTPZodLSsNMisWltt1qDofbRFI/tvZ1ZmMHlcc5hhb0dlM8eqWOzQKqJJoIMqTUTFaWyWX3b7DIBMgpSAgCGtWQricYyCKUHQhbzRyhdiHYDXwxy5rE9lf1arf/+95ZXQqAIzMyDlJcQQKgkBBAMh1MMEy+wqEGwfxNjOq0KfiDovYgJV+/L7iwS+KIaUdmxjeDost//uUZOsABN1RTet4Q3RTg4lmPekeDt1FM62YVtDwiqo1hJ1OHzm/Vybud0/zLCQVxlMfx3ZOiSsGoIxxBrZMfHx0hKlUKp1bFbAJWey1PH3berKqd0jo87s+nv0W/3OE7iuYsJsSI0isYCSSAsglA4Q6XbiWyaxGYD5aolaJeO/FBN5JaVj7Uf+r27v+uoTJOJ3euKeuIAPMcGbkRBBJGDQI89a4XkXPOH1+jwJazuTdO6zMVHEomixNVtKYIElk0FvJl/BWiUuIddn47wbO0jd07JguAMT261y51Rl6ECdEElTCk1RI75mCtuU5LEljbQrzKffv9Yrz/vfU9BhU8A0hkDfrk2ZtbojgaTBKooKohZgkmKwMq2GeXm2IWUjVUrBRSLf9iIm6b6ghdnuoRxUx/uNu2f8If7t8FmddAIaBiRAEgTKCDAhRpcLAQQDexd55KhMDibq34GgeCVsdmYNjLulUkTlTrmVWaj7h0itUe27TyU/Uja7UJdg4xZEd//uUZMeKA5xGyQtsFUYxQgp9PSYZjekZJO0wdNDijyp09IjeIyzRqe8trRRFoNhtDXYxDxYcuTQKGClgJcgMMe0EoIWtEIbOc2XcpwJF1LNxJgSICMkIKAlpNyVsL9mDRSODNH2ITO1IU34lAZYTjjYWQsSB4A7P/1NHJ/+pCep1iIVQaHgcNpwQGzOChgWJREhg5WwEVFDQNzxOurxr9MwOVTkTfx+4fZbFoaERIghe04dTmfyCjSyQ6vYDXpynfQGHbVWY/4ZUBAgXUM6tA4kwSDjh2HAQc4Sn14HG0cqUFGKFfMu5zUF9GpFw/0L/O/n4yh6ZBV0voBAiCWIxaCgboXiYFCp7oVcSU6bmGcY+QkEpP9EzRCe9+0PnxyqOETt8KLJF+H59aVf9H7v/VraMeN0KCAAClBWUMKEC4tCAzIxnJUGoI5w5gtnEbnWuwDRRPjsOqsLDsWBI5JBTaH3HZUL9bO1TDc8dX6y7kJp0jiONY5HKB6XYV18ms8G4//uUZM8LA3M2SQtPHMA4ITntPQYlDiElIq08csjriec8x5kIlLlTSzC/mq9oNvb1619/nFXs5WWq3XEnYZBRjqp2ustmku9ekK7DtX9qC0DVAwghJsn0HaJJwfkGBYNSqCLaLop1+wMccSQ5ECJg1BGIklqt7uqTjw8q46MC38Bf/ooTQtIAGAHDKlnAxCNC1rEolfCxQYDa2bck8kCMIo7jaPzAcTgWxTQCj2LEX5izO28bVKbORKfXHLbDNF/Gxy8+NaK7Ctc3CtTO/bwsQxgrskMaCvgsy+LEFochMWMGYOhmMoPDYgz8HDc58Rs4knlTXOHP84MaEKHJIYTLBxkkDIQgelRHRiIRyXcG45hw0/Dd3JFIDq6jwmKhjWlW1P+tRdtyLv+j6q/qSioACAAEAgHgNihUgOYj+oIWoljwrxNS3laLDUN/dmL1+fjVa2SLZIjwyIzkTEJzurS1RjbQwWXoQkhGYSi2qtGamVHuRhSsj1tWco2x5EG+R7JK//uUZNODA7FEyCtMNTI2wel2PYgaDlkzIw0wdMDShqaw9hiY/l/OZHg3ka0EmPIjF5BzBrAoJwGWLE4bhkCNNihLkfxFtNdJw/tsEeX8kerIZ/5bodhTeuuwWByRX3aC6Cxqz//ob0ZauLDi6SAAZyWr9vgxcCTIKQKzusk22hhyTAGbPZD+NuGIVBODn08MjI0DZKWnxqT1cKmPY4aShuQdG9OHNIgpAePRZcYnfys1Mjk1lqkVMh+ZNlfyPPz884SPTgRLoh8yo20+4JWyJULj7krZQGDNO0igoQWDFMej89A0VggQTQdiwhIhabrZTtgxqKzJTacjy/SjEBF7fJVmHkQ6onX/7abf/63v0k/+2jnaIWEn6AMAAEAHhI5qADQgCHRWByha9g7zoZjAsyl9PNBmHTlN+E2IhKYmlKRB2bWE8Y86cgex+EpJ9TmSQTL0Sf2cRUvrS9KbfMQvC6ivZUaz1WlHVHpeRST0Y5AQPX6d32tpsxO7pwy4ZoDD//uUZNcIAxY6ycsJHMA4Q3mZPSM8DRT7JI0wcwEJpCd0xgkUot4QtQQ/uLmVhXDAqBUnlRKJjCBFpNiy4WV6F983qnyIZg8s2+hYDSOMRZ1Y95q3///4VLpMQ2VU+ECAAfwiqc2LOATBGDaAAcJQ4pBwIKmnTbd4W5PCyd9m+fqKyjKTpIjSOQ22n0cQ+Fw+ojsueKggQJ01LQOteIRXD0nE96Uzbr7GrYoX3bxPs/tLMr9hnLWXS65fJrtnYPV1+P/vZlnphgXdzIsgFOqqFdjCA7MiSN72NN+arpeLeABEgQEEYALwAvJqaKnaEgaQ8KG97nKs+36ltgfsEkKfW3VwY9tw9dfcNMmN//dciUmbiXuU0mtgBBpKVzZzpgCFACY8JsEh39RtZMMliIezZ+WcqJvDg9z/zkXeR9rkNRu89VZvo65Ehm+LIoq7Tch2bSiUldKLUBr1W9P//sOAzj36RT2lKymRm/tnmd/qD2Knz5mbNEDoMuESE2fi60AA//uUZOIIAzROSbsmFUQ8Q2nMPSUdD/lFHo0wVYD9CqXo9IjwSSwtlKNOAbAnRcPFnVwvKZ0mYZXHZ0t09tWDwyAEMEZHsRE6tRlMmKNU4O6oWex6P/W7sC2ipE6dQoCAAHuUmrSGLII4lCA1RUwYUvwBQ7G0pEv1yQuZiNmTQQ7ThULny6HGnuogf7nCsSECrChh8fNVBOciY1ODZoOgkhc5pE2rBGmHbcKS7dHZ59mytbCsZnPv+eW0vy1PeFw2Obyms16il3q3oqPjKqEsjUr/t9bLIAxnJlBcTf2bqDoUISNFlZyV0tvaw1AWrN842pvNQrAVQFCTpMOmg+5jnIQ0OwKsPssRd///6jZ6g2QEBg2YcBggXJQIMHAwVaeqoX30lVCoTw0iEWnp2U/E8EIVqXio1QqZWw1tzeyPmedmv59xYUFXJvBUiidlgfi2Pnm0JhaeY+uIEqo8UOHJEFHL0kXfdXPdjlc1RQ2aFjDs2lkYCrJJDKpQRso4GmWB//uUZN8AAwU3ScNGHTA7ofm9JYIODiDzHq0k1MjzC2q09g1maOYvgtRLEYh2MhJARiiJLP+qE5Eja3Rn3hFKOR7YN5kTAwUpkAWguNtt0Tn/0fP9mtaThOAGHCVq2TBRIspLAKKq2qLPs3ZFSCogtW1MRungiVQzhBjgQc1xOG7Pw3KKlmuST5h3nnrXJSnoBJ985zoWlrB02mtZr75TxmcNKjsW5DRzoym2KqO5eigT6Wvr60ZNnDAJVADv2Kp7BGN2VbEmliw66dACERxhSECM5KzTBzFzHGDiw5FyW2G0ETzRKUzNiHvD+LF1CjWHHfLj3GmgmsMAMol1E+Q0OgIQQMSpOt5i54kdlQDMZGEuAEYqCB+D3Nl8Sk/FAc6geFK22PJUogW8zBjnOb6ORMNyfRlOajBDGmWHHjZQcSbKJ4VBa1vDwaRxIpSu+T7flZMkG3YRpd42H381nzYkvX0ugCOU0J7v601+EcuBgcgAAMBajatVT2akyAdA8KSV//uUZOgKAz86SANvQuBAYwoMPSNZjFkfIs2YVQkoDmXlhgyoSsLJ8LjgjliJqrUbPuFsd6+lScy3IyeVGyxZ66ii5QRretyFOD39rwl8kgOMrpavAc4g2AQIAAHPAOUaEajpd0CgUCAVJKZX4qBOSGnAp5l6nEdGI24tHZdH3Eaotank1IPozKyeTgdjjEaSTmpHbJWlHolukga7FBVhOCzJgiWHhKmZ+CbMpDmH51D+GGApIDDXMI9NKqf//7gGEVWBM3WAcLfp5wI6ia6ARgCEDSLkIVBQnFQiVX9QVL1f9kHScscpIxJ7JD5y/z/M0y/LvHSebi1mJhe8opbjyHHFdSF1E+lBqg65tbHJI2kwlFfelhZVQjI9NKwSENOuthrwmZc6M4hG5sNoXhN32xpVDqFL7bL1GVPlNZImshBGiSZNKZxms8aIlvQoVPoU+QtKQqBAIV3udtIwVaUkaAaAIDh3IFJWuEwSs9gisshExgk2bj/ZLv2lO7ZW2dqj//uUZOuCA0M4SANvMfJGQwlJYYgsDOTdI04kdNEnFiXxhIy4TK4Z6zPHmYJdSZ4YR3BJiwpFMJQ6AggxMTkZu7Uz/vG8ZefEivwkjNlz6nnf5lfynXLnkX88EYWOQ1SGKrLEkmkinzJRnq70jYmyB5X1cV4wkC0jgPBsySk+E6FaCTySF4dVv9WNFLIoaCUOEQorynDc0NiJbZ9vUJfhUtnvpYWhGXdLPhcr3lzhfeG0L+HMBAbv//9SAABkAAGxocEA8bQrTfCwioF36cmJOMgBwQCMecXTm+nLpgdG4lYZOMCxlOsTaICsONMjLBdZj07YxQu3JLTSjO9wOWrkiiqFCb1ht7UygjD9AQAAACsYvmNAVbEgygqAipZVNxhzjjmOQTFk44azp+AvLpbKRoTgEj+5S9V0dAoJxUCW2QEABnK5Gg3USqKzGM135EQrzbKilsW13oi3965GbYt+nX1qRjraC//o1AAARAAAAYByyYS9k+FwMZc1lzAnILB0//uUROmAArgmUusJG9xbKMk2beM+C2UdNawkaaFBlmUtlI1oOKUzQArMvxLv/BZ4px5wjB2BRVJgqEBhNbkpe5NAOD5tupVUwd3a11JxhvvUbFmDPWh9IaTbsaRSJBMS5fYBXQ1Xanswx1Gjx8CD5XUn4SF0wXD4Tkixx5ShlIGas544X1TnCh3aoEIX/OskV+nqZrrJPLTFQ/Ovgvn/TyjZw/s591xAx7ksLJEx5DGfX//1AEAqNAGYsEwHwDEUKuOY5FIUFKGAnOAiSKAZbaLRYcsd2B88rrG0d7Hss0mhzfRKdmv1d0c0q9Xv9f6f+3rf913T9v/cdCkVAKpAgkklUw9hUBm5WSjySlBx+SKokenM8q6IJirSqWUNEgV34WrrVNhNANZCO0NZFzgLhW9Ry/nX2+kzbRbKMYIpDLVyO7NiayDqhHR2I41ERiUBIwJFxi1P0VqnfzL/1ysUDf/4K0HQbZ/zZcpGOyNIuJEkRLFhOWLa+EdodGCSR1li//uUZOiAAu9ISbtMEmBKgmlMYYNKC5jvP6wwabEcpuXw9JTpciW1deqdlAwM7rZ33khHDYZQIx6nb6GtDy3PJxVopSvnP2NXSmr6zKEFKSRtsiNIlYX50H8TAzipBPxywM4j5YxKCpdVW0qKLJHyWMxoSs+H8W4L2naf5MgSImzhe4oJeV0OmtWdVKy1dcrsrrW9VWtnbq20XzA3nT/9pvVm3o3sLWSqbuy4qn40AByMAANFpUBxHqaJ5JJdLdggB+IUnDmjqBBx4+NtcUnUk5wqtAKBBQMBgYDFI56LyZPD+EyU119SGc8zG2dia6vV7Jza+SbV1m6fZVxQz5HOqjI3LW2Q40k6IauAVA3BlCElsJYa6rU5/IS2GMqkzMTu6yEEyBFVevwRAy+nLft85aO6krpx1LVm6+7uPkEZhtB3v26LTdyMJrGTorElLYa/637fuSRyg1YHBDG/1k4YrwBB3S2g2UcuDiA1iUCoiYY+OjAyCqAQ5/1//6YRIHAn//uUZPAAA2pRStMsFERAI3o9MSJHjA0zRaekS/FFo6X08YoogrzHOJ94BEtFIwwlRGWbHQLGuY7tXG8ejWso9D0I1mmBLDSTktkaBckkvBw2pLlTBXEMjXrAbnruYhQ0CnUOOo7c+AiAk1IOzSq3/88cB80d/3NzL7nT1osyPJIqzw29vVc6ajuWSh7ppn6iiw00+lQWJnii1NKkWI7QoCy93+hGC711gSW24C3H+nU+iKtqiyUBt4IgOcjKSSJpDJVGRE+5YISR6MSwLV1rFHOOuFnKkR5MWaTeJgtSG3kICIlB9JSmxkol+XfKDhZyakCW1CCiUmkoc8hd8qc5RxRUtSwBXKwxbRqjitNgZY7Nn7K2TSmoREggVfI4gVPP0Eguz2nS8t5eP6UwqfpPNxNmx+SRdmX1llun4r1SI8Yz9DobL/427+/QKihpOG5Mk4o11H/9Vsl21L76aDIMbE1tYZtGQsBeyck4A5IpQG8mkQvq1lY7tx56VVrBQkGx//uUZO4AAudLUmnmE/xGBKnNPSMoC6S9Q6wZbpExCec09JywQnvEWtddnmzE3StZGWk7Ia6Nb72uxNv/ttdZLIVtpkY52vtS21xe1MvTIAolFKGzEYIM+IQV3IJcE+FYJ9wGsyTOddO3WIDgd3oxQNL2du5iRxbbqk+gUuxXQg2EvwFa6AAhzRR5FFhGVm5crtRCKCKrmJsYKoNlRCTvOXtfTEFrknBaJ/UcIEgKzYsRCUKGZ2oY4cbhyGZUBidxhkFUG5s5Dg8iVLolXIzKOpLt9W2RlODAzwMWgw1xjPGez1dTuvTs67Safu3W+laUN9/aj/lEGntqCAhbQAJAKS5wIl41bkNmyQIJIngsTAbXPTIXlcnoY6nNb0XE9QqK3xCdjN8DUwwBBJQSj1sNoiytQocwwIzDIRUmnmRnv53Ps3hHlzL0NukLBIguJoWe4J0Li+x6Xj/Z/vcV1+xJBJSqCsd4BkDZPQGwTs6R5qSpgKp6aBfU82omit72Cnou//uUZPWAAys3TOsJM8RRyao8YeUry8C3M0yYb1E6pyWlgYk5p9zb4G/KQgwog+RL++RlUWBDkUoYdo8yjszu+krcxkDnVVud/GXf/QBtIAAABK52LhrRQdt2JgoEHImsuQkkgTCSIZWPTAnIaw5OW0pDA0el2kZ7n9lH9RUxq0t2fMF6Raf0WNMU2ETBFQT5uLFkSpM4w+hAEaKUqDRpSCBUoCU4DanmryjrVJLCZ3//+SByRYd0Mk6i24J3ikSAVJOv4+zSnqdyB41Kn3Jhureb8v4malzuO3XdsZuG0RwwHsqmJN3OqyOFbMvejWfRFkt0o3tWLnQ4eS5NGRd5ZbjYkStgrQ1JLGkkQUVKJKi6siGzmL1UoVsZA6TsP4t9+LUBcVFD+b6OmpuRJlh7XKC6V1dx/+zDtI/7QcPCY0sZZotj8abxDkDaccg+Juoi5aoqtreu+Vrn+O1umWe6v0Q+M2xPjxwwQhw68UlzX/+sWSXWNWirFNWJYAOq4nyd//uUZPGAAxE4y2ssGXBNRDmaPMKGjMSFKUzhgMFMmWe9hYm0V+2kuw9sXj6Z6rRAGAw5RjL1OyU0LiEMiAwUPuB/xRsu6ApXQuvFa2g9CKnMZuzDnyN02OZxIJbv/rZY3HJ0Dsg7iZAjUhJScNR+mIPSVyDLCZR/MNH6eqqk2oSqr31FJn0a2QLrz3HColaCpMRwz+mqIGq7BER1Jq2gsOcIvW2Znh9nesjqn3oEMoKZZOSKzZ54z//0giMjxCoDRCcvCUOsFaW4TI4CVol9cZmYTDuOg9HolhOWHNviH9Xpe+EABYAJHagKZZBSTYlv0Ia8QSdS1xRFBS0y82VFcbc7oQmpJI0kSSCaCrkB7GQMEyB/oaJ+PoR5GPBCV0ADfFRIn+P1ONwNOQr4+UrGMJYliANJ8dGZBXRxaeijBoFDSssRFWERWqtW4iOAece+rCNkrdr0zUG/VBQFgAkDbb73///3qTMsvEKjYcl3AKtXpweB5EMJ+wRzEAumuXVS//uUZOsAA1ZJUWsJQzxF4rosYSVli/DxV6ekT7EijGd89gzYQ1lDmvYABVRpba//+Y3PJg5IMFLcpUf1XoUSdmq2jsIr2elGS5UVflFDg/cxd5Z44OD3b6E1H/nGBElZuJpAkpN3KLL1liYruBhhKZvO+8Gtxj7S2RqPClgyQiFGbZ5lSWSvYXNds2FUKuSTbQgJEuVDI2owa0+Wnz8UDR7svVnxkUzua6tRXYiHm6I/uiLfZ9DiwTIf/9LV1tnRJARcoQ5Ou1sgCFBpJBQqC6YbhiaGijTCqnk9RFFsYZlGoh31rExMCjDFOHg+rn6Kvo3oJHyIaAQNIFXEjqVIXC7KStWfFAy5Qhhw/Fw6OKDm/ecFlQy7JWkCSSCaxOHy7Fx4oEftFkWcJXo2fH4GBqdG49GK12nuLiSxay/MWz7OdmbGx7mwZdGIDoMCFgwbBzeVVuxS5TGRyO1Zd/7JZLuSivHUQ9IrBqSaczNv/9aKqsqJIAUDgDFGWbL5mchF//uUZOyAAxE60OnsKuxVJjnvPMJ4C7UbPawkTVFhkGZ1h5UYQgRSMhvltRAMETgJtt91JAXRWvFMD83MYmLhM23LMLpUL4+XaBzQkAwGAEt/4WH2ppZ7JAACgAAACXzaoTXSoT1buiqm++Co30eGdgBoeJASTPpMNIhEVBCXNGVSXOGZ4zFbSq6ihOinOONOIwZmgQGLo1kvc3dNLPeqzVrPjO5UMapnKm67aVr+/uCdBb/++7b0xVpSVFZUEFiisXgL9KkQCpFwcJYLskqN4LiRKRuEMiZ/4+Rfn2ByHBOIQWBJmIEiULriICGi7HmLWBcui9bKbayJUjIKYok+2GKFICScQAIJRThflerB3RSoXSMjT7Zk8TUmlMtacypt2uV0YZGYRgLq1OTe9mUWChLqqyjNq5VF70joswgfdLulzeWrIx3efdjlob0NfbpLZaqn37W/eYZn//2rBBNNSAASScA/x9HKISvdGZaSHpmH7/kkbEwcg8DC0COg7ixZ//uUZOOAAtdAT+sMEuxEYpmdPSNUC90ZKUykS8EpjGfxhIzm9mUGn2AdDhfLPNdW8rjD0cZQq7v9pd7cv/S2vkNJOp7vdR1Ps4tCPjiAnJbKiUiSCqmPFkUmqRIBTVVZ8lys5lzrMxZSxSdf+1PU8ldwyPz7qyxpPW4skj7jMbAeg2dWsyqTVzXMPfRtKkHWPY3e86hDCiHjkLHj72JJpcLBwWDc0TEb/+zRRSDfOAAAIT4h+HgICwCZ8DJBboSyHOsrCboGUcSHHe2Dd2bcdf7+IuLwpNzGTFnB5LCRkUEKUTQhABfQbwxpfoQ8uMJEVDXAFTXILnjyhg13/u/0Khp/gJRRTcMwnWTubO+yNcOpmPLXf55YJaU0+UR55kkR+1FbZf7ZPNMNYgJP26jcdP02G5TYSyI2H6Z0V/QcCnPMFY4lxM811vwiB/JCJPIqYmbsJgm6cg1yDF8t7/GD5EWHDn6gI3bmAAky3AWA5CclhC8FjijyJ2TknZblQnUQ//uUZO0AAtBHzOsJE8RNgZl9PYYaS9ylQawgUTFLDKVox5iYiXqvY40ZpmEaTwKBKRxFgDBLCXvHtnZSoDAbEKtZzFOYX5p/6YIjU9wRMGWPPlgvPy9/09/VfOfirn8HhAKiRIBJBKVaUxJgi7o6sEmEXpXPwOcYAN1dNo8FzMcCbISXYq4wICgysttMJ2rPJUJo2Jqs2Ou7ecXg2W4u/64viNyJmsgmS1Z3LOLuZ28S3q4//qf07tJpk7Hy2cUPETkEw1/9baAVX82kEkkngCwwHyg4MHYHlJRHikz0VLxRwIyCKUo5Ezi4FCTRh0HTllttREyXdWHPLlAQSh7lfxF5fwLhojYyBSbkaIJIABhZpcphYCFhOGjRHGQgnrmQpdIxTJSaygY3zyBGp3PKmmCYjWjEy2wfG3Vuhy01yZ91Puhl1oUSBRzOSKcSZOiFCDLW6MyikjzvdD/19tXQdqoQEM6Lv/eoXABbqQAABBT6GpU+Q3UWxgqTgZsg2L4K//uUZO4AAyRLzVMJG9RX5jmNPCOETOzxMawZbxEABqb0x40IFZKcpFila+xM41yUo6FBTFVGvBtqnR7fcrOM4sOWcgrYHpFRa5ppSK72CA2gzWXbVP//9YUdclRTTbamR+e1TJmMrDCpatJXM0EXkwjjWfFEG4mB/Ad76J2rU5iG2/Cf5C+8/DDST8Ih88LCpGAg7ntLmsoZwjCDgEePTRIRDCcpvYJN//ReR//4qBJFZWQikk6C0Eg9L04YADrh+Un68CY+DmU0J8vKa8sfSdnfU5Qpw7/crqVTT27PKd6y1ZFn2dOiprqxdP99nrTfrbX77GNY5IAyeik2pIkUkSCqBqoeTonSNOk50iQ9RoVhXkxRx9QE8wFB18Kcqs9FRppNnVhNZEcQZgSXdEarOlkOzEPuuvc5LV06N5l3y7aana/rqqrpTa5wSmnRopU5qfR+lYkUJAAAMVmVeHaL/u7MrYe2VMRilqy8F4QSRbhJesUTM88gyGtrMnLX5KSK//uUZOoAAwZJzunoFMxLZGltPYIqCtCXP6wwadErpeZ0wYoZ5F/zUY26k9i2TpHu+R9Xz2oVLoVrm2otypq2ra6It9uzGnIFCtAhJj0SGlAQSSlD+QLhjU4gTTlRuCg6kl3zzG2/d249s6/DibgGfgFQTLEGAkJHE3dgs6KzWNz4kSbMhRI85jerWfpkvCNdfWxGEMSue7POJm9djldT5a2bqHEulOnTI2VtGhH+VOjhR8QFPB4ADn7qDLdprpHK2kxr5DmIKtFsTxCi5Lx0U82BsyM52xwYMnz8spTDfMyK2gh4yX6HgUiZcxI5Sh5qtCVWT8/qbnaE8U1OQgqjFAm7I2kUiSAYDrocQr49A7gjIgYpaiTZOCCu0+wJx4REgxSMB5lk02KLN17gznd8ukMhCu3Wtis6NKtVtWhZaqYjChOmqPd6P3NXWzvZq6t3p9iKRtYodwsrRqCJH/+gr6rmaqhuRApZCiwiHk1BnBqR4IS6qzwZo7TLqc/1U2sj//uUZPEAAt9G0GnpE7xTKRk2YMVsTakxLUyYcVEXDmn09I1eXErNrzCEY1OQLpESTLwcy86JxTMpbI372TK/nwjHsfto/dvyYaKcrTKSBAEGQYhAxzGabRZsSEs4706tJUlpdJiLcnsknkgszg9VLVHCkPZVFqDFKNwMOWtgyDmgJyUu2nzOClcWB1n73LComny4xxGLhRy3OMU2jH7RU0X/p+oCBy6AAppNPs7MXVNDIEa6WEwOkFxxPEAJieLissheq1SLuhFP4lKUS5PhoXcfNgdR9RSQQJTShZyx7UuEKVERUyFEKoPNrmuRWXbDlH//6wSHGkSUkkm6AJtWXQkuxBd0EvmzNdaPDB33V8/q4CYhCQYRTaVZwmbWO/cQNBQxEkY0Zgw1i92ENZTOFOtmeOFwAGkLe8IPKijRQ+m66KVBEuRF0jllHC4FXGPZ//2hUBiwloBEhEwFwdB0GkENLGpJB2naSxzQFkTTTBNIA5Kk09X+2VGrPEtHzBqV//uUZO4AAvZNz+nmK9xFpqm5PCOoi3SnPaeMVPFADGY09g0YvJvjGID6esPIW1CTsczvdqcxPsJUhsKsFcqGy0pG20WkSCajxZhEBHB1GyYhag1C/GYPQjmo5BUCZ9pCJiy5VAFNhJZdK/yoQUTQHUWVVdWWv2VzT749KVeSRFadJ2cuglg4EcZcQyTaGpRWpRyxr+neyONAkeBAiPrji59Dv/e7XsxgBeoCINgfCFBUi6k+VCrJeYCtIlDpJi2G0rMvlhZtWUza0EVE4uoociPs+J1ewjl2UIbZEhaZPEkl/IQxK7C9Vv2bc6jKlvvu11N+8Dd2JQTjRIRIJTotMaWgQQCJVIbLdd1MpZLqKVrbbsvZ7bUDyyLRWOSzuTNn5ppqdqGa4nBEKLvm8RXsaq7VT+yTpJe8uHouyrVUKhgFYg+LLDqFIY3eWjVb6VefKiOdtvZjqhbCTjRH//qqFzVTJTcYDBAbAzxxinls0XmOTmMLcGTZEQiUKgF+Zit5//uUZPQAAvwlTesJE0RHwyl9PMNWDQzvP6ek7fE4mOVk8w0pYYLKs9WSlnJPGo65Nk0k1o9RRgcPkED+7G2t6Mw0Vjw3n6r2450Sox/nNd7+jdCv1oUiUk6NQTYgAYhogfkKNodo9yvxHfoIy4lcOou0hFp6sgegP5iqACjz8ZqckguiR04drbjDZFPvEQ8Va96ZqaixNjuhLKa/P6fb5IqchmWSGf5Xn5Sugl//pxa3WEy6l6gii3IGJBkHFxHnASKeZULTBuRLSYjYR50ML87ojRiFuDATuQzSgToj/5GLKXly5Q9cZxL+1+0W/SgFtX+O62k1/vML/3eIh533npWv1DRSbdMp0o2nRJwY4uGNqSTnVExBU8vf+abhRETQ4yzxK3bl0O/3S5GYWU3KGnI2WfIxEtqzsPIBlZnoY1syTuymZyZRJ5sapWadRZkGxhmOapm5gvDQoY//0Vt6SSC/aoNESUYpJ1Qi5FaLmC3K7bymGGPBa372JFxF+u9a//uUZPMAA11HTGsJLTRNBdlsPSVKS+EfNUekbVE4kqZ08I6RJINkzoRu+rzUccphUPAKMHzP52uX/bT9T+h33QXn3bcyP0rUAD0q/qZ/IgatvoKAABAhQYUzEOXlMQFsGCV0ntBzDV0WRcFah8cNJb8BLN7hPI7jugm5gZlksg+PQWapga6Lk81czjwXKJ9ItXt56HaNYu6tDqrC0r64d0Xj+laqtr6+f06TbCwqp9KmbDUiGA2olVLGQW03aIAaoQcAdMq5jsBpiZk7ZAApORAQe+NTeTgi8P5u+KCM6Fr2tKipdQkAjjrs8UGkSCiadJOmlqg0JCAmqGCYhJTnrJtsQAcaIARIIS6RAsUxpgZSqBXzZ3Dq5WGOMDoZA31Cw8mQoUBaRKUInTaNsrhIkm12sVTigzCb0jGGmjksdjnZtmUe6fawU6GYBNlFED2m1MDShUapCjs1//7f7CI0kkQCUSkop0mq8rqp9BcSxlPKkbip6lBIoebEZlgjRQ51//uUZO4AAuc6TdMJK8RSiKo8PKObjFjlJQwwy0Evhma09JkInRj1s9Fwjr723FMqnZwQtQpzfH38dS5ciKtvLpaMteFk1aviaQR9wvVIAoolKo9MGIhgUBUCPYL4mcZbZXqmrJmIzE2972QImS7Nd6Cd6miNSgI1Gz6cJ2lCLkKFiZ56yt/90ls3zXaXYVn3pIiGHgq33s9t5qfuuhGWu+XqRJiT4h1Z0Ryhv/5bRyBMbaTCW6rGMkCVEWDRAGRdh3AxFdYVQIixpJREkg3w9snkdGSep/yd8l5ZsFijsqK1+9GUVOS5ugc4PHGrHuz4oq4ytCFG29jyRp3koaTVEVmAEkkp1TAUEqsNHdRfahjpwU2Jx5MrCsPASsh1MU5yhMIFnWPhqwisNKDyGjmfstzBGFJOViRClrTD3WFEnyxjpd5b/9zk5/ZLrdf3kGfkh5p2w2pTCBhIIg//6fRueTIMLaQBRRRcAIBUPISC0uhmeh8qFKEco1/v6vPHTgXr//uUZO0AAtgsSusJGuBKY9mdYSJUjJEdL0wkT9E5Eebw9I0qLNCxcEgjBS9r2ZbZNu7Ne5Purt9nf4+16tbpX9K7reuWjy+dVagcOQgrVqUkkk6gh9MZcShFhJ2ZB5vx/F1L+pbsilMtdGIpkejyvRdyyfWOLG5859tnQZ0JExYMsKZk/4Lo6jHbwruGNKFyplQOXhMVlmETCRqzXbl7xK7l3if/0UrjASyyJjSQLBjixEGLDJPa07qBCh7LvmHZcPlz7OypTdnZxJxep4VzWxistXaL2V7u2WvulO0zVoqdm93XTJs1fN2Xp6dO1gyqUrrAFJJJV4CbM4A0NMYwB+JEIwSQsi+F0aY4J4AYqTgjijqRj8/d6cELGZkPV1CRzoe7YcvogOCLc4nghlJk7JRJIbhEE2ExSksfQdwQhvUQUy+r0xzuaUz/cDDgFgI0M8b4GwPkSIJIHeFQcA+uAmZD02lo2OPqdM1t5rq99wvgaP+MMW5bpuoWmY9VzGyP//uUZO6AAwxIS9MMG1RIKMl9MGKIS1ShNUekcNEgo+b08wokTPkSfPLr02smQH3gvQlQJkCIrS0PV55+AhykGWLY0lpuNpAtEgmrkJ8kpARBRCkMJWb5lHBl6X8gTkwLqqRoYxKwCStyXYCBUe1K7fcT5t5JRJwbLUxkWzo2eY019pmq2RbTta2f1vW7oLVVVrs6XchVstWr89k0HU/8dspFeZYFIlJWASNtOc0TjO5sEPJzpZLFxUCB2tV1ZaYXqLTXg34r9f8gzFMiM5DobMmA2Q/hVNq7LeJ0IJCCl+fwmtxLULNJ///vJf+UkkAARRgwwgAcQ9R3CkGKYxBRMC2qUgawfx0OpIyqpCzDPjWswbPVOE5p8oiCfjPjIjWQZmuwhNL5SteDkUPe/Qgu+CBWyizixEeLhwWBfMiZ1zReeoYCif/0UKAANrCJRRJS6TSJ6hhqpPHUbAlmQOh0MxPvCJDrftVdgeMH6OdvEEtueMKWdyOtCK0jkQ52GaHA//uUZPYAAtUyzNHpG2RT5KkoPYY+C9kzP6eYUTEjECZo9I0qxDloQ/Sqt7naudqIn6sj9cewnFkjBelRQoiz//7DTrlKSKSdDwErCGAbxIw1Y/SwE7FGYAsg4J1K2ORZHkeBzrmFCWC7vaTwoTWJ2XHMegVbscHPVE4kmfcxKdSlEdR901J3CU/aqg7S8PBKAkJhmfJGo23XfCcypuu996nL5Wf3EN67v67m/dbmtvpRb9fhA1/sqIVesEAWSFyNJp0Ny+N1JoOYhhSpw2oSk5YF0LptJ6Ccd6WKA0C1viKjE2bLVptcV1H/DTaqMzRkorTPRxjkh3fPep2ZsaUZxPSbaFkJW1mgDNN1Za5EoklJ0ockUBChVrQmEu8rhFCiadIYkuhYKSocVX76yGoUTaY11gYF67DK/3IYKSxyIiqk8o6W0nJnc2f3HAeA4EYHJs7TfVS9H6udKpX7OqrdK6urVotiyuZPyM4sF/5xummjQQESwDUH8EZSCFvGiptO//uUZPkAAvQzzlHvEfxVBwltPYI+DyEvMUeZdpE/lyZ09KCoDyVwUSZ1GfZt54vwjJNaxar6N5wAMQhlPqQX5nhMefc85rvJ1RE8LuXGXevWXSoWaq9VdpIe1V8oSSKchiJDqjKwSwwoKPrxZk/z0Slw3ag0BwIBpEJyiB0hgGahU9k2h8V1nP1WHysSuqcllror2PWYtndpBB8FguGpHb/7KzJubfS6ItEITEMojZtUtfPzFu8oxaVT/+jS6bWyluJJMBGhtAqzGIQhhDzJmhp9PGQztbcntohPOtKPLMtGNmw56pLoZPjsDdDygpm7+d6BxAE9XbWHhVBIV+p45OUQmpqEqkWuVBRJKdDlsGmY04DE1cxZmEsgR0JbF2gv/TVSgtLQxA8Fif34kejHDL/eOWEldz0XCUPVQtNI9KoIRBzYJjN3bSX5uq7Vr9HWLKSqf5/JqsEcUPX+z79ABBtLAASAKgN8goSCYSqvUKCU4GokNsMHHVVU9fLMYSHy//uUZOiAAyJLzNMMO1RGpUlpPCm2DEUzN0wk7ZEklCm09ImepwVYNBYBwZllDEBKWDAnGf+176Hek40SRRjQ5rB8DOhCZqWUEUmpgtgic9KtiqCGPWytTnW8yl0ThWD4EwMs61e3VgJTDVX6gIgRx/lk/8NPoezx6K9Rz2tMyQKyMSpzkGIDeMIq4IszrCFYz9CMxCMqo5TC2awZnszJsl1d/+3ki0KkcOwmz/JIEwCACuJ+CkBJSFQtt5Pzqwp2JuSBoD0GQcbOn3ZCFGkWZi12X3mAQhFMQtf8Y+6UV6nZNQGFnxvtqexEI97UR8S1vu/b/++1NciMOjqm7j41GIOqG8FmOPGQWFnRYYJIXjKwLF3DYC9K6hCEAPpP2OlmUBr5NRtqiZlIZU4TXl3SRnsM33/hyW2gsLNQTg1UqYqgKpmtU8VwmaaUnCfl7RCYmLdbKlFVzSXZQ6itqmyBO73cUKch3zjrpF3LGlycOwziYk7jM4CFSC+dBcZmiRR0//uUZOsAAsVIzNMGE9RCIXl9PSsiDRUtN0y8S9FBo6Vk8xYh2TsYckswRFtDEQsNUOHh9HUuoAftwCOMaBQ2m5iO40xQhq+FKdME0IUhOTeVQzIAHqPBO5xVeb0xd9bRd7ao4zUVAVhAkqCfmmLqXQkoQwBPHUS06BvPESShQrgEaFgAhYsP624gF+t6stgaTZHDm2Mvi06HHKrpc9V9ZCLSnc9r0TrRgOaGEOPbrXYOnmTCWNSymaFigLAQS//vcktrtaTiaSDeX1OEmGhtwbAgtHItxt940rbcvm9GD+XfqxxErio4PgMbTULOrbR2k7NtwDepjtFeynl6QKhAEEElQpGXbS+gpqqDzD3YXVIqNr7Y4HrYrLb06C4twVYMmvRW+6cfV8a1vWr2XsNwlCqm3ZhjcyP9bFX+JfX1ALG4THHLXM7vzpQ72lOZ/+R+0+ldNZog5oyP9jddakJUl3B3aNRNOBbOiUetQtBfHwZZLwwQAeZH9T0nmIHo6Oxg//uUZO8CBAE5R4NJHqJD4qndMMJ0C0CnLUewbxDuBmn09LBOpUqXLMsq6qujZPZC5dSorETOr3azrO8s9nzBaPmqxd4yfU0kjOKGuQiWAgAAAAKY5kVgrUekQ3QcKBKHtbYI9bQ5K+D63Rg7HIlGaO2+kJiUxDcVjk3K3ICCRKTx0aloGMAPIBTJlnZlFfahkab5gIP0OhmFa7waQY4qnaHfM49cNREoPEJtRowfEd6xyrK9n/6e7UwMrUgCZWwIyaY/ghURAD1lm2DEJ2oE8Qoe/kMZ43PpXU7WQst/BDPmnaKcXsM+v2xt2PeGHC4deJzZ4KvYtRKs3jkMb5LKm2jxCLnIxTRAtEWfQV2AAAElQDhExp1q4WqrCr5Zh8NZfC0TiWQltGScUTSUpnLW/0pY0WLkGnFnoZlhA9SA8xJ2mR5FmdM7IcU7nY0P32SUORmwIFASIZUXHjTwZaSGEuVzZ0p/+6xVxfrYAI6ygAAAkoDiDgH2K6Uq6bBRC7HI//uUZPAAAwhIy1MMG8RNBdnvPSJNDUzDIuwYdsFKECWw8JoAZaYVJxL1UhOExLgnrN3EnmORAwLteupdDxz6zPqJSwaaFEKdZGNO6BxlqG1XPAhLbmbSJJJBMGAIcTJWhjjlBaH8GA/RBbEPGET9zUSiUEBlxSSkRtZq7lfiMQ424b9ITKwISOvlfpvl0wAgZB1OQ28rzPvOcNZ9rI/mtX2deDETHWivUj/1/0oJYLqCAo34SAQ4N0gM4KXTKCwpAgSSrGXmxuASo9yQzoFFvMuhEICSMzaaGnB2lIXAdVwRUhzqkHCqzO2RLtYXdakAmUAQCSVAbSFgnhHzUHoCNj/EoHUPNzFda3R8NUQhl8tXdbZs7Ecr7lQAY4R1KOiwfT+U9R2O3+8rkMffTN6IppapsZ8MpNFEgss4mIUwlIoMG5gPWBpwgFjFJW8QgKsQf/lMtei9UACmaIKJBLgEEF0AkIo0rY7Xs2KioJCw9MTQBhWU6cMBK9TFbA9oRmRG//uUZOiAAv8sS1MPGfREAhl9PMJwC0z3P6eMVzEHCKYw9gwwU9jMuZUoFDwIK2kRVawID4G26/RM1m1ii3KW5r/KkxekB71QSQUnRNiCGqcZzFANosYMQR0uRdxwspPjQlGRaMvbXwQr7sv3X2AH3poopFMMHOGW5cEEuLJYolV92UuTNFByksZR5RuoPakzUQIRnb1ds1LdPcdwImbJ//ehBzIiQkkpcEsaqybow3VJCdp+NoojTJhzPtMv+8EF8RIdMGaEsv/9ux9VWxAGT5H/nOe26B0JkBQhKEyttZtGZIvGB+LQ6c/NqgonJY0UiSUqesYhgmQakyBeIImd2s4SSIebgQHS6OlfkjvWiamP+Q/gFWtmzMRQoKGB3rz2yPOmeYcOPqnmcI/oUp8xj/9zlUsjnaX3cfDBKypObO4iZ4N4KmAD//+lOwGhtpJ6ADi5VqzBGOSeR6o3AopWFED1hvasNdUFD5cBYFZqW1kd56lz+l0WurmCAAh95evQ//uUZPaAAzstS1HsM0RKZEmNMENyC40XM0ewTREkFmb09I0QyURTF9baku6S0unZVyyr97ezowEwyXUpRKSTolQAkTqQqVtISt0fhfMEvqyZrzzyKIyd7XejMAUk/IrOq0rp25wchNVhm5qbIp01KNCWo6s7LuL08g9B2PVPMnwAD23U/C05RHTAIITiy4/XIMXwKefTlh49vo+al/fuUMtsMHr/+kX5JehWgAXAAABgtgBCNgM8G+zHOe1GpLoM+TtT8sSza8xM6x9wX76TSKZpdaVGtDCNh4FtcTTN6/n4LlFRePv0QH2nb4R0TMNq89/Outfyt/3JA6fVNl3g6hav5FhBC1VIoTUJIfJYRACaaaX67QpCYSmer6aQTXmLfcuaPZlyUmvkYhYNY0atTuT8MSIwNRmzIi///3EBAwCcBigwKIbEVdHh+0KhUPqwmlWjqiUvv2qDaEMG2VhUqZcBMWASD838CIZAqLXyeWUTdf7NllueCCFU5Bb4NCnt//uUZPiAAwJEUOnrHJxJqOmsPQJNTlUhMUwkdtFQEWTk8bLRFDsjPaO7LVy7OjDhasGDC1OFXjw0FIqLOCXIDoC2kTmFOSeJ0FJuNpEJptuZDVdoRxENQ7Bf8vfBorgKVDFDGQ3JHGm65rkR3TW0Vrx8qnu5jPMNC4+tbJE/q2bBBDkCOjdjriK3eqKaRVcvMzZtXtc7p+vTtxmcY/+sBiNQMAkopTlhHSOQ5wFYm50RRhcPKqEyoES2o32HInZMLf8wfcN61vV9dgYyFSZQi3eRW+dGHLNuFxUXagFHmC0cVY1aifkBgeERQIAo//11AabccRRRIJgt7OlD0BREIFzMcsamfHGkTSVsyHrlGSPmBsjmXEcZvV+bLk/piC6gdrv5+0d9QCAgr8pBgjuJC2VPqOmdzPLx8rpVc0LWeDO/JrKPxOhNhWcBf2loCFDn///6QBfjoyI23LQAjxbjgheHOO0UKFNE1cn8jnGJK8CRHuVfMzmwoEIJgBxL8MCG//uUZO4AAtVFzsnjFd5KRFm8MMJZCr0ZPawsS9FCi6X09higt0O+k6bnT/F+Y/1K/dpBmzZqq/XOZeeArtBQ2T5uzDuvaBmqwEkkpVGgHYwTGjjSI2RRCmwfphFNzIORaZDERKFKDhQiR8Rl2e7P6eN+Na/CgCUolIYhAjdq7L8GMObLNWU4k9Qud2x55mf5EfYV/yWeh9/YBk6p8Sdx4uUE5QOB1A/8WVi9m6mACqNlAkElQG4RYkkwupdQFgSIjakG5yTZPVLzCNI870O/m6fRULkQ9DQoG3rLJ4jJiygsQSxIqtecTfercfXYSz5nlwTnVgCJQAIBBUOQSh4UOlEDqJCtObVOJE9vYMgRH90QGGRKFh2iVYDwxGMBAKWAZIWTtTi7F1LhTayMPRrmCK5E6kXg8pxlbM2FOoJxhAYi9Ain78+n/T//RKd7ySc2JfO+hjqEIFSYS4ZZ+sIJFTGyLZAkstuOLSghIUBgnfgrQTUZxo+Vc/ihWjqI6hHE//uUZPYAAxpGz2niHgxOgumtGeMqTOkPL0fgcJEbiGW09bBA1tyNpcSGa+UHUS6MzK5XjKcAZVpS1DEVyqzVV8Ug9Shiu9VoXMBdImVtIkoggGArQJA4DqMugVkEOvcDBq7a7CINoYbrytfIiBGlq2K3i5RO/nSrVvwo3ElmagOdqvG7F/o3alRiAsZQTNHIyNj1HCBATrAsPiBbtW2OFCf/9t3VoAM8ABFsF2ENC0j1G8qbklLiUim1DkVHJm7KpCGmgfKtVI506JjHuhODCyxtygti2/tzq/Hb3+T/u97+5e/0no+zG85pv7sTowQAAAAnHwKpDC0qHIAIQJ5O+7zGmBNyXRKXnYJqcJhsiJqo/MBBXHxPXCA+DInPNew87xw3A0tfxy95qvyFyVz7ixmM2XUxExC6ztJ90O7bOf9Ssfuwu7NNCMxD0BMFhQNFWpCQ00F2Iz3//RRasMgDO2XoAxUjGpUbFFmstaa40bQiIPNkY3x4hh4WsoqWc9Fp//uUZPQAA4BLSlMJG2RCxBn8JCKji2DJO6wYr3EhCiVk9g0ZCDCD2DdEGGdW/Q8kudyJVBizNyJyrERXPX+5G8dONcF//0srRJWI3MGtYvRPALTIQQCUon8qARocleSyC2zfL9kzusmBRAF2FQuie1ZMysav8FnDk4POmT0CPI4sEDHN1UriSDjnxZyIn00OnOAsaDgHTv/g+l57J5xt/whEn5bJzsXgQkBlv//6vroAAMhICkNYf6JECHiak66ZUfU3SEnWqDfsVaRuizhVX0IGkyZfH4pGx/D5SQbGJDBCgwH9v/UjBIwvuvl13fvs/0O83Wzv/3188SloIKUbSKRSSbwFYlY1FaOM0BDD5JUpztJEWFRk7OBganWPIhJSBmU6RyUhywE3lRdBFYzfXz2xQGVANFbq2ILKXmvpX8GSZjv2M2B6XhTRKOMxckFCTZ6hDond//6gAFKECUAAnQekEXC0foHHQkHdcP4tLQfMjxhk54ZV3m0RacT7lw9x//uUZPcAA3Q2yLsMG/BQp1klYSNYC+UPL0wwa1EvECWw8w2Ji3RvWifurFGgirb1rZm6Vbt1Zndbeh11JVrmJsxqdX8owOZHyc6NV8KJBSU5CV8tD9LyXYiDWM0WqpYTELYii2lsSpYLRVrJD0RCQlAQEUIBQFg5g96HsdiukY/N+maE/6ZG5Ib5rqtxg5pF6pos8u/sZfPNzdLHc/d2893P1PfNdf1HnB6cMgXHrf/kv6yeugRBSKWKpLDBLieTsWkkArikOdElYXxBulmVEKnurNUbs5wLUHrKyuVd7vs3RGMWx1HMGoaFYyyJNqexctZGqXMZcsv/b9/aUextz7WzzEmwdu/+uhE0IIMGAJMoA6BQcDyal9EkkRXgVgCHjACFn0sFiAKMABGl0g+jww9i41SCTUk1gjOEVYY44TKRBGumkkXbAASNAYIggCBWQOC4841td6qRACaDk7Dj8zT+OusRrkCFx20+BkOTcZFlDl2SW1N4HZwziKP+hexF//uUZPAAAuQrzmnpM5RNqPl9MMVqTRUlNVT0ABFXHeZqnnACKRb8QgSq48mtT2V2n5+uP+/9qH5ia0+0SvY0md+/TXYu79v89WKcvNI8b2b/Tc9Vv/////+f//517eNPHI3q5Ftv7QRHDv//P////+B3fl9uzWsW///99py/GealWRTsPh7HJaVJhUAqNaZTcnnbgwAUGlEJIBy0Zg585DB24z1QdidE08ZMA2wB1A9sXYfQDgoD0YB4CTIoThFBQwlEyQOrIIThSNg9waw5YucihcQpuXyKmRNDWIqxYcupGRu5cIoaG5bHNEiIeN40LhcNycO1v5fL91TJFSZ4m0DU2MC0RNP8Z4ztVmKbFw+YnEES+6B19butSdDmTHV//0EP/XWXW2YIgAignHaoYjQhpLWBv5G2ZJ6hUEbXmEAWIrVxlMZwCaApQlwAUgqo8WSeXiSAvh4lZ510biYnjFF1JKSdJIvGZeLxLGz+olnomrdT//dVJfpVmqXqVR61//uUZOmABqlcTf5nIACkK0odzEwAjW0/P72mgAjMiqx/ntAG9fqSSf//q/utFlajI2WC9FBStDECOhrIBABBiUiyiH+dTwt/xT5/ZVDEJhSU5Ra2ovAjxeNkn6jItOf/YFbm//////Y+drotUrksjc5MUmCpVA8h2HeK6yEnFQ0oExtMciHntGqTIeL69tNrO8O2K5Z3924HEkrzm3qWR0hHTz7W9MYEBAxS9WtPYzGDDKWhdRM62tRlV6Ie8xu2heuqsipVqcfBv/WAZO2JktZbIHBxfjGlBE+h1mruenGVsJZrixguSHYEHTnjhu6dpGujh9Ui48cs7m//+jm/yDXOW0EAAA0RkgVHZfBIJFJAEvgqh5b5KIDyJYMjhRdcGODIXguoIkCUlrAoGPS9ELfV04El7FIbZvajGfFEzQWRiy2lsimo3CCqDcCzQSs1MJelxjMPAPp6vaKpsQrqFxM4YEI7DfaUysyWLJ38iPyBCYHa2W1HQzpLdQtiZ3Qw//uUZIuAAxpLVWnmFaw3AwrdMChzjsEdIm4kdlDMhalwxJjO1AfEgXJxeD94CLgE1iSFD1MWdUYW3STLEzzSetcsl73pv+vC3/0y/+NUQVJ1CAAAAAOamw9KDrp9lRbALCGJKqstE5IdSYhvM88Utw1bbmxTjxkwAXrDkuhyWKqw81qVP9GoHgfEZQ5NMHOaPFiVCB7FmFLOJEWBKjKQJnNaxQ6WIppGOOk8Re/HG38Os1Vzf/V/zTLqrLD933MR6DA2kQsZ9TCOmS2r2SNxhAIKxaEWKQOdVYHxYHAqi8xZAScLWUNrO0evmV/TlPcVgIAu9X93////6JE5JYe1hAUxJI1aQFFTLnhwwiWKljj8KGQQ8a66ehljvKzxOdYC4a3LkoazdSmeUnEYMgS8UoJnkK1owZQJkJ3Q0w7baVxNfptSaX8dqKlT830urrF2xFAcuPRTheQQTvarzx6TMfbcGYJwz766lRnWXOau/uiCzxV3zodALgyBAsxYkGYl//uUZJeHA8RDSLs8QfQxgwqNPYM1j2kVHg1xKUDlBeYwzA0QvApGF5bLakGLus7cR6opL8uNR2xYs9hRh5kehrxNrtait28w+cp//bso8kkCFGDlAQARkgWAREQCTOmTkyeg2jE0UQAJu1WGAL8T7zTLwV4pRvE2r+vnSe2Cy9sGxd97US5dm6algOVTkfiLj4Hhdkg+EkGWNQMRJOnFxpAy37U/hsbIncDYfvtvy7n8OdjYal75znbPnYoxXOhGYnDZcPuNQyyC2pYCYnlgDzqq9CmcFby3zgmlKK1ENxEn3I33Q1h4BNRDv+7A+o/Ql1GLVen4o/XSNh55t6wKRl9zPjkwAZMSGjCBhZoOHBGEFrjWcJpS6XVpzsS54Kx22HkqFMZqdcUyEecB6bnApj+XlCeJBBiGCUw4CToBTobFPxTP5qqRgkvdwbGiH7fWSOlgJ+zx1UexxaT5OzsY9m+pj2xuwntkDFG3S3bd15v7n/VTTyIEucS7LX95jbJm//uUZJYKA4VISKtmHjQ5IqpMMEaFkAklHg28zcjqDGq08ZmmPbrHA2mxodAnLcoYhBTjntnwqo4FK4+3Z1+QOKln2isPEODFHKdP6VdkC/q76/dNMb31oSHlsi6FAAcAAAIMasMabKDS7WEswCwZvXCnzBVYZeuzDkYaa61mmi05GpfNx2JPrDrQYTORa3UlUbbnGGiT9HJp8M+JkmPVhpmXPaWbVCHeQOptnZlXe80u97SpWrbU+6pHp++KRiY4uE0zbCYpQRrlGshQxFxiIfrxcbUSmbUu6h7hsZ/pJwwWnCqUEhwYeY3oSur66f///+E5WHQkCMObMasR0KEKToooYum2ny0QYpNTQknkJUoxsUrVHQwuDYabOqz4JEckVjJM1zKZQmy2HIom+aMwuMJ92Rn9cIR00c7jlz7loFe6edmqxrLE8bLGSOTeRQbKILijbE2vdLJjhzU5LbXFPwsJZA+WgInoSCwnBSK1a4+XLke8hjM2nate1XcWOicS//uUZJGCAx08yctGFhA0IuosPCOFjMC7Iq09C8DNC6lwwYnOFHqSeRcm//+mp/6OyioouDVEBctAYJwEjDSEAKRUEQ7Lwf0/XjiYkKopA+vTuid2Z/5LhItGSM2p0GbfXO6f0RSqsC+a1rH/+2o1PTv/neb2XfDasi/Ym/8XAJojYBARadAtKIHW3E8N1mIctLoNuGZbAhYUSFHMRNvVniyJJmwZpasMBWAAfueIBZzBZaKkrlRpQNkFmVJGkLZcs4dzpMg/rYLiEa2DJDZkINuSgC0WeszZW5kFRdqEXhTdL8GwI/IYBQJKQg+MS+gyMLKd5maQ0rOzSy9Wd7rdUtdFTvlea9eysjpb7WpXaUWFUW59ZH/smMCRAaEIJ+mAQAShwpk3TqLcDFAmZBx44igOoHjaFCiBYRWphhiZApE7WXbBMFNY/e3FCo64gTF62oceNRGKjWoZWSUvLrioqhRSEqkADUAAAgBnB1QUdbBWZQAvjFHkUuAQ6d2r7sT8//uURKcAAmAXTeHsMMpNApmdPMNmCZjpN6wYTQkwDqc49gzkPu7Ltx58F60M+SMkxQKlqVWHEIq5+JwnPLqN8aWiiTH27G55DG7uoQW/G9Yj56znb7rJ3N+7X/b1YFF8JVLNI3X0MJwCdIMcqzGOlQi6I5GSG+hp+WMzUCpW8BOPnIimYCe1utzC3IHGOP83bZ/6L0Yoc73GhGJQBVbByoINL9F+UDX0Bwy+GCSVYm5CyeAVRZAaCE0VKvKlyMD1B0DZYddi88IPRcsWeBhSnkH2WGatCa0zzz4c5N//HkLyXnfK6XBEXnl+XRE0koKqlB8PkQCoKgAKI2HWH0FlKjEYnGIDjnqoQzHs717MEhcVnByZBZqfCZMoP6MclEnKbuu3dJ/VS1qBfe4UJArIbmBXfWKMDQNwNksYRolBiHAwHnVEoQ5sK0yhQtMMyNM6SN4pRkfXFVjNl2pEbZOdJ8lolUM9cEHIlOVy1XTgfnkut/MqW/o/Q4g5pDD/5yR3//uUZLwAAsIbScsJNJI5AYpMPeYni1khLyykbNDjh2Xw9gygvEPIbd2kxATCdgjAMVAEpoCYKCsmq3+Sq4oVp2kLwbWaW9vLi8iDoJpDppZpq49wlAxEKjwLMNAtd9uu5lOEbIpcjWcWFkIMaCGspsg0xG5JJNJkKts6EodAWQT93jBdwlGRqi8bKjwJglFQ36K/5eNU6+r68dnBM/0k061gjTH5l5NRB/M0IvXA9FuL/f3/2xxuvv9C3aqf6xGPMPtbmYQIAEoW0PJpBDSEEGX0qNFRUjXVIcDjqGDCUq8Fj3/IlPPsB14e+R5d5FwSBTjSIDXGOULvR9HnVyzu1C4Z75SUQQDCFk/E/HOXovxZlM8Kw8kKSZfklAYDrG4iHfPT9YDIlaV3Hh7yUOyLDINoyZAr6XQUfTvMOqMNAYUVJlBeUDQs20a/PKbRWVcLlwsAh3/QAH2AIC4fAtg+zlMhOKA0VhPIxQmcWPTh9EQNueGXSxedPkCuSErzNDA6//uUZNeAAsY6zmHmG7pA4elFPYkmCzCPKCwxKQjykCWg9IzQeJgJL5jpZX2M6GR97/ol0MYCotBsudW56hDZBQRJJVMB01luJ/ZKgdJlyxH4gXCmXY9h5ADmyIxIwNQZcKhsEjoIHy4r2Pyq99NGStYkTx0o/jG6GkjldAwlAzg4H1heSvPg8KsrFB7pk5sfQVWB1kFJiojIN/qo9/aCmrEKEWkioEQhoCcTI9ScJhhHUmLwrXIQT2T3zsIQ7p6XIObDZb7McvsMKngkBAqgo9YVCCAtaLzdtFDP918p1scxI0ECjgGXPFg05qpNuOtslJEgqltgLBvBRiu2XLElqQjrtanGCp4Mu0hGsgLtBshPfE04sZt1vXkw2rTO1bmvqckDhjhEnakwqqszmFkfzmcjXevdv7WVDLRpkdCs68HBjq7o2SIBoODDcqm7//1g3+oSkkk6ZAMDZ8EkEeRzQS+GYBpA8II7wHeVJvCvzOUfa52RkoYwNY4hYvIR1N0M//uUZO2AAqcoz1HmE7xDIelpPSNiDCSRMUwkbZE2i6Y09gzohZWvY931HJRtFs/Tum+rs7O9vVWf7OzlJCKFT6aP//rUbmQpFJJ0MmXcVIlK+DuK+YqwSCGssIfjCuBgI5hgCMxXMtFFnJryO8QkNdF0UIQO0qfGbIztQWMbi+S9B/oHT2h+N1aOnn768b1OHmRH/ke5lZyf9JkK/rPyrCMx//pABlbSASg4ZoMPIozohn+sBpsYrxP1GdaG6VDR4Ueahc5ruuMe9baj0v9iZyZqTwRU4aSecFSahYhZubeUqz35S+XY/QkCGwbEbkoqQb9VFIpJUDigoFuKqT+NVUff1fECsGsNbirlt5Go8OWGOW1nh0EkIKSDnGfplqnDyHBAi2uDbotSzlhqflz6WnXa686n/UkLzmjZs4gXFQWJdOlG7LMbN3O7d2oALtACUQlxTiHBcEcZKCUH5+wHJWUvBxEUkb/OPMdan4w9M5F2zimdsPzvQlmlVpihFThy//uUZPgAAxtI0GsJE2xQ6PmqMYIMjAE5NUwYbVEsDGWw9JmAIORe27vmcgZ8DqH9BhbCm6sfehwXDlDBD/s3/qFq+lIpJOAAXuiFGgpCKRYmArxbzWV62hD5UqddyCkTxoOhBRv7nqBkcqscKiLwnMqbkDMfpnXESDXyx7fKejS8hlV0Pvd+Q76eScjQN/CT8vk0KiAKjmJrsCR7EoDSTpJACKSoF0LYDuE8DeLC5l265y+glyR5b0pFmgtc0CNqXcaBzXKUkfQPyYECiOhRPYZyUZTIzPoUjeYaYwAsv9iT3kt6vKQaMheCHqlqAUUsjSIQIKqnHEcJIz8CUhsDxRaMGSAZAiZkBlPSMzPX3GEU8UbihAeXOFlVc2yPly62eZX15S/lT8lLKER+ZghsITKM2YiRiQqHgMGFBlADJCrJP/d9ACdKgALBIAFoQMnZIBbR9LCMIUlyQIJSk9R7xRkx14kXTONICaRra1JN6udjIV2DNYrpbahDvV3dde9m//uUZPYAAuA4TVMmG8RQhelaPYIuC5UhN0eYbtE8pWW08I657L16dJmspNWZORXkWxybMybbvbBoDKaC05ZI3dqKDBL8OEdxNyYDvJ2OAQ4wSbF5Q82DdPAvznBQ0CSEtpHqJH/WLCgg3Eq8KLp9j/cqo0/zZWGMrIx0VkZfm/c19dzL+d1Su6AAk6eaKEFV7T5xE0CtMqy1YHCF9kxSahUGe+VCSPlNnShR1QURAfJxmbFbsm1OleCWSFxnoLmBstLiG+al1YTZEZdXJ1yMB05vw8fY7nVP/6Zm9MqYiiWTpdOT5f4UJ6gVB/T6qgQYUkSUtWHeM4UoANj8J0bhZuIj2kW5HYyLAgSB0le9VXF8x1XXpV4k7loqZafVjWJItPUstmJVnOr2ZPwNT3FvIDc60bndX2QOYhCDGOMRATtkSt/7zNKFIK3QoBJJK4Qsu4g4BcWENCBEKwPgGKhwOQUr1CDrjjtv4VKhH205qnMQNCw/LwqZfYADAAzo21za//uURPeAArsvzOnsGkhTKNlpPSJ2ixztQYeYUXFjpOZk9A4jGFmC+gghFuyKaWv6iIuAS0V//ZhgmQY4m2iyiQDDqARzbP8Y6uqSQlZpCTiaJd9ZhYzcImxMlR45AhckkpPWeEES1OkLEpi61+L/1isoZKSz91UQKPVwVrzLE7L1WJBuRaoiplZrnf61vvXdH5nnXRZlBsv//3qEK0wAFImiDkAIBG0duNLQuQ9TbOJR2HRhqby/L5uOS6lp4mrxeLCW2lzWFZPtiTKqVJ8hQxE5y8OzvS1niqtZ7adtNr39Tv0/+rIIgRxJ8jVlRyNtFJEgmhIi7bJ2rlUSgqYKpmWwzNLHbwn0Y7Y1A0YXMLGWdri5fR6498HcQ6D/T7mUMYECWZpwih5hmtBmVAR7GottljDMtES+11ZW+hv0BKVE6CkcKQXPA0TWVe+goy/+nVKoAkEgxCgcEloQAbW6xpZporMglc4vl1cjfpBWEDuZP+5k6w9//eMQRt7dRRuq//uUZPcAAs1ITWHpE2ROAxl6PYcejDUpPaekT7FApCWpgZZob5qvrKSEhcy5BRoYGn2XB0cZXBoJmVCUGm20dtySDSv4kkP/RHd9IRQAApwKZROA64RRsigTW0qlqAmaqgcCkGCsqscdtxrXIbwt/+zPDmHyObe47ydY+YsGNQSw8I3dDo0/o7fVtVk89mMw0pBZdnKw4EXiMXsLOa+kUe37JX/5FGiEAEBwBKGQWIKc6CHnsuS3HYwuTSaLkS0tLEX1P5zffpBsA0u0vLwSdEMtGuZ1V7pV79P2bT1sh69ft6J929yLpaT8Ed53/+go7UIAAAAc4S2ItI1A2BIAW9MSCgg8MBFFbVBAcD3lKFtv9VWy3RPF/GA40tOoErDDi0XqhqGdQNGZeCCnF6A+UcqvbcvXqfcIs0sGUb8lMNANDQoomWESSCglNzQiB8cCFuWitItEZ3WggdL1KJLCCCP6dGuW/chWS1T1SWjp3f5sYzNMkJPadMktEG3oJ35W//uUZPgAAytBT+sPEuxTRAlXYYY2i5znO0wwqfEwp6Vk8wmo+yJiS27h0Av8OqQzVc16Dx7HFCPCuLU5hBDdqErHnSmRsp3/sRVFgIU3i4Xe0ixZlXN2JG1pNivGpKEn0jBjrEJKsJfUAtCEAqwT6XIXpUwUaQdeVkLzNBeBfoFRGWXbmF1euiXxvHCqmrhKBsdFsShsN87JyzRIkpb7mWjt2epFuOlwcuhNJ3k0R2u/RFSdaofrbZGnS133RL3QFKw2Lrt7WcIogkGc/zrA1jLWTBQscwPsrwoH05TKWs9l9e24UylBDnb3IdBQJaKyKwCYDCgSNqXp3O2f3OUpK+miBajrjRLSRKwbSGG+QQTdLGyiSwl5O1TGeQFUJmZyeMjKosScUMon77qZCMGnx9A2LoxREvWOe4RbkaJPLr5/GXncMRFvTlhBEIRiHK8YbdxJSvbu7OlkDqqedB5gc6muFSB5e2//qD1u1zjUjaT/NPEykzmh1Wi2EDaS5JxZ//uUZPUABF9JRxt4MvRHAyntYYIqC0EhLywwS5D7jGe09gjkR54w5ll0yKJ7wSOmet4GO3oLDECqqlVURbM95byvlcmdEdi7IjJWqk23f6yrTY7s9H8zs/ZwdAbVHkPwgxlSMX1UBzbW6SKSNJd0eJqNaQqSxctMIysia34HfKApfPQPVdOgg7BfDaySJNUnQ149CAo2jn/S/fUUZmKpUr0tzX5WITsLpLXfbuWaKXWrv+t8bK5oVIQJYMqvoPpf87//pADdTYEhTYBVGkXWMhTOIQhgQGxtIYwMzE55VjcvswSLISlHVlBuyAoDk7wK8mx1UxRB0UiNL4oonMLNuUtCW/7HuKSu86hqz0w0LNRVALZBFVsExnEjFYqoMijAzV2WPqt5WAf4R1BoXDGKMs3LD0aVrteSS5zvl4VkUwvM222w3hYd44CG+hZmY9Omtl+5d359MvO+l+TOUymCKLmrNRPm2fjGFTsBk5QWFMCoyCLQEodku3+lrjjhTBzE//uUZO0AAzZIUOnoFGxZafptYeIri7TNT6wkUbExiiXw8aYQ/NAWxDT/Mx2WwfJ2E6Q1DUlEWoQSmZAhstSSxCDwFA8RostWit+6lQcYDCRdYsKt7/3fZ6bKjqKkgFNxoohJNN8G4E0HU3qQthdUQLmWI8E0Qw6jDL83UxUhTopRVWB2vXu6TmNEP/5eWocjQJaRVKoL+ma0o3DUg5sFyCOklDH3y+7epzP6eWL7CmT9gy2ES5bpIb//2BptxtogtNN8lBfyXhhj0FyHipRXzQQShJYUCEIhvhN9m2PaHtSir+6nlwx7N/NYxp6ohHRPz2uDa+oKJKqFh/pT3vpS9lIXZ0rzF1kpv3MVVB1JX/61ADVUGVoSLC7gFBAWzdQBwoEikJfuARLJg8EA5RFtDKmUNojj4ZWMZ9uddbGkGRMvMxjJcxLqB5CkHEC0c/mILI5Of2cLTI3zuRz/LLp5LVOuW3bpiN4mTj1KqJlS41Ah5EppXWANUr1prBAfgOlw//uUZOWAAypLS8sMGvRC4oqdPCl3i7kHOaekbxFWpqc084pS4GKM/NYksfDNih21isyn165YggAwrJ7rHotEyUvEgHOiV13atqNO2Ma2625UqA622ikG0270hkImStCTIU8iEHts6agiXLYPpXMpXPqNmu9TTxETdxE5yaA93WqdEMf/hhvHV3KySYqy6l7lRjxnsQnHV4+Vg96kYUXMoUMg+9eqM6uL1H9tisXX+Nj/474SwWkCkFyLXNcVi0Xk6m//3gSyIAAASa5KmBMRRQuQ/6mCD6cEbgepDjatBOSRoUJ05RuBdJ8Z0nsGYwOkQHEuOxFc47m6PR3Y/ZOku+lP+98lw+e5d0UgM1Fw48CFVmmB/eedJFf/KO/cDdjrjZKaRLo+NDODTBQCwgHYAYC7T8iOQo5zH3hUJ4Ak4MLehCUv4rGoDDAIP/+PhsyOwgo3Ge7Z0+erpIbe3aW9TIFhqflpf5eZZ9pfO/97+R5AwoI0Cu50f9/+m8ChEACS//uUZOWAAvI/zEsMGuQ+YomcYYgWDklBOaylEVFkHCUphImqQoCeGESQXpEptTk6W3ioESLaMJOUv+8PL9Zvv1RULBoIKYfGuplH60UkXbGKX/57FkVqvYi8JOQS3cJKSSbirFnnIYzpHxjzAHda+9zO0xYg8UscWMvDBGNg5CkkwvmjzsfluwlQCv6yBibHnT6JFOfVJ61Pe0SlHI7st2UNZGqo/bNRPZ8hDoN6JFEYfiZjXh4LvKBBSmt+8Kuy6EoRpFQUekvEYFGmIKsIK4L1HyvnOJTRGW8ArwekRuZn9pUg6DVMMRdK0JpuUxMPWMuTzc7cy4o9oiIWrC59hQ+TJwJ1QABJJfONYLmoclBXGVC3Vz4Q8QVj6QlZnglkIsrnFocplQ/tZss+tYZ7u5L/1xNemaYzouLBlhEQgvIf7OZUq2AB48alz+fXEAeEUgeYRaWMvcgfDgcFSbHpHqURbv60a6RJSSSVJsRBB4iOgqYgIJEkTePCQeFGjL8e//uUZN+AAvJK0WnmG8w84ol6PMlKDC0BN0yYUREVjOd08I4caqFCzLdulqPSUrCguGwyV4ZZYh7yFi1S7xOUcU1IXTGiyj0l5k31D1f/y3aAyoQIABJoIgj88BfJlyN6ARHkucwKMOSKRKWRCDK5HU/eJReJpicnRiyjbhYbXNlnn8neeMbxtXBvOQ6fIfT/7LqyoaU/yv02IGOH1/tPayhwISgIitosyOOSIbGoPTH+ljCbJaWESbrQACSCUCRExCqAd1EIG1GYiSvPFyNBHq5lhxLa1TveOXVUE+ZSQzkoQOAbs0EdzFLUvX/31UUQJrZurkUNZoKxQwqKDiZpAARiIKyrYLYDHAymMBbPYb6rJ8PIttS/oQXNQqlXTvVPAZVK4HZD25vCqBoRckpAgq/5aiGqjVSkhJb+HzGJf6aoGPBRnd6MmhOhjMxVafSl454a+sv7MVZR0RsSyDxZNrSyNt1NDWfIYgasmzyRuJUIGtOly0pdmiZ3iQxgZI/5//uUZOyAAwwqS1MsGnBHowm6PYU4jMTrLUwwa5EiGOY08I5wbpXsvfkvNsErkWvXWi/q296Ky+T5cMRiOVXV9ATpSzt7MSgyk03JGCkkSCqiqGRLfoJ3PXSkYrIu6H3Aa63Vq7VZl7GUBfIGFTwqvbska2YOTkZLVSvM73gy0MKVzOM0yPb1Z82zaLPci7s/3ovqUGW4NWZXlulS3RfGAtez/+5JNORhJpEgqngeodpklgu5sx63CCigp1gaekcff8f/w73lr82GSbatk9qPkzZ0Zdsn8lH/6d9lRuvqe1WqquD317ghCkZHQcOUE4p//SoUZQCkSSnTuxVwqoLEIQDoFJULGV4tOXgNYTl8xoDsNC+Lk41FUqTanH1E7NLRoZ+cUzG1i2cqgPnb9yXOKORu7DCUV73yViS6UehipEhKLkOlWe9Hi8hiGr7JO9iOmQREAcopDiVVx3/5Xq1GBigAQSlA/SmZMSAxDEehiSI5KTjHrqk7tluriodLXA6p//uUZO6AAv5KzOHmFTRFiTpMPCKLi3kdQawYTzE1peg08wk+//eUchY467vqq1gsq3G5ZbPVKm/cZ5v7rkertZPrnhf2Z1rz+9VL6y/I4ms55EOIo8E15mQcIc6ASgSxZ+yRHehhgwSXNphjfFaG2CWLIFomlDVTpFeQmNFb6StjR/NQDZcvB6BOEZSgCQgpluofbLNPjNfddHl80sxy4p/2ZNsZ2F3Sy2rXyDGjiWSup6v1NxtDuVx1tgAaNAGe68VUfgHFhBq0jVEwTxmoJq3z0/pWgbCKWAz1hrkTkrnuwwMUrC3oJwYA41wwIAwYOBx0DOqfS7bkUJ3MNM+bn3b/1KSJBNTDDtGAyICumUNekCUDMQHmYGyAukuLRLhHBo4Vp10nf+GX1UgwQJ5EYV24homRyjAjv9IEXQf5oF/waUrDT2q+RGZc6NT/IzyI6xufTOEXbmdxJSOj8unJLK0/+miEPsFWQAB0J2DolgyJw+C4nwtoBm5cWCrWIOtk//uUZPUAA2NNS9MMKvRLYZlqYewQTaTjIA08y4kUjSYxhgxoEzR2md3Rm1kaDPrdrUZ2dut6+/WqXb19u/pPd2Z6npGfbmdeQxLKkjZTcowoYkOc/BZLMZDBLkv27ahzvspc9wEKAXF0AMjdvSEkyHI7V7ct/p+1h08vUD6CSRhiU6LiXm6TaLW2y75CO+6/lMvdIxWCqRaUWiz18YFk8cAAdKQALJKojxoW4QsWMxaH1WEI+wWLiMyCGTZSDm1wKQ4QWTQ8QoGrYA3B8SA8eFSoKAOE2SJV5qNaKPuCnek0oqtIbPHbXo3gmL/eQgVI2wSm0km8InQgOa7KB1A4peZNVh8Dt+6MRjEBQC2lOPJEw2KUUVhrC00cFVj/al0Oq6kXdsKaRCtblRUq6tUMbYFlXRl3uX+yXulLyJyMKd8rdKtKq21QjLTsKX/vyg11IEKgABEErkLFCFSEQzJMhqKNyOi1ahQlWRHlIqD0rezrvtEGqKOCZzeOFqZlkxcA//uUZO4AAutHT9MMGlxIKBpMPYIpywEXQYwkTbExByWphJjQaXa+D9i3BiUiDbhVOWPDp2OY0oQAgBlEoe6rEptrB3zppCf7wSaQJSSJVAgAWForJAoBirIF6iQWJv/XAYQE60fxqeM1xWPCRWiLynWOq/2W7HD2Pff4o6O38pUqaeQ4zhSzsWSvCTN38/6zSovEar1g8kGCR/Ivmjc+MR202lgSPgdLiRk5+SSiP95CKpQAtqbEAqoRpeBB5JUkhBSKcne9sTNunujRC5wkZAQyhlgQgiVpvT9J7l88FeatMkXRQlexdqrByRjbkJFkoSwsuTvf8VfVFb7VJNpOAQ8G6S0fwuKJhl0ECXZxqVdKw5T+ivk7pFQqqqSk1YmomXpTsCM//XUfz8dgkY4darDlIscKcjaGHO3819VdeRFpZkVd9PIU9rI2zeyGoccoICegm/sWkk03o1ABiCl85rswTuEPJiso/uRYLSYgolbI741VwIABhlHeLC0IKW9///uUZPcAAxNLzmsJE9RT5IlKPSNYDPS1MUwwy9EhEGXxh4xoL9S9b8/LeZnPXULF7nbQXI+laVk4hHiVQBcLsqAA3/9V7+oNSONpJtpFKjuZwUB4HmWoSAI2N4ge0auT1QBGIWrC6/ousYy7xpa9jaaT3erSPgKkBjsa41QXJChwZ6aqVU1C5Au6xlLOnfyFR8Mc3MtC8cxagG0SL3zjz5FMiG///7A2kEBTRKnFufHaC1FrUy7NQV84Ira3TYlzrGob6KuqGbiW73mcGzRm7F5XU/CmIsM8ns7NGvNKN99+k7sW36r2QjNqqJvd22q6jnhAEP//SpUApyRFElEgKJwUtmJeN5SAt00Iofw9ZbUGeZISYKE66pmLNneF86DldPag+M2CTa7fd1q8ltgGUpFatQT16lHuIRIf3M88iWLxju12h6GWqdFR+hE/r/jABEQav/9UCG+jBccbSgCGDhLssoQ6q1AoTIQ1FrSVRooAW2SeWU2lMsM4i3YjY86x//uUZPGAAs5HzlHmFMRQRcnKYeMci/kfRaekbXFCI6Wo8YpoG24Ujg4UIuHFlpXBVg2zStfOIRqcNI7FsgA0MOLFA84aK9SrVV4ACkpEhlPsUijlMXUml4xKKiRCKgsjA5chJXiksoHyRSd5LJQg7ZN722lEcGmfDLOilSJd1ImgmrR7sq3s6WeT0drnlEaS5lolEuE/8u0S3yUQvMAvKAAJJKreDOBAIg4dHwW8IydJyH6BsMjAkGyHWDaitYUkyrnKiIxHdTHetqttfIUZnGqm12dNlordgToln0Oit0bK/ZrM9139ZnptQCixRwLId/9CADNIZTIG24k6ABMcxNzvLmSgsZPSIN4zDcMo4GK6OfQ6tVI0uXGLbxwcWQPYpMTBXPM9mIz2BLdoSHVOp0xROyfrsvcUped54MiaTP7Ea9nahp7mPiYmhZSjhcCABG7vGnFBzIRglKgYGJhdZmcRhtpjN4zL5A89PHd50k7N3WSr9faZd9GmSubbpRuL//uUZPIAAu1GT2nmFTxLQ9ndPeMLCxDzNywkq9lVJCWo9IliXwZpPtyYRNumNPa7LDGXxZUHARMaCLkVh7/7IRqTHuc/zaHbLe/5f3/hbnTxyNqwZdkRKKKgEBBNgtjpLQc78uCPLmqYRhCapseLqoXiKDz6J6la1aPmGiyxzIjO+zKhiKxySoKKf21oVkzsr6hHupJTu/2TtW37sna++la0KRYYGve4JwBCQQBABfMRzj0CgLeJosbf1YjL3cZPcDwOiIMjNtlyZnCyJk5po+rSiBFyCCzbtVkRFBOusSwIzddShxdnX9/72e3/Ni3Kkl/z05m0WZ/5/zvTPM8wYUfcv9cBxuVppJpEqkMMY8OR5mEoKIs185GIdlp6IIVSDiHA9viQdr0MsnQsKB2l2sb6fByACBj10wZqjnk2Vi0jDqZdYjuf6uJPiN3TTMl7zm6DW8U4BAuQyHyoZAccabVUoJELO4mPZZyxFfbU5EGhMjZpM6nkCMgbymRzaZxV//uURPSAAtpGznnjFUphSPklaSOkirEzM0ekTRFro6UphI1oMqAA1yqytY+obh5ZdfIj7yZahXHQHPi8+A71EL6GUt9epMegLOvEc220ksjbUxt3AygyCnOGOTcfJfA7kiVF2qAYBdjjYWfqcsCwTc5J+ubbXEu3GOrK7uu1HW4ZAKDtqlKZ0o1csyZNFTRJjhJWXVRV2inddFP/0/iRSHtzjBCh1UOEWDt1hGbbGSRRROBRJgAOmE1VbGNDj3HziOrELwaX9ok2mWF6kllpcSkBLJ16/RtZiI61yt1toClBtU6K2TN6P10pdbnoilXepLL75aiRynUxVQpJpXH/00AuI4nxfwjovhlFvFvXRehzn0zqfC+lauOEcX9iodSi1aBCqgOiQWjt9Deyo7NP1DhnqA9tFmslXIq95I8+ml1ey/uvV2jHDRLEF1a3uFkaCZ08OUASoySki2klTeZgyn48JJ0uLWW0IaGAdzgvpm/KTztTbLiL9H0xUcqLIwgb//uUZOwAAsY80WnsGlxJhMn8YSNLjB1BUaekUTE5pGd08wmk/M/+lmCrtgxYQnVyqEoIOTYsTadEVqk6sa4uLT/4k+L6P0CrVgEkknKAIwgVYVCBD5NGLKtXZVdRnzv5Q/Kp6ggiQajcBUka6GlEHR5B0dNKZ2xPe5WYQvDZ1vjNkzvzc26IPVWZBU3roT+qWpehVuDUHimV6D1RPs6B4eMC+lRVa/7h03Lq4XJGlAPeCA8CPsMNfSCFPUM6rKJEyEkrL7pYNwpivn7zZiitXo4jUkiMPmTAXJjEFBApb7tOI7mEssoP9Bx6FBkaQccplhMqCLbiRJSRIJoKBsVhAjZXQrpqhCVCWA7yQIxm0OOOBohiDESf0mUSN2uSRA2r3nfmoHkefqOwdDBf27qsPb7YDBrAY107+Vk+NVz+4L/PdgYSe5w98lPqplkCb/9u3traXCEClhJNS4hrEKUgTGjMImrLiJW6LeKg/WCVmZwrlaM7bNyFBvta29N+Rqu1//uUZPEAAtM80mHpFLxORLm9PSNmjB0ZN0yYsZEqjKd08w1kNnGMyQ8dV9SnnVUUfjV60Im+W8Ca//19fLcz2A+b/TTOElS0nHESQTEmk6AGkVgolFVorncdarRWml0YEgngZYbXLI4OXKFU6tSz2g0KD6XkhqMkQq7eGnzOIUbtHc1S0422taLGCQeTAf61W//8TN3XI7S7Xq2P2EwwDx4AG0DlNCJz/+pFgetgFJop0GOBgHrQ8sKEaYsgJRGBlJAChtyIuTVWOSQZkUM09NqXazWV6pIkE0CmBA6kTKrai3tU+xdCS2xrPHpFIed0noEJ1QAAICAAAAC8SYCxUJBiQBgreRWha+1hQVFAWBZknXQIz8QbNk7wKCI6zEfpKcVQQslImW6wQPlrDMeFYItpP8pcwT5wgRkVPcSHtVe1JSgoXqRc81DqwZi3//19YLUoBFIpLBTApxsFqTtLCTDpFcFtH2gT2MbBzTMEZ83l3bgmCImebZnmSB4Qj5lJ//uUZPSCAwZHT+nmG2xPBGm8PEOLTGjvP6wxCzEkjyZ09Ii45ijOd+2GC4oWXq7dTbVCnzCSMldHpSrZ4RclRxr2JIKbtGMi1EU1BOsW/+R1AKWNtJNIkFUhCFI0mp2HYWIZo4Q0iYEs0YHysyFyuTKKHyWgOzzhO1JAuliFs6iZq9lWtd2vNunGIWrHTu9lRgj90d1apHRZJX9du9JGcUzylZ1anI27MEFxAk//+RkiU4o2EkkSScUg7jQJqbhBDuJDHEIblZELyXymhNpdj+1xZPO4mpJ61coLw4phHCtWyKWQS9SylPufWn/keRnD/hgMoMEYUnI0HeH75V4paWHLQMDAo//+hQEo3EUSSQAYEnHGQ07BhASJNhCCClUYJLyfJxjRRxKdfUjUr/wgIis5ebql6HdLMlESSt6cB8jlLJQ/+h4THgMF35/ZsZ+ZV5O+GyCjlPhwbpDI5rag6BwL/8v6QhPZozVy2E4JQ3GREYM0fxgLiAzdeVAaiIvo//uURPQAAuMqSmsJGmBcx1maPQWMi+0dP6ewS/FgG2g09I2m+7+6fw6ZLCZ41QTegkgiGFwozItW0VJe/sprte8lKnktFoXbW8QQVFH1EpIlJ1MNFdK8qjYpAjX4ovGUrmU1E0EDdonK3C+fQHCeAYo/Yph9LmNY/787n378hX0X+0pUKJD0E1tlJ5bedjaLXR2UloQyAyzVpdaGZ9HkBGT//991IC8gAEkFQOSZCFmU+0oqIxHcDgACQLI40pNWnToHD7JjlahkPupd8zwUAyoHbCBS46QOKeDtRDm9LNi7VtfLLLvYMc9UklKpz0sD5tVJJNxgFJEgmHjbE3YSKBz38Zy8rJIjB7mu5IJ5/GgwLDy/pUShRgAT+wctwF7jg8nBpD73R0ciKvDu7DbT2ujolFHafRH321X7955wqoWF0ocKvY6N//auoQZ5iYQCs1jbgUi+bccfxfSEmWIoVBisJpoGGPF3Oc+ehNhKpA9WvPVHHWlRNZFyh62BY1Lu//uUZOmAAuI1zunsHCxCgbnMMewFCzTPM0wwS9EvimVphhigWpuny7a1GHqOi1DGPW1UtQn1jhRAQqYHMPw4QxQIQ3BVnWWwhZcxXDkIQnKq8/2gv61libQgMtk71GPJTOlDJL3Cr1eD/i0E+lHrt103ag2lSOrv0Z2fv77JQ9Xfd7OrKEUZioo+owq+c/uegCHQwAU2kpeGecg/hOxmvYJ4nsKWhK88eaP9IucGd4I1Bgy7Tyz2hTtNop8VKdhywsonrZkkvWtP/9UWt+/5EkbXN9b8qx7fkRplv6kOSOSJttplKrTn0t4GTLh1IZqaOQWaP/b5gzqtNBgYjKBAM8UZNpU+m5EVEAmAMzv7AEDtubgr7C4R+bEdNrTpE3M8v4KpmopZQxwUQMczL4sh/kb/mNAphJoSi5gsyoa////SD0WlNVYqZSYoYaymNMvx0BnGeefisJEliYEEIMkowCE+Kjo7RzmlbuOcgECk6xQoHDWsKEg7KnTtSkanZqhI//uUZPULAsQ8T+sDFFxLQ0n/PSNhCtD/PweksXkyI2Z08YoohRVQw2S0lbiBEBJAAwAkkkpUeACIyMkHAuwIA2DKUwwSjl4r6ObjKjhxs9a4kbVCJEPAUIXUuwLiKw+X+NMwNwTg7WMTikldoFcVRYx+k8ofkdtiC8bdjCBbcza/jR82eE9Wz+CjBqnmQGeqChAufWKg07/4hfUoPC73KUFG1o1G40imJJHyjENS6+ASxMG8jccDEvfnA1drdhFkH2tJoa5DEKGFd1Q3vbVm2ZV1Lfya5K6/ZW/q33Z6a9ue3u5i2d/8emUVAAwAAFAF8yWz9MCKFURJscIZmlUraMFj5DgoIGRQY6Uac6VgoJ9HxpVcUAB2T5ESoiAgJBQJzXgb9NO17cdp3Vn/+SKJQxZK+UbflQgdypLdQ6JXLc1UeY9yUdIM7UZ30JS99nTRqXBNu/9vJChjYhYBEgAFEBLncBNmgTMnRuSugbCjC3SLGxp9idU5Lt6BhSdHJytv//uUZP+AAx86UmsJHCxKYpm5PyZwjiT5LUykcVEjpul09hSuMUWbLoukMv3MjTVBDCgoeFGoF5tOtTUsudJKkZYkwJ2wyZ6Fo8qmh3pJZpGqq8AyF3K7QCJfR9ltC0OMjxZY+T+QIKC7yNg41grsMxyo9mPQJfrWxX21/dW/rzytIMp+V2EZ54iO0oOUl5EqNDdkWyui1popVoqDjjMg7a9fW6tlHXtmHHJ5A/+mjTWKUMEdZX3eKcn57JMAiORV0vM/esOphScA7ZLjKmyQ3SSisw1C02FKc+n8/TcjNer1Tt/6BY0zINdXPBahAAYqRDSSKc5IAOIGPcpE5rzbPlAyKzNeGQTIDAaZSNiXV2k93cUQwOigL4Z1YjyFEDIxkna1jTXkNF7+245oFIips4zWJFqFWwQLGGGkPSboLqUtzv/oBW6UpJFN8YDKHo+Mi5FyPE0VwXXRtHcvDAnFlBdihCibq96mXbl0kR+2d79hhsQY9/2MVf3psCGufTAM//uUZPkAA4dGSdMpFNBOY8laPMOCC1TtNywkrdkTnikw8IoW8dqx8+8z88ytqHf7SrIbyHqxue6ee0y4mDd6P/+GgA420UU2QCYoMw1Z79tdky3EUXlKhmAv8F6zFhNLVEbtHpqY22sOb2aqKZazWXn9hc2a2UQTGrmQWCZL0tMH8BLfL/zvK8PNEnDt/hCR9gUFiHNKHOkz8co18i8G1n/9evM6wSiYkUUmknODRMFRHL4qavwux2XUaM5/w65lZhsRTNMoBHOQK/cO1X67RU1utrY5lSAqjUEVUSQoMFK+bZquX98admZqNgRSieWTLL+lPm4P0rsZEWR3yal9hgiZ7/lFAAgAAAAKcwVUDBw4O2ycK20IFfq2LXVFZG5QIqAYf6Y8Tg65UVMiWyieNJlsjhaODsHT0NeVmvUOz/bpF+apmu0UhS4N8i1o3Duf32zp8zlPJxwAihgZAtcVV/9f5cDg2EsBy3gjhLA9F3OdmhQnziLMwiGSySn6Cz+G//uURPeAArgozGsJEtBaSOm6PSNqjGEbPawwa/F9I+c1hI2qAcCXx6I2iXO/0W4RUDswYs4OFmq/4tLamulrtNDCtwcQdHGkB1AcQCQAVSJGAxZogAkdMAJLsBAeCX2QmsHvNqs+MqiobLVhZPzwKYwqsLZWTCdQIx8DF3u3azVuPKRv3x9pzoMUw7S9zWRk50jqr3Ki2LZbOJ+g6p6o5XQPDdq9GvbQrKjIJ///9YA2BAIDAKIFc0gPxeWBlHKVCSV5+oUeTLEZaTVa3lxDnQgCbbB4oUcMEsnaZNCM+k4qC5kOpexrmly0Q21IUsnYnrj3rQWCSL2uciVZHiFWBJRRKVNAwmyaiODTETFK5aqV+WaQDEJa29aJPOufWMJj8/IA7llornSzZSLkAWe9O132Ylq9s4gQttkzk9DMyZ2r0DCyJq0sah58QFdqh+zmSFHWknm+vdqgh1BJALzghDlBcPwOz/1GunQpLBSKJSoZgQYhwBCbmqTFO3OykCpA//uUZOuAAvI7ylNMGuBCo0mMPYJWDKkjKO0wTdE3DeUk9I5AL/WY1xk8fk9q7RAixiGRy0pxA1BCn742//s9s+4yC/PwOCfnfF/P9RBChVp4TvS8+sXiKR//6NIJMTSACSSLeXItYVihKUVgt5FlyEhABBFIAgOx5JkZCPkKl317icM8yb2zPSBL89db5gIc1UkFnMiPgtWDAi2YRSRDgmQzBzFsCky2pbvt2pdxQcQOddDb0v79FGnIJX//6ciWdTrQSm0CoANEsWg8PqwPyqwjnJsnwKoyd7F8Q5Bt6cgzmTRhBVUHV6VVFs670Q73p///pd76f6UQZSaM9vo0maGI1QVqgJJRSdVhGvolLEZahCudi0rZeylNdl9WGGr6lhwkhqM2Qm4k14jdPfxg3/f+JF+xyWP/+pwnwcY4RAaIZfZn/5+WKqAuN7kb5sbDIYNfT0KWQCZOdv/t2AItzxHmQcE0VZ+JEOC9AVhEK6wpk9cDcnGRjl1uasLNRYaD//uUZO+AA248S9MMG/RNJMmHPSZIjGkrNaewSdEOI6c0xIjlmTkG2UOw+21rGtYHQJKjjgRzKAJ7q2NQq7OWpULZIagWArh4YSMCblkiKbaRLokRrH4EPHiIaLckhjSGWLi82YpzNiISMZTv1IYAFZ7R7xlkm3Fd9/iWBF5oG3JrNzS2rHR759IWbsg0NxoCLKIg0DaDZ4KHzJE3vV0kQ8x6+XDif9SQBuAAAJKeGaOSfwByoaZtTLkGfDqfB8EpIRx3uyec74/khEFvkmqW1q6qa5IGNYHJ+soADq8XWMSWA7mEeScw0qmTdQIhqgECDEIWHFrJD3N/6yUko2SUkSCYhwXQmiyLEIEN0XxDTsT6pKAWtqmZ19KNzu+ATgcgQr0blJngaG8rS4/X20r7D8mcMJnS/Cyuyd8YMjEmfKocLIeAuJ3rWdQAnuHYoAXG/yYSd/UCAQYgFXrwmQno308YWXDWN4TaXaIY0N/l47biSw7ln2XyMoeMli2vYfHw//uUZOyAAs85zNMJG8RJYqmsPYMfC6idRaeYcXFKC6VphiDoudcDBENIsoDeXG7lArQLy7x6g1Ud3SjEZm1GJSSkpJJAmJXBZCrcRbKe7SoQ5tx12num0j7MvnmwV5RoYasBD1Inl90lviJ4Wupz5qyaPEdE2GR774a5AfDFNrggMa2Ejw+eXp2mX8+FQrKS4Gw0zC7/p//6gkTAAIAKPcJ4yiKe4QlpzxuMjEIFKxnMqUXUrXIMGShORVrFSapBUiIcwh3TU96PU5NT6P9Kr6/1ol77tZ79adibxq8z4INmphmRv//eBe/ZJpNOYFKPwUk5S8kgCrawjQkhqtJYW0tDpPFHqKVVVkw4MFkXLBI6xzD+0F+5WZmCFagn8YzIStp1nqiLMzLRyDqMexmI9UODRP6SLVt9dsZGavZK0qCgIkG9P+mQWQuBf/QlEkAwcJcBjoeuieELN1Ej4Mk4lSXVjQh6n3NmUiisRak/51zpHrO9/zccahKOWb6LAwYQ//uUZPEAAtklz+nmHFxE4umMPYMeC2jvQ6wgcXE2oaVphIjwRtZfOwt4kJvFuXUqXq+Vjq6eCv5zyf+/nV/9MmvgxWEFf//7wBlAAIBJfKlRsCCNJCIoHVysK01p7oHogjnC6J64wOYzRImKxKVKQnmUbaMD7/l6T1C/kl/H23qAYskyeLLc1JG5jX3LGKICBKYXaYUeawBpGF+YHC8hX/1BVUg0zeEEkfQhTI0TXNLUousHRkZg1lu8vfaifyOAFIJgo7nEAd8leFFpHaOJ6sWghqmmf6zhzhnELuWsP6ns7rT2TtuVLIt9as1kjMKEMNCWPkITNva5FdUFuu1tJpJJLE/RhFqYeguAm59IUUSrSYpYoTPCoSEy9ft6yCbsIYo6KqTYnuCwIA1sJMy8pKWOLoO6yy2YNM4hfM953/5IDAA8xYGM0YGRiCq6GYGwgeTCOIBkJzjVg4XUC1n/7U+8AY1h1RQTjbLgWllYsI4IIRYOwID4IIhBFQLXvPPz//uURPqAAxJHTlHmFTRbCPnaPMOJiyR5K0ywyUFjHialgwnqZLxVsMuBE4LGWOCcBht71nkD0zqVjUJTPWZq1tVaWCha97Kb2AUAAAA4T9GsBqImBEhwcaWgIIwRK5rLW3AZ4wGWQw8EWlMghl/qixi8jKnSsORC6Cvx41X05OQ60tFnGYyUiowq9DBMlWDYZJmDUFoZXZQkLgA5k56Fyj/OXUm1IpyECh+ukyFzHiNppLCp03ot/qWjTsNgAARIAkVsJRL5DzkzlBodhLvOm6REsMgygMCZclDYiZ+EUzlC8dcEJGMBHaVCJDSdMs1uYJufhfpzLw1nDM53zf76Vfp8/fQOifYdu0LHfPJIWA023IiW0SCqW9pQa0CCQSKwEiVeqzLW4vV1IAikijDTmDNrKIQWBeBGGT2b+Dt0PPOqbekyVpPOXWRJM45Q3SsjWNf0mZHdaT62ZfS7bDH9bqdh6tper0XnILOOERW7L/zmE9VCAABGwngQ0n6HHGaK//uUZPCAAzY+0ensG0xEQcnvMMJlDkzzIm0kdoFKkSVxhI0pGFOMEcHRskMMxZxvO4mpQRE0I/DiV0EEOyVZEZ6dl3tVflmVT17f7dSnTqreztfV0FxTTnpZQJopOEFQNEuJQlaJkLiT1DjRLsDOMhQrKELaIVoJnwR+oxDSY4z6xNOYuU1EfJEyFBPs5lePfLnfbpVf33RW8Xe7ut6T2aRa2BpZ0RvkFRjt22U1mdPcgz34vCBUf0ggHOUAJlNzhEGAaaeOqe+XtBMkIVWFAUcqqSoqQpM1UpRtoGUOLiYarFaOLe6lWhl8g/Gi5kzW9WedLCt4mckMMLlSIYUGrpHqrwQie1YRm91K1RdbRel3F2xx/nbpoz0qjVJ/KGvDrGBGjrbIR8usH2K1nVzTN56mTW/WYVHTzz+zTq2umzVmkqqUo4Tf9zK+ubpEoDZuJl0ipApQ9t6QgCX1nCa/ljAgBklpuSGo5h1WIJEM4DRVOr/SZKL9yc5LXCMPrB0q//uUZOaGAyVIUGsJLExEB3lWPSI+C6ExQaekT7EThmb09IlQ9JVDzUgXSk0oPWISfo6KVXoQP/vY5pLdEQc1KKKjshASRRKoJoO8CAZajP86jXIIVhIjJLkN5Kmwo08u2FV61Ba82XvzoMbLCkqwAIcflNDQzJ4wPL5kRbTzI/Kn770+8aBIbfAjEZVgMt+qrkENsqa/6xAgJe/93W2ODIKIABNKgI+G7FBmDjo8SOVBAtCvK9V+4kRshwZZ1YwmtA6SJHkpnes5/vz0cGyCxQib3EUYDL6SrAGBSfC0laV6csPEYmxEIJjcRASRQKg7ThGqY5qluOspxQAFBZnQniSIEozeYQwMvM6LWlfpzHrHTq+wu4ZUMdwbg9UIP+3CgZoJa+1qTiAOCMKZZEocvJJZ//nmaZdWnyZsRP4zdIzFg0hO4cf//9yDlssZUkbSYVqllu49MElBPHGYGcSzMaokt0Yzv++f95/jpGUHPhz+y/bFbnKXm1dLYvoWv69P//uUZO8AAt06zksME3RCIincJYgLC+kZQ6eMVbEfleXo8I6gdsa3K1an9UqiJVqNRfXu22erpqmMgljUkjuAAAAHHkmYrJyNGjoBuxEeQsNaAQhCNGlfDxwUyZgMdo6WQ08Ta449xmTamN6t1iMOsyFp0ptB0jyya2EHgzlIIAJjXAKQqeDppzpkkkoh//LPG+3KqvHgtX1SXvWgujiGqbT57Mdq0Gj8MXHEG3HDgEB40mR/a8TjlTbXxzwAVAAAAAMQCYYobKqEnVx7NAW53JNCTwCCIhPTNYlbCNBEQHrcqeX6UxcHRYo11y5IhQXRFTIOPEUWo/Ygbz9QqKWIqJwcB8iRcXoV/6SkSQDC6G+A6Kg5ykClZ1EQo0zUJ2gDTIIbtbM0eLJDL/qYo3kQUISIlGMBXmGVAAGyCfkVkc41VmWgwdW3Iq/fcpD6CWILlnRBQqlDb1pezVJoMCFG3/6/UEAqvECWlhsUoSARRItSOLx5D8sIoD4lMHGML7sK//uUZPqAAw9LT+nmG8xRiMpdPMJ/z6zpIGzgx8ErCiUxh5hYSWkw7Ng8WG6aviJZFGg1GGJVeNVSLix6LXK5w/1sOHdIaTlbRKaSJdX1Jws1Q15E1qy7lzuEuuXNfkrvyCWzI0NifOE7hFDzZFX6GYu2Lp5A/GQU4IERc3cEzNOEBGQWzC0u5WptZFKrudaSs2utiEIrv7vsvejYkrCAYj7r/+GgAaQBJJJUa2kQFlskThp32ZcBTB+Pko8AXCoETHS6q2MKFDCC3sLWLTGDqDRhhoeADhpgAKihgabSBxYKH9AE6aqS2si1qNbnMiQuC//9lf0VWe+UFEkEwvgGwdg/RzBqRhn8M8W811GTslZxFga0xvuc/hqSKuEgyxLNuLW4I0CILrYvDgss1JcuUlRlcCtwZ3MMh2khGP6lAWIBfT2qvnfInHbD5w0g5tV2ndHqgi6Iqs//J9xN/6QAFAiAAAATAWGl1isyismbV4E1lD670KrLLAWTh47ZClb3//uUZOqAAt0tT1HmHQw+AZm8PYIdC/0hQ6wYT3FDiqXphgziqWYBZD5rXLdZAwe92MzLfTstLtPrO9dGuRW/YnypzLq+XYi9KadPtV6FJCG/85EaGZArfQAogACAU7DEIE1F1tqnQ8SqzdV3rEVjizEZK/d9YC7rSJ6SIf0aKbKeiueXBYWhSAHybvHLptKV/yIvq1vzk+zP/wZ0Ndyll88Mg4aEGeUbcMsh1RI/5ne/8WKss2oV/4w2tKukIQDTclUIQV6tcWVKliQ5GuDV3dD85+krKYWiGQSidkvv3xA51MWWCOqGlGrQTO//Ne5Hwa/J1qUFJuRpJJFAqCyqEiVyBqE1MMtBfs8YkIt5DEmvMhwCAlIUR+gy1ggdHn3Qgs72KU4RNASvytD1bR0zHS6smaODDlCkeoKOhXmTvstEoiJl59WZrep6PDHNFAvEqG8O//6ggBYkCtq2NkInmBwNKvmFN1bZssOxlaLNmfSsSk56YdWyMmV8Id/JJ304//uUZPMAAz5QztHjLdxUKhlNYSJaC5TjOUwYb3EIEylw8AoGLhffQYMSOloLs03om4oZFo+ZaS3jm2uQ9NHnVuqI4uCsDGoq5EJyNppNEkqgxrAVGWuMOpGssHVK8jlLsZ4yuMWvfmX3Ilvny63ZUd5dIIA2bzOcvpB38ji+HIGe9aKlpUu5XLoMiO/l9OAjzhkV/pfzykGwqBw5M4aHS4eZ/36cOgQM2QcF4oUX/RKdu3rADFCQAaaaeDohQQIeSGHk4k7NxlZFCtFjBHYNmYQ3UM3E54cN1LBkA7U5kZ3ZN7L0Iv3yEIlKsrMy7fL/fLTRWPSiG9P7B0UFysFGqIxehQEaQBRKKVCxGRs8ZUsC3NG8hExiH3nireNbfkEAWTNPD6+pnjpdmVMj56VUxL/d/TEX61nsjzQJaxzOnuczXLOmQYTteVIvcxqz6rGkoqIAvWeUwdFEso//Qvc6wnKMMz5sDqosICXw0yaGwhTC3l+OINM4o0Q3p2eNFkeb//uUZPOAAwRIUGnpE7xPBAlsYSNoDVktQawwdLE9JiY08ZV4ZM8LI7OlsEyjnikVQsm1LqmqN4KHSasq0Qrur/CYo8XhZY4osyTLPlkoJb+kpokExlTETXFoKlsA4vTBS1n+YtTy9wH8hUapozRR/U1waNbQH52ckU2hyhUUQYKmGSYohDouajuZkY5v2rHx3DqRXbvS8/D3JLWoStXvpaUHmldUZ3u9nVK2YcS//+5wAJEFJpJUM+XoYwyZnErmGag6ToD87Jw2LxogYXg1qVsXrDK0vAd3MYacQ+8xELcS6U2tRbkZXMR6L9jNJLTP92de//2r2t+Z7IihXxUsRVGboCKBBMVCkqDBLzbaHFztsqZ84E+hjLmtxhxn7BFuz1u+ndFIv+945vR540ZEVlZ1Hku6PkY3a/nMyHMJG8iZnj5LJsqp2mVRwduru6PfSgtp1n/9QAsiAEEpQVXLEzE0GB0r0PyjGwGNiYZbKM3qZhLKDbRhzg9olkm2PErh//uUZOyAAvMpTFMJG2RIolnMPCaDDFUdPUwgs7E7pWXphIj5cVxyLzA+VCoafvCiOHyqTdKWRRMKCQ/DLwHShQHIJCjjsbJbaRSpGCQkKK0vSmKEQoqdlwmVRwG/K9N4e6Yeamvd1dHGNvO2hXreezLMenUxA9yB2HTocGtKlnxMHfFy9/P0fiGC+N5GN9x4+FBgyOFAoLHAoNeg0V//5/oBCAzkL9EawQ8uI8TKmPofsYDs4eSiSjjRWWXTJeTcdL37FNBpBuWxycjB86g+anLMu76SXmUNlv79upl5U7/1fs/hF/z/l4ouPoreLCMRKSON/9LEHtdQRDo0eW2ZDD6OkTNkpDiMg5cQVQpmt3bNoTdCV+JyqWl15ooMGO4weIDwaCahECAO9zeQaBqflS+yrDStPB1C82zs9gU9s7eQh6WepnVkAEKEESRxK/usQsdA3zjamWZzUZbTQS9mJZByfzmobjTaGYsPc7gxCjLj1zT232lznSLa+Z343P7Q//uUZO4AArNFz1MJE8xIYhlqYSY4C9DxRaekbzE3H+TVhg0wS66baK3L6VdzkzO0VqH2vb3L/hwJEdlglIpOVPcQDeVKhW93k62uuQuqAWRNRcV2QwVFQzp8qJQNCF+y2wWbFldrYV/cMUMzwk2TqGFCUEoZjc7S2Qre7ni6CWxYpqoyW1CV6L3VysZmlHa/ydHFAwyfd/9aKoSEkpJOnCBFEiP42dXFzLYX1wnX3GmoRERO77WSn+Qn0pX1uIqFElSYvyrt+rqOYqaERyukY+gaSMExYOdoo28MQeVWX050KHv/q+sBFgAACAVDpCgxo44KODggQAVLRCBQgaA0CAmWpeOvTNTWKhzxUPYVJoK+inE5B8KtZ8SL48H1n1CxisOBe+PCp/4MDeZXisPx+pnTTFY5meLDWc1+NwcYxTWNQ8enzfO//6//WN2195h4l1MHyxhQ9Ng0yC4CDH/tyFabzTAERBQAAOLQQ8CbGeqFae6jqiyhfTN0VbZnulDQ//uUZPaAAtM7UWMPGmxLpUm8PMNpS/UZNUwkTZE1lOZo9Ikyx5WExYP9FkHu/01De+M76+N8zdn9mz1/e5bQa4idKkoox7UuRWdlj+wlY/FEkL2GCpGTGSqDqCGGKOp0Wjsn12wtBDHZ8Q4MNHxSCMICDxQ3BAgMDM3MUFQFl6WDoGGfZmzFpeozJiNQCwZtxs+gEmHZW1t9jbBWqjeGVqOGQxSMMeNK8xWRpJncmfx2JTA7rw9QzQGMDo1oPYWnnGtwzKHXfqnlkB1csBYNlAcO7TTItjQ001VuTVPuhnJZzJxH4h+3UjFmUXfrZVbl+7jl/zdmzbocIxqKU9FO3+TlH+tymUcj2dT5//qRzXKVr/z05nZf+c////////ud7r9/////D96/qN2vYWi5ZjQaCyWy6S0XqGSHSFrzD0OcBAkwTllGF2rxhGi+CMjw5XPtxWB0gx4p/mmrywEGJwEZAVzrPRCyfkwzN2SOwEpFhPdVIYxO8dtiYprbWJCR//uUZPsAA/48SdVp4ARLZIlGp5gAGo1pM/msgAKirOi3MPACY8VKuYrDDj6jUpumu+hMeWZqp5s/MsZDIqkUH17TsbhPLLEd+8t30kWd/abOPivcpHLOv7p+SjCqpXryJAg2nprcCR/9f///////7////YH0VKpt2VEOJJtNttuy27bWQjVHhnnXiEfNcNsRVqOpKMYJBQYBhSuEYQSCQ7SxE0yEUxAZF6DQSMxK61OV7denmEySB08nas7FZfRv6aBK0oPbRe3KezLs556an0zKV2vGxGMXakpprWvoLO//3lhh412skls1a3r7nfpsNWbWd+HrdmU27nNU9Xe7Pfxfn8asJlF38JnU7PWneiFyn7R4a3+NJuk/Wc7nS3vuUlj9v1xj7ACsokAtslOW6y5iDCiMaSLIGjx9DQcCnVLx1CMyGDTiQz9DoHkHCBQBpRCpSJPHqYwaJvNK7EzaxuJuiQPJmM9paKKPpdVQprUNWJjl/tqY+h3FozhvGzS///uUZISABW1L0u5nIASYKTnNzGQADfUrM12WgBD2imcznrAEv5uls/zt6g3Vvf//rLufLOu4WMcNb5vX0mFJT28bdXKr+8d8x+9r8MMu4fz7f8y5zX7vbz3ljyyNFv/1IEJCcgAkpUcDSPdNohf5mzWFrvKtOXK0NKQlHW+ECu70YcT4S0kgkwCkF5AkBGxWHiJwVjJOnSVNFOaIIqdbLZEu2QQWeSQNzZBR2owSUynMTJteu9ett0kGXZfrpdBBfUtWtBSW6X22Vq7K055X8mAC2BFlEQg7KENLecDNGVhfkLe5xIIWjdVcetw9A2WjaSSG7Sd9Ts4OjxWRaAyQnCIO//+n/969X5tHsH0tADGEAApJwyoiggMAeJUjfs4WdA70wUzOESsM0ZO7kPSufdjHB2ark5wDvEgFJvRygSm1wijm0WypfABo4kcCYbuinuxgRhApNxQkCEsDl+VfOZNqwlvbp9N6EHIjYVLf9AABIkCgASaoHJYL1o9MJoxW//uUZDkAAwlKTNMjFTREAimtMexQDMEdL0yM15EICyf89g2EPix7Nx+DMWp8LJlnHvVP4ewaC3rAbp6nA0/Ks0Zrq8miKCIKudqW1OpyGKpjCgCvXrO1AAlAQASU6GNBsEOLxX2iog7I4syxrr9U9Qo2Yon2xa1MP1y5QXc5VPZZbgTfb103YwlIQRyMYD27tbAhAw2LkBHB/V+E1Jr8zhroiv/38J2rnumpnmDOHysx/+VC5gsLf/mWMP20gSAbHKIaaKZcCZfCaIWHW20onTFV7FNch4F4VDIOEICcsRkGFdyjQijizAPETukXc/s9aNb2fX3SEgOvmMUUhO4CVRBRkJQSSVKiUtxAlYqWjsLJe5nSmy5Hxll0SDE4hJXklEpp3LgiCKyinaf2OdLKQsOYWVqSRJta85Zb57BVSg082a/h6pRxvRBZprkIHiJzDbTLNo5r9rG//2/5dUBAAAjXg5QJAADkyEY2Xw4y4tSPi2qmhBnd5adZSdyZtqQq//uUZEAAAukuzVMGFGRDYrmJPMJiC9i9PYw9CfEypmZ09ggxxcKT3Q27JMElAoZIsYdfZZMoTtPC5gkpaLnofeSSQk7SykhE2zMAICIyiyCDJ8PfDCjcFsNYCcZsXIa5fSfKp6yKiS3XByFO42kVL20tW+G1yaiNsztwfB3TikNdAbNKzO72rwVPPFDxEUYaU+MpDKktAUhNjJ8EhqH7OauRewABFUIghJJ0E/AUAXHc0fVCGGh+J1llQHFQOVR5RprglOT1bI7GS70d3yG3wNrytru7XoRK/ptb6a388zL7+l+3as8UxyxYhqOEzvvQAALDUKQSRc4NCHHB+lqpGu8YBYxL1UaHYbQjqLUaoU0SOaRaNFHotdHEylPNSO8kKnv0K28wJshLZNukXc4pBCxgoA9ouRefTc699CnFRxqdbhOwQ1mP/7wYIKlAFUAyFaQIRs6HhuOZOIDvvhKaYrbf3k2Qry4Snm3+thgAHQ6LTouTCMWQ2QXhewKzjNnX//uUZEgAAsYfzGsPMWBDQolmPYYmCiS3MywwbIEcEWVVhgko5gYOQg+8xbv7QPdrAINgVv8C6EK2JoT1B051Y3weh++tYopxeSwzBlVWnkdaOEYRKLcS9vBpnItxZuZwjC/MkMzvP4XU/1VDCkC8GUyYNSxhihUfa6FrWol7mnE1sOJZpQJNKyz7mpWyxNKLt6blVFcCx+wzb3Vkx/VgsVihx5M827sx1IOQfWraznbghR7nLGGp0akojftdaU7ByumXCj8lYsxVOgBQhBIKcBkJfoIECCRah9AtVwnmdnmUkZ6RNigiPGU0rRth8FGKBOCUlRWuJIgMVixVPpFtCimUKZ2v052Zdp74QXLIYnrY1qiwnUPQ6vJzBhf//SCSAgIrYuAMxSBIxT08wvBLFvSGkTwnEX4VYPruZmnmXISlseaBcKKFRZ520z0CmlTEK/1c0hX9OZeAfsWD4MbbYETIbcoCJpTqldMGoT3XbInaVIwufhmgaJCHbKGZsKH3//uUZFuAAqssS9MpGzA+Ayl5PMNGCrCHOawwzMERDGb09gi48YZBuiJd7BNpiWP20jPHvPfBJCJTbO38SesMB4SJR4ECM6BAGBA+69ipQehzlId6U9YFJ9QSCJiUoGWtn0pR/SnCwF0MDQubJpAXttNzSGH9ZqyucOlXqhGNffFXjBqNRM49aJz70qLmnrT7D4+sRTc1zCvlj6oAACAAAEEFUGMApMcDRStUwEOh5aT+xURjRzlwTTgOCjDwotmii5KwQIBS0ReosCq5sGBRtxZxw4yAuMLGoEs9tDZcszCfy+cMHS5hQ6G8GcAen6ENIwm6T629gjtByGRHBNmEz8y0qmtn+g414o45zxtHSB4d47j1VLUjz7mDjYiUPd1/njWF2Pn4ulB/UuT1/qkESSxcACFEgZA82VURsnStFcSyexGV4vaWk4/ElX7Xs2FQSXj7BgqJ1etAMUUz0HjP/6xgBEAEgEIwDpSWmQaR7e8iByoGFuEeS3QLXitQRHZw//uUZHKABLZMSutvXTIv44nIMQKSDHDJLU29DQDJiCl4bCwmzJklV5+GNILKo1SwtuS7VqTpeZ4kKJLVbQbzCZt7wAGMcVaO1d47luCk6dWvlL+k/vqV7kyR8qJHFWAu9AQs18WQDGWGCcGpoZIdhCxAp1mjhIFyeyWdVQLorilhAzMR6Adq5j4DIvi4fDLQl/7B4a6v9X+v/V6aAAHAwI0qF4Cw5fwwzEJxtirnLhnSR4cotYjF6iry5VaiiHxaOyudiDx51AlEQWGn84+orX9yg/i7Dm8uBQe1Jb07vHXfxSHIhpB8DA26p7utvuc7wZBGYPf/OlP4DnIyGgJkAUiwngLwWHnYleU4bTpuDOWnC4VFLatNNyPXwYQLHGd9/dqMWE3P/0f+xyHM9kYJhojbFx7IRSQKeCxAQikiQMGTjXMf/MiyPo8lCy0RiUy/j4aiAkfCAhrSQXoOPrRWJN5j30wE4EZI/Jmlp70fu5MN8a9zVW2SSNQr726HzMt2//uUZHIOAvwqShM7QeI0oWnNMekIC+DjJi0wyxDSByWBh6Qg7X92Kzb/WSBoFT7dtDrFED1N0SGcLYuWKOmULBQ3bQjxwywqy4mr5LpJDUSLUYOnjYMMHwKY6kkLkxTItUM93r7//JUAgQBABg2JkAsQAQCKJkAzBiUCAgaYS5TecU7WfRucPCalNDh9xcqYWQhYgrI17zC8/o9VNL+M2g7CzXYtKQ8E+qXpMpdchvU6k5ppmktnpd6HSZ0zh3jG73KaaDDUAAF4IPeFhEZG8baHrb58kVgP64ew4iWBseNP5yQ3N5pKA///rxQ1YhsM2XskNcXda76qWFqEpNwWyNNtIpcAE4kBNIco7ADdWrJeNyydWUtxWxEoUSJqiINnR5Rg2pSbvJJr58e1/6SOQw9eOIUyz6v7tPoN3rZG3Y5Xan0dX7GdnWriwjmqjSYAz6wB//+UBQQ8bf/BYQmEFDCMykcDx8ZyBGugZe/tMnCDhPUFLD8yu9ekYUq97CNV//uUZIwAAvE7yktME2A2AZmZPGZQCzzxSawkT3DYDGewxIysbnY///+lLciv4v1rAoCAAUuIaDMBGXQcIgjZ4os27GLZDLeZDB7XAwCBgPklxGRSeKmwddr3vVzATyFGe12FMcKjhGbHEmiNQFFEOEyiIvnkigDO3gsw3QoOjmFi8nIWgVH/+7/W8ACANMBAAAQQYiIEJXUyaK4FAULRotHF6QHJqQRsACA0PE6CwTNmg3Iqs8t+3////6krdLX2sIACbgiUyouazCiCgYQdFQanfR/cGGgQqCG7uk7MAljBxBIvDQRFCuRh5RtbeoTG8V1aIW+WILB7PzVIk0p5Z7SXL86ZP+Rn8+vZ0rQYdPiIBsBxJRKVt////UUinJEuuBjzBgOFF934iH9RKE0HWVKlGdqgnxYIeery6BZxVykVJuQ9f///sfPOpVHV+nKqAcYkybjaQcAkiyL6cDIeE8RB1dHPEIrmBjUkFbNh6RgQESskS232jax8QCBIqwFx//uUZKgAAtQYyrspE0A3IXmNYYYkCzjtL0wYbQDQCCkwwwmOAeSG0PTFnLS9zHlwrR9NbU4KHcR3FjaUOMu8VSkqOFuN3zDAtiFjeJUZauchQCwKo37sT46CJMEKwMY6xqfMla6WWi3POxHEBkk0quM/zale2f6e13Vd6d3q//+u+8pH2m8vvrdg311gaexCUmpQI03g/z7OkaY8w5iqJokPLTAfSRAEL5ZNTk1LqiToFwbbGW+y56/H+qgg/7QbHJqXPQRL4qr+uI/7v/X7uPad/O9+b6q3oQsEaLutSSNpsHEVJnl/ofI8jQRRznQ5PTRYwKgx59/TkoPNQ6Yq+0cr1hC0jb6ZGJKDdM9nsIjIylLCDBCHEmBQg9TnmasVLnFJ45amRBUgSEJUWgXUAOlQBmHAJ+PovBBTFHrRCkWBfxPnReIhxlYE+LahzG1Bik4UnsoSODQokbpnQcDgBgtxFvnlmf6QSZnKGOUypKXl3P8oRfy4UEwWQMJuNrep//uURMYAAmgVzunsMOhLaOosPMJtibRFM0exJok1lOe08w2cawOp3pGoEjAASABAaY1waxI00jyqtJwMzBToZyW0cdrTNepTUPA64QkDRqgPDhONi1/336khyjt//5e4rcIAiQgNAhXE3gMMXkXMorFlsL8Vvasy8GpIWnDZSUnqSyHEvYOymCTZyy466cXJx2XgJzNevqLe1aQQSRqRnG+0gj4oWMxQ6dmRWuRq38pfnmvn0GcAZkydxx0uGCny4Ak1ettckjaDCVFlE9RCgWj5a+yfVhQ426WEQEeH45cP9pDK6EDNsexXHJH6RbuGaek3O67P/7BTvQAGAAALABkLhgQ8iFbuHFRVL0AEmAeGKdwdVic+bMLv2ODuFtkdbAxANYz5FvWHyxwhtfMskyDIXZITrV5S4Uv3NPPOltlzkX/+IabApVLTzgwePhx7/dLAEbGRk0DK4oCgCNEiCi24NBLtEUJVOohueRFWo3CZTsB+J+GpVSSWBVkamHD6//uUZNoAAus6TEnsG0Q5IdmKPYYkC+DxKsywa0DhC2p09Ize5U99lFBMg7uLMIzHoEDFCI9iZ+3+tAwO0rQWTZPa98p5Vc+kEmWCCti8+1NhsBSq1sOOJJ0EABgwCWn8KcdgyAhIdI6k4WD0HC8PUJdyZS027DgTBjDSI4j4EczM4UonycyKoe6labdy+XpdIsqULZj2JPXbRS/9vtWLw4h/+R53sjqPABLw2REyhQQ6gKOjxdQhSpqwFhzXIMl1Z+aCXSZ0pDTR+dkj9SxeUpDa2IFiLab+NY23nd60zfvSjHSs2p7GY6DF5gjEklBykFTeS13kWuAT58lMd0fXV/nOkQAVgKSSKnBrB1oLmEORbV8zmJMGf4QA2Li4qMh8mIuwilvnE8I9w+lc4CGi4hWHPQUDFvIELOKGKT9z8EdudB5uff61yBZF/2zb/hlfSbdL/WQ+l1oxnwF//yXWAgBsENZpjE1ohAV+tV52RMYV076otlhSVyicQuPJHklW//uURPEAwsM8SkssGeBbJ4khZeNMCs0hOaewaSFrlOSFhI6TyYEvOLqU1xz6/Kt24Kg0MFc6CLboJePDpopD0FQW5x637Idm05cjpH645ykVDmHEFFACkZblAAhAAAbCxYIoC5vInIYQqCsJT6RvOlRIxmamZtZpG2JKtw1cstTCUDMgB4VybhSAoQU6jU+g3ilpPapEWwpDLfpywp879Xfmxptg3t0xo6+PUVQPg7+KiAWAognwJcS9SqLRoojI2sI8o0xjVZmpCnFXSMcCseVXODA9KKeOyP56tzJeJHYE8idGMYiLpyEZ/C3PzBFM5qcu5GRZiaj7aqTL3nOkkWyy1fn/CLO0PQAAYIAiEEXQ/QKoLMSXFQSSKNqpvOAIIQSAQHz5k+miK46KtZwRxpJpSSxxtc8u1qCAAAGIFEBBR4KKMRnO3/mu1pntnwGh5xBQJeHyCRAaVUUGteWIoLMDkqiUnE2qpRQRmEgJesleqchECterNaisla4/hxg0//uUROyAgt9Iy1MJGmBXZ1khZYNcCsRdKSw8SUlhmGUZh40xZQLoeyVvuDNyD2q41uR325fes/DDCE4SGJFruQKccWYKarjflx/78HGUWdB5eu/3HSzI/w326XuT1f2BI8iJSLcCT6GDXAQSOphNIW8wK8E5cUgnWA6St1WtqGZxoqIMIjyuqtSzCca/TBICsXEMPtKItHqrmoGat60v66d+HrCi0O3669HZfzrcY5R2v8oa/grsgS9drFI62nAKwIBEkBA+CAPz0BoIjoeh3CoMGl1OVQOWqSh+2l2Zza2FwVgtX/8ZJBkyKW7KYpiUl4K1Mxy3sObIubSGce6982fq10rlmYVggotOqgCRwJEBKfL8NAUDA51woJlFV0KBJhgZKB5FF4SYGiRkhkRNO5CHB/oSdrdftY1+z9/vKQ2GB9RMkkdYbiDNCtWeNs3Me3couqGSWegJwiFw25Tj6nk9SYWUx1aBEg9s/+umuBBSWEJGAgoLrJRrVaaDI9ZM//uUROmAAtMuy+sJGmBXw9oMYSZpywSLL0wxCUlPk6e0xIn0hkJxORL3hrKRkwvjOE+Pe+jaqkmn4pY//k8Fmomk7Q00Z/ebOubtXKs75e1blP1oP1kBgIAAAAFQbOQ5nMCqJDNoL7KVOW8qzG3UzxgpDKi0rDtc1JMJ8lKpZClGsVXdVHlX4YsnqXZZamKMgkkPII1tX6OZTDAg+oNy6rFkhlBWVzYHEpBDLS6qxzTIf3GKmhuElF1GK3Wf/6wDCSCCACQDAG2XQJYOQfRpn+APQpDgJRapMrKWAKD0ygACnwZJ/J7XfLwcDFGXM5DFYFWHwwJ3wCOqlH1pT9dibezesHkoXicOHiJKAIAAAAAB804AkDuUxnKFGY4RpbSY7lMFmHfcRg0JgQjs6HwkCcg8vKpYP1xrZ6NzqUiaS5Npm0u1qOQpBYHAvBJKue0DO6FKmddC1FxPdd3HGtSs5ShgVOBsqECSTaSRdOY8JFGU19QY9sZqWwFylYQmAJJw//uUZOgAAxQpS1MJWmBE4+m9YYgdDOzBJuywbcEwEGW09gzQI0GVonopaQIQPsBkIrvehUUzoyAyjw9GaWCe+tRJQ0I7Oh6vuEc5xUcsjklKnWD3yl7bPiwpTd2KJm13oPrMGlLYAEEEq8OMA6xOWrQEJUTU1jUq6obacymGKRAhFB0hGj5wpCWm2vic8jiUEvvzbvdq5mCMIIY7zsutdEW12BEACHYtswk33tRspmbWrbL67qtP+6u7vUfhHiYIqqw1YJRoqKLhQHQAmknL+azUZ6MJ9aPFc4Rf1w9OYTL6TwiBRfhtqXiZqPms2fWUHBKXc87zgOip97Fi5HXXtXq2Kt2NdpRacj0rAEoAkAAkwDWM830AXScIYZPOdLNsBXe6LXXDQbVzEGhHb5IBMUpFR+uP1tALiyT2h88cGlauNvW9KtpdL4/AID5Zllq7pRttH3T6WOMJGCMOuP0qeNzxjw4hj8ypOijuzZYY7o3dVvjOKqKvnm/uK6420uZH//uUZOiIA2owSdMMQ2BJA2mdPMJyC8VFL0ykTYEekWXk8w3o4gB6ndpzqYZriRJKSWBTARR7GOfitQ43lcaKGvmJvJQXJnWUCxMp03qFh1raWMVXZkXMo1GeUDhH/q6TinYu/ZTSjXzpYwkXiJgvpELSVL2ju8D9m//ntBF3NEtAADDAAiibkqNU3ByAGAQw0zvEyA0HM4FI8r6rTghrh/dKnnHLEqRFGzFaksxURxor7MLTTYNTIInYMtlHGrdDa48fCVY4eDoei0mtd3dccS3xdo7cNWIl+15U6i98shsVebkx7iKLLf9wAcoWkVCloFYmbARLq8tBcdDwfHwinqxezdpDjLpM/ElaLKzPXRklRHREFjCx8531rK9Lc+QUVSUqlFtKLr6vTQn+wjRVAgARhMADimNIgAKbEUDR40RWHcF0G4PA5MbgibhvcesNsnkyiasxOPRDOLrJpJZSZKaGbyp9Mod8Lk0AVBUqna/nLIYiUsDjWC2NlAuZlusO//uUZOiAA+BQSlMMQ9RRJUmqPSJ2jbDzO0exDfESFCWhhgh4bIuBOxjDZ/VwYkd/4/YR9H9kqnOWrx1wAEsAYFPAMAfp8gMywampNFat1QzTDxW2WzSO0VgwRLKZkW77qbfBZXms8z6mMxN2vUlmbzv/BE1vLndn+oFMiMibFqRVYoEGwsTMGNWRWbomEuSNLufalb11HxQUHIeEgVLDywcG5MCwxNFrzt08cC3TyZYZfL7hcEcvGuM5vSso1IOfLtlHQh+XuZe5WSX6+fJqCqxyqmOUvzWU1ATJo0b5Ivexr6XGMNf5JJh5RFK1FAwS5GEWIvvpAcEQrRZyM8vpiJxWMPfk4XkmffPfiZ5crn/YFdQNIceudyR9tiSfu01G4TtsIUfn0AAAADwrwRFlVJR8WGMohOVQxabLEcXDcN9Zy/L5W1bjH3Da7GaVMqmHJLRlJDUavUo2xSejDUWQd48Y7b4Xlw2Lf4tTVUc6XkVne/TWlkRl8mj/wwfDLk////uUZNaKA0YzSatLHTI9hhl4PYIeDcjxIg0w0Uj9kOcw9Iyk8i/1vkxzd9jfCPfodKAAJAAFnBzgbQNYTINSXM/hhIF3uPz/fRFXbiKLrlWL6SnnxTImXeyBBIwhv7Ojv03t9Pcx/6fl1vK6pf9fXT6Pc8zVT8PbtQrLMAYNyRFk6PDBF5g0EuppK54nA9yKuh6RyOEo4AeOowJLm1ZsSX3k8d1cPc+f1yjX3bZgVgiYLizG8f5Sdovoo9oQU9MMg3C1CsQnDcBstr3Rywf742ZsbjY5N2y5D9gps5+9w//BTNgEZQIkGCMMECCzAkJRubj2bkLPHVwtp17GLfqMFc8xIYT90GGO8ziKGijK7io2DSzoZqcnpZRdpu3bmapz/tqqAJYBAQIKoy/RMKNCwQ6alLSlII4uJBSTQXNBMlaCRlEs1JnDReaRhFD5m7OcfqW78SZdIIHeXRElEUoS8zsJs7FpczJVVf/JeqkM7Pbev7PVP2aqnsHBKC155f2B//uUZNqOAzZRSZspHMRHabl5PGJ4TTzzJA0wbcj9DeWg9gxoG5MCE2G2II7OwBqBnOopch8kMLlHW1GrqHCnzyax1OQuTbug4MjzMSDTPoRrM8PnuVLrM9xj/+hFCLI5SEgQAIyoCJADngIMRpgYRoLXmlL1XgyeA3lhE7LcGIrOoQREJdAFUZgGmwwTPVgpP0l5TpFkkBeB/vCITznxRA0T9rCWmtbcvrPMc+q+vOsNutRVnWitqt6Ks93xVUsxcTtV1RY+4cRa3iUph2ecrAc9Ftc4BMBDAEY/SRlSgZBoZI2yofJTorJ7mYX+SuecJ+MThAr4VT/k4LHzpxBVO5UscFUuQM8d6Df1W+klYYXWdrTVDSgAgUURqMHAxbnjz0Mk2WdPs/h6D4Mw6OrnBAHEgum6cd9Wq2jwwcZmmUfXwKlsD95caPEt27g+YkUg+9xhg8QJfvky/qee9p5FEB7xNl73qZMjWoBb4+uzN8/8TasZ9jcX6BScLAAASwAt//uUZNyAAuFIy0sJKvI+gzm9PCN0DcE5JqylERkODaZk9Ix40BUDoDcUbWQksCNLcqykyJEeQS9SgwAhsRMkGPYlsiuJ71e5tbGOiCa9XpJffcs5uUN4CZQMBAAAgFBiQ4SAxgExxhKEWPJZqDu25NSq3WGWdxqAlmt3jQFwjA0Wgk0+OIkEnFJpc71ItccchSrNLxWvG2VU0dtaY5BgnVECCBVT8J5/Lth5Zs7prrbdiH6DTswrvNe3my9LzryHxtkO1adS/RvLS2P0A9aEgqw7CcI2ld4Eo+hGijDENJ6BBY8BjgoK5JIf9vdH88k7UjmIYsUBAq1rr9IIDou7wHqcXiiMynqpusW6ZdqdealLSKoEgBFVYgKJ9BIhbokYoNTtect5GKRjcfxwToZdXklSPZiTco8hu6YbkFhMYwbSCjEAww8UEJo+BwrsQVVzSMx3o4gOTI+Xf8gYDEjhRiUsBpDqE2aaaErW03GEyFwATrHRpmXFYI8nxcjnhN6w//uUZOQAAx4sy0sMQnI+QYmMMeYIDjzrJK0w0RkZCiXlh5hYPw8xGC8nC5DwQAYEilFs5TKpPmgTCIhELyzBgTaJBQnaoZdhZ/qDH6Iv/+wbSNRQQAAFAIoHSAB2N1CUEkoCAwZxFHSB2EkV6wW0o451tqDeqSVMKa8OeYFlRwHGlH82gIwvTlgSiEhwLg2zMODA6SyzAPapQFh+l4k5Z6Ju7P1jnKze6QRp7u/e3zKuLeMjdf7UdPHz7u/fN61FTomXeVAm/WwFYWwB8OJcgr2sgvgTPB5aGDOE4PligeDMGP2DgxO1ze+JspNjUETFRoovVc5xlJlMyKX0GEg8KHy5Rt9YrNq126jhgKvW8F/fBwgxihxxgRih6cxhwCrl0UKf7rM3YJCKV7wUIQTGpoGFyB5IogkJm6jPdhuSLejjpr5iGEuzGjLOKqGhGwc4cY2vz8lh3POK6HeHf/vpPkLKigB5lC9RH1x9t7vxvjYJn1jirKKRQB8rBYqCh45J//uUZOSCAucvSqsMGtBAAcmsPegIDhUfJK08x4kxiyXk9hiooOFwXQKODIp0EuhtUlpblFHCQILt2dFXlBWtnMK1gLFhTQ3rr5LyTGKPbyYhVjpDA14QAAmYIdGVgC7hULLVRUMAmtK7cVg4UjwgPh8fEoG4l+bgOQy4rMBCSocB3ZhtC0quKI3eLlFrJ6S2WFk/aiOwndMi/seFMpZpFBZB2YqFOce1HtgseWcnt8+p7lvf300kSMxd5vaWZ9OamQa79+p/pPaNzybnSTsdsn/SwX0nArhY2WFFbgh5B2OaskqsYos66ZQZZHiIl0FZTEqOUhav2fQh30emo+gIUjtU6rrJks0xEDg8qLVZD/+q/9be2jj+xLmbvdBWHxAAEOWBSDZkdSayIAHVT6ha5nVaH60alxOOIjRQQsQjBw4ihegS9eC0F3rnybIw8ky1U7YagjrLeibGjI579Lia5qZj1JGMrTBUQ3TwZdOyRXztdzcffFLw8jyDBURgcl+8//uUZOWAAvc+yytJG1Y8AjntMSNDD4UtIA2wzYlIJ6kw8ZYe82mQUHVgTDAioML4jBoswmkF8Id1Og23EbLg+g7b3wd8GcA+8qCzFDYS6bTZNSQUVsv06PrOzdu3/r/91ejXZXmIpzEeIPRukeEBB1MREL5Q6pw4yVr7Mkp5Cw0Fw2LCoF+uLERv2TLr0Jh0hWpi12e5wgdEvScOIEsd9GJxzp7X5wzh44szI2Wxd0zPJUuX8PM43lM1/n+ud70sU4jBWGggYIAQCI4zcJAnAaAFVnZ4ZBAMcTD02M+WgW58yhPDY0cyfRIIU2svQ9KTXsr6VarEf9ePU80ac3fQAAGDtAuYpqRAMgUq0mWRsC2UB6LDxIMWBcuPTH4atFiYcqlstYpZDCFQSGCYro61wpA3hQ7KehxzG++hUyzy/t7Php3vPS9lmc/MFuyY269JjQn87O+TQAIQdjBRSCNFkNyQpQpafEFYZliQoCo2BJoTAUgEJIqeoGBo3SZViJyq//uUZN8GAztKyqsMQtRECQmJPCKKCu0XKswka0Doh2XlhhhQ8lMTWgjO1HQBlgNrMEwRTMlSFQ7Id3CKfUyJKjqfxyTz9DWoyabyM33b87KkKvizr9OfRgEBINYDCwiYrSwxHhLTbqS9pDJkICAUGh1IhFbLKaBYTBIjMRFKb5p4hAbnh8KqDwZHPLMkMky+wFjjo7jbLY3Kf13ReoaiEznP/vJf9vttac/6T3b1dzq5BEWSBJg1bCR4qOyqTnsMlDDGvNzjEANtDAfMBQhGWEgk3HYmW3y7jIUyuDoEsuV3Ih6mPYxREeuEyXP7//0z+d8skbSuj5EXl59XsP/P4/zgPqUyMfzBz+EqAgAAGmmcgGbLDBU1IEQhg4QJHm3Y4yFV7L1RO5xOWHlhXEakTwZCCTjNPct3PEXOcNL5aUMoSzE9e/UsVvSFebazZu6g/r7XNu7kELS9zudohe912/6T6JvsoRyfaF1tDiR96aTD/NoMcjVb7bs0nn/uu3Nr//uURO6Igrs+ShMMGmJdSRkyYSJcSxyNKMwka0loIyUVhI2jRtXaWT/mYQ0ix4Ik8JvQelbEKQk8CUM1y6J+nDzGsj+OhhgINS0gmwdX5C3ZZ/adqNCB7GI9v9yhU1StJA0sYmBBjswQYzh9FkFJmDoDEuk54k3FdjawuHIaXzP0N+mhLNH/l0fXJV4uudVezie0YSTerFbNNF6J3ZJrZttK30sOQyIp0ZirGMFQhOEO4d33x9dXFbsNV1cZ3K+MeuF5jiL4d6mCT3z4Sf0NICz44AMESAaQSGloAwONbzhyqYjKFCxjTxYDYFCgbXrDaiDI9MlffnBYTzYYhAKAMGtI8UeTMfRr03i3VW6//2wEqxLgRHUAaUCQSCVzIMHFCrGswGypCc7DZF2QEnPFqBcImJmCcTlSMM2SCaTcXIlJ0qUEmKyoNJMdrsQHeQwYqubZB6WLctNfS92KsqOssdms2VLPYiN0/010iyQs4CPfZZ+v+oElbSNISBGjdApG//uUZOiLA6k/SCtMHHI9IlpsPYJVjiUXIg0lFMkNC6XxhI0YSOuOcKDFgNBLFAH0RX5mEwQo3dtEznovXs2XsATyCCYt+nb//19Z9KBGfHPHC8sUnG20ioQQFOElBAEaZBRosyA1YDsQ4vhsGnEZV9kQSdrKJmpQcS3Mz/GLTRP01J+6XpZGo3coFIfvj73zS3uATQSnBWkKzMjE3npk5iErT31eoJJ6UoNFR3ZY7anIV+u8TeVYnTDEuSwvbmdx/l9A+uoUY5DmQqPhUoc93Gm0aCXg5MWoEkfIsxkcyMc23/znna8zwvZT6vs33utDvRJyTykNoHBglMGD2WkVCWiUkk5jICwAWC9EmOotyFshKy5qMSdDxgD0E4IIEfBVljV6CgKt/Gix3ZmESuAQizPqvSCJjuTAhFDucK8X/L7T6i0GmX5/oVpyuYt3/5l/KIlWf4Pm3f90g+3iDdXw/FKQX0g0kmON+UsjFzF2JigSxscq86cIb+Pg91GaA3xx//uUZOMAAvpGy9MJE0Q54bm8PYYpC6UtP4eYUXk2JOdw8Ip1rTG/j7bKyAkkGKm7XCmCpAPmhTIRGYc9sQb0+2Y/v1OeIrViiiraQ8VZ17YmUUYcjAjpL2+b+BU+bSsosNmglRxYagRatEgYyalSA5e/cxy/jvM8kSJfdw3szV+1JFvAVy+0ZVwb2+P/JZY3C15Zvru32dylnMPczmV213YuUYfkSQMNf/N3eadd3yCEYMhJmvBIQHkOs/iYoJTrpfPg54hyyhqR81mT1GaMSBkmxENAq2SSBXnz0j2oFERIPS2fajNRqxiwMAyAsrlHma7k6eM3EOcVAAkAkEAldWgzJF2q2tySvSQaa00QbHqSwWBDDDRAVq9qQmRNEm5mCSiEFsvT6RsoZ/f8kn45ktnCKaa6hKTkGLIMRmplAQ/btiLKhlpEdfgocCssLOZ7hPooQTJxHywQg7pfPsgkPU6DIEczswwMHXOrLIuQeiGSNtJhXjdJkuB4ObLp+2Ms//uUZO8AAy1MT1HjNXRFovncPCKTDGkHQyyYUfkfBqak/BnAYOi+JBGWaJPA1h+Z/LrkuQ3Gh8h9SZnWP+4siaIHQ8MeFFVtbCiOF1gRfsWv7A79NJUPgKUYwmTfxwQAgAwIGiKLzchETF2MMvlDLQnEW+7zWgUmkMNO0qSqDIYqS8oTvOtZeNVHkTjDJx2PwXjh2JagB4br2Go5XiCSRPalkN7GkhQUKkrWvw/Nlzmb1HKaJSe6ZDN92ryKZ//l59fN85+3+xbY5f/bs2eF6UoETgJRawHaPw5aJp0CUYPh9ctefl73Gg+B2CbGHFrEq3Jbep82Uc+UF7R7lh65DkURdQ8Dura6qvUltwVnHvpiySwAgmgJAgiQBHiKDIBArehQo0B+wwvqHa0NRvF02uYytncO2BDWuE2n0fr4nPbr4TTczGkH1zB65gqLy76OigymacXRqhKMW7MzizshL2LIZS3MzK54yhVc71bd32/1Loh47J9uwuASAAQc4Qxh//uUZPGIA5dQS1MJHFRHRCn9PYMrDz1BJA0w0wEHhSZk8rCASiHaR6URJVnQ4pdMBCc1RSmiakEyFSdO8rr0oidyiYyfMOsmVHWCGixau96P6v+S9pn55qwBZcafGPBKmTHR+RGC55L9lTXW5wAxh0G4RxlLzpPL2dnM5HBgI0OSfEuEpChSNPMvPLFcOVXO0x5t24ch8fHTVLL1vJUu+trhdhfrENK2mjcL6VE2uafv9vEbymYm0ETgMFbXjnhuUnRerKqAQkAVmsIyJKaouQx4bO0N+jFiUUJCy36NGxGS8r9/OwcUklkNanwoVD789FK50odh2xrSfPua4sAi1vsWNotbrF1nGpTJVxAAAaYcOzEhBIUIQkwRLhQ5dq/ikzkN1lD9NZp4elNLGWQtThiLguIk6B3D0RTkRyU4Y0mbXmnR95PtUISjZOsJ7YTlEgbzb+xqSU4u2uk/O71P7Gj4wOK4ICaHy+7//7rP1/UHKCEQSkklBDMcnK8ABDcI//uUZOSOAy1KSYsJLMI4hEllPMJWDgTbIg0w00Eak2Yk8xVgIDZO4T5xpRALF4CBFBB7M5dQuTryVsEaXbqMDVLrk012v3oT3prSqrXtp0/p2In/5btQx6GfGEancZpgAAIFBFgqWVJj6CqVFUaPNWlrWlbnDh5uEfk8HVa8Gwc7sC8cDQSKyRMlCWZh8XIEnzmwKlUCGCo2EAIETZpOLmo/TPhdNIHvXZHtZJQNxKLVBuJfJn2YPrAqhAFulUlzf3zz6X8RBgimQ/ixacBsHmlagjBVj9Hgh5fkaR0J0HoiOhl09IUYZIu1ruQIEAlDJwBoNqbKke6zTYMi+gZ1U968WLF1WUHEiZoXIGTLVQCBnp0UDIZjuFBbDE0Fou2vtejKIGcx0C2c2Rh+rzSy46gpgdKq4qp/+Y6vOO5Tat425l//8deOZza30TCcUECYiu9AGSGVROqwUrmMGkRLtOxBj0KESEnc98WegBohgAgoIBII74bAoabRGygYxli4//uUZOgGAx4pyStYSPJHCZmdPGJcDlEjIi0kdQkECCbw9Iys4NczhMZzrxtY4VwDysLZYVtoYZjCeUjQMAQykJ7cuCUuxSY4WByJELDC2WTX6+5Sv/QiwMFBZCZwXAICvIcGeoKkxIVNdAapowzJRiXgyFZ4UpQiucjYfF+ExePKgejNhYWft8EMEcGs3O6OHW8jmKB0IyEyZGjWFcQ2x93SFUzGwqt6Sz0Y2pSUkIiyO0+zXsGI6VZ4xlXzCilHKD6CVBw0YJCR1P1A7dK7EipalrtjjaKII74pjnf7rCMgxUOUMqLhI4QHkCNB7P2/vIkR2m4bW3YSyWbyM3/X/7q3Zfct6aV1s/hl6f/s/d2AzkIBEBxl1aoBmcHyTdNBgcZRVeb2sOlrLmkOpG2nQHVuSiCLNV65JmHBAfCHQzDb5kNtyfL3Ks52eWLHGHOtyPtK2Vk4Ps70q9K97f6s2z/8zt2orBjCXrJJxWbVuNpIAjKHBaySqBZ1FPZTQJHg//uUZOaHAzpIyYssG3JDg4mMYYNEDmUtJwyxC4kdI+l09Al2yGlLtikq+TlHcJY1QGBXVUPjBx+T6OhK7+F330fs//8Gis85sW1sAGoARFcNSAohaKuUZlHB4SX6iabi0mvvYjIy+Vtmkca7EqBdE9Rkk6CTQ4IWRFoiUCJTHkwnlvPO+aWBjDi0lqTNJkjqOxxy9ZGY46V3oWY+yOm6u7nNdZFNSWe3a6ldrO0qRG4ihYbDcq7opY7n2j+M4IW3+WzbjSghKYcuj59cE5wY0aJ6Efo4q+7//Ov0afmj088IlgWRS8vPmERIr8ocyA+Vh8p69+XyfX5f+CGBTQ9UWt//ygcAECxjG6Gj4fLcsXYam42B0VztGa7JmWMufV/IXGJfTZPTSO9nTOthhwGh5JuEREPDSjotCHSZyjVScKtJIMZEv8+zlFYcZHqHZsd+cpyVKZ2FxFLLXnydf6X/+CeuXkB4fK4skmTZLbG/qVirel1ODupWv/BDXl1KaNpJ//uUZOGAAotGTMMGFDQ8orpdPCWDjk09KywY8xkUouf0wIr1ZincCLw0a5zM2Uawdef1G7S0X0pk9td/ZlOZEOj2nXTr//eW3qt8yf1pvbf7YP2JxDsVRqnIAgAAwoQ38xxickteZACt7A3H3A9HL7kAwxhRt2fWKOxF5N2rAN2CJVAJ+ALeSMNaskoCjDLJXRTAYYYIs9RYlA5HJcrccMwz8JISmtU/TfauhkZkin+lBJPKlk9lHSfm5yhx6TgjOW+s1UGy62M1tFEoIh4PE5RcTVUpzjdbicViqThY43fooH9uWY6qCMmeuzjuu0j/r/k37qQUEtV8TY8WWRYHE87UitPUtKsms2VciKOoXYUAABU0oxIcGtHKGhojW6bMYJNCw5fGUo0MSalZTpn60nV1HOiJ0wAwMjaGx5jNOOFspa/qCLmtWdxUlFkbhvV28YPjlXK0n7VF70Xz471uVTfezN/3kqm3P8xyJVvRfTgtMcJqzK6ZwaUqSdtRDaab//uUZOyDAzNKSasmHURMqao8YMJ5jUkpJwyYdMkpkWd09g0UUBY0hY9HoDeVzHQp1lC1FlEKU3EdPgKNe/uc7ke51faLTW3I9GWUyqBr3d7Iv/1etbfuqmb//9SP+8IDyJkuZHZIAYAQIGhTnLFT0Ag4ENJJpsiWK0oFjkskFCJF1xcMoo4CmZJkhgYsuHure8ayswurPy0c7zR8FyduOZda7T4pb2/3cbq1RyNQ1dLn052X3Y+t3//2WyqoSsHi6Jm2KmADqiwSNGAkH46DaLFaPk2EZxj+mPnW4uzlm7d5Uq3kW9JkL1df/y9E+np7WydK2/r3////7qUEdDE6VQCqgJJRKfJEC+bwjJRjoHiQsOY2xMmQW8eqdQKJFBIWAtWikVWoFkUVBNnfC8VfzK9r9bl2W1286VrqPC0FLsl2nZEmM/3zvort23VfUTkkTZhd6UUqD7pVkjXK/825G9ZIG40iUCBGyKSJiS4xKx1coG8h50K1zbnDW8MU9b62//uUZOaEA1JFyasvMeBIiLm9YeIMC8ipKyywyYjtI+aw9gh43oLvUYqMFAxTwSMz0gkxsatkzrFr1jXK7bYdf5Sy/sWOODHiw0KWUESiinSxgBGQBv8y0Syhu0xacNOBRwI38sn68OROXXSotHoGqrgoEeUQc28/QaVnwc3rCV9U/Wu8UypdWxnPVVIRjTVd2et+nRcd7Q6UQ3Z+g9k+5TRwaNIHHf/9fQAg0IQESiyYBPwNpwEJMdSwWY0zfJkkkxlqCKQVp8PXMKzn+8BqZLCXNu2hVFSI9+2KMHmEIIB1x1MX24a3GOlLyNxcWERKcAQkC7/lJJJOZuKnDrTwlJXL/KJxlvZt/IMVSh0hDJI0GxRjiNdmbtgAODqdiUgDI7qXCoyUK8sPuRkbai1yTsVG16bZM3ybn47HVsrJpZPR0HaCWMKYfxv6MdtSmqcAQKdKAAAAL6MoqNGwAFdXI4MBweADkMCQrBc8URIS4lqzP4Mdxp7Ao8WINJDrYYD4//uUZO4AAvE/zNHmK+RHI1pdPAORjAUlM0wYsVEmjmX09I0QtWemRrDNy1Tz00St7H0a1wdpcvkgmVZ//qVvBgAADmnadP4CbBrQiXSGWXOM4ZS9soLwMiYrMQgP3iyeQGth9tJIPYztcX7CasTneOsxRfLl5viK+NQyqqmZmj005xtZ7GbueahsoGK9XOXZssizGUiWnG/s/8rSuhjsQN1sQui/44RfdtAPYAAvRAPCN9VgYtemoIbdw2hPMweMhKL3YDdedD9EQDpKjPTRsf4okBQOohvOnSt4eW8klVLazV8pVCl9U+HI+5UvDbBQPkG///2KAAYAAAABcyqTfaPkcICIpmJKBpfKANUh1p60m0jEDtddocQEZ1GwPYiJeinhzU0a6GWwLqYwwIG6kO1ECX8xz7DV7OnwIqLC0BWpSMf/LPJSJPp/eWZFlmf+kmZPzIeKB8dNNf+SZZ3ooAQIAAAIUeRSOBQqBSqWipImMyLTTFMoHZ+D4FtcqFmD//uUZPSAIuJMztMJEzRLwdldYSwgDVEzJMywa9EviqUphhlQHREecSIRqg7OyksTfuflqvus9/Rvba39OyN2R/b/TTv7LKOc2e+IVFpGm69RiBhGBUCpUAChiol7g1ghi+yNctcKncX5FAceTBsVa5UwIpLiQ81h4rW7kPeW5i5O2kK6Sl1OHnZTJSnNaW860rZtM/1Z1qytdNGIUv/q4u62FslXWgk4ZQBqACCDgnQSUJAIGEhTcMTAGkHHJK9ozsTll83Q3Cr3GEZKlIkkIEAslOkV7gipxZzLjy5McDkXD1/TbR0b/Pps/1/3RbNWayMLJT+jaAVllBIRKVBMAQgRKnCVhpBzC3FYQYxU2Kiqaq4JAOHwHIx86quQvUVckKIqM4Xkd+GCRb8nyJtMKBCeAngS1jQ+f5lsWXSL7zncjmfhWHM68JGMQuyT2lP/s2ckAiakgACAQTiEI6kW5LQORxM46zDLcLWXB8cCOeI54pMIRCNIxCHeHhO6orW9//uUZPMAA05GSdMpG8BHKPlJYYIsS8kvQ4wkT7E3o+Uk8Ypp49MX9MSYh+8+fm0MFBU0trxVwmC33DG1pcjMYs+tBlf//WUpZG2i0igVYBSjR3YMDjgYAc4RDZ/TLWlgqNBsMDhx5DLLJ4OKuCG6RzKOStvU4WQr0cLKloJF1Mkg9tLoVZm5VW6XVji1HZVEBATHonTXVfdWU4qy1mpfRGyXURugVInSJX8sz/uWAjgAAA5DBqGFnhq1oTliUL0lUC5NpIH2Gy7osG5rrH+lA95RKmQxGCQmfZ3UeBPzKXpBzCkE0S+tL++TFXKvNUIFDLhK9hmoqYiAO1X/WBJIAMAQgBEJIGgD+ZDggj/KxbLeWBcvisXml7IwWZfpcQ3888eXLcnHRDjH+eaRfzbLEg4p7wGGhkE3t7c+5rKDlkWirnQhF0RF9Om/i2Rgov//+tSE7AAgEBURU6whxDiX4jbiOy4EaZ+/FybeFInmlpUHeSi0e2fFNhhcEMV5rulO//uUZPKAAt87zFHpGsRPgyldYeYaDPEjQawwq7EwECTlhI2Y7u+TtRzHZaHX/Zf+3/fte9H1Kh3W0rXmSoIEHDGc2A1TBJJJToOqlmluvVKhiT9sIqKqw00h12Is2GQdiaZdwTHZiXIHCk1XnLfBGiSk5r+FF0OdycWJDvMoDHnwvvxACZduRZUsj36chf2btw891YYv0z/g2etIWo4BdX/VdoUpdVETkcs0f6WBOgJwfCjNlSF1JOZTHXqBMPD5bGSZfyQMnSufJJi13z8gx54Qcgz13KkRB5YREDk6eNaWQQEE8uOj2PU3F6BdxTe7ncVBpjCUAAQAAAAHhUo6QTNUJqy5YWAIUd4vMkHZWk21Cnj8b43Y5TmXCTQxdkjaFKehLiyQEPnLL5XiEb0wECDSawigBsTZAXq2bl3jbXDKAKSI4gfp8PBBh8M3gVLAOlDxKt/JAZS3iQNC0MP/63+sDI4ZSMhLKKIVbh8D4BpLp6kDwM0DymUE/hvjsQGM//uUZPEAAr9ITtHjFUxMyOlaYMJqTIkdMUwwbRFAEujw8w5GognUoL6aDDWqfugAIYPhc/Ap82MBYwhjWGRIUB1bHjXzG7T936DqFKBT+UlACqiSAQQCU+IZGXCITX3IS8UEXUvB+28kVHQuuNkRksiJewQChJUP3Siajacn3st1v/WAXIE7QIKbyCrpSvZloZXkFgIAosfhfnW/7cvn/CFzq929O/7Gal+1ZRjrmBs1/QtLMt3UKaQTbtRwvQJRCTDYR+i1F0TJgmUXA5h2IMsrgQCvkQZWpGYXuGq5xK4YcjMdV1EkGkn1wAw1WRiE+LQzKnNa2vN69uipSVJJlylm+fpQNZd1NBSLKKkMRAstU7jN8t9dy7YFahLm8iC7qRkOJfGYoyIqny1+KKUqq19CQQJilRlU5gbs3WnRE1PdQkU9k1R3K2jZk60n3tVWDNfskjfa5gYLH1RmScWQOlXcRk4Eo7kQUUinKIhgli9YcUwjRiOAuF6EhHwchIeF//uUZPKAA1YrSbsvMXBKw0nPPGJlDGUbL6wkbYE/jWb08w2IY5xV8hXMX+ed6KW+Z9Azmx/l6HULmEVrRrFIX2Ft9dlNxkeMGC0MK5cobERxxa9796ZMQQ4ZoVSlUTQUFvCrIe/FxL8TNeOw7C4mgVR8MJ3EqMTj1hBT7S1qwJvW7OAu2mCFqZGORxjHaHhpG6ZC1RWAXKzDmVFXVKEnOxOySUnCu5v09sv0ezPz0tv2bGozYHEu1hTbRIEhEIkOUK8sNPJ4qIJuXjEsridkunyjlTyfm9XpT87QoYYKrcDghSWcBDK6MrXZuqTQsQKH1jelWj9P/CrqAAVhRBRJKSp6Qd7IFpZLbgJIqG2IM5chYN+HEjBCOYhJVGBk2pb/B7cco5dazF2x9A/2+mC35paCJCz6zXP7PcjNHfIoZJ5ZlZzaGRpF1uRQ/sqkGFwl8oQL73RhKQRwKPaT2/cJzN8Zf/QCl3rccaRBNMBMF4gk1ZbGiEpDISVOLhvBqNj4//uUZOwAAv5Rz3sJEzhPBImtYYMqC7VFP+eYTuEQi+d09gysbM2c6V9/Dpa0mFw5WaSG/I6k3uXawPkC/M5XxV/XoSblZaJDTnjiKVtoW6hPeW7JI2VZQQs0Ank1UhmDI5mayTr2FtB4BASQdQAkHdZEnO42tPv/V/iXfY2imBBzAQA9FLijLtEls6ujVMiuwx7foyulzHf6W36otKqp2Nb9V3o9gzOUi2Um9nBsAGkIqkIFFpJQC6Ba0seRktAgwFkuIIwvl+nJ6jDQKOT1OK2umVpfGC6HFwECLmBP7ic45BgEHiwvLufRiRwgTFWMe9s4cn+4ahnaLGw85QtDwhjaARpAgAkl81JMVzIlER2HgIkuU37PntngFhauBYsncJTUJU11604MOtvuUjW2r9nY1zOlweH+6bdu/kZtrBTsYKe+uTFKIzyuZRPsyyxxrNAmtLOGZ/y3qeuf8+kaP7CUqr7Slgj2tN93XIQqNpMDKOUjoQwaAoKi4y5gLaA4//uUZPGAA1dCS+sMG3JJw1ndPYY1C4k/QYywSXFOj6Z89JWIOoXOZag167zKI0RRQNLTCwpC1RskkPKNbltc3rvRKoK2pCrFVrQOUTSOAzide4rIzyiSSST5k/5iYCqoyJWmmpaL+ve0xrUAMXXVBbtwHTgawSgD8aCEQtNLFuZdjxKkWYkUS9lX68btC02/J2hXGxuRGKKcHmtCLFA8xBY+fCp4JmBZY+uMZ0A2WDBd5D/SigrdCVoQh8QcMwOEgjCwshsoGIlq1hPF0a86k6x6RuUPlyX5SY8EDBYJBQMELBUeu1Ddb2rG3NqasWQUsmWdnTQkUDY8iWmqpSoBbYCSkSlhh4mZIJFVlymzSW4rdaK6DBHAf5hrgNJtB5YHIwixpMZQg7wT/yjYQdIooDXae+dSDUi74n8XETyyvK3pIA2kWpeLxW8k+5DlPdGVKeowgwJ0HCro797r7LaQCNemzJI0pBopBMLhF+0KycMcAwOEK0xBgYdu5T1EkLV1//uUZO0AAzRQS1MMGvRGoWntPYkDDBi1NU0Yb5EljKaw9gzkjKZq0rZltESxeqUt9WoxfTV/q36Lt7+zICUsSDlVF4ofLKNVUkSUm6AIBnATJyEIT4gyYFvCobR+n+dZYBQAdw6JB4BDSMqgsPgyorKCA6QS2KqfdFoXBFrP86nMdL9lertZ9WSY0SbbYY9UGbURlb3RX4sLAOlBh0WLiqOxSEk1EBCHZFDfpAyoAAQYFsElBgBBgIk4QQEKTI4ESSYnGyz6UOoXe4vVdtXssSph2mlBcEjYiaNLvpIFUuOEA/Y1YdfNmKNkoRegIVNYORfwvUWXJG2kkkCqFMCgIVFBBoSU7BOeo6UITKOPhuNqNrhUyAMVe7BdS2D1+Fat033F6qEKRwj1kqzOReQ7HmXUg9SEBhSkUooQ38+2yf26AhWt0VuGoSGf/576/eABHVC6thwlCAHI+lJxDhPSuqXCUHhbdZr62JrqbGETH3Zpso9EYtjir9RVqfmGrhFb//uUZO8AAwQmzNMJMxRDZ3ntJGJtDL0dNUekrZEfiiVk9iRY3PRQKkXQdo09lV+Sa1C6Q+slN2SJdyQYIASVTLQ1iCCkmEc54k8gF0Sg3y5NdMjAHDCaX5PdECErmlPl65nNUxks+UJ5aXAnX9O/+bWGeFzpHxIMYkfGz6Re9LJAncpQx1bz4ad/9NvQAC7CQQCEUoFAD7BCkMQlhRSlEFRZSrucoF0pq3kbY6nct78SJG0UVRRpQ5CMKY2K/nsziqxp+mX5PnDlEH2GpfqIP7TLs+HC2XyqTgfcIGFlTEFNBv5VJJJN5PkI41lXDT1lOIgMW8/qR6PAGwzKB8sAozo0DZafKsxthZdWzVuPWev+uTi1dViWzyQwMKoLaJvSpqntxy8E/ZtnC6etyK5wjbzX/gUCbhkCPIhvOf9qSGP//kT0fzawIzEIAABWaEXUODhqtxkGary9UBwga2Aw9LSdgL6+ezC1IwbCpS3Cxg8NkiIsUpzbDTx0g1UVes3K//uUZPQBAtM70WnpE8xCo/m8MEOTCvy5Q4ekb3FCo2W08I6pRZw+FpDUEwgpz33OyaT4mLCA///+oFq1UkmkngFse46i2EQ5lzJ6Q+YeKmMBD3E4nh+NlQ1xpuK31Shuoa2uzVzeOrDVbad7u5mPDK0pW0hqZ5wuiXoiiDZ1iWDWTDGhZhhXv4s8w6oyz63rfV2i95gAqhoAUQcAEkEiKi+YccRn67KyNEjKgcIziqLVVzkWQIQVc2aYXFxOgiJTZMMuW/ahIdWj841R6lVpp20JTFcKzz4RGOmVIJpMIabjLdfQwKkOqIf5iHKeiXICfTOdqccVFETh2HKxFbBx6jN4dEDOKSuKs/kM7D4sMV473rnE6V6GRmR2HRJh5WEvV3KBAlO12J20Iy9UXTg4dBBlrRv6YmwENN36pwDlAAJBBXFmegwgyB6lTaIbAuSAXaPgIdF09mQaS56hCplnxmleOCrb4gmdD5ka46kQG1pHTAjsWMhC9q6DDkTLDaNH//uUZP2AEyxITdMMGvRPAplKYeYUC3CNOUekzlEeiWVthI0gcrFi7Amv//3ApzOJMkqkLL+Lgfy7DHDvBXg6ieAoQlh3raXRCIQ2bLW4qrfhMcBV1qp6yn3vBUdsNI9HjkcNhFTT8Qg81XZGQdT31Tlf37jUadikZUdgTFjNqL8k8i/nDjKMdl3+7TtywEVzQCBLRTgWmDG0CXpeWmdRgaoyoFONurKLASTQ9M4CH+Jcv5qKp497CDHTswyyjubNw6aX8FvtbD+f7V9//K3SB+ryL++s1rWyBBOkcbsFOKtJtJkgmrZdZNGB1qo+AhqhigEgiULdBuEvwlT428FyoENQiiRHtI+2Wafef45u0snDI279Qq5BOPxRgJMcuvWqT2SmujOQ1rb2dKWtZwQ9jRUBzIWFViAob/1foXpIADIIQAACso2YQCpgxdd5Igwd+VohW00qI52ZsP+VnzhRKlFTEcYYOBMPrAYyl7cxHhqaW16iTqB8TMUjT7D58gso//uUZP8CAvBH0OHpE35MYylaPSNWDCEfQ6eYVPE+juX1hI1RtBqExU4B3//+iwFb1Wmkk3iegC4Og7xIzeL4ZKPShpLQYZcikOhdG9DilYMsOlCLP4dKrPRRth1fYwep2PSSWIcwZlLbmfQZpEZyss1fR3VET02XSqNnUsl3TSrLqjq61o3/9g5v5T53OAEnpoAlJEo9BAJHR4R0ROjpQGdALmcpkpg0UgpEOgYmagazeuxypbAxC0oNHggBY6sCrB0FSrwyHB6z1EWnUvr3I2XExMJGjZQjtT3//9OmDbkkibkiRTEcakMDXOtxe0NMEXuyptGcRhpdsXgStjiyyOGVTOpDEI9inQHiMVMnyGawOScvGF+nUtNU9BOKyI7XIndrHXUqIrdHR8hOja2ffQalEaryNSXXYv/3eUrAnAAECAIBRJT5LARhDKpBJYvtKmHMSjkVcuCFVGJdsqQqZGDYwgR2/028QgdTO7RVLKFHyMc69IchpoyJFZmlL0IM//uUZP+AAwk70GsJE+xMIklKYYNIC/U/OUekT1E/jCW1h4yoXUkWnguFrBoBC0KLF3gAPVLf/6wklJEm0kSCq5CkgcZuKYKhqKS0GQPqow7sjbi+javo9SZxtyOKpOKWZGATo3RH8sslXSrapRT/2r82Xd/mv8P78Pkvs5Y+ks0WGuHn7m1lk3HHBtJooBRqCyQqT//ciwEBQEgIkIorrDFqgAi6yuPWuuYtFhjx/crTFm2fAKSIa3L3XZzezdkzTyYTA70ThCkVMgJbSLFIHmHCllZqDHzTnIoavTVDSt7//voFRnWSp2f2x2OyWvWaUHBSZgKOITCAjjZVRyA0aFUBA4wTEAIZpKqHoYHlVElbBD6r0wFChSStvZhhSp1TTDThAgjpKTdVhxNatRN0YTJkpG6cZi73xSKw0xBSCgbEYgZqZvHv5LYPfWptx7GNSKcDs1qKPrefhvKunqhyOOdJKn092nv0D/y+IxNWxBZIWta7Ny6ev4vJFZBct5fx//uUZP8AAwZRUmsJE3xVA9ldYSNYC/CjQbWEgDEtCeW2sGAAHOHZPlY+WUcdqQbYnJmVy7f1ZipzX08qo6eV1vpN15Xk/9Nvv//P////1////////FOmZsRKqMYRCRBbj0yj1aOABWCKADIEQczBzoGgAYDYugEYe9wYEBQN9oDX6BQICKLvjVHFWMNAiqDk+1gHJly90AbfuspFpcBTjm241LY8/UXgqWyx94y1xekv7YctfcRorTSrEov1/+9NttOR+R2LGql6nvU/dUsnqyuxjv9fGIYmcaWkvT8MW8Zmcywww58Dy+t9zm+27e69ymmq3LlXuHP7/f13OpjhzfdWLQAEDIAAYGiBYCAMWBwLDyYQ6BAwCBTyGcZ1MiGQINCS7KgLPXwfZ+lCYqwYOS02cl2pZS7XLFozLaXHHlV0YOycqNZY47pcasMq2l0ajpIPMuh6mzq4/Zs1YjFcf7l/63vev/X//6yy5+Wv3VlNLlV/99zhoTi7Iad//Q+K//uUZPyABntZUG5nIASsyXmPzWAAD9TJL328ABjGCCo3nqACTBXJXPUY0A4xgZ5RkqGCnV4tT1qkCEJ0Ok3C8nKlPard2ZgMjXzv1/rO+tfX7f//8FSqanJINXJJJG3AqiuwDPZ5BzDnlXK7q+1i9AgP649aPCmi0vi21vlrIBMV9myiIcczaKqO0YDIcPlOk2Qwkar7sZX+z0qdvuOfSjlN1ZBrOtWerNf/9IczcloGC2logcCUcyIL6OxHbJdDe4QF184TDL1yrnPKG6lMTESDLv91r1PFG//QX/21gdxxi2SySNpztwY4DBP6zCUuO4kWgpiV91fsxt3n8EPYIR1roKosMyHmfUPFbUoZd1VZs9aCAlDFam9yW929Xdz3PcJS9p003S7eR2agJ3nY+j/+VUH//XG2bGkCMdMMNFAKA5Hw9G8dgnDRQ0GS6Y0gz3RgTl6A44G350+IkkUd/IQJMJFv7v/n0btj0Q2eEtVpNjVxuSNpviPixmqfaBbT//uUZJQAArBHVWsMKt4xQkrNMElLi00hUawkT3DWByd0x4jUCNMxFeSnKlQKfYFacyGAwK1FV5Y80SpnaXFE7PLwLA5OTbm93LB/+VLGBWcv7XqN55HgqST+ETFD8/O5KzF0LeQtmBVIgyf//R3IUExBcAW3G0mEw7Ww/EXN9To+FXNFRDKh9Ov2fYGt1aSVHsq/ZkUEwjkHMeBJt2zcPi0phuwf6Ba/fyoWSTgsjjbjRTxBQ+RJ1wOY3xD0ICQk+JFZOmPFanFDlAyxVS8kjFjUMtzJLe4Z21viX374Hw3IbCOyv1uo+BeKSZcJ+ZNPerdFRHz9MQZYec36HiUY3//i+wmABpgug4OwmQh5CcWq2KllDvqgOjiNfnaCGQVIRC5cc5oeWQj4iAIICr71Ci//u6P4snE/eXMgd4Yl9KohuCNtKNJEmhxm8QhUn4IS0gISNL+VOlcn0wpEkuzwTYwCKipyJT+oLXyqWSAzKK5dZ7MtLLfWwaQtEIq5awvE//uUZLYAAt1DU+nmG8w7Y6ntPYIpCyTrS6eYUbDsBibk9I1MxOFgpfGIxEykZNB40RUZT3X7///ZyIIAwjsomQIQkEqbXAsaWjieNNvH1nM+kz76u8cgeerGBYbDxsF0MWpdKKn9y3vR/2+wt+r70Gi8qkBTlappxAAPHUIWmgrLU0rbOKMYVSuHD7wP65UFGdI9C6Drq5fVIGmUUOZb2DL4ljHBMJCxx45Z4mho1tmSqu7DzETow4lMBdCHDg268Tt/9STlIQlW9VcscDgSEYXIIMLqBQP5XoSogHAs81NREH4aRMiiJmpqkGGk1sdJ5mbFC/6v/UHs129pL2aKKSYcbSSRQKok7wyQzC8I8bgthIR/l/mXBkGjZTrp8qZrMUzxohksOnH2PFFlMZ+8dQ4Nw7wjP7FFeOGsiSuZFafBRbFPI5nTsm2jueYnqhDOqbdP6cwaNYEnhJTv70C8wQUmPhwIZ8jPYC6rBaGQPif7Wrr2vRxMY6oCG+HuhMWa//uUZM4AArEwUWnmG9w5IcliYekWCmS3NSwkTxDghWg0liSUDnMHcAQGQcC6AOagBiVJAK3YFYvwvilft9rVcVjEG0AaUqUSUm6YEEVlA4Ule5Yc0KCaG8yaEvAeSkylKSzFKtXuonDNRaColpzkkcag8/Qj2+Anw21jbTVrFhjWOHzc8xU440XyxMw1j8lr4N2vScakxFZdpj5UgeWcO77pB8vdf8XKwbMdV+puz/llxCCEha0mwQQoDYF6H6olEwm6e5nrqfrtIN6iXLgT2fUBCzIGSgZFV0JXL+JbS1lT7ojPKve363o7n///9UVr+5fttQGqM4uD9tUJNuNppFEkqqPKVFu1MEMHYRpGitHbkXjbOvOKSHs26oGUKHc5QQhjKFnF9QnMRMQxs+f/DwkOLHFbE7NKWpwhHRHwEJFFvZJJkZKD5ylODFY1wWupkHefC5n+N3jAhYFgwjv/7duuYIUpugcYkVuCeOk1U8Xh+YI1CSuGcszOejUvWjNd//uUZO6AAwVH0OnoFMxAAylRYeMcDdkxM0wxCxEfJKc08YnkL/4D6VqYb6VKn//AvIFqKcKKUdEgOOV1SVlt//ZjFdZ2k6Lb3dy1ZkXopM+yRxENvYaCilGmyCaCGUgsR+jQOhDDcFUuiSMzKchyAtEocSZRr7RPMujfr0LOHdk2923/t5VJsIj4ndO1mb2niml2u3SjEQHUt+lj/Orq1ET0VI4VVi7JJEhBIx//Tor3KBsmzlcYaZcHJ6aaDJpGSL0zSkThiOL4h1GcuxJYgKm0g4GhCFzAT8JusSdFwWFAltYdFXmFFeh9jPNnhZWWEJEcbSOM3mHKBkAEAVMF0yCQykAMqAQUjkvQ4hbbfsGYg6EAN5k3k+8hGQwgQEoaXY0aSa8p0XU9brH6itWtphrVUePio0tCKBkNY7o73R1bZMsw/V0purOnglUUpISmNq7Nb+6RnDn/t1wY1b3NRtxOBqO0MhtBTk0MWzJg109HsRCMHn3Wrwr6IVIvKl/C//uUZPCCAz5IUGsGG9xPaVnNPGWKC3TzQaekTfEdBee09hkMKterIkR0Fr3WfZG/Vv+31Tu660rtrutxT7+1kG9/9s7qYqG1mtL/0J7qUiSQTCAIovB9iTEIQBfSDmWGpSZiEFUhiQSi6h4pBzU/lieiVuq35qBSObZEHheFxK2ptfNPLoyqkdoXqdUrRWvfe9rjrLJcx75JCLWlxGo6uF0FH//X6yIIAFi54bMZ0Ml4xMQCaLKGBZ9esEBR+21rPaKpjkDKTBo1kjbu6MQcMFDfUu+nc/bdl7olv9m/2ze+6J+9FR9EIZGGV4wEnpEikknQHsGCQ8TgFCXkW4iBWKCKiB3jERac54CtWkrpFrepLoiXeuXZObHm1Ud/L/pA0D6FXxWx6mptNz+y+Hf8nh1c41mq4hF5rKkElsRyRT8AljHQtkUb/+9IAJ4AKFiT4iKPfGkqjo4DolXuaK213LkI6iNgkArZBZ8nEdd6/rvnZ9K7vlbYUmBniwdW0UJO//uUZPGAAw1HyjspE+BM43ntPYMrS1DxP0eY7XEXpOVZhgiwqmQIpIVa+7rfD4xV+OUFGnEGjJwmIiI9LtIDLSlEpJyNIJorhRDXQ2dMN0FLFVn9dNzp3lp23eDRU+5zmk2DxhpNhLOq9cxC6UdtX/LniKgYMcG59mrmtqiM8g1I6+jJAUpflz+4iBjBXnbsV4/qRPMsv2/BiYzhk//9QfT2sIgjWX4njCJqKWGiuxvKw+wolYnNsUwKiPFMXSplZiAwrnY6hrNypRDNbVNdn31SoZ1Etltqdtz1q8hFd2pbz0X97v2tvsCJfF89D4CACCoYPYMSDwigpliqZUEbM2ipG+fd0W7NkgSUPu78NuxKcCuLyzFIth3kxyRsFeHfP5IWmFR6VmzHpHkWaKSH4wIwljGH0I+c/8/lnKf/goqXK2Z1PmqfVNB0d/+epAQKgAEgVY3qMRxklvoCfZtmamh0BlwJpJ7ZdvKdxj+1JlQsWwtXUsKOR7KrVuDf/pfd//uUZPgAAu07zVHpE/RQQ4lZYSZGDDEjNUwkb1E1JKdk9Ikn7+t9yrzogpec+rPMaTp791v20yWwRh9WO+CtOAAAAecw5mwj/hkkkJoyeFCIggiXYkAuhypTF0ZKKI9q1kQnvqieCHh2fCfB6hfU7UKXeZ+zbzT/oRWPDjVbuqj0/eXTxhUp4NRmeQzoxOdBmQtDrFHPzgU/EVbGOR3puKDa3VKtFC50t//0ItfUUiklOI8GCn0gQ+7GcJB1tCnJxXBlixJWMzOcjLCi3d9bIVQjuGVaslmOKV/+HZ92Yzo/ZN7MqvqhRH//9vf7/fVUBKbDCU0GQcT+d/9aO4CCSSs2xEQ1FUm05TJVr6u41l0pe/UqcqNvrsaBeOHKh7qezSkMmaaQO93V7LQXEAQe6G9t8qHYYo3UyRRGY0rKIS4xPUuyL5EMuVhS22Mnxo5J/qWQVP/qBIAgMGSGTgmgSh8w6CGmMlXjCwNLBKICUjmMdCusLwsyVOTdo/yEY7ZU//uUZPeAAwZISzsmHFRN6WlsZYI6TXDxJGywbcFCJmco8wnSsnKbjMsk6CA6CszNr29t8hs6Iu1ySAfW4Mh0KWkpWLQkRHmDizSEHhkgwv+T3ABQASoHYQwEoqFqGwMogoGAw+BReHYKmK44MVMCFAT05SPsopT8omXUy9Nts7sxxwopv++N87xvMeKgNca1TBcnFTsqw8pGozTYVeG2thMmJHS+SGHTEgIApFQIOpbEBUg033vWY0Oka8+024cYfgQiJUYGLBC7W3uzCVbuXdPGe7vI8ECFCqtPsyIQd49VultFKy3WU+bIfVKTP/vTVKv3lSnBOLs+meN1CLiabaJRABg+z/AXB5nzkzwyyS7Q1ViTpSFM8JbBVhpcZ3LSVINPqeM0dB7a4S8cp2UFHFCrtuylK4CEd6T9KUN9nQiJnCaK5T7bKgqPntx25Djn/6ABEkwQkEiVO/DsAIic6e9E7JaplEZWTUsFBODAWglJCeb1z0RkJcEZGC64sVTO//uURPACgtA9yzsIK8RX5LlGZYZGCtCNLUwwxsFMIyXpgwmo3y4QLo95zv6wGWGjXjblNnE1kGX682jfXjkg6WQsRiAw7f/+wJuNtJ1SrCaDtRJd5KpmfVKFMEaXGd4jFyaAaJi2IKXI6WITzjzhj1dpQisnttEKUFIrXzqBBSlD9z7Rkv/W8/P0KPpZdBXifUkkfIoLLLExGvS9yw6RmZ+w+JyAAJrgBIJJRnTDzBzWvOCbSaDBqn6cflOdoNFHPDqiDViQYv90U4Mf1z7TehjGevWtSUAauu7aALQ+xzyBNosf4peuQtEhi8eEn//+tRaqlaSSSeJ8K8CAEzN0epMjrOInhPCcIx+hA0mSyMstLLYok5px95CgGpR+RxtNb1vqmKIifH1Hxn6ZQI2iOttD9WY8Tv3NserJ57bKhET/9kT/M7XICVWXAYj/rspRWrWWqoAnQNUT8yhPFsqtjQWAFjdOKx2hI6GudKQoFmbZeNGRITpn9cpD2k/VyIzD//uUZPAAAq48z2npE0xSxGl9YSNWC4TpQ4wka7EvjSX1h4xoWmeDQ1bStBGMF6kIES0hYG88c27Rt7ENIJ+WWhhS5RhqUCyPEbjKapM02jwMinNwhxxlIFyAtRs7EVHqFs9Hbqv/nRN35kfnB5hNf+GYTWxvMfbxXUqCw9R5iEQaWfVCiQlJEdPKlklVoYh5jImQV6vq9ACAAAyHiCoOGLUJmPxKWkQNIGHTFmIbkEMWANsMLQWkNP69q6MFNgVRuHxoWFNcjYk9Erf6r/aldSOa2+idPs1ktX/bOFYWRDCThxXHT6oVWZk0kkn1UjQNipc1o6jDMi4CkmDxOAp163Dlsaqw9LYcKHhUljhq97FG0sEEhrI7r9N5itsGbPcMlueHl1N7py+0ZxirBDK8ayK6xrcu1mztLWvzaPn3zAzqOZ//IggwiQSSScLOYAjJgk652mUXp6c6EJFtGg/h3bLL4MWrYRRFhOI49fW2ghTPyWqCresYTDa3lVFHTo4Y//uUZPWBAvJKzlHpE3RIxGmpPYMqi2SXNyw8w1E4JSUZgYnwJCHmgtpNtGXLXSVyqA6HwgWGusez5EpNNxuJJIgmuxUEiFrDnR4bgYJguKwsEsMZwlgwQd1feGFQ2pTFob0Cmo7VlUyX53BUaBcJCBF0gNAlYhQIEsKImmRZln//b/z8p0o2+h3LyXP3pRlEicaGiCnq/602LqZSaSeetqbjLYInUkRhniO4kI8oAMFE+wnNJbqc382CqqT51u/Ne5KG90vU4pg552cCJQ21++2eq5d+Td07fb7fVbZkBmIuIGyv9dVBr7kkiUoCVifgxwfhJS/jKO8XUlrs3B/GMh6JL/DU54PKEGXoZOhB4MMCS9TIi4HL17//TKkNE0ceYiVNzuNTqnedEKR1RVczL1JV2MVmbjLaOTp1fS1tVRmGiQKQxnEWxIAimkoBArimKH41VB2GXyj8tc9WKDMJa4c+PmLNJT5tiWp1q6MmwI4BTRRLruYFE9KWtS5t41TZ//uUZPsAAvdITVMGFGRRY6lqPGKGi7ETQ6ewZfE1o2cphIj6G5ZRDqF3omYGioiGEBCANqpRJJTpggyRQZTVg6mbJWWI5JErFdBNZl7LYxDzykfE4LgmdU+rneyVkmnAOIVlVlUyIRKMDEbQQGHd4tlSVFcqUvQyh4oFVhMcWYh3NTRSqw8j9zqu7vEXvs82iIVtB5hpGFDv/R3pR+tIC1QEDxSpB8rimIsRqMrgturrv3emIU7itGkM9BTECN7coyrelndER2dNCUkRa6UpOstvo77XPcpxoRlv1o7f+RUGZtHkLurn5jcfuR+SUb1FEkkp0zRgOgCmRCUnhKESUcU3wY2W3SKSulUCAUVHCy5NrBVz4dtBiDnpFSHblhasHBFPYKjDOaRUM1y/z8iJiNhWZ910nZGpcvz4eTqLhY22t/9Dyv2jChxj/9v12BhSp4FJppSho69jTQuLB1LDL2wE05xoYvUMYvWsKfj8lQmq6IZR2WaThsgOXxvpwEUh//uUZPsAAxJLzdHmLMRF4tl8YMJkDX0hMUwkrxE6nuWlgwmZCFy8XQLMIZXptvX017pZdHSU6oy20BjA1sQGlHI20kiQVSNvAO6dJiUAVhRhGhmIk7lKyFysvRRACC5Cg+6TosXVVhZ5b6kvme3gg8xEO10QoNyzl1RLk6Fkt3KptvDO097J/jvsl6OutkbYkODSj/9najUCC0oUiikk5mEn4ghiK6mTgIWGihmZaJwQAog85o1NbVDtnWmGdz776bexbU/9e/o2uqE5HUpvX/7WB7WgvMzO2LPCEbf/ta22rVVEtONpkpAACD4CXD4H0QcJCGefhXwg41gNTsBg5lovmIcmKb3HiyUo0MrAalQZOOIKUjDExMQ9RIPKOIDFbk+f/78/uUI1kqel27Ab2saZMsGPIvpbJMcKDDf/+zoAAgAAAcQ4A6AqdCllDBZQqQydMjZIEYERGGcMNamHLoQW+pg6QHdDQWVi/2qI4/bbnE+6N9Sf+/DI+9D7+/1n//uUZPeAAwdGzNMpG0RMSPmtYCKOC0EbQ6eYT3EpIud09Iki2XmhH/XCwftY6V3FsQUYgko0kikiQDTVwzsZQ2qa6zY2vqAGxprInQ66XlhEYEOP3UcReugjj2KvEUCibLo69lOltdlxqxGMpZmkeFkjvftztpGcHmZ+NHOGEQZonGEVq73zJs8n/0LLC1dyS+lAX9UlIlFOraaDBLDB4NGIBL5QXAIF2rToIbZqTgQnJwnFMmunjdfPPNjenU9bqbuzeQblSuz/pmOnm0eRQaazIpjpNn+ru/+6Ux7XDjp1gtD5I5//+ogqAr+4pIkEwdxBA62cfq6WBMQMRtnOGdgesyZU4pxBQEDgaQK6iVYmhjadN2CGE8NJ5h87iLak6GXLtNCsS6RX+/92B7Jnn/tSnbxclM0P+ylgkDbxT3d8YE3w6G3kiH/1AIAAGIDBi2bBBGpPJGlLBkCyGxwHkYnaIqmQNEPerJKWOVC1ZRkuplFsIsdyrhZ2TLXYiMar//uUZPyAAuozT2nsGsxOpck5YYM6S/jFPawkbbFaHuZphImyUp0oXvR2s3/7bZOwYutDihjQ0B+XAh1IcUcscbbSKVNhZCqNNJE9SpbhKpY6D7J0h8UgBRoJTwnREWgZUtTw6B2jebeGisgY5VQFSDKl5qOv1xm3RTd+RGW3623HodDOmqDySFEyR1AUkZD6P+5YMArZhCSSbgIGNoCAP44i+sDkcx2HuXkgaFiyyfNUER04Rdm93MhEGoUDzbI66m2JufRqJb1RWWuYj6FBC55cATEeFb1M4+tM35QaeI1MBRbjjV9Swh4tIvn4vCeotG9GF6Jcbx+uGoSujLs+FBSgWZ3yBjubjqUj3aAsU2dDhV1z/LrmD2g/mLDkxY2+QpjvpTKvrGuxGScKJZ7Z3LP+cP+9lheKH+gBwtptMwAhRlsS5vHIPSP8coxU0SSdNpydtUOewL4sIVbQu95sUZ8jEDyNkn1oZ2nCqRQSwerGxLWaelmd92f9/sc8z01c//uUZPkAAwFAz1HmG/xNh6k2YSI8CuDvSaegUPE0GKY08wlYG1FqqkWtg7yJ5HIrAfYdmGUmnE0UkCCqZliAKYcvV8n6kMzqLsNhDBHNbV94ZZHT0RyTRg7u9ft4nVO62uRbC1XLZPVK1IUIBGIqx7/l+R/elvdyJEOi+28O/rNbwQZp/GEmhGtxhWSER0SZD09yBb/FWqN1dV1eCamYPkQBA9XvTlFCMFZEzJApU5ow9KKEpuM6ZK9iVQRBlKt2ZCOtHM4Z0a7frW/XaqNX1Xt37v6DVQZUt8Q2vepBGlSiSiniGSKCU7jCwFK2gDAY037TGUJJOo6MBTsAZxiaklNGpFRAec2VUwfAUou0zCWfx51n7f5jveH7jftzD9j29kCRBQGTMmuf8aNz1c9WvEK3Ud3QxJ6bzP93GBAgYb/+Kn9429TXLJAAGGACADOsYHEIoBk2H0rwOq2RpAQExIwQN0sp0SLokjqVWQp5DLVVyNueyMxGK6XMXayAkwb2//uUZP2AAshIUOHmHDxWB8nMPMKLjFzvP6wkb3EYnack9Ikq111a9rU39//Sv7+7fRRKg8Dn///WAgAAAB4yjDWNAMh+0g8ouoXlLutUFk39lD9S9z4m9mNF2/MNYay8ggHyU6lVxelCU01aJV1sbShU12JJSaRwQ44jeQgkNkzS5LFmWwX3oyEWE5GD4qO7lSjHLqvKHn8tjcgfKYq1DQh9ftdee11c4QOQ5FfZJbatSkDqgJAABXW2sNEAFpYCEWlKgMFBMxGBKjLX0dt/L07HZYU3JIK+wEVwTELZQbRnYHyv0JCYBoViAh0MiKVFpczHrMe332tRKVqr9SdX90o+laOhUH//Whh35SZSTmFEEFJEbgtYRafPol7AJUjS1DXBKTtUCNbFFmCtsi7lQ58jZfmBSJ5FilIoZMRf9WMMsco4St1G9Y6R8FiQXAWZphuPK52n9DEwu3vm2XkVmUzPhZ3zKkqGKYWWsuUg2Ndvf3sF9RcTscs/6aF+KhQp//uUZP+AA11HTNMGFORMSMlaYMJKDyUfIuykU8FapCUpgZYgoRs2qsBuG8jTsk9IH9ITyWA6lZ1wBo3BUiPWX2RQcHFXazujT0e1FkWon6v1mTD2/+ia9lMLbhc8SlCIPCAAATAR1IAS64CR5yUHZY6iUgDazDz2uTEXkmXUkJSJQyfHXppI+Ixx5BI85vU5xlmw+buIFLKKyISI4jfPA8nYDRsZrqgKjbNOHujbu5dyM/Lv/jkUCSAyRydqD3/9lbr/mwaAgNiEjMAgCbC+ZU6/KOYbM09kBOE4ABmb37IGzkmeUX+drN5SmMBmCyKgoLrffbZioCaRcdUnYZG+SENVmcYgPxGalKQSTeRfRqAImAtOXa8bop9wy4S/BKIhEkgwfFZeE1KIafOSzvvhSqTWTX6KBAIReG9C2tWRpUIJKjGCLRKVLT7CjO1ELYvR9Ff3aZzKo9h91GsAS4cZHHA0Y/+SV+zcgN9qyxNoFAwPjBOkHKNJxViiOAxHKzUw//uUZOmAA2FHzlHoHORGiApcPCeXjIzrKuwkb5EHjuXY9I0IJvSlfYYYBlLMp/nnKQfawGt9Kt7hgFXff2bui/06I+rNZdrf6f2b9t/+q71BnpeLLTMAopxtNNJIk0/C+k3HeWJLlW1D9QJ5m6b8KRDm5YB16JJMgLdY2lije+T0SAxcUkYdPISZFL58r3uKA8FWXprqB60vkqdmSqnb8RfG/d1oKWiy1di7M/o92zjQoAYx5Ibe1bLNf0P/Z2JbQAJla2o20kmNFIdhmSHXhTl9QE3gJ9kI0uaOxdS/+eN+Vku+dikR1O7bT/tjblKOCVIvIjrVTyhWvaNqQ+AfHZSl5146pawSchXv65Jm91Cwu+jy77MbIqUrY4MUg5+2WnFAuyHlXEb2XEl1z7UsjrGQhdF7VXW4sGC0pHuKh5Wcz32OTd3BBQc/MiM08+XBzKPiI4ByStjVsucIEYY3AAVACC0OGpuC0C3CB8TzWGVVZaSmZMgUJiI0JGheXVVl//uUZOsAAxQ/TdMJKvRJ6LnNPMJ5TUjZQaelj/EgD2d0x5icxmbvf3k/sCWvW9vp91X35jnJVN7e+/9dbw0z5/fsZre/tnzpKi/qBH5gGFhWmusu4Cm224qpVg5D8OE9CwDhDoMBOiS6J4ZGcJYZMkLVr4o6LOrsvIu9zLhGhWHYzkKcc62BQYvhgzMaymWmyAaPmdUz+8xizI3Sduoa72/T1+ohQwhwut+5ghAYDaKgQAKcB6uIG0vgcChhAuOxVAejTunbJ8shh5lmi+4/D8u9SKGaqsBsxACYtk+8/cAF08ZehSCVC7UC4s5LBcaRQyJStxsCKFYAFYCQASVC0IVEJTfdBlJNdbEWetInmdsOd6/Tx+RQQ672UVAKgZKKJBra6TKLN1dSLTIH5IXT3ylDoluWt56G6S5AEotYuGhwWYXYJlKQpnuMoHvD4LhQ5I6tH+rXsA0LQAAAl8ICA+gSoiIXZ5gtAxjdRw6YesRgE0y4cDZOiU4JXa+/RrMO//uUZOmAArE3T0MJG1xRg+lZZSZESv0ZQ4ewSbEtCqX09gy4ESxANFRVzUmBsWaUdagdu2iyTDC7GJbtcM1G5uZJnmL//8kTVdZJIIBgXyFABQN8SZQDOBTHSXskCgNpSIeuWEyhguT96hxiYjbAu3Lq/7L/NaZw9aM16jypjorDjVDtufW6OV0R1OLINaC5iaDVqWQEuwNXanavrl8pzAzlFjci7/92LIDZMSMcV4AtC0gZMui57Sb1znppSMjLNSIjWRiCcnp3zadJvRM/duHbHQkKiTu2nV23yI7TrX0b/961vul7u+gQKzaiBCkCNL3R1SGumSRSTmLkl5Ch5cd3Gpr9TVXdfrqYQawV8C6VSwfnvHPOQmUbDppl6HSEdocNLUpWGnJVozr9LeRGoM/jAw3bLKSFv9gM/LqS3Jl+2Hzm8/sln/J3K5nL+IYBZTBnayX7l0+jrBBccbSSRIJs3I1gndldrGAlK0wCZUPikXLCEhJVkj4D+nXsczFc//uUZPIAAvYrS1MGHDRMYrlaPSJIDBEhO0ekT3EuJGb08YnxT3wajWz2KKXody4/pw1jpPZKdXrTb0/Rv+zN/MYm4drzkYeQOAA2TMJZd/1ahW/5STSTeG+CBHcLWN/BommL83ybkwQpcokt6ffN5sLZQiBBfeXpS2dHYT8sepLGhgT111eSgoh4cHBIm8zDKRA481pB7GD7hnlQ6ImhIXqxinMe//3gQpgIAWJ2pXIJwERYRqUPNqB8fRCa45LCTFBAPzBxZmQyL2HTNA5yx8q6jbBYmfeJwC0uIEJsf1IQf9viAYZWIm2hssHAyXCDDpxwIAk5JGmkmiSaXccIasYwE1CG+LcA0CPFMQg6yYliIUooCAOZVFokdskUkR+H+1K1fZv2E+O+IPk6U410ZuYmQp0zYefJfe/p2f3YjQn4CJgCAw89IshatWLf//SACoUAwkSSnVk9ifmScDkbokwAgClFxUTntRoU4xbXra2+q1CdCl+99nwW8mse+0aK//uUZPQAAy5IzdMMG2RTiEn9YSJfisR7O0eZENEyDGVlhgzgWdb3I6uusi1Yx7WJ06dd/lrs+3/XfuxyUZK60eCqqjsaNnZD//0ILdSmkkm8DRDIO0TFmIgjJJR1D+RBuLohKTFIkCJDBIKE5Ol9VZ0HUFI50j0nkNQTc2GamWr6XMI8ku8N8lrprbY+NzUCAQUad2mM71/3dnftBX9l9T4G3yYqfuBd9Cv/o9KkID2StKDcyxPz4dPj+OEBoRxqotdF6C5b2fbPfgq1zGNcyRHkpDgISjpZjzowNwQZn3rp2Wev0/09K/6Svpaxek+xjPGKrkdSqgmM9In/9NWRbpUmkk5gbYRonwp5hI8QM2S/tBMDQY4Z9qqTawpDO+35fAYFVpaGo2OHBQiSoaSIe+XdrdlIq2MriT5KXbn60Mm602Kt7Uz9aMr00YLFxM///pjCdjjV0ow7iY4EGwiLyWSBaRwMpT4eCfsOk+p4fI5lFRJGRiuYwCCQZBircKyN//uUZPQAEtk3UOnmG+xXSRmdPMKkjHjdN0ekzVE8JyZk9gmaan/N+x1oe5UT6aV9nW//6Xqqb7I9O+dUmS2MlI9kTpgJRpMokkAwv+XoEoCEDKJE9frid5yH9bndblLX2iQMkTtxrgF1BMs19Mr9YdiU840LIunW1uLuGp7IDgqnpoxmKGgdUIFyFK9zQPSPXOJc7nJlz/8i/+G1mYuQJ3K07//sDAUslt2pY2j6FYDiRqY0qxxLZgNzCc0FcqZxMeYbPAxMu5nGmyUUtKSuzuqI890XJRzS2KdChWrXtvWvr6Np9PeyIhyjsmZ1WRNXRkKGcdJFkOPSKhCq1SRRKeQ0O0+ARYjpzCPmiDnC1M4ipLS5FiOo0kiHSATJnqptImPxTOs8qaE73TtX0ubFGyij66LLzc2PvMYlNZGnsLt/zRDvBgJbXv6zVr0XYl2JBorv/oiTEUhBnGCOyxoNiBguZ9Z7YyhosAEIgCAFBhLRBwakCkMsijd3qYYoUx55//uUZO8AApxGTtHmE7RNaVocYYIfjEUhPawYbzFRo+bw8wnkVAF4I3cieXBIRYoQhyoc6YtIQc4OzYjEwZBAKGhCcCIdedZIe47v39dDOthtEVICYJGpSkkilnzGAK/Z6o6FhPqpdAEsfl/2VNPgYGwzPVAZSSZa1erPLO3OeMqqbrXxxK3LEGTzVRdDo5ipOdiq8yCC1mN1kyeDO1kd+TU8WFg0082omKwHpAtQ4gW/94STQCKrDxMuVGvkxzXg4Lt9aU1GXsEj4CFojFJGrul5Gc69yiDOFRL7NNUkuy1o1bt1dndKm5SZnrs0GlVWlle2RK+1HfayehBd0tL/sZblVQIWlSKRSWMyFCk6FDgaPSJtuUKzOy1xlUAxJvmGzb+IrsuiY8uwGRKB8+OxBfWl5A56M9adOYpzL2rs2mT2Ba824/BKEDwi1suCiPP/Yi5l93CC/W1uEUcqwcFcEOOBZzVRZQ0UyHA8h7k5s2Ai7gyo4YAP0NAjRfkMOQoE//uUZPEAA3VMTNHpE/RH4nlcYYNUC7S3M0wkbZFBI2XlhIlT4QcDCkxSiA0FeymaOnKh/R91cm11ZVGFBwuIBaMhh4hbPucP3vudT7/31u/UolSzaRQ8AAgAACAXzhigkOYMsAAqwgNJhiZAEtJMgeDypfsPQleaimwA2Sbnpen3zKKFhxNfWR+mOlzDw3UW+qmmJ+hQ8D1J9fOTybkLKIeWMNcYkdci8VWKtrCTnqEIhDssqt380Qwk40qpQQlhK6YATpfMSqsdh0xZF9dK91DkQp8ZSJUASIwYXo3guqRVJgyzLbwaCcLtJqEru/lHXKXYiZfmEWq35Ozke5UJFlxpFFEgGi40ATDQHIFuJAuxJxJkEykJQJccrJiJ9URxY+BmKCoNg8DUH4IIy8N378YlIPTIgVcmoxYTM752lxSLBApxuMcQidj4JaPqivz+6tou0rGjm0f/6wABwAASGAArE0CXDEOVcCBTXv/nqyscrD8JFJuDyKn/UpVVQq8S//uUZOyAA2M7zNMMHFRDYvl5PSJKDBzBKU0wbQEWjufwx4x2BwmbwKaSWdaCYkXenaSbs4DYtTjC0mnN/YC41gItqkpJJP0SmiI7PSQam6SEDs9S/Ze+j4xeTs2ilvhZICJEktXpIhT4HZm+NV/mQVRPXVXzRKwkwn1GwZ4XKLzIfjv9FOOBoa+71IXWxrJrzLnYZ1v/M//+EEIINxPU6Yd/8fLR7dZFIo1IBOR9EHLiSJCIEAtgkAixEJHgeQrwTcz0uH3b2+AhITD4CeCIon3vEhoIiKOdgK40EQfUqlsy1rijRUVBRE6MbQEJOnOEFQiaWJRRRXAqweET9YAKzLJ6QQcRBTyWmc2mdGyyrz5iEmuYCLNUDKYxJ/yUVD/GKklROSjpbIKRzdjLRKj/90V53fdzcZBv04QiMIf7ERIYxpS2PS1G3pR7KVuALg8m//NfKbwl/77qrI5zUOEGyiZ+XtSkMQa3GnaEMZ4UyPPxN6TRgtHgmlGfpTsOg1G0//uUZO8AAuBHT2njFHxCIvlZPYMqDGkxNUwYb5Ezief09I0cp/kAi3fTSxlZtv976KiM73F3Iuaz7f/776f7GM/Mgq5rnDQSo2mUWkik8gEZkWrfNPCmLnu+azLyS8bA9D6y14VOhwPBp97owhu0V+qxJZEva2qJK9VWbEVtV29n6VaTTVDgmdxZbsvdNDIhN6Sl0crUejaslPX0/6u6ESL/+jfQtQCryAgEEqMikAbgmjDNYbBLxTxKmchR+7RDEbprTpROonVT4/3f/rLYNpnqtOISfCbksqa3t+lC4RF/1fkUiokNlugrt5/l3fPnzyklyzqbi3QQQMCcIhc9X/1KQZlkkgklwFTWm7rK4eL+P6qccG04QQHCQHYFkyclGSOhbODf4t1A8XFDMyNWdUirGoVGOJ9Iw9TFlzKkrS0ti8XcaMKTTu73t63vN0MHSy/55Fz6UYoz0KJUd4U5gH/1f3iAAQCBIzRAcS2F4LGYxyGw3lLPFAAGTykLPqcj//uUZPWAAx45TNMPMHROCUnZPMKIi+0fOawkTdFtI6Wo8w4aEz9JAk99zI5rlkCeuDARjA0RDokSFYKFhgRsrFjaCVTT7UWY/3sJcMrF5CtoDMikpQqvBQC2yh7S2dqoprLmBQVgHliUCsOKMAQTLtqH0BUlZPCCbT4xTaMI0q8szTh/vVnDWVaCM9OWSEWgLyomEf5/qh0kKKX97n+1zmOL4SdQJTrjdQpc8Tf+voAakYkgklVWs5Yi4lHc/g1QzA6Q50KPhIq26pfxMcqHFJp5PUXhCbmCNSDMnaRkUINrFCZMs5ChiR+5LouG71sVbIJz6ryoEHJiYr+RWKo66gAAIAYm8kALHmxhCjkAjrEARguoum0+EyOvAc664rZI0RCCMWUQcTAkGCQx+s96uYlqzKWuNGW7PoLwlaB5jxZMylXUlFi1cplHaruni8v2pA2pTVFytoMgdEfdI917y5os95L+uErhR1UzvinEHHrMg0qL5hZTq93TUAZ9wxEt//uUZO0CAw1MTFMMGlRJA3lWPMNKC9zpL0wkbZE/CaYo8w3SF2TlqpSlnPStqWb2RX8ujJTVmatUYEhfdXUrMx0d1Rsq69QIa3VA7jbON6xppooFa+iiSQDAbZLBcUmhRYB6idIpXFjUK2iWaI4IhCWPFXHTUnMPivWu2idVc5i0OcODqQjpwYPqg8jQ4kxtt/GcHL0AltMrlDce/YRFJG/xRp//fv//tg3PK//pMCMYBTQiQCSoCUaRbNKSJAF52PY/n48ZTSoes15pbFTXEOyLs6XcjVr1am70/3WrNMV0Waer6q3XXsd6rnT5lVmX7P+hyK5bDEoNNNyNtpJEqlYjRDGEuI7BTC/oGc0zqYkFaQ4ULPlTvGES5NLxsmEMkUsSF+QMlT1z4QMjOOMu5mSA3gLQhJY5Dij8JJoqGooUFyNI5Yo4gLmWE3gZhuhy1i4kf//+kFBoAAASVIoEBcgCJFDMNTmQ9fVTMW0SEl3i0J8JSHM8b6sXRVwZ9BAm//uUZO4AAywtSJM4SGRJh1nMPMJLS2EZO0ekbXEmJCa9hgh8RqegQiIZS7hb3L8ss++dP7YWfz6QOymvP5/UL7/aZWF6ZVZkZXrgigid//6Gm/9pVFFrMjmjOIBBGRqZNacdljwqmUpafMO/KIDlcNQwHJEEq3nwAwEgdNhk8Wfp6PrKtJMu4JsVWKeS3tYS2b8kBRPn7BnE8zU4nDzBkQMyGI8dITmWCLt9cnuDGpYkgACZ1GQQ2XgzNdAiKsMuXoOWJo3IiERiqSw4IyW96Jzi1IXK9xKOzEq+lCorhfq932uyq76vK1We10LRCslP7d/N/0t5Waz0IYjVBLUkr/iUSQDCfzLgRUlA/y7IMdrUP74KoY6k916hSLDrJTflZAUceoTY0o393DOAC42CLNiyMKWY85UBkWQ2iLikKygqqTW9xoAX3lb+fGv//9pKE1ujTccagWCUOZLoWWlJZAyWN0cFAvo4EJID6ZFeNHOWnMMIMAmggwwXAh+MLXRh//uUZPIAAvQu0OnpHCxWCLlaYYNIjBkhOywYcTEwpCY1hgi4MIhV5xQXLg6oO3BwmWbvape9niFYq9kmRtxpJIgqjpKVJEyOcdRmB1FwLsJOdxt0V6kcl0zv1bARiNZnnnXsmGOpCSzDWb0rvlAeFkuxSeIJ03L5urV2AESUreFe89mLlYhuf/36XzSxOj8I+eU/sITjYuX/2GW1a1v1sAjeRRJJNKAnYhZeG5hcEe9lVh5SHAO90WiEICF8MkGMSWq9lXsbhFl//T/8PblOGRlvYfuq3Mff8/v1mtMtlWv8D7j19aM/ihAaVJAJKcGmCkBvg5Q3xdxhiPEFQCTMsV8tqsU5bYkd9fBiJPDmbQCYikGUUmSsufD58+56fYwhEvDu2P1P9x7r/GeXFw32i0Y/fZLar9dZXpUipZ/dnTjKlQZwYCKSGd+nUkACwAQASlxEdIxBA6CEhmrcULySFGeFUer2BNnI0VQsCUatNWWhSEdDe7x+WC+l5bl7kRlm//uUZO8AAocoT1HsGkxHYrntMMNVDJ0bQaeYdPEhByc08w2NbW8uSpbprH7GMj5KfFbJufkcI9yn9KP+WaQtjbGCv/+z1AAylBIAAFK82TC6GTswUi7qZ0AhyanZOVCc0eAqW0riGcLXye44tgTNT/60Oe3JGnKzIuJ7CYJY1UMRjdH8ymyLFDjTo9bHVj2KTq/KrEff0X/Z7OwfFhqyQVO//SkvxRJSSbp3AGoSBkEsLgTc0W8+hzkJWmxVBB15AVUwdFZJf2/67+cON5EkHB1Ww1McbARWcrRq3pvqj6kK/XvMS2laOWNNWSkWETNzFVnjxQFJ///xaiinI0miSSAYTOVKzBQp21NHSTrRxgxAcjY5Td5OErWTYS9ybUMqQPhdY1ednSxWbTD+qOw1noXcga7GOhqKrGBTApKtsXc4YpWtLSQ3+xYg//+gIBVNlcBNgyzhArBGCydMSMOlGBdCAOA0E0mk2BtxcgdAZ5DqcpkENzIYM4GIDQMeRF/W//uUZPoAAxpLy9HmFHRWqOlKYeM2C7EhLawwp8FYo6ao8ZW6l7L5JgywFGMc1MhUXqoexbVOTgAOGFKFQOC23JGkkiiVSkfh1nyHgM8N4sZyBFCYIbQ7jCJXHYFGzy1xQkjjMUS/XDYyJSHjGua5TVyGU+jfK/jO76Jd3JwN2Ch2TIdEW6K7ribaVmWFHUT/3I7JY6m3SI//d8aAUmqQCiCip3gNIBFCEA4DyWiWF+C6N9linM3phyVttuFPMs7Xp3I3LcZpSmQXNu7dtzMzPdSq6pUZZSsuvrrTpVftW1qOja2rauWyDnMVIgeRDX//60Smo2miUSCaGeuRMW0T0/gGkn4aZdl0bJYYqVYU6okhFZ2CzxpBMCu8MkrI75jxeu6Pre21AhKuYCdDE5PYq86WWokFUbRz06uSIff7n+8QeGCCUcwGWAjgETrjz0pwG//+pRZ1CUiknnILrqGgIqhZSYHUspklyqtFnQ4vyQlfQsMUubfwrNciboRgeDYu//uUZPEAApYsT+sJE0xMorl8PYNUC7kjQaeYUXFXpSW08YogiaY506sn/nl69sWIRvluXfcjl5O5f9Q8jPz/y9DTqZfymgBT7Q6jmP//UAAAAeTjjoAFaBoiZGMaYCasQGLCZYucQh10k6LCkUkLwgAjy2mENYHEuonj+7EgDrMlyRaWjKhzS2Yzmx2md6tOZKciThBFYQwGkQsutCQS0UDYIaFnuHlGiBQOY5AE9KMnDPe6faESN12jikAyeLc7EXwr43y8vLdiAkdhI0ENL7kAIJ5cZbEwAd+B1ST/1pACpQkBYQkA7B9NSVQwg5C0+pTYUirfNCpHLpZ+et64bfuvgcom95CkZKpKoNt3rIpLsmxQ2nF3tSVM5wNIxZQ9v7uZEgfQqgAAABz8/ORg4QTVIDnQSsDxgxxeC6hSO7Amnyw1I6VUPpVPziBfhZKttgogQGQ0qNTZEpFLDC2PRTmxW6iMhvoeDu0P0Plduc8R5bLdD//N1obJts74j7Uj//uUZPWAAxQ9z2nmHFxXKXmaYeMckh1FGm08y9EcC2Vk8wWorJ7M/7oLAxTzv+p9+scPn/MAMUBCRAgwzkeSg+4ppvVOuG1ks410+tGo5MtBMzSgkRM8QmlCZX/vJBm8NO0sU5H/vsuHv2KAKlR/j/EMCrG1lXy0JoVCpewSHHQYc+8qJtFS5N10BoXh0rVhK56qOpr+CzK1LIGcgQYQ57sJNRhiGt52ZL2sCnZmTWr5TszJHK996kWcuOIpMJZLzBKfeZFD0AFoIF6R/hKQunz4MR0LYIoj9UgGCMYLgWEVl2i5gWHHXQ+gqbTeNFltSke9aaQ5n2///9LVTntk6gAWUAJIJXDxTYPTWFkEKhJcsFKpk8FGHEnyfl4Psf43wYq8xOrqBnUwkk5ChB6Ck82EZfJxRMNhRXSKSIEQLdPOI3v7ajuSUr87H6vU/O0lxkPdr7bqbcxFo/RV98vELn5nrc7d79+eu513r/m3+lajtBEArQiDXdG6nVxEA8zj//uUZNqAA1A3SRssMmY2RDl5PCKKCyjZLwwkTVDrBGXkt7AIJbR0pCHIaoJIZzMh0R2QPRIqVZzP8ubSRDM7G5mfz/94RPdRxGFBXP9b+t6e9TQ/osXmBwBOvInnErULBEcxSpNUmgga8YksTAwcCSkXQlWyB+2IU07GG1fekrlUwOD01WG68SGEmpTpUFMBcsZL2TFE2ls6rzj6UF6znLauoePLpnmmZ5qF9tH2oR/f/cZ26HSlMos6psM8DjECKofXwyCbCXUi+wyfKz5Pb8Y+GAiL9EqtE6CSHyISdx0Iwcpzsr3SlfSruKxs1X0SHCvLv225gYHsjJzt60glAxAk1hUJjYZQfUw0fCDLOaz1ryLgaCzQnJqC9QIAEDcDdgxyDJnK1EdEFShhUjlFXldmCZY+kta4IF0fJHg2pFJpGHy6rOPYhl2UdqULcnGKct3p1tzRUUZWQsXUZ3oULiZ0d2sR5HZZWKyPTRkYVvGi8dYpbHLGkmrlukAoaRkz//uUZO4AA6tPSlMvMdRGpHnMMeM3DmEvIA0wccEdkeXk8IqYVXhnFGSxNi0ssYeiYS1IjnSgnKoSpdc8kpoFooEwaSYRE4UXQNiF4gTDxx+yCaVjFD3MV3/JlNRAV3jKQVqkJRJSfBFY6EsUWDVQg1bsnOwcDtOAgFjhEJZicoaPCZHH+765d0WQ8LujuhgSKhiGCg0dDrBOUHYL7LpdYIzuwlwT75XZJqvSyAXVnapL70+5PNDpNM//1AKbNRtMggmDBLRCKfvgWJoBAhJO4nQLFpcmovbX/9Z8jytdYQcMi62RO/b9iF+r1q+puvXTVbX/Wt3Z2gk7WmrfOp84sSiiQMAQACud/YYgOhCnwYkODkAALKmbOwB/nkIo/AoRDIrLh8VHJAHF8sKU65tn12+jX/u7dnJZuzR6tFTqgqqCIdwbFDM1RCkceQwEhEFhM4zFmbzcGIq00K3rr26FPz7/C53QVaD//n/V9oaTkrRCSJAV3JIeKzHYZLtGBXF///uUZOCAAwI5SSsJK+BBAcmcPYMqC1UXM0ywR9EbI6a0wYpVLqk5ESiFIdKrozyoz1/6pVAAHOwDZhiRkeZC9yqUKrFOrrNWsfRtNdev7/36badkf7pHwwln/0aMl44Vqqk0kk3iajNBYSg63V9szawup9XcgY6DRABTCInIUImksvrDnMa1O817XqlsaYwJGKU0AJh1CWUOQKVrfLc2kKp12PbmatNdLfuDStbkre2dkVP/9DvQExJrbPmWyCwCAH8X9JyxkEjGd3Ic0U0H/RYWQFl0gvfVHPD4YMfEMUdyHQ4IQ3pR7rNVDvW4fXCsoRoMBklVAAggAAhF8J/FqWICzYgFWdVQQqTcK4XgdXMmxYKwnasWoZgV1xzqG04keafoFuxCg5XAe+RgUNRrxlFCjoj9nWh+93R6tUQLYBQJBcQs2mT8mzzYvkf/2M2fUANmJCBAT7IU/jSCLLWpRuOA+DE2sE4NlB+9AhnS0Qk4EyO7EyOdCzK6u7AzEddL//uUZO0AA1FHSTtMGvBO6Pn9YeIdivztOUwkS5D5C2cw8wnUOtPOkjovVV0f09F3f/2b7mBldrrZDWXQliGeHe7/7qyW5HIkkkSCsM0/FUd4hStMgLQRpDUMMs9HBOFtRkcWC4kLeRjoZGuKUVBbMNs6alJsdbz6INlWS7jqvmf4uf6tr4/jW/b/nn/6uk/gxHnrur5earWa5lh91ECgoGP/nrtbsqISWnESkm04DnPsDRIcUrmlziNJyOJ4/dQYCBaSAWEJDOdfbrVB0xjGfOepqGMjaVrVGk1Hjl9r5NFU5T6Kat6N+n1qap2pJJNOt1/OVI9j5OAqBLZsWa22isdrtdkttuVgTOCyawxtjiAMFjugcoCrCKdYUuCARiYNBCMsH08ADptjqNVAc8KnIJFAUMLvFrzOiw5+LPDkaDYWAEJVuVSb6fjUONWAJGRposOWI1ZnCzIztchryJeFgzeJjtQXI0tqrpwc0B1n5ZJGqzlMEZ48mccgWGZRi5MP//uUZPMAAtUwylMsEmBPKNlaYYIqDFUnQbT0ADFHJSc2nnADVICdWW2KsZpnEvQ4pm379pyR3Cmfa6/0VmpBQ73L6OOelwxC9EJRFO0t6LVPrTD/U1qdfaHZ3na/85qP00PxuUSf42/HD/0/gAABWJWBTCGV1BQen1STyANKVKQOokgl80k4lUFLPBxC75MSGFd6ddWYBQSmbdi94/ja0FkiQW6LfX8zcNI67I4FaVKpx9Xrl8RXfews/yZsQxR14X3usGyw1KG7PFP3n+pqOxLeWI5DmM5NX7Nqkr1aeOXZqll2e5+Wy+xC4h3letd+zjjfpblF3WMgsdw+jt/Urxqcpu8llNXsbp7veapedwzorgPCINf/zYCTu4AAAEIxEX/urclsucBVADQ4LDS44DHmRDGKHmXOnSGGnDGoEogKpGxvNzCuePGkTGZC0RlgEWGQNYuyNPwPecaGUrnCb9OF/n3BqzTdQINodMANONMaiEhwOHh0ZCFjbgtdFjpE//uUZPGABoJLTu5nQASq6Ql/zGAAGGktMfmtAAJjpCY3M4ABDctaKuYGYC0iCIIgZtXvc+Psn3F31r7oonQVIey1CoJh3Pd+mzzvXJytQW7nurLfppvLt6tf+1WmKevZsfzdaMX71bDHH/5Y5zWd7Klwr3Mr9mperXrF+9sl8sCe2AABiQBbpGUk3JUAkM3ImVHhkvpwFtG06YY6liOCO8MGaDEShWUAUAtbK4EkaD2lAS7QC1ZlMg5c+/xem8oH3cwfGYh5vpPFaSvGajd4flD/SulhrJ+ZVjT5T8vvY/9iUZ6tU1elpd8z/ta/zeWv/95R3PmVe/h3+Y77nlWz7jlyp/39YY18b9nVb8se83/9xpqlLypA9epa/KkUiSTBAQZw/9n4QZRk1ej8OJLOmw7mILUjQBhGHnCyyKcnkGIIrEmPF2NXtDBq11UJdW139a5k/6V3/CRVTXGqf1WZdlY2uYqZ8dhQjRUxbookyBSf/+sTK3kVtyxpwWUzGOMK//uUZDOAAtA5UFc9AAxDBAn954wBCqz7P4eYUPEECeWw94ww0eqi8ozchKJWsBkUM9UtFAa/19cmzyjIGMjVTn5f/JNlDjbZwGHWzjvqdusy405XSmPjQZMvKRkYSdUooOdTA2hGh61yoB1gjJyNg4DhOBQZckBIf1ChZ0gAFk5qIWB6m7oWiONXzIZTghLsDY1muGV+fQttXLaU9pK03zF0pVdsODUSnMNQqxowpcAAIgAEABwWAg53BdlYoDshpNVl2U72c/9AjLsgJARoTVVWnTLNhQCEwlDQVSTsNQhmL1GfQN+3XVQRc/QhdbJBNSknII2ikSCYFUK6LiBJnsYKuFrOMg4mpKBrAWWdE01FAImL7mrEPqwnVG8OMaLhmZmZbGLFGEQ5BGUUhGQM6mSmjXaupehK839+1qu5Kld5SaLo52BjkR7P/1AACEtAAVcD2hAVp5l4ORGPjKP0u7hAngw3pjpl08UAYa3LMYFsbe8aZuV5J0t9r1uyOgKE//uUZEeAAtVDz+nsEtxF5KlsPGKIC0CpQ6eYUrEDkCm88ZpWEEbE2EFLLKfqfU56qlO94LlAk5ao2kkSCqXEeogwVAfx5ATorh+DLFIG6MI9lKxjRYzzVyVkZW5R3pEssYc+knV1WGHMxkrsiwYGdq2laS8usht6oExKbl6wBk7iBZwofuCzegRoF//9X76BIyWFNVVuRAphkftxwGTCxOkTfAbgLChULXRqd+E19bfRRMrv1aW/0Nlc4fs0lOjFY1duyrdRn//T+c+wWmDoBQoAAMAAAAVT/WEsDVKCoKdymjKSsBcLtK3ZMMf+SfLVEXLXbDSxISvKPnJGFMtB0Sf0qcNwylvtLVVdx3cV3fKKOmuVjpDiqmQ17RdewN0SztO9V5HV7B45GDo2gADiX/9gjVbEcaTaJgLmykoN0kDUyosFoX9YiEkQfTPi0aXmOdV5QhrkV507n0MW9Woi+ap0TiTg1yI1T2rSqUqUzWp7Ix9PtR9XWqXSecK4f6Sg//uUZFeAAwU8SdMpFFBOCQnNPMJZC/jxSaeYUPEUC2e09I0cbdtlcjbSKWQOwzycDtL0TM8D1EmXZOlktUmwGwlpUosoNLilAgOceeWDlKpnNGFb30VtaoiBHVwbOe9Vd0ar2alnoxVvq9WItPa+yGuoJi1Mh1RN+A2hBQT/jrm4p6XB1e62OuNJKBkfkFIQri8K1XjhTCCaxCJxYNOSp4dLHz4dFk84V5DHARssuC55V4oBFJbOVKxGKRZd6Z23/pxOBCf4SO21ADSwgkAp88uGvpRtqtwts5boL2f1mkAj5HqYeGhZTE5zt4GFUcMIqKwdnxzhnrugI7T4xj08s/uSxniLfXiZ14n8KFwvKnoscvrEDrvIHhug8xJ6SJf+sghxNwCYBBmtL4hgecwgAKSgmqKNwifGouSt5U4YXE8CpNPvqC5RgEdDgtBZvkEs//sp3ttQowG3y9EVMgDKQCSAUuPkDzoqcHANqkMhAvSC2mNfd0dKKh+JlCXOk54h//uUZFuAArw7S1MMGrA8YXmsPSYXC5jdK0yka4D9EuXk9gxgYfBU4mq7NhKqkuT9uHMcS2G1hgJoWbnnZwszF1c3dF8h5zv6dk2yL3CxzgmJkoNSsQJF8cz/+itfWKLEoypYI8gRejuDwHThGdOwokzBbHeu+EzPtUv/IQTIOVjHCYonC/Muc575XgAROaiY1nc136Uoar8Va9DVUgABESAAA0H2mChmAskbEjKZILGaBSILE4eQaJDs8RJUqWxn1YDQhEUh2iPKGu5oakd0PoopaZ/kRFciElk8EkhqUI30bzsbyIiPdfslzXELi9srruY/uYUlkubiSKIQzguBbVCZZOTdH0LWhiqRt3rW4qMv5vJ4HyM4k4GoaOZYd0btX61CZQrpsa7rYj/+0JGlgHWiCQAWuZDYSEpwpFvDBDVUuumyNjzav+4U1NweywDNE7PLY4nzbSl7b2+DZ1ZghMkg7Hnfe2lLD7x0LbPqGYUiO9PKalczogz8z8vyBE4s//uUZHGAArk8SuMsGkI7AkpNPCaDixzzLUyYbwDpDKm0F4w+h4qWD7Gv/+kRCTayRxoBMFJcFZqHvYw5kq+OtEq5lLgazftnW3lOZOJQyMEZ/ZPYZCwbu0UDIq93//6yXSpuuF0kagAeQBAAKfVAJvFmGnKHqZoGl6EIZJSsPizk2nZf+XSp5L4gzgopGkZdkaljOyYUqcS6c8ainuczdD9UNVWdgUpcrVto7EM7s7d3OyF9V9siNxtsON//7usALQAABwLwBPA6ExBvAHq4lE8mlQbKjAeoiokWfQFjoFMijwWhN4JGh6HECK9IqWdDNFLn7HOZR7GW62zSJlZR8wAoEpQCJkyIMASCfHoTQyySpg7NgOjqT4cLK1kzYbU88BsQQcszC3IjEDi8YEKrW+cQQzr9mhGrLyjRM3pEfC88qZJJXPujH8uu5qdl5w//801JCGNANQGZAQfw+gZIZI5i7xhelAnUPrhQMcEoJSsOcIO38PTAbszis8z3KFPT//uUZIyIAsBDStMmFDBAYWlZQYkYCrUhLUewaQEJkiVk8w1QFo2bPYehQKooBJlo7Y969ROnZVqb9CWVEWpUkkklQRFLkI/KuRlHQxdFJNefZM5TIYcf4PnA5JiZX6JBmuPQtrASpWIwAhBCDJ0FZ3Bq7DiQ8IU2hYmdWLIYCiRomrdsEqiOXe5XT0LWl//0xyzz2WSRtJg3RgIpdF1jkxTCguFUE6xcK1jOoUIJqFEHGvTat2Gt+VYQskEkTA+/o2rQ/bpo22u+kmCwbegET0AVFgwpYI35KVt1RNq1hvs5DDL4QqGDjRZ6gxbysHMZgMNYbaymzRoSoYgUin4+bE/6hVUpJGtkP/9/KHnf3sHGLPkGyz1e6zHOIBAaANLWOwSHTNEZVKUziAEQhpkGE7Nh/ZR+O/yrEMslN/KigqjsQ1RiNl4JBUKh1AFZGx4mMzOmeYsCueTIpSzFHASNp21OCBImAEKEWkASSSVYSXGJnoSlNplJtPBbcrXpZfxg//uUZKIAAqEdzFMMGzQ94qqNPYI5iZzJKywYbQEvDeWlhgygdG90UjA1Gu2wuWPG/MAerWr1goAzZ5g0IyP6hYPk5iVzJM2EKhMJFmplUVKxRGKjTh/VehlrA5/+2j9KzYIxgDTDjsZwGimkyrjoSRqkuUyqDb7uQOi7/UjcEHCmEQjP1QIau3uOtqcK02EvR//uya1qoeAjG2Qkkkk5h6BMj/HqCNh9nYQk6yEFxJkIQiGpZ7Agi8RTEhJpZhpVtsaa3CSlYDqpwbVGR0ee52Y/cKgjZbNvqvo+ldF+ansAUqDfy3QYS3/+z56gknHJrXGnEyEKFgECuRysf4flJXosbGjF4sahgPG2guQSp1+BakFBonNOGMaioVpEilEmV0///80mAAEZAACA4YwD/wKce0WdLjosMCZL14WkRV7H/t0FLEcqg5JMHch1ELuLYh6NooJJIon7rodoEH2ZdlZLvM5INiXZma3Xmu9Cftk5yOXdyol301WTuM7xBwB9//uUZLqAArEiS9MJG6Q2QrmJPSIoCsjzOaeYTxDghSm0x7DWQBRHGYDiMxBF3Hj5LAqRhmuXpycYR1X4dtHyCgBpc298pkRMdOGMQ6SEjnLdchUjahOPa2+91TO/dIjnyjTIujAEEgEvjgAQ5iDKk0EgkVWLLtdcbrQ/JxCHcWMwsrYVPcSIKJgWe7h29aCslfBe33TIY6NMjlhgXB2IhQBjFiNY8rclJkLaxYxniKL/LKGnQsefCLP7+gBLQgAFxlZbIQlYPF31AbGOhlGORgdELgxFiiHd5NQFQ4LgwcShxRYxZhyXNUtrXV1rJ6PHiJ6lM+tdOadLHgCGboRqAAkAAAAA0hKLaBwyBgCM/IVGiauNiLsPU9vLQiOtoQUQLnxETLTIJGXsd6JBLxU8WeYtAvKdfNa3skIk4ZFl5FA1HA2J2KShRQeTsDoD7yabHp0U0mgCDQ539CIViAAAArAFoPkTQjxS0PaC5EjIouDqtISsU6dQUxBQxFR+bdmZ//uUZNsAAsBIymMGFEBAo0lpPYMqCxR7K0wxCQEMBuWlhhiYgypm5YIaLT2mR5Ndyp71ZSXrf0MarUfRfl5Lps3UwObFaj4nepzdQLo//9YB1SCsthe0aWJCS3plb1dRFNBRRp7I3re5xGvv7Lrc8/c9KZLLbozuDLiEVNyBszRiUweiNlw/YSFXdV3HMgUnqumuP+n9Vcz2vdxhRJGe6oysnYqGQInIKXGgTDAAiSUuFEIuEiEyIdIWFXhJlg4Fa8jeFJr2XCgWCNwsP+FTPqcgIigxhhmHSiRRQhcWZPJl7lPl1vHokKmocfWCLmhUXLECLY4p///11QAHEAACACQ+Fsj9FCIWqAk0KimKM3QvoXAUBgEVBVgiKj7aQqaYm4LGFIyPib2zN8Swpb1pgiB3GVNoCKiY2HKJJLbn/nS6P6MtcSmUiPVBzWCBxY5aVC798v/+ZFzAQ3uOo5//7t9YAHlIYBabSoblAjA5VGos/9CKoZKHRePyKTtb255n//uUZO6AAtknydMJG2BRx6lKPGJ8C1EhMSwMstk7DOVo9I1QLT3+i3SzVUaDMC75yKEJZbD4uaY6l6xKyzU+7DMN5iVdGKNiM6fxAi3cpSSSTojY6i+mkIQfpiE5C5a0MJuwol2jEHhoeKsdmkobVEi31PIbFRjPCSQ42CJPmkmx4y3Smdwxm1UmHdmOyMCXQ7qeqYhDKRlZGb/Nbv+rtbcZMWAy9YOf+kERtgAAglAFotoM8GyZabJCNjoVWGilAQOgoxMAGhVnm6tKm/zsLRisc9zMBWPcKAptJMYMrDAfosq9p8dIjTS7VoxWBVGiiBYOAcQB9QXr1JaSSlGEIWox+CbpQVI4SbDIUZJDFTuJU6ez9UK0tyU9I9VtEth/49U8pUOZpI6CW5h2+ne0C0PZUUJO/y4wLV1ZypozYR9lRF3+76qyVp16KQ4plShdAxX//orAroIAgAlc6gBOAqoQFcSM/DXRKXMUX7hhs0zrayDNzFGkl3WZFlAPhkj6//uUZPEAAz1ISmspGuBGAzmNYYVEC7UfN0egUVE1C+Uk9JkQyCXOnL7RIEyDmiohPy1h2EJGeUMuau9rSpK14GbetRmeMt/9vmlCuzMSiSk6FyC1B/CAJgG6UopAm4pKbyhi4ORbs31Q5DAxMpW+3QY2eVOFonwz13T5ZqSWK/Wzspu5UkmpfxCM71j0dXqYZSwWjDJZnDS/VloqN6UC3CI3/77F+ygBgshQoABhoBLBeCDIaA6j5jiolkweXCntPUMtHIFR6Q2HuYeZCsC8ErFVN7mtor+uRudy8oZYe0TzKAJGmUdP4oynf3h4ylKVWIJJKVDBIGmUDMIHLAEfxxi6FAQxaJCUhnRTW4U3Yozsso3TowCdz3IV+pd3crCAwNaqjCGQGCQI4N8MZ3wGnpVFAladoyWptXS8EjWvEAzoot7TuRTQdCSJgU//e3NppDgDFJqzFhmC8Beg5TJNY3VOiIrFaVo8GBVy3ReQMcR141mpEogwq1UUtXuljKqr//uUZPKAAvJMzdHmFFRRIzlaPSNYC5UfM0eYUREakqVw9gzoKh6pqYHLZO4rKrOSJCQcC2nf4qPjGitQql4DE4BEmpVEkkp0HMC3FfEYHymxYhphJXoxjwJIpFCzq9DAlAsikYS5EtMwJmDTktrDGo/2XOpvGw3ICFoUkrd1S0yiUtUXqaYK26X7+rdtHrod+ndW9sqQv/9U/SgkaiASAkS4AqjZE8A+oemiSh0FggMpYoyijM/pvUkuvcwgoIzZHS1sppbyu8fZlswhzDVUl936IzN2Zlf+v0V0Yp96e99pEFtS1VH/uSSSTeFCKMcYAuXZZneQoDQHyQVSJ8hJnPDSYWEY8gc4mWlbCwyrnrX+CkFUfJ/GB8dIQNKYQDvn+gkBq8MZfFDk9X4fNSrZ//580ckcEQhFjHQ31ijv/7K/oUAAAwAAQnchWSkFuPOyV01b2TQ8CBCXJRXbD2EMUaaI0iKvQbaW1qut701ElOYKfXDSXzrZefIWYqC2IjC8//uUZPcAAx9CS9MMEmRLZblpPMVKCyUjM0eYT1EnI2X08IphYZcVnB587a9Ls+zSpJHjw2HQgU2jFNQcoDQBIEwJpUt5ZFYphuGCeZBkOgLKezRiP1oJw1JopiBVvznKec3J/vlOUpVb6ZarnNOO2JS3tmqSHPhVFMWJoq3hUVxeJ0x9y1p/kKGGDofvfBUTpGgEd//bQlU86smkVVJJJJSqGlqbbKELHKzw0QL8dLi1pOyCQ2U04KCqpcUpfOtXe5attJlam1aoTTnBBci6lxFVo2IaJjRQdaXNYjc604Hzv/2Ft/rVBLbjjbTRIJrU16plQ8hs5aV40mBFpuFGpG1F5pbTvo7TN4VgiOtohEwXMzbPuKodbmxqFZrlCFRGjct2tnsE1lb7PkexDOZy+idSTRxgnDB75XfYtrqpmRGLsTIx46KXFHnRnXTphxL//QuyR2GWrFwAMEAAAAfgMeiNMTvdqRw+5D+P3ZkzSIGfivNWc8ZLErV/OiWO1w4b//uUZPuBAus9TlHpG7RPJNk5YSNODLUFN4eYc1EskmZo8wnKJNc0NbDVtO0gR5Hxp2TbBy3ft+r6svnR03/u2tlTS9v2nwhiH/1FpOWRtttEFUYTmGOLkUAtYkyvLomh6mInxZElMI/1ScCmlYZzuJyCjoJhAUvIIIlCUx4IECBjl1WTGIcQIGUipA1quYR1JN/cj97KVXUMsLdn901Xexjp5fjRt3ZVS17s1lRSD//9NaEA+XBRbAlikACRTjxXzqVDzCIMhiTqEH4ltwTsYgjo0OqCNn3d41M7aJ2ZNWvq4QU2pO1d329Waoq6//B+3/p3nXjapZbN/ZyCTaoJpONtFJEgmDEUYdTgOcIeFYljIQgcbGWRcD3f9V5ZnmVQ2WFGMQYcUQZTwFAoEte9JyPLOSYUzFbA/IxHR688uoT3Md0n/das3q/mufNKTyrvWSQraPUVF53/9gAeAAAglPukxwOc3dej1wfDMLeZmMCwLGnVpL1n7N69hhlq/v9h//uUZPqAA4ZGz2sJLGxOqMk6YGKaDQ0fQaeksbEpi+Vk8w3R3OdUyTyUbJCkjJQldoJlkrOQGdHyG65TMqIuWWxVmdx1lKMlBM/+ooqORtJJEgmiiGKStHspIx+GiHErXI6i2Fvq0m8qFYplkQE2XnrE6sBpD5udhx/bcKXrZ3WNQ9hcUoz6VqxlipFQGTRDJU3l/Pt+VzjzC4hA6mHlPIOHMaHv/RR0bkoDatkdaaSRKubB9DNOdDVymgVTgqSCphowdbJVIs4I8VdmNcvWCcUNN7Qa/zy9v5nSgLpCdDGstMHB++lOaHnAXglJD6X3/mkPLnVp8y/POyQF//oqCUbjaSTaaczVG1S8WIy9QpfJMZyHFa611XjtSSFR+BZBSUgYcYFvAAWVuLwgWTejy77J2myY0qBBSZjzl4avf0rRVSBwKVFQSciqwuxmTXRlv23ru5r6U3Y3beJDTGP/crz4sgGWmACQCXC7gaiwB+H0OIMEOs+C/j+SDWTghOEe//uUZO8AAuFAz+njPNxNaNlaYCOuC9jzP6eYsTFQJCh09I1esTq1hYlBrMtdWy9gwpBwzqR0ewcdCFZ33OehKF7L2rZrWt+9rC35NHvtvO0rVo68m3bdoyAQof/++tTqpSSUUnk6Eui+UTcxeZrGhEwJUo9ifHNIazOjk+vbYml0zPKysfox7rIr4DyEAxNDg6CTJiMXr9zxjIGblLKJyPLbLaWHfgml/7JHUy8i/2ipbS39E//8jMd3/kM8x4pJuEoccAU3EggAQE5xugVj/EbUpBieuk4WNCjlelvQ8JIk1lk6LaRZbUVSis7uYIcXI62XzGsGvP+mmm+nt5HuDzV0uyLyndmIYhGJTsgMPqATlud//XUAKJGAhEghTihQqZLJ82cLub0tel4wNcOLaU8Yqw7Vd+9G1FouLJk4vIz1iMUrHLcuKtJOzS7HkWtzVp5N3Y7O5WbdFGBkFHVYaM8+gWYmRSbcswUhr//7gAZBRADC9B+BJTnHq7AX88jA//uUZO6AAyJHzusGLFRXCQlaPEmujK0hM0w8aZFHIOW08wmYP5KKZOockYEW1AcJuxCJF5pXamp0brHdZZ2vU0zPRadluTdX2aj2O1MxBQPa7VW+vy5tVxUPqDIgrIAEgEqAJyOZbwDKBrkSMNdHiSgQI3zFsqEWrBcDoVSeBsdKDKeNw3mjFSSTVRLKgaIzFTCUJqp9u4HzDXXpcJaxVwq1XkM1P7D+dhS+omzF+LVq5EOH2lCQXLEgCtohd/9icXmjWgCoQAAAAV0nVCICGnJXQ6yJe6ezKlmv7VpIMiHyFPgRKD2eIQdi7LLzzdzGFUcz4zTTqRi9rEo76tBl7EQ2jOVtNX+v3p1I6V6Nulq/Cigz/6t5O4UlaAMSAAAKDQsCkjaqTuJDhF2BssaQ7rhQCyWwiIQoFiQ6DD0RskKJFozR2zM3aND2TSKbQ2O+BUzPcdFOVz9UBFtzmJMQrCTZPoehPXSt/KNTrRfkozu7GTebICiiYACQbMmknAWp//uUZOMAAsIty2sGFEBJpblJPMJ6DQzxKUw9A5FbIuTpgwnocQzRvRSVHE8RLEiHShYFjyyqFOOW5zPk+MQ3JADJskbhqXlbW9Psn/b1vZfvpdnbTlD1l+5tSQCTEQAUSCW+imkI01K8RtIgigVLmqQ8Go4gmjJouw3VLJgcLB70UClUS1mbME7lkTdmhW9ijUUPxopdmLXV2KtURH6yoPNWjL079mczlRHSoQkQXneeRnfVkkYcxH/8VEKNrKACJIEGzKMc7A30++Leu46IRKMe+VZnxGzGtTwYJyvpswi3IkXM98mpDjkF/ksih34jleh5pT/+/XtKjB1IgG4BKFAAASnwiAQlK99U93qWqlSI4R+Ng+KyXSoJik1OeLFvtlIafJGm0xNjfOoYAgoJUkIBBEPDTYMFLnvEpyeAbbhLmjkrPIYlCC1pd3V/+3+khECAMEkEFTqHtBHBrth1r7PdnahRhDbAi/79HHTJtC6A6xbHBfO+5OMMYDqhgUSh//uUZOCAAssoycsJG1JCp3lsPGKKC/kXK6wwqcEMpCZ08Ip8YjUWCF0rouXPWMvcAcpXqF0L0HDQn//96i1KkkpFSgZ5AMu+k7CXoLbo+0TEVZIGkzOpBjInofyAp88gRgIAFhDkiENhPEWr2ZHadLVjdinqfulS/IThRxmxxZAs581Ag5VLfqn5r29dv9VrH17Up+u5YbBQsOlHpp/0IAFAgAAAGdYoKIodEWuw0tVYgV4Cp29/GBler9EGz6+hxePFJQ7tMcReQ7GbazJv6LPd71rmShnN77Hs7rXu1Sff1slmLvaklhSQUf/9f6+mKJUcSJRRIBidKiYkdOpri7mas3hpwUZWqw6zRtXXnY9hY2RHQ8ti2WR5Fyyz2zC16dKMYgS90zzNZQcY8gZ3qgwzp0naXOo6KZLfIy2PEEYZFmZBCetyl//2/JiBEiAUQC+yl8AWRX0UW0exolOuTuTe4TKYj1qBBMW96FFsyZZ7T96tt499JqSflS6Zh1Nd//uUZO8AAo4WytMMMcBIoultYYM6DG0hMUwY8dFAIyUphgjwKurXPa5Z2a/nXr9NPW3rglDJQSVPPCswXYt9f/9HSgnLI2020iVQMZooVc0i4kAP4eJ7DhNhIoB6CRwhS0QTeqWI6liK2dWSsLLObvWmalUNLvzCV3N2NTp4k3SzyvgAIiTUG1b3Jh2QHWAkhy2PCrQZOdIw0+0XA//9OXBLWAEEAlQXMR4mCIYyWuaiTwB+XUTliRjw2pVVHRLww0YXcG+S7nCkFrK7UZN0IHy6NXb/b/+vz/q7+z0bS4+nR22ru+JqRqvg1f+v+ioBv5C0USnSEikF0EyHrJUH0DhB/F1bDCWT6DCJQqywMiEoYUWqE/MmBYb8aebAdrBwUmgWtRsEuU5Eo2zNZ3SG3HBsSRtGa/fZqsqfd7K+gghWd2TZtnVCeRzCQX/9f6UAmKAECQSC8GiTIfY9zlRpsFoS9mq2qon6OkBJmp2v3pQUvHYNckLsDcMYWeZHura+//uUZPYAAt03TusGG8xTB1lKPMJ6C7zBQ6ekbbE3JeWo8YoCwqgSiFwBbpTINpfs0rre5hskWbcHEq57//9QAEKIAAAAIXPRoSvFcgMaCRqBIRLGS/kz0Q+yP3sCoFDB0bZPnhIRPDpZM9kljUY4/fKWco+oPjJjA0Lo9ASCTo7kdKDhzBylaw2ecd6OlA4qV0BNlROj6g17Neif8w//9T/bwCsIKNkAAgAkvqUJSBFG8S5DzucQvwqBbW+x9F1UyKlYiCoWdaYJCOEkoQj1oPFVBBpulUUpKpKrCCWoSRtIFWpOKgVKyRNthlgulDEkhOIzWoTkf9MAJxpkpJJNOo1sfTTdppTSw4kTZUPQLxomAZBuTi8PpgQxSaLLJkxzeSj9KU6I0pDaF29xyw+W7K99F03T+xKLZT62z1aXna/y9347d/Ati8WRlIaJIcDCjA3Y1GAb0P/+gV/cCUkUnXhRiGkEHWjT4agogghJCMnYXBD4jPd+p1AuVDwMTLZm//uUZPaAAv5HzFHmFERL4uldPMNmDK0hJawkTYFPjCU08w3IAcxmDreQ+u/TVrFRbOtWb/9WGZiu6x/d+ypN899kc7eaKSLq9yogILf/R1SVfKiikkqswWUnkhW3Ru642Qo8PA5TjoT2LOw+0ALGAR4CNRMKP88BEFxiyfxbDI+B2RczHz0bd2M7MopUBWgAQLWFLeUGcg9DvinulZCu1FQatrRG3s2L/+mhAhPIBAzNgYgvRbx2rKujwo6Fi0pkU4MUC22rbuizbhCwo49n6LkryerP2/mhlGKaaJVsnfaxR0vkpKj+rdWPkrX0fVEppdu9j9dB68W1UQMlAAAAAL4wRFceQcgMRTGQugxw30XfK4Aa4111vaxKn0polRSaMEDAUkMIj9c611wcMDuYnZ+edjb5890+bSqqQw9M7jOYr6ObeQ4xWZKdaeyFV3U+q3Z2bt3rZgQZH/7TNKxmUlAAAAAQugKUDAjBAFBO3QUgPCoA4ET4ukojltWTBcj1//uUZPIAAwwzzWsMMqRSKcmaPCWGi2TxM0wYT1E3JSWw8Ynx2LaEELd4WgCd1IzKInmY0yPz+clKXueX08z3mdFMaYxhIVkrG8aT1eBbBgmSc//V0D1UqSSSk6LNM/BoKVRzKmMOFw28XhejTJRYJQtfhGYlNPkl7Hfes/Bf1m9DNJWp5vdSi25GdJ8sP/offz6cUjIpshE3lZiqZSmPdCMXmQ2EnE0hW7hrLO//3dymsAwN9fNIEAB0Mg3CxmsXUNm3kdAwTIDrZ1ubU12HTkVuZYNyMtxLD5SNDtEQB6Bk7uJjwiGpfjzHf874W/frfyO3Ng5zXvBjs2lHUqo//q0kiSTESXYkIqZnadCb7cnwyaQ118GX2BOZDyxCovFrHgqVGBOOBYGUlXnLVbPHEyxbzsUix9b+kj/9dz9rCizOtGnotn1VIklEjWK3Kc48gscEihaBp3DDUiIoSUHCVpKflUtQsod4DX//cTuIZBACcB+FwFAc56GSECP5CKjf//uUZPGAIyZHSesGFMBTJelNYYMoC3TdM0ywa5E0i2TY9I0peR60o4yWjbzn9/FlvYQCRkrkKwNpmV13W/58P+c4u+OD5Xvkb8LXt7jGn4L0tjtH/oH/pvGAm1ea5ykJOEE8I2F8h5ejWL60MhwAygJFiaKrVv7lfBbF8ZKVlF4WFGuYtnZybqzreVmSZNa7uujpT9ifaz5yGdisCBLn5p+3NZUE/rQBERLJIBKbgORsCg5J2WSsBiHkDix1HZJKOdWo2qoIqQXeNqmpRCRzWwgLVZRRvebBQ1Cz45QRaYUmLInRVi6zEHz1bhGm02SkkmnMPOdwEFWOJHRoXkkG/yvAPGYLjyUBG0d6j60+/DO5XaI2IYKvQaqYEhCBrXcmQqzzjrO0qodG3ZaC2FuLClUzDhzOXI/9Trq1L70a9/reWBvMvFT//bsv1CyO2SO/psymhAgNqnnHHqg+HX2YlGG4RVxiFjusojOp+Hb4+f6plMHfTEW08rrgn9LJOXkb//uUZO8AA20+UFMJM1xFqAl6PCOsSjTvPQekTTkQhqX0x5jAP3N5q7ZultPbdXUsg41bQRVIdLG0ihooKKBjcjjaSRIKqal57odKFBYCwEiUemnYcsFTLLMg3pCKUIqJUB68khYpInzjikJD1ZFOujh2VSOzxdCM/Npa0t+e9rKurfXImVKnFFqrNuyVbo2CZANUnY0cG4/9Fux3FA5uVJJJKUZQuZW2IJFAL6P1MESGHoYmQnhIEBVjpMfJkPTu83DPurKDtm5l5edlSKNEM86FLE0yVNNj9ZpDWqWwvOS0o0qBVhcCO///uppCN2NJJNNtTB6pkBtMro/AgZM1Aat9lCa6wqQ6pX6AcC0nLjkSTp4bXIXM5uMQRq98u/WKflIM6CRmLZhDkUlSwgxWOrKKR3ZN0PVqu7mOpS0x0e4yzP/p//CqDB6ZGDm/rWgBhVEEAAElLogXw/wboLg0icoQTVGD1h0F5UB0iaveUBBNkBMdSU2BgMQjXMAB88wk//uUZPiAAv9FzmsMEmROqPo8YMJry/E1QawkS3E6DSYphhjaRe96oUPmhoLrJm2WHDpxyzTyrlNYPAAsf3AsC4hPCdaP/+v1DHdIUkkm6f0vSpa2MvXBT4U7MFLF0uKAZcJIkINIFyXGDSHIxk3DIRYwH7i9WpaaKOwcuCAgNhEVgbAtb/WwtXMFEC5eCLu0yJv8v+2CoRwGkh5O4aYdJrwMGP///UJYpXHE3EiUI9hwFjDqHoFKPHEygUSD0/tuREtfjWfQjrP+D5g2UkK5jSkKdQ50cghhSbxJrracHwXVY9DXomWZXNrxVtDxcEH1DackiRSRIKpVPBag/BYwkZLheHGmhRJVkHw1gHKgIJGDZ62PCKuuZaz3M6PoahXgRngv2Mwar4LK0HDLfu3RjX+XX/K8I/n1OO5m36YV4Wmc4tI3I4ucQZ7f/xXMfbYAgKSAAABhuaPxiEwVF1h6pXgOB2A6Ey6MayWqPaIXLOudCjN3hV2inQTPCY595wBo//uUZPgAAxhGzuspE2RVIkldPYNSC8j5NUwka5EkECj08w2eGHAVURRJM2ixJaU22LS+u9ATnnJCrlLtNi09SMoCDTWYP82ByViqYQy/zNM4u/b0w8EEYhcSnT0jFXUOt+xtWI0Sb/80nIwyVVaFln6Rg4RIOKDDDL4nHPL8zn504l+LhmpOEGWFUnsCIKvxGwkaBJcCDLTQASSKwDpABRqCYHyglUWBDigVkyseNaTcmuzBaLBm3BUQIXsghHQUSTQ9o6MjyPMZyg2ZOtZn+Tmdnl83zWX7omz6ShkDgziVAAYAAAAGc65UHJgEmLtiEQ0OKKvXVaYlG3fZ7PxRxoEIAVeiRoSJYkQFGSEjtWRKWXpqvFi40paPd6iqMzPcsijmk3biR2PpM87sQjEehpNNToy11sShKN1pYrbkOKCoaQgoojD3/0FPasNSWWNtNJFG57Kp41T4PgfhexbTSbHJWKhRJ1yhNuBY3YRsDUQ4Vjc/5FbSkZP7WpRGItnG//uUZPYAAvlGz+npG1xJwtlMYYM4CwDdOSwkbXEuo+X08Ip5VNy601ezq3RtWvdbtW1C2WcIAAQEWNYo1R///ULP1SSSSbwUEAvR4RZDzjL2XgRNAeOBFdGs8cMmjk9EZRyJctSGLhfOOxXoe6I5EZlY7rCqQaMh6NbdSzO9ks8afvqlFz5/2Vl9bCzcxlFHFRzFvFqv6f3DELDraAEkkpTq+GkOSdISFz4vurp0n4ZKeapEEI0WJXGIsoLT3ZPZgjq5oZkQ+pyH55EZn6fzpFnCO//Yt04f/K+Wxf/zKpF3iWiaRFInejBoNH7f9CoY96Smkk5kcAVZQZ3i9ygjhydjLQ2HOK7134vLGNOUJlyEiAV/zDea6V0wnVxU/1NM+QPa84w5kp1V1fcQcUWO5lhpMWFoSbuco04+jfsQWAouDBpR7/6gEABCAAiORcMWt9W1IpViBHKXVoetj1YuNpnQlxUivVktIUq8xPaQKObFo4ii2BhENCV/JKGUyakC//uUZP2AA1JJSdNJK9BRJ8otPGWJi0TvOUewR9FOouYplg1S5r7eQ6co5F/tZy6hi3hfv/+zxPcYwRTfLC8b/r6SHz0Iha/9SSJAMUQmY9T8bp9B0p9UpdVmWX+G2MhKqE8aoidbWt488NsAEhCUEA+gkQaqrJ0u4NsNC8tVrVdMPXaLm9B5Y6MHueCE8KWSykucwoLJPn/zCkH9b/66gktJZIUkUUuyKYNEI0o0zAYi8neaqFPDiaWsYjGD8JHFhNyIgNQozekcgmvhj8BRQkxKvFRlpA6AgC5LRJh63dPsJiBoIRN3iF73Lclv/vZVBRzaSCRSJc4Bk5iQybjoIyl2KTqlgFhkFJPJbYkmZaOSwVYz12Ks6wvTahLn1HRQzOR/eRsEfsbswyYlxVzUlFPbHMO1uzFaFVBx4b4o5BIi9Ih2Nd//+kBmEJAAkpFPiDiJhIjZHoH+bxeS5Oz5Ub+PR0twhx8KY3bqIjadxjF8zECFCIzX0tdXVzPnd7s2//uUZPaAArwkTtMJE9Rax3lcYeMuS0CbPUeZErE/i6b08w3M27lylZLf/dPmLTVpUBzTFKVPg4sh5dzD5r7Cr/tKRJAMEYFhDTUB6CUHSL9RD+qm04bqhOhJKhma3rDEuTpaKwoZR8YhwAiMTSFChlDKRg8mbv8TCKevQdP7kDBn1C4FG3Pp2ZaY1swWiFTG7TZmf7suODU81go757/ZRp9YTTcjZSSBAMlWEJIpFOpLi5kJNnCElyQpPqqOQLsL4v/93htzEeHVen4w7sUHWuQ0SyRvmRl6+96N7fTSdr26WpbX9dPZr2HqJ9T2sCP++lUJSONJVShBFlhKgTIZY3BGSXH6L48FYTxNG7BU5zAIJi7h45RrqOcLEjOLBsjMNMEQUcIOlHMhXEGjiNZUVXG3qyvVcvW8Y1qMhX9+a30t3+3v3M5I4pIoHhNawCpQVVaBZSVA10Oe7am5hEXOAV0/+drA2YAUeKE92Krmumch2FREgiJhZAwROrQ17yex//uUZPYAAsIhS+sMGmBSR6ltPMJ0DGUpO0eMU/E+pCe08wnmpMk1zFstU2ZyyQrOk5YRn2R500DZgLCUmrOpqjMPwuds2WzGxyohi4DigQrdZ2EMEJKcrXUckkwNKRILMYeZTQswnw05LwKDK0mgl1rsbWDW7WVpLpg4cGPW9i13FgCRuW5b8PI38ZrRaich84al78VZeri3KsqCmpopA0OSLO+wRli71TuvX3IrMem91Oyyl5jVsRuvS35ZA/ZdvHsss0GM3MymiqVLVvN+Pv0nP+eq4zHfo8N5WfsS/6stzprtJhn++9/VJ/////////f///////4ch/SAWPoBgBdmRGVzZ11Hsu2zsgHMuoBEks2bO9aOAiDTBQiXTjyP0g+mS+6TzvltTAeRCxGgcBaQQQc0iI4R1D0cHWIXFqIQrCcSMHGJzFlmZmTpiVhlyyRYpHyLrTWmLIJMnjcnzBZdJFZeLxWFwFwnxzyfJyXVJnlLWkbOamZZPF8nSfIU//uUZPUAAtpKz+U8oAxHYqmJp5QAmJ1nM/mcAArarmZ/NwABZsoFYtFcxmSy6bJpoHDhqixOLc4ybGqaBubmLJbpntt6akybTdSi4v////J8vnDQ4zpnxKoAgBNoDlXUAJK2ty3eYBOCQwGWWgIusSRCM8JBQRN1IxUWOGKSgVMGCzIgNjAYpPoZNEJC2aBwJhqfAHRCFKbJJEoynBhTOCTG4GDgzZ2YmQHtKpnLhzVG9AUDvL/WNSydx1NQ07vE/2KvPLnvvUsSv7nv1f19Lep6Bk21yzNSVWqa1UpKSQ2a/zKxYF+VVb35w1Tfh/OfvLuHdWeSmK3Ll+p38prnK9TWtfzCtVzvY/nVqWpLkGvR+G6SQASwMlbUk03Im5bQDqXCJJh7+0l3DsMcb6ZszvZBfASACCHDdFIDEISAIFYGni4ZEODVMhwqwtkH6icCADXBuEbShCxLY6xZpq0ukVbOMN5MWsZE3Nmoqdb3etlOsgROl0pnkzMuGtT1uckV//uUZJOABc5MTW5vQACNySodzNQAjEC/OV2kABDnCCe/nnAEZWi0y7uqjSda2RdKmyc4YqLhkcNVnkVqcqn1KOlh0eEjZHBKSTTmSjbkXYW2p20lhABDRFNI2tJEliTXpc4NE+4HUIB4ETbWyBpQPOn4+CTZPNpaGxFdxBU0lXz8Pz8Nft3Nm01Qf8G41cenD+OxcNmB9KRY8x48CwfBksHTpH/q/vNDBwaAAm0lAoCxl82h5lQ8wYxYARlpnZFhFR2FM9ut1CAJnpJA5FXTstl/3bFsDBiDE5/7Oox59gTqkrgtjbkbZUyh6tytKTZboGjdRMNL0UGFgyEas/1I8joNipH9l0Avzc6YCGLBQcZJqZFFsRA5JmqlibYiV0g0Nhi//8LFFbZdP3QR0t7Vetnt19pZHPmZq5XdiWw6pHIiVayUmsuA4TEhUSCjUW7vxbXVW2BWAAlGioB8oEJCjEJQpdIbaEJw0oSpFTI9rmfh4acCE1NERG9NyxcsH2jY//uUZE+AA49IUesGLOxAggnNPCN1DBETVawYUbEgCOp09g0mTCFBwcWPvfzKKZKKW9AEfY0es1bdBtrLbZHJwxjnDwSI5bpoceLoS92oC92JmOuO/ENQLaonCSRbHXjduLk11gODUybnO7O7Vy8TzlN8W3fi21235WQdxJHdxqbkvWtd6kjLrqp1SVx9lTdzssWhLHf+39al2F20skbTYThfC/C0EsOdcBIjRKnZLKcqD+LT8SanZ6/OVCmqRwKEXm4aMAcqEF6i6AfcfLaeggxX7ZWRWttr6X3V8RFC6x8uOixtNtpFLBHCFCemsTYV02ybE5C1imzEmY0koAYRGUCJk3df+DBAE2ZLQEaqEV6WQEtgrAW1kvUYg+ljI6sgR1dTPbv1+kryXI/ljQYHsasWA5o0bLs//13MPaAbY9945K43AURymiqRMWB+aNmgMx2JEwmeHlYrsk2WmZrbrG8zj5kIMQxAeIEjwSTaBK1kjzxOqLKbcvSk1EhB9dFC//uUZFAAAuY8UenpE0xMAtn9PSNXC6kxS6eYUzkAjGo0HBgmlroc6fiBRoesO2nWyOONpMF+lHCdYMwCQAZRiKY1xyTsRODwUsBPKJKQkNZu+KOGkSY1W01QL0+fZz+quG1tZOCktcu+KghrPvW6MDZaTDbaXBs9LaoJpMq/6vb+zMkFBXTzgESNE+01lskjaTEDAZUEOSHdxTZq8PuHYg0IUK/3Dy71mxPSym3/VaV59+NOsSEElk4Zb4vMp/kE76vUlHXlhGxPAqoFNiRpJEkgGCcJ0S47CAKEMcWUhxxi30QKcLsMITREJiVenYv9O4KiJdRLUy61LLfkVpuhUIqI7W2C+Hu8+tz0ZFR7HPjWFZNDZeOplAJy6VOCQ5GAhU3/6C7I/Y5eQMNjS7N1pQ9qSKGo0yJR0oUdeoKYeGUp8U5D+5DHy3oNwcs+ScMb///6/fVyq0qPOyC7wpabG2o2kUwe8QbyHk1B+jfJoQcbhcNlgLg+a1LMDEy0jlcs//uUZFqAAsgyT2npK1w4QupMJeMNi1UdR6eYr3Dwjuaw9gw0tO0MKprdMQFkUkAxlb7lFNeKGPdnnXdBJTr60ZLTtM1XtaRv6IQnbOIhxRn2xya5mMBHYUFcRAA1PJNaAEJDMVTtHHxOOANiWsLJ4nSniUQO8L2MAakVynDPU65RN7dsrihYUF/qIyym//7GexGdW8d9CSW2W40kkSCoBmnEwPkfg7B4Awh6kOHXMhDDlfPt+JSgUOEaFSNIXORoktx+JOtrbtZViWYk/qsRO77JczTHUE9Nc99CI116PXXRTfYE8eykaZm3R3Z7nEMDnf//9IEuKAkAgwEDVhXipT05gPwphH1SjoUFDDnuSJz2oaUcXJF6r3NT24p+Qs0tff+9uqp/pQs9+WJBKRZtJqVUX3NK2t3aW7IjOlaoq0pUMrSALnQMyJGA1Rs8gzXKs6p0dXsUEUo3u+WQjEOjI4YtVscl3VFY/RzLddP2uIPIlWHIyis8wKnR841HqVaX//uUZHSAAulHz+npE9w3QXl6PYYmCnjZQYwkrXDlBSjw8bDOKjZEquVjcgmrKkDnFoBjuKYgNkAipj1w4eFdNQPRUKKQ7q2vuNJV6Pi9B0LkcboeXepbBTLpMkkVBKQ6ACCr4D8GUUYSqa48bZ2OOE6iSVSOr+awiFAWnCARk5PI2SImplxCYLGMv6bZdWLuMSUMsh565Q4XmRF364Tl5JseL+X9/+mOgMROufvL9+T4IhIAIDQLoLcGpEVMljQNhOQIeQE53VjhjQeOweJ6ksc57CZA1CKNdperQ39L1nLGzCpcvDbUPT6Y8AChEAUUthjTqzpxBQNnrE2AQ20lmimsQUsBngQbeeQt6nNa6EJIURvqozLooLd0TTLZDBI611ub2n7EwlDPzq5g4tpHTQjufl3/My/y16WO4RU7JpkHFC3JKo5/wsTQ2cykKZhDhQIE6kQaZfVuVtgchR3j6GDGPefl78W1OjzguUBUIgWPVm2OdM/9Pef8J7qS752t//uUZJGAAp8sy2MGG0I5oYlpPSY2CvD1LYyka4D9i2jw8JmGIqoASkCCSCoDnJAOx5TRxHs0n0oNMq6hwKRuNByeEcwfdhiSWP105p+cPu19iXdDhymFGPe13ToLLUXwtuE/DPzh19M+mRNl9LLK36zLM/8UaqMYLQgXSHVXCkAUQgoCvJmKUPEyRHlQxCuq83VOhbx/gVGdQ8hLA+1IBVaAa2EVKepq193kxdKVjXq/+6iSZSiPHh4EBiRgEFJF3h52oIOtxVScAIggEEchpWpRuZQiYsNCMt51ezRyLEg4WA4iwnnJDLNUJoDlm8ac4bYbQs5ZJnlt9z6k///8vqfnOMpw+f/ddJgsixf//1htySWv/ZY1BE2EqUq6UwCwurVRk6bHRvcnxF6N82hqZ0yyzlI1b3GFpM4cEQH61+WQ+VplUGplz6tvG7eIK5MFFqNtFJEgGEJShfjWMEtDzZxbDkMKZDTORx/HwqiMaegcnnmTWrtJXVrHFenC7Ai9//uUZK0AAq09ytMMGtA9YXmNPGliCtUhL6wwZcD9i6jw9gy2OEDVJPhmLPGSdCIM3GLWpecWLpeL+JXEWqp3cgCYNlv/sAAQsZISRTUBPzRaB0sq86KwmKbPJR4DIUk9cl1DCk9M1svP3vKEIGmeIwzWAk9QqyK9uPU33GNRxRuwv6m69xLYogAAxSQCSSUaDoeQoTIdteqzlJy6MsVCWo1iEELZCfMjiHY9ryOOsAmANvUppnZmgHA6uXYhwWRk9wyhoR5zz0L/+TpWkZX4WZk1yL0v0vL/730+ezYXOggZ8wABgRQVcVhDA6jQDIwRBMQjjNHnMos3NTH5C8zuUhlBAPmiZuPcKE2HaZILBWMQNHseM6cy1CyqZfjFJu5xDugAGAAQAAVzzVZ2YQaqyZxAAseG3+bkzyAHFlFBDk5SgE+BZ2jUJRIxJyD+KPF0TzK6iZSEV3AHUyBnW1GelSI6MbMq6UNta9pkZW/l7Evtpq6J+JmTprEf5r1hof/y//uUZMaAApYhT+npG8xCozmNPYMqC0UrLaywaUj+CeWk9IxwsyNIqBIqweAfojxtQEcSag6bg0WqiqZubQgeKGxxfEJAMhgUFw4JT6RS8ew/QpO9iXdl/v0Lb9w7UfDwCegAkklPheZIAZBh+Mp8KseR8Yw8klYg7rVEJCAyiRARjjciAthyDhwqaQUITog6/nmLIlJwsFQGUI9jnBm+k3wvL3BrmCVkGmHMkkH1rFJwqWE1h4ekowweP/9xl9210ksbTYvgd4wBbpAbQkgtTggsBneKxKPdcjszfjewKe4tiBWnmEAwJg5M2xTRwTXkhWnEnW2NR1u4qHDZwuoQLHOe5jvTwxUNNRtJUyig9MEGUgy6CTAIIrzScRui0ng8ZjLZCDtM5J518adoqM3dcxYTEnZcj0+sOXQ8pEDGJ40he+puWt2dmj+OD33Uf/Pf6z8hSLuSiV9CcTrEUCCsACoRDkNhB1UF/5PzG3dibBOEkRwfUKKEyY8IZWfWi+kI//uUZN0AAtNMSlMmE8A/QWndPYYbC6C9K0ykbMEnDKo09gjukQjN+mQKsLBi0XLiREKG1nXtrei2vr1oAFf37SHqO5sEGJNkElJFKirJUKSByJ6fQRsmjAKYToTk9VwzqNOItVIefooUBKtcYxYobm5Ge5Kt5pWae6Fe0nFFTmxOHd9dfqTmvBJaFdw5YpTk5nbRqPSw/4lwYJWMUKvMPnhiBjVf+V8s5QiHaAl4TwNMRYlQ+mNFpIDl4JSJgsw9sRgEMip2JuhJ3M9kZ7s2KSmkf5d/zlxR8XShKDPpLxkCCV1bFHXjBc6taBX5wmw0fIGhn//0qihuQJJJJUetJs3lB0wUtdrAKrIpS3XaUQhC/jhPN4lVXRK72lpuk4ToZ1b0J9FGT99R6ee1sN1PF9RZtIcbfuMw4hFBpQZcQPVqKJEzxOLKGLQUBJ8eaRBINioZPQ037PR6lB9F6IAVZsiyq6sJNUBSQ3cCQOJpsJ1HiQIRdh0vxe/W7FH/B+ag//uUZOqAIq0hT+HpG05DY2lZYSNEDKTrMaeYUZE0EGVo9Izo4TCqiqxQOijxhcghz7u08NKjTyi4rXbtLc0q88/2rCYwQqDYRajJJJKJSVUeLjGIAFQLHe9CeHCTruJApWhSG6JbJWWoTGKDnVrra9exWHmOqcKIGuZKK9JTEc7AIdxNEJe6pNp879slsMu0ZSMPbpeprj1ZPrvHpcC1KTCvGFUzm4QiiQSU8shFIHZNMn0cJKwkATSiLjphQNZ6UXpwlK4Z4o8rNac2Zgu1OikQCFHQbEhzS2ldwmEx/ctmSdy/SXLqFWD6GveO//XVRv6ZJJJJ4iQHcJshgzySEmCKSpgngP4eMB+SCQ0ZB10jjkQutCJA8HoqxfEdThNST7jyIZTkIQ4N2KPvBu86ZN6aojqzo5msmdKz3yvfz6p2/t6gDM4tQHBZ59lflz6i0nqkAAYAAACBHwGQ0hXwhbIDh4tSFUimZkK3B+SQRrT2lGD3cbwmYYyvC4O4MUJj//uUZPKAAxcmS9MJE+RJwml8YeM4C4T1M6wwS1EtiuW1hhjQEbExSP/SgNulWt0pf/C8pZb7zyvm5/8L+dy73P9f4e1J41b5kg7FQFaQFVWDKiEY6tSsCiiqSxGkLvj0uhpStdcBEoMBkqqhQTaKiacWXUKZIhYMUdUYvTEdqzbI94U4LXZohh/tc8pfQGanlMz49I9qU6XTKwjz/8usf/9vHK4sRAAKEbBASQJKfKxFDIXSBUzUhwkklcKT4eDD2/q+0nxcMJIkNByKLBserdKo2pwU7xi+650Tix8ATJRJbCemKpN3hg2UcWGCNLSo0GnfSgACqQACSgCuSLDhiUA4C9E+XAhpcDdUBgLS8fcy2ew4GowWQIjAxyZ2Diw/dgINdbcw1JIuy8nZ8c8/BeZXzamh6SSgokFxREGyRlIQCS5FqSgXIJeutyowiZUhjkAv+3bIdIJXIkkoFPrt8ElGKXKOwvDkgoayIUukMCSY9UoOl+P02OwqmVnAzMR///uUZPYAAxpITdHpE1RUCXk5PYMuS3UrLSykbNEzDSW09gzgJKpGSFv6l3pKODw0Cpj6N6j7ehWLAktDAop5AXrWTTJC1FjtIBUKRAIRJKUNLF6oKKmZS6aPaE9NZ584gr7r/LqbpDQkki15SlkySQDLSZ/eV/e9aSN1eSalkdxKuhKs2Y6Owld7ldnVvMqKuUZV/7aoqOvZG0u52pJusUEHRd3/8qAG0qzqRNRpJQPkWv6XbAhkQGGX8v3OlWIphUVn8rUWdGpBjOelynrghiAtWuzWdNlWsYtlM9jdk76URUqi75et39GT9afM7SKHCEP/wpBaUaqu0SSSTdHRq4LsKmJShSCVcAL5bNJH7a+0p1ITF5NEoEgdygLSVbnBWxGsSk9eIU6cecPw3JitsXFdQqtXi0zJ6WxYI2ZdobglPTczOEf6LudDc9BZXlIGVBeRTZnhOvVLw3//WQARGyCAAAHmcRUDKZCIgwZFwTEoR2U5bKsJOUD0awoG/COU//uUZPQAAxkkymsPMGBNRHlqPMNmC8EjL6wYT1FCJyc88IoMzIVn0DZ810hKJQM0BWGMADlUOYtK2jOpfrnHJn2q2N8eGHeo9/6EDE02kccMX+rM3Zk5CkHNUOiUrgaBpJGJqGIVLoMyHAKSRM3uCk1a9kQZGsM8KvZMdVye5ykNftTwrA+6UpdUB9ZFM28xLbV/dvR6jOrN3Zxnn/pJJP3XjkFJLbWSSSUuS82yRLy7evyfI64/0whLGW1ROOkJhm3FtO/ll3E86OFEzUpAU/RbqhZbFLOysWbbXbkweJH0Lb6KRKhHM+hKZY4YIA+zu/9SDSljaSSRIBgsyOoC1etiQ7KUza5E+i6L9N0fZ5H2gl/IvO9ODJF42ESSzOBuGSRPgHa6WWUYj8/8nEkG5+loFkMCdIwztVpnjhMizveeyb36aPKG7mcfo//aejOsyHZVF/9XQGC40wQCSU4BMFCBdFJcV0sI5DT9JtsJFTxos5ZVGzahRZFZP+shlGKD//uUZPKDAy1LzVMGHERJ4qlNYYYcCxEJNo0YUVk+oOa08I60hBxY3JCxgPHXMchKXFeuuhn2Q1cQjXZ42EQVuUBAAAAAw9rkqkTGlkMkVTLEAxeLDA4S3deLus7yfp1IfeZskzA0WUVYS2kPxiJWoLZu+2GD/wyIkYmXEhZ6zTSEovAsoiJTIysKW5Ho0s9EJdMN0528OjmXJr6eDZ9RIj8JYdWzrzn3pkvmnT95pAtK/675mTniW77rT1yxfV/+aIIdhIARJBT4V56l5Gacz9DXaaNEPj4kpIYDZRVjoyE5FKhyogcUCZkgk6AACKDz580pTGiIu7u6NvuFWKRWil6WDnHlLDswj//rKalkiaaRIKgVUpckAEBMgO5Mn6K4SYHOojdNAyEchjhwgELUs7ZOUNxJoSAEMTDiAg76akb1bqppU5JXnln+Z3c8uscjy19HLbYztqvRsUcYxR19Fe5ldtkCKDhR5b/2eoBAQkgJAkAviaENAcC8jiUWhZYo//uUZPSAAx1MT2sGFFxFwwl9PSJUEA0rHO0k1oEpCeW09g0IhWY3Z1nshjYVDQMgtIeOJymFR3FLl0oaUI0WCVJg++0n0er0bmR4qkYO6mgAeRNJ/+v2ANSoFRIIMxYgzwbBN7jBCHmULEnTDUYBMUKkJpSK8hK7LGCuJYcmft2Nijr76fe2icm22h8JJyy1tW9crnNRcEDmMaZUuz1XPrVN3jOoKrUWqvs3NEKHFsf/rEDJIhmI3E22qF0XXJukkneRG4vJd1WapCmJTMseA3S31rG92re3j6zSuoXgR8+LQLRBAr29GplJU2c5Cmi/4ibyz9969n9n2ZXdgoB6U01BwfwFOyOJJIkgmEIVxbkAfpWtwcwX5bVyPI+ULPJlq+hKptEPDO8ZRNgNPSHynlrZ4Mk6vtlTpb6jMqRFgCLOU5Cnzlwf3Zost7AoDNIjmnxf/6CnJJImmkSAaVBkAWQrwQBCSUEjCJZ1IdaQeH4aZwP10ml/v0PJd5mPvAnk//uUZOiGAwpIT+nmK+xG4rldPYMcC1UZP6ekTfFJJCc88I8txJGWdcWAKIShFDxkWQ1tynBoccZxXGPsrmc050Nd4D+tTBY72+5sUoSQk5Y2kSiQDAuTmIehywKIeBfCNniVxqjnFGuiSaRx08kjAdVp9MKMI5nE65m3vSwhG4JLBaCAVNt6JMmbBwJJr84c/bX98in7lmYM9liGqNo6gqZ//6SwnNY0UUCAY0B+HMEbg1huRSJw3WQKoD6EUeDOZCE+Y0IRUe9yMrOQQjPouq7xtaPbazf8/HSzJNW6oyoxOvf0vv15U/sWOhcW32S3+qoAHAAAAADTjT41cdMUCzAAIGAY0EobA4NCBRhzGV0l412Mne1621Z3FoszFvpI48OuHLdQWCkEAeERUuStlEelfJ7xQXOjJNw8kMyQooJJQTJrKqETB5pO2IUyl0moN0sgRI4MpOpi6q6TXTShjnzY9ZUspzIzZFbfYpLMyhiDx//Vc4kQQaRIuoIJkkkZ//uUZOwAAoYiz+nmFExUY0n9PMJ5itTVPaekbvE2JGe08wnORRIJg0gNHwl2WQWHU7EwUNCKyHbxXQ0qM1vU5xabNtQwQJ7elEJSGXrM/dqo97SO7BqJ3prMjadF0ap4LxZGQxmrf0MrYMF///oEXqAkklJ060aGgRQUYY5K/2dva0N+/fmAYdcaablBEmf2Yn6XCCVQSFgHdwRUytPypuCnlIei5lcreFsScZr5C5bWZFXGO6DO7hqyphY0gKteK1HyInp/9AsHOmqgHj8EABwH+kABsfojR3kvJow1X34FELACjqNlAIRG3qSuud7Owi3F2OrawXNMjpbBSucGEFCgXLraVWRURODalWp9WjS15JhGB7A1DgAAAC8caqTJRQGpUBhZdolDUYAbBpcnojyAyXmRC0nHaYhrCSVkXLWiSgkye69spcTNZCoRc/bS2GqtZSLlyLwMAggwPQoty1jmljSGaYc3PUZWmpwY//pAaVUAVsExDUhqUqM5lG4l//uUZPUABExIR1NpLWBOyTn9JYIti7DdMUwMs1EsjmUk9I1ogICZOIlArMszVtoqKGqtTc5o1LkoI9kkotFz3znn8nzv55s/CBTSPcu2XpeXqWQyGZQ4bSUHSouFdjgL9Sw03JG0kkSCYMpnFJARD+IUIcSlrQQsZ5ohAKOOZK0EFc3gaswwLP0nqRGzjPzOS3EVMlB6P1Aqiu6UPpb2F6vjfN/t+fz7ySWnyWGc7V7AjmOXZf8eIp3//6i4M6q2iSknQw5Q9CSC0JWMzAU8BUSwZEbAJikU9dsgQQI/JCjsLrVjHRSohQx1PpR3crrvf0MehXbftJpP+ta3mbTbTtPm/uxpolXm/6NNUELVJf6UpJJJ0zDf8QCSJa0uZquaon/aa5TmOk5GD7x2RzEBhdQaLYstrPyM0w5C/kyvOxjhknOOObtTmOWZ2FwrQpSLkIVB0eMVKlyUo1w6Jn7/SRcL//Ub+xQKqrEO7m65I3AykBYliIUkiwM5DcIFx2Sj//uUZOKAAsojSbtMMWBNhulJPSM4C0EJP6eYb7FGJ+X1hIix15jZLBy04rb15jw7iFPkpBouffS44l8FAwRcx8JqZc1eId9foLZM8lIPDPuasiC38iSSKThMtVyCWbdNRJ5o5LVM2Aw/JXsdufis+5IglN9su30spPDNGkWd8aioY9iRKqPcs3oN703DudivEv9O+VjeqejQbq/zHUzE8gJ3QpYRCufBlH/+kJLaMJFJpJ0FWGoENfIUzE5Tw+g1CQeDATAlwZZ66bHkcGA13Y77b75qTpvZ5IUIDBSftjXKFT4Xl5EV6Z3Pz/77/9u5m+2bn9b5ZLSk4R0MBCk2o2mkkSCYMo/RczjLwwC2DnTKFn8/YC0anjpUBjWCS0CJhIikeQxE1j3OirBI+MtJugnlYo+frPbyB1Pzuag3S1mVaPV1uz1q2rs21Eo1FR0O39/JX/8OL//mAJpQAVHBsCEHea4tzG3DNHoXmd7BYT/cpdyTsMs0HdKwNbzTeNY1//uUZOcAAscnTVMGHERHgpn/MCOTCyTzM0wYT9FFo6X09I1Z4mNbzeMCXNkQZe8LNzs2kzXIbJkSlKbX/OfIzWc7taei6WoGMW7tUUUk3QkxvjsjniHwEeELCVHQkUAyDpL7daTBNZTvKFncy3MClf7K6KbUpSGlHLsDndiK6OrZg5wTyE858hwXbe0xVe5a3qHRXXRBTi2Rtwh7nJLXc61f/4kM9YAEcCIABBLXXM18BOi800ldLWHZVUsyuWP67VPnnEtxO/810RmDPcjOiSPBPNoruDN5z6Ptu935O5ucvSO+s7+RN8kzoLJ2mr///+sJJONpEkkgGOI/CWEzISN1SE2UpTTH8pmc5GeClZ46vxai9e7BFsLAgmf2SFtylKZVTHjujP4yedQ75HFHBAexm2ZT1HoTrRMEagHNBrFuWn7zb9ugv9Xo7VfUATbW0EiSUoEQhoXpOyxG6o3I4Ho9ERfRKBUjAxwIbxzdxoFrVvkZtJMjwMITHM2g0QZn//uUZO+AAuJOz+nmE+xLSRlJPCPKC7U5NUewTREvoaV1gI54wTwQF3SM6v854kkfPRAgM+3K8lc5eoEPmJqqVEolN5AjDoQdExa6VKaizlPQUwZ4xrOhCQR0JRVMh4OYFsCPYInTxWrdxxblq8h0S0+QEQ4kJmDCC51dnQqfW1L7OtL0cYx5RysYisq1O48aDIECsyvRGMg1EZEFGILAARf/1ZDGgFdCAAADA+rLRbBbWXLseWEtYZvMNQwdt+r0tiYBJEhGXVgFIfWLdDPOyf40i7JtDOViI9yIh7os6E332TW9qro579vpZrqexv+NtLS6TEDMCKczsPpQm5CSSCnweqbYocahOKgaYipVSNfVsddWGQpbPokS3RvRagASPaJqNyBgdNGj4gqcJDUaNDExlp9U+eVRN3KG8pyfp19IZzqf3LNWmu6imurcydkN5hNReLbUd/xy72ExW3NSOvl3SB/qodxJoqOCQ7//ma3HRCrRAIIJXJUEcADwX5ez//uUZPUAAtdIzunjFVxL6MltPCKuTQ0ZMUwwq1FVo+TpgwnpfdKoDXKNHGir2A4RZkephICjxAeRYCgQeVKC6nRSCrJIriAuzqWm+j1Yt1KUWZL7KlvTtkW/3V0dWmlN0sZnSRmodFKqPdxVlbDsY53/0PkiqAJkFOaiDKCIl11Vscf04RsSRgwxpwIODAY2y0oSr5HgCZpcc3RcxpYypgt8bRce10fhUzRymXOIZIkDgj6LAslVSBRNk4GCHAGCQdw5JH1yPRDhpBgGlq5p4VHnUj6j8FsLdeJJUQ0xVTYOFUrYeXYP1duXIfnakEufyVzEuo9w7T3c6uG5bLr9+ldN2JS7kNzEu3NVt5554XJflT0/y2Wu5IGd1tyqczoqSef6WT0sllf5qMP7P6ne18PtTrKX+kEilmedTn/6WM4UYcHclQGY9nrLZbNZbDIAjhmxUAzd9kVz0UAafScSDllEGNjIBQaMMoAn5YBoFwTIGsdYVDg+aR+A/E+p0TGL//uUZPCAA7JGSlVlYARgKVlKp5QAmR0vL/mtAAKIpOj/NPACw4wCbGTFMRngl2bKpRqU6FEpUkGzzOvBV3UlpYrNmSsQ/4T5bcMzrFI3eS4dao7WuzOt6fWzCfQ27DlJiLeFFi729ePI13GFvvr4bW+JBy/wwvabu+1rx9339RtRX859FTW0IyS4kkSsSl2QMAdGmNwsZb1yCiEb2EhipBVxDqH4Caj1NdIyNicCyKJIlFZIhQgpILcdPkssexUbasumJiii+Yl08kl9Eos629Ve7upKpba7uovBoq5RAnst8k7Kndf/ptlnnLx0o62BISFEigUDwOoAdApYz5425gJXqxyOUP0mKlgA2Wnan8qcTnO3LHFljzA0Z2/XlW9NcY9pIQ1O/x+va3naxWgRSSOqrxJNYZJNlUeZM3ZpLyoSwl530uKeRtllEDR4MKo5TaluVW9VWFvO06NDAr+3SIEjDoTYtnFELupoIzMX6ORFg6MzvfF/MUJnIb3N+/5m//uUZH2AA0Ux0m89oAxAYppN56wBjIjxNSyM11j6kWXM8I6YfTF+mCqTSG1Am1kp+upM6lIWwCUCoDbLCfZe3Ta8ZmkEIStujYfaZoy6BXT7raFCjZhwOIYWBZDJNIVWe6/OKFBkRkAP1VbTH9quUb96hJwkGg0VJJpKAXhFizrgpRmg2W96H2HMuzIOFzbXJZazUiokwX5Jwq5XwT7zeeh78LshmMVvS+7yt0MiQyRkBKDl6Y/ZLYC0+XX5n0/IiL7WK/5f5f3hwRQQwWfgXGFImv+lw/T7GApi+E9O1Y0fImEqcSi6PVbVEFZ9pgbFfSWontRxJJJtL9sXem/6rLIH6VYgpr1IRZqWv60fsMv8ugrT1nwvv5VeMR5/5fPk31HQEQVK4YwNHIm2kjOMMUoQJsLqNwWk83o5ReRZThUjE7mgHLGjtUGjyLK9nFjg/4HIlMHRnJ3vDRW7V7S6EmRbmZ3JyrqR+RGZZd3/feqJvLzLdW/dlkHkjB8Qo//H//uURIWAAs9HzVHmHEZXaRosPMOJiz0bN6eMVWFIiiUljLEBgAUgAAMEKWjDBXzgNWZgLLndCiRvV8iadmo0HTD40x45XkxqZnN442/vmfFz3rx3eQf6is1Jik3t8B+dons+D7z1W/PtfWHDf1PaRq2X6Jc52SoEDjACiSnyoanQWgcMAAMQW2p+D2UDZMOLWcRu0Rdk+CSyyFRJAfQmEw8aOsKoEvblq+3mmvak6DMJr3xDUkNz19r/3pmlMiPMomxFUrd1rZ/Cyw3n55/8f4UKQY///6vbUEDgKACwX4mA4E4EnMk/EMXZIR5gOAaOYo5ZEsOPq4hBiNBnp+XRcQmBUgmecypr3V6/XcXP0V187tGRnscYGPFBpQEkkpQLiplGRGRzVyMii73KzKAyKVOlEGtRN9Y7O5UVqi61ccOhi0Iq0sfG6+bAurYsfKxR30vv0V6VvsalrsR1uzM1/oxaihYokm5VBpBZjv//9CrsWskbcSKQX2BCQ4HzsQIb//uUZISAAw1HStMpG2A/4qlpPSNGCtTvMUwMUxEUD6l08w2WkU2XXVq7JAaLG/COpJlYQNnCgrbyJTRHnwtIkU3Xo7qoVLizHIN6fd7l0k60pY8rVkIeyNUGSpESUknUXSaClyouIAFMUW2bp+K11s1ytXEoIigUhoUrWI5VG3UWQSrEDH5Pd4XhX57cpp6m9mD5Tvrux6Q/ZFzpQbb5+cx3WLMByeV6VFbX3Ah//+gOwjUmmk6BpEN4rLhDUGAWYCNuRUe6QD0KBhaUCcabS8Krh1DVmX2qNlmW8n/fddtqjloxXk5gOnaQTaE2iU0km6TMY4jA4xSSMkzPIFOBqGttZSDQny4J4/FUph4tEzGSVhXf1lq5NzVmqLAj7BbKY6QXXNKDgMyYoDffqkR0zEfkDpSgaqkgjjCJGdALJS0QPDZtKkJ/9gBgTZAElrC+OYteHxoPQ3eXQtllYPhEM5p69AjCFp6mXPANcvLpGj2CgkhSZevMs7pJ3F1IWZen//uUZJOAArg2zNMJG0Q5gcmqMSMkC9S1N6eYcRDwhWYwx7BA9bzZqxUCFEEAAAVj41TAyJYSnuXWEJ05nRUbWlXXAIPKgcKSAgFRK82tItrgmBAwuAaUZu97zuDeoKOO7Pr0o0SFyR4SRcDnTdzyTLwJH1zg9aWAFrj9brajgDfPQp/3yXNaSSNtIFBcD1EmUYkxeGAoDxiOVoExbOmwACLUwo5eOgsx+m1xW9MEjI0hpZ6S/L8Gk//o/6GqeEtHJFL6qwT52MIW4u5jJEmydF1NWHAQ1S2Wm0aIwzw5Fu4ObFekGef0rAoA0UfBf1SIyh5WNWh0pYWf0+eVB390VPM8J18P035icRPFmYpN1IBL7ASCSwQoJmkw9eK00gyA5S9nQbxc1BDWqLShUhzsiOJDrI5qYZQskWeRlGxjutu9pTCAVEZYrf0B16qnb0SOPadMcTqaJNYABiEgAAAAPAsxugNAXaWCFBhp8kyeHSnyDujKQFhM4kNxg8QoTLTk//uUZKuAAtkfSlMJQrA5gwpdPYM1iuztOYeYbyELiqYw8w3IT2bqpM3SsMzK8jTKcr7lULQvdDk/YzLB2QAqDIq0Gz5BqRZUm4vi+1vwdOLNE4pIgYVR/3dAD9SZMEKKk0lWPJRQqmmyqsviMaApsnRokIwGqasfkUObkGKixnIODCYfe8B5nQk/cvOL6f9DF0U2n6WglmwCw1eCG014gxTKYDiM4bDgdg+DdVCuR11UuqSv9AAGGA3IDF0HUwgILc6AH7jExzK8OVcjJspTMiUlWHdDI/PNqR9fmf1pTTp7oOCSYPEzW9as4iJToCQhaIkmHCQEWIQT1IliaoDmjS8o8mSGZiBHnJIrrvOF29DMfKrxzmzGZ7Cg8GkChYpjhFtI+9Riz6/zH0jxNQAXKUBABeKCtyIjwMyxsb8MDcFR5uUBOQrQmMUGWSRsRGKbWQqIuXiI3JPYcgKp0e6CzZZWpVtWYjvydK5dM3M1ZxQlOcqaIRUOVU3XjAobDb0c//uUZMIAAu0nSmnpG2A7IrloPSNUCxTtMYy8YcD9DaWw9I1YYr0CpAAhStAIirBdhczeYm2fSkN2RDFWoDSJKHjAQWCzzvxlIU2y/5QQyiAr9n5IejM03IahN9dile1VFe14vZKLALUEAAAJcypXECRraQHw2slYZ1nFcptxMEVBscMph8TQNlBCvqilPJpLRRYYOiHCPRCLdvBCIVYmymOdyXc7LTv+cyneOV/7Cp8+fvTjlstBbckh60fHC3/8mAGoAhPiiMPFl4TIXGByjH1ou8YuwXaRhoxakUIkzlU+8NFMjBZ9odJQ05jLkPLVJe6pktV/tb/t2NSqCLpEkkkp2CWOOEpeDjK6jbLric6KSwsUsOHKnmgKJG9ty70gzNyNFIpJZh0mlb2VZrFE9fcogRF6DJn6KP7p7ZeI0b7rIv5QofIhdL2K94YokJsu5znw0IFBY31v/1tn121IVaECCWKQuYGNDBPU8KIZZ9FOeScGRADIXfrnQf8oZMrS//uUZNeAAro+S2MJE0A9wxlsPSJiC1T/K0wka0DqDmVhhgxohDXFItmmQewcICB9tUgSHIHki4rBOgPrYo8i4NW6RdEWBaTEyBhM0ExPK88wBqpASSClRJCt4QyDUjo8qJy2cq6bK/bgRBrFO/T5UMqcMLDqLMEO9mWVO6edLNMa6qkluR6JsCGVEd1tZOrIxa1p9JNGRHehNK9FKP0NkIjiL9oABAJf/1ddmYAWrShIARBUCFmaXUboN43hQnOOQTA6lcZWnqKoVkwH33c7rpKT1yLN4dOGX2dJryiC49rWDjQxRx/6ec2OrUKgUcHH1qXgMePAcMVRqVCASSlEpQSdt0AD7NQQda2yVgjwOjE33bRQuos0bRDb4mtuqRyYOvwQDTif+pFdiizmmMzFZwJ1nKqfO5H8LnvpCKdJYXzfJ8bYIsLoU56TqjAWf//+noBMSiYIUrYPSEmBAgjhIjtUQoC6JEyFoRjBg0JU2Cpb+WNHGlbv5nPJnSkYJxcH//uUZPAAAxNGy9MJG9ROYwlJPSJYC3kFL0wYUREsjqW09gzwxMBQSSZShg8+tRhS9DiMiu33Rz4Nhll0NcIROXoJBFuRNIJIkAQIFkLpDUIHaQghxtNaJOAgi8XtjftxPmJyKTbAatFtMt3wFtbIgf3MYSsp3o49n6n/9n23ac0wfms/2M+kzsMWObw/OM5/i3ZZqtHAztH/7v+kACZIFANIpQLoeI6ykLG9eXpi0OgfRaPyiLXd2BjqLYOahubRp/aEKKARKGJKHQtEdJWdbZWuiRJ2pa5D3TmxdtBhM8ky7FpRqgknbE0kkSCasGqKBiHyHKTRuNovJfh7MYOVRaN6I00cQjYoVglUO4iz5YGZOIA9aWDT9h26rSiBvimbTDwyU3LIXYd82z5/0+w7onGPfEoH6Op/ZKQnSoD5TwS//r3avSA1sABIAK6ezhkpm5N402CoBTkfYUnKseT9eeLobNctix7mmXHelKVQt7sJnDBmJBB0RuN3XMPLe07m//uUZPIAAsw8S9MJG0RMowlsPSNYCzTbPaeYcbEijGX09gi4YxaK2GuLFnki9KAaOJqIBf6jZof7aaqQJJRKdDSAwwH2qNlqYcOrITmd5qkONv2ERehuDRgLZYaMJ/plnJQYThyiWZ/mbFz3xTjYwf62HSzxp2OoK7M9Usl9mdLVq69rqtCi7K6WsjI+ympWolcZINMHm/9NIBTTcAAAAAGcVSIolWG0wFrEOtgVid99wyBgCnEwrEgKldwh1VdlikZWcZhjfjuws+rkY2nSy+HuXjHAEwchATNyrWKTS2XUXFlx8MCjijTd4Bb/+r+uFqpgEkklVEtTJTYDWJRJ+MTSiUvaoBEeTMBQtCMkq9hKvCemV+3S/x2aX5jHwtxvRdVIzIOhGaO8ooPBN4qRGjBKZnEg6m4pCRUKoUA3qF3l3vp/yY0kfFP2OK22pu0AJmt0kRVsV4XxDh+FKMwlSpZGc54URzaIR2w1WoUFFqvYtLuLaRhXibLJW+vb0VUZ//uUZPuAAwNIT+npG7xP41lKYYNKDAUhMUwYr1FZkeT1hI1gtXRC7Z5SDmtmbp+qjPJqwCgW7UXkCp+LHfKkkkk6IoImKVplIIkJL3M+QkyxStwVNXYaafA96w/LBy0TWfrSI7M1vni5b9IFq5v6ZMV4lDz0HEDwwqhAxQnG5fB1SqhwQiKPsrTeA8tcRUL6KSizMFXggk0ldikSx4xCa6f9GkUaZCSSSVFMFSWwhrOaDMn1hzRB6Bwqk+AwGJOUUW2HZ6Mhv/hdXKncZkYt8kxTu+4J3qLU9NDlWZmJ710rkVG62SZHn6Gu1/97NevBgTd+66oEvZCUiknQqVIJiqiZlqS6GGQlA6lkl8cnTFtRxFcibbPNzlrXhQWW3qjdvYbIlloJJE/d1HjBFRU1q2Vu8gA7GQ07IlhmB+pGuzo9zzO7P93RU/2DwYPEv/1IAXtkkIpJOAupMw3Gg3odsjMRRkDnJiqtPsjBrwdzFM/52jUdzMyO9C5NdyzWZpUP//uUZPYAAvsey9MMGnRICMlsPCKuTPzdM0wwbZFEJGXo8wlySi6vX9bsyu+m3+jN18260Yt7JRhZcBik/+cAFlQkkkklOjrAvGaM4kxihnsgmx1wzmLEGQcZTLhC2GGxs0M9LTF2sJVQ777ar8V3PSxUI5Z1chOYj2a9L6Uk68qLJNvdb6+9MkKmLXkYHPqOhhoOB4g7/36qtIM1FAEABLwhHAhgyUsS4liOJIvXn68yTiNVtqimI9LHA0cdZ0a1bH5CjkFwDW5KC+J6mR5dibqUk4syhZjQo+81Jh2Dij9RRF/9vrpBJONkJJpJvBRj/K3Ouk6mgtRvl+qSdBrKekvU/ccaES905qKEujleyrCr3SSObnXy8a/m65TTJxDpCjKvrG55HPYP+joB/iuWsSc6DmWUIyKmRIm2oEDrFj/qWRDU+j6BRq0f+xCqXlMJSRQgBKIAAAAArg5Q5i3CgLC4E8T5lMs7oW1wTSogxKuiRBZRqIdjPVdfhT5mqsxh//uUZPOAAtNGzNHmFLRKqLmNPYUaS4URMaekUpEti+Vo9hToZ4xKkWj5ipaxUweGsURe+hvH3RLBlZZiHtEBZhg5Y6//6wRqQBJJJVN8R0WQV8PkXwSt8N+OabAZKhQgxFhSLWNkDQOagdYVD9fcdm2yvxFxmeoAKj9uhJss1yLL8tG9EnlC7Te/INtkILoDFctaiYJIKf/9u8S0vYYmtrM1QwOobox1wK67doKwahRZeH5kdxJGH+cfckt5XU4Db4mNAOUdm3ilEe2eLp6NBueR/p/o0f/nr+fhrJnWH281Azm3uhflQPvRIkkp01CBgWXqXvExBvcF4oPofNv5fA6UB1A8Tx3LDNDRf8Tk6cqrqSm1yxlnIKL1i7i8G0QVCDYSpsljzyvD+/OPZpz4muWZVvzPJCwhWBZZnecM//c3HGhz+wbf/uisvUomtYEMktQIJJJnUImC6DnHqBVEwP56L0SFeRjGrSodFg2FCdiRncR9CWgufDY5YFBwJOcE//uUZPsAA2BFzWsJHFRQAwlNPSZgCtjlL0eYbhEniSaw9gy1DhTLoTMkcaKuA72DWvQUNPUQUi/KKtRwu42fGiAYS/vtCKBcaKSSSbqVSsRfFBSDkSVQqcxhNAIKOmOHAchByc02YjAmpOmRmnrrjVNdkQbLWlkfjKw8nHBG0YuZIRpVGfuzBCXrAXuYQJmDPr+57/4TdCYgbAhvLlMgcV6ebhCXf/PVxDZIOG3wAnlaxKDZkvYfKf6AkQXCyuwOJVsuS9UyWK8zEIJqRq677RmgFq0KkiEl2EmMZetZSXDK2iTrlvjA9JR0Jmib/AZ6YUoySszf62fnssQuPk1iwTFLCDalVi4Uk1/a1P7HkyAwUQ8NKikgmonEkiCYhRWiLLxjEsHQxTmcCbZpjCHuo22sZLCcxlpH+WT1Hy7SRphUiabKqkhrQ2IY0wdI9VGmTFwljPZSsM+PuKhpbw9P7+dn/wy+enfP3tfoup7AjStJbXiq3/6gAwKABiIMCesh//uUZPqAAzlMTFMMGuRS4ultPSNkDpEDN60kdFFqkCWxgZqIDkidTGeR/WdBEUVA2E+OVl1JQuZ80MWtaxcM40sYaC6V4dNC66QqcYHRa0kpjP1e5/1V4zTn2UD7kXEIgkmkSAYQJTVZ28As5/040JESRCVqvyVUb1rxy1xnNgHNBQEhSidB2VRhtHkLSCTTIcFIzBFvi46Uc+99jwWzkfkR6oxOSrfv3jV2EnjcLhff2Tv8//scZ4wLInA//1K+wNQBgAKqLjS3BDIJIMPeUiYsPk6BAazk5Ucy1BehGZm4phmApgxMvmDIq5kMOWlVh9VU5SYHjGN8imq8gt6WkK1L9WESyAeKrewp1mop1dChUmWMIRt0EKAEpjDUKkXz8G4FshLCio+EdK1iHLJHoocm3LbF/cbW98rPrco54HPdpMqDJZ/Nm+BlGfoW4VEIaFzreufYDfgZaRYxCiFrSe35ZPSAWU404D8O9GlhqiybLDcezTi7uE8zvCqt0Z+d//uUZOOAAwxBUGn6G0xCwil8PwlwDGkXPawYcPEmC6Wxh4zgBkNjjmTcSRZx+frMEShgIoEyp1jVnXMZ3dj0uXIMVUimMh1snWKtShSSJToJKIkAcaW7CEO6NTVoYLAS3i4YsxmB5uq/cFtcnKYEKGrFDabMcmvcUlrg3iAcpfTxFBJslIFHv1P/xXUdSn600KRVTEOFoTv/wvL6O261d9ts7XUJgWRL4AZaBxI0+WDwDZMf36HOWi9tC7HzJFJJOlma4pBQnGeUZPohLI7aH4fIpCW3kAHk3hGglPms+cop7Lf7lqIvVfmJ2ZW+vutrOnLRq6HdZ2oqUps12b0wibfAiCmzhBH//7IJprSNJJFAmmIVoRI6S+BWmUlS8IkRCUf7pMG8hdplKwHqAgHeE6dAjWxcEUzszbytmMLzFlrytO2f8Oeu5d4aS6FRcFJA5MFU8+yZe2WeXGhjhjU5glBpjFEi5E3Abx39Ht/WBESmUAQljSpS+zmBhlMV7vyW//uUZOkAAsQ20GHmHFxC5FmaPCOIDiEBL0wkcZE9oubo8wniwrRrZUGpASLV3VbPrwQcMvVIaHo3pZmpm6axqWf5+XP/eyE5Hr+5l5ZESU+fD+rPOt7wWhgoSZpoCoKWqDdijjSRJIBjfsMNiExhEYsyO3GWqVuEs2kau+0nh6q/LgP9QxqLQO/bkQNQd0JVhyvn/VpZr1mf3iDvds/8b5Gktwtmat3JLUzFKjldcrIrpo6td8r2cGtC2+jum/qQGHZsyZ/7QRKJGRjKrlYhHeTwJkeBOQEosStUqNeoRIThAg4cETkLC6KJ5X6vHLjbe5aI/qLOWwnJzqihIFjTwqV14ac+eD5LtWaWlUYIXhgUS5yQmxbBCxFGWoSgiSnWiCIzwM0V8gLBCEqIDcBub6v037WYHfySs3lcTxEUoEHGq4RR8NZNN5iDtby35Yp03nSSBY03NjUGwtHg3BnfRW00fTndesvT0VVZKE9NC7wzCVOB/+ijuerWsDZBuAk6//uUZOkAAx8+0GnmHFxO54lcYYM6DLEZPawYVTFKkGb49I2chECQuqUBTIkfwxhAADdKT8rVASjHlpUXZX2mTIRp14BFgymXMEbVsay8mNZRp/+AiXnW8e+LCcVWEAAAIGgR/khOa2l8ZJpgmqVuUsIhtHXpmKYIiSPrjwkEmMFz4jkaEkDF5BHG7rw8PNrcMIWW77sUD4AosjWKscxptTFjqZgxA+nmjmCFMxgI6GQ8L6DI1LcDXtKN/EZGSwSWnkZx8plcOYwVQYQA4AgAABgWsnYKcQwhCAOCKIAoRQGlCcnNb8jPHOzQxx2okxDxmXm7u6+63Z2qJPnY3bayqrtQiXlT+3zd//Xtpa7EVF2RrVkyFaGk27l1If5VIpJJ4GDRoC41a4FaIqo47JmwNXZM9DxvCGA6OE6OdhFz3vTRdrssPteNP97ZUPyj1Bw6ofZyveLOeVSrOX07OvnvRl3uWu3qXZ/0t/86FHBm//rtwIi1pHGquQQGRg8YAUTN//uUZOGAAwxITFMGFFQ+Y4m8PMI7DbEfIMywbck8pCUw8wohNFG8TUSsyoNOKAyhFL323ZgqjzBI8aCShcFAMfi7zo1NH3f///3j0o+oIqxxppJEgGn2fQKg3j0aB+nQIYCeGbOUCkakr0wPMLtA+AUnZ4Gi2JOm5/7ZWxPmdPCdJAYNsD2OqX7U6RfY1M7PbyySf5rRB9hE1QOKXsKIzakViwZDv5P0dBDEahJHerBSgjoXRYw+iXRqwPFonrD4qLz0G0Lr6V1EZpC/8myDrNy7Kh0UBCUn+bcUM+Hr/RYfafJH/vdT9AuFr+WWz9//r5hbXkTKw1HzySOSYQom65Uikkl2BQ0LJRdQenHrVKyROd3WzO7FnsbeH5kgFhJWfAdWqjB2UwdRfMf7v8YxTjiKyWlUgojlsDUED/zSehjeJhSfDj0JDwoITpueWhweYrmvCVFw9Lvyvs6wOxQIJurOhEN4ZCzxnrpvJBpIJULgMKIhdEjZbjt1AdY+wx2h//uUZOEAAsNHTdMJK1Q3Ymo8JeI1i2zRP6eYb3FREaXw9gypwxQkdzB1f6D/dJn1Z8+ieHsQSzzPN/dTUl6Ne9FZb9We/+X/FZS1419NMgAAADmFQ4iDhQdRQBIAKjwBBS7j5t6syy7DlrzcO80qPu9CLLttZ9ysWSv09ibRFTo1hRUVhJidxAVkpo6orLiIXgqnJVJgZ1etYJMLBwhHvQres6hP3yy1z7OX5lyH1pMN9TX9IHaQVqSiBHty8Zg40g+vyyrn/MJRfeGbKNe99X3SfaKqp0/0/DXIqe/7WiyxtJCWg9B3lMPpvT5bUPDPOEnNnTbgZb2bCoj5gMZCnjzpuQMDQrAiC1G/V2RLp/9Ub7dHGeMsQRt1Xl1vYhfcq510lQQFCSAwQISCnAr4cQvdJ1jwCwJoKp3+cWGZfJIpN0TvyRiagwIgFOa1meQcDHoMgBR23thx5/Mray9P3Ggy2OZmjaslteXJd0foxfOllurp209DfV6A56GFjjAa//uUZO8AAuwwTdMGG+RPY7lIYMNGUYlFHm2w1VEiF+p09IlWTWK1wmoAIXRsEHAGEWaBCThtfkquDTSGa5qBeyp5iRa/JdsSpTQiOQkbbW45Vuom8xV845h+i+9rF1KE83P+8AdQAAAQDxC0D2UDaoiyz8HzkCohl0HYiualGC13zhKV217LPORhnNyCxld2FGtkxdCi4IRmVUOHrOISrir5/KCM8CWmVIfMvcyzTT/aU7SlLL/WesmUMoaEtp94SCAzgqeoBqgCgAAoFeSwliNjGifyaRqvesKtfBL8lbpowR0FhAZhQU1YOAsF0jjiDR1KatyQCk89LG81vruMsiwYY9mPU5ZdzE3SYbUARkAAAjhJCvi6aE5FlPd9FL3NsxieopTC3DfqggDK5LrMtk0YvOtLWj0aN+yoqLOYitJNI4paSfN9kSbl6eRIDWsXQhHWfKZrnV5mK+I5V1LpkEOXpyOpJPbNmd/TG0dFhMPYv1iHG7aUgusF/KIkxJl8//uUZNwAAutGS1sGFFQ+Irl5PSMoDFE5LUwwZdEaCuWk8Q2AhZbVo7k8abCT0yi9uCpcrR3lKZ/jRoGLWWCRM5eQYpiRqQ0CvyfZjZRbMCkRasj/08uD3pyM1+EpYA48If/QJPUSE5EkUiUkVgbQNAGoJsYCFKMmxvE8G+I4QRDC2pipgDjwWJGjY4uzy9Ickwi6yFZbllSOPh3s/XwvA5i7UfyIp8LXGbenDKfM1Qwj9PgzHrGWVXZcTLEKWiZkWqbhgANFhDAxqelF8o1y9VRlnYhI9FxHDzcWCEQSq3DylWsbAzEVBBQqLpUNr61nsXF1E9xN4vlKxseLUIUNPRE7UiYQeHj8sVUFqpEQkSTQAJgr7w3BSIaWzrLRY628OQPI2UtFQXcoJwshZe6NHohBiKcQIctwjDEb/nGKMKLGOaQSpRAgKyVbXTM7HzPXzHNusW0fXt1E1M/VVFlHcFkQVI9WRxe64WFq8+Migr8bX1IIJ2QM0SxpxHU0GhUu//uUZOeAAy44ScMGHTJOaTmcPCOuS8zjOaekbREYBma495ggfxAZ6WHNksZYXvHcLVGFmglerc5B17yBRSFMQZuTfX90xZSPc0w4NZsSuFqVoIMCjtriTZSRJwxjJOkfwQsWsuaBG+oDGI2zF0ZCTnlC0U0mZIMWJmBK0sz9e2snKaNG5WS4izoKGdqRPPSg4zqMcpd9aAINELkJD/mvabWSK3x7IvkR8nv93+KwP2dy1Me32STgjo1uzEkijaK4tImKoEJMKEwLMcIKCeUSWggQKCA5hHO5lsx+6o9CorfooV/VybekUYvcAA4La7XJdIuQiL8U6HPhkBrUUpNXuqMBZQCtbGDEi0k+BXuS46tD5KKJFOW3ZPpMdogYIhwEI+LkLatk7VufAqGt4wH0srnV7ClW9ChD2Ogw4vQYuspU6M8XXHUsc/SQ9kKHQ8zmHq+pe6sqsqEhSeue4yH55Hx1zMTEaTUVT9ej2Mj0KWll9tMFYnIFxtlP/oSEgioh//uUZOkAAxo8TNMJQzRCAnncPYMnDGSvOaekrWkwkqf8Z4gspFkEFwKdkExTARNsozJATih5ZATrFDCrYelE5mKp/J7IlKNrQibFdTsi/fSl/9SIqWfCNeV+lDmIeze/qhnRj3EKxgtK4dDUIh4QZSAA1QoICYPiJgQAKAq9kTJKIFxUFI8EAeiYcHQlrU58ysNzmqE6IC4OoKuGcfucKBFPuDN3Etviz8yhw/L2265zXk+Q/26npkXJ/7S6zT02pEIBqASA/ohq0ag5SSBokCSZWUpQQeALUAGBUG4Tql5yQyicqFwhr1/0eyZuKR5Z/Vk4DFDIncHow/KYZxbn6qBRIx/ff27rdLN/ECqKb0EUoi4FUhlLJUVEmUS+IEIUBALyCyOdQjFHMlGs5UolTuLjKeDZJ3jatydyoTp0tYjtWb4GymZD2T3IgjkGmtlQ6nM/r/JuzsWLiJf89urelVQzrR1/9V/glOiA03u6Q2ImTf+wrNLYim4kHK1HvDLk//uUZOyAA61VzGsMQtBUSomKPSIsS9kLLSwwZ4EUB6Yw9jw4YpN0LR5QKwWclI0oYZ6FaRQIYayN3fdwQPi6AuV5ItC33oAYjKJQnb/V0650N0sNoDRN4x4PDHoa1IizSIkkgrBmCag0RRiHMAXKlVr1Dh9IefZYFO5H3Dc2x/BkOe0SKbMyImQCIMAKu+rYtJAjEJ9BhxQufJcNkF79WTFDdZuNTdNG1l5F6BBBo4Xmoul8y9/JDz9BDT8l+D+kI/LlMryfYb7jU92B8BVgWNC1LdrpbYdmbCUC3n8GKCoMEE1CSKDY0tFiwFbbOIMOdTF3n/Blnkmg8MJwUc/pLRtzlOskLpzkijpUUcaCRwQN0MMLhp1ftc6qlHv9kU2Rmaz710aNiUgFrXJTAArbgFKpYRwJ52GOTRFEW3DDHxFJgGigYQBcFzKFtEcIksk0FXkD5nFsKU1UrWjX0oMUUmtLXux+bW8yO0uf7kErFt855IkaMZTL88Sx3QVYyNNT//uUZOOAAvRIzOnjFcJGoonNPSJIDolBL0ekdVFmI+Zw8Ypwwws6UIqRHFEXWUMowpuAwj3EkPsvLBWQbqhcS6rzmlXJ/JGgKr2LpND9ppzXKTO2UAJZycIjwwrPliKeiaTCUutHXjqgo232Qz1YELY8p79frGgatheej9ACORoAslEqALjIrLJVmJyNMghaoiQkywdaDjbN9RQXOtYRA34JDAAtAgse7OAgI6HMylQS3arNSzujqLZyI1aUV8AYzkevLVSzsFJTXvV1RU6v/9KsnjrMZaBCrtQaSTRdCkSYuBhn2qhYIpdjQfM206i2RT003Q/y5uOomp2xIRIwGla6mDN1iWt2a7qWquJdnShlqqld/6XIYxn5V/273ZGq6r6EHK00fuKz+SoBjFgIkwoSTQiQQjFcJVyikocB9Y5lOUMfrxyAp2MQ0kgnnG5Tx66pLxZqaTowSMuqHp2orurxj6nONTVe/4Pb5p99xEJNJY9BidfumNTt9vIo7FpV//uUZNgAAtc+TGHpGuBRx8mtPCKqSz0lMaw8Q8lQpKZ08IqRn5trut9pLtHdK6fUVyrw3l4r2w0m46NdqsLvNAUKxHXF1OfIqLpVR3iO0NbOg7dZ6okoi4RSZVwQYGxqhNrnw6PSTTZb13RW7I0vbf4IW0sZNMizRu23H4XrCkJAB4MAQAADzpYsKAUEWyUDD5Y2ynUOQM+ddyoJjkglkPRzCvDY6ZyoxLpbBkoMRy0mPcEGkxU0ic+HbUvbvbX4/t7aXfLi/77lZIKBmF9n75/TbMcMdCrViLWQ90+6WprL1cG7cpA4cUQfLvDVDKTuCDpkrBvi2hBhSRlkGOxWj/YTAmXTCdVKzQsvdQGVaDB3CCMpVxtFgNZ7ZUrfiOdO9r0YlOT7//4Uo2fJtoVZWyqcyzHxa1qmhdEAlQogACLhhUyUBRU3gBc7LmnNGWDYe10f1SGLGT5dCasieEp1c+fOn42bq27i4qcIxSIhyq7HFq7LVmfKk7Ky8+cxSGJr//uUZNiAA0A6SQspXTJLJkm8MGKnDWkxJuwYVVEyGSYw8YpgYybUa5xFsnSj3ZT69P+9HIDcr91yqk5keDUAQmllMDXmSCjWUZEFmQ921qEqxWl2SCOiKRVKLwtfbHSxRV4mSlNIZaU/jHyNJ/fdKfo61Ihq0imz0srgrreqduR/6X9n/O5CjGLkYIrEjkWpY2EUiCAanunKllAwwuHXygRHp1nygdxZSsQPHzTx5SPQoxi5I91r7Jss9hKGTTnLJ0gvZj0Rnuexr3qD6IjEUwJg6OUaZxCPRAxrjIziZX8uj2+z+iu2qEEfHZ6CGjAIiHP/SD67KyVGkk6C+shdkaVR0l4LFGY1l4DFSAzvjwn0SdbbmedPrOrjKKvYZ00uWOXUutreX9H+04y4yzRnPm1jVQJ6SIJIJVAhgXyEkqJyJQ7w0BPU6VQ+hDU6ry6qM7vtSZy9LIvJ/vcNhLKHEZQtEOatPUkNpxCu5e4ltI0gTnmjy9yg6rUxYVGDN0ol//uUZNEAAwBRSuMMEtBQCPm+PGKZDLU5PawkTbD6i2b08w1YzRzkD+7Da7ExinESP9v12e3e5aSRNJhnjqPa0ItiCio1NM1UfoL5HK8nanAT/lIGwGHG7mVhwWWB8efS9yRcz+x66mBdfPD9Vx7M3fbkiSPi6miVFmkCSSSlj0OBhDSHSexTJZDBIu4LaRls8AujOS+HgnLENgfB8AKCyscJIPH4xLPgwtPayjuR8M7LufLNC3f/3ytV1fdzSff88XfX9r1//OzDeTb/qOZ3Sefilm4vQaMt//+VACgIEFeDaEPCTEmVra1MTnPVtjv48OiiB5doJQmJDTpNN7he2fKde9cAC2GLQ4meuFWpOx5Ys7byz7luUhu3pqhY4GvLUMLKAShQdQlgVgZV7Ho3W9/n3QQxQra6Q9NhITBhoRFAYqc4hc9Xajqfae67i35g6hGFAF/lik/aTA5DVPKWLLYe2dj79xuffpZ7oq+hL5zMihymuVYffO726uRYZ8cZ//uUZNSAAuEhS9HjNRREokqNYeYnjGknMVWUABEdiyVanrAAmkjcw/lPH6WxyBbdDTb3q3K5zLCjjcSkPMMcO5fnXlNHd1N1LPLWFrlWctXYpAjqRmgws0mNemlc/LMtZS7HPk/+VSlostb/ePc+W8pXUvWd3f/////8//n/////8sxtcYxhQhyZgVEdU0Hy2tu/3sPvQ2BTcoCpJVUbABRqHoisnAz0knjYNbSLWwwQgWnE1ZgTakTJ7g9DGC0A7KGtwUzemkbvwVGoi5UiikXoa8CTunfhdneLgMmaTYpNX5RLK0/Kv9vbczR19yitu5yaqy7dFy1/y/9U92rT0t+zYs8qXtZ67++ZRiU2pdWs17OqlFR3Z2nws/B8Q5WnrH4U13dupX+V47pLFF0U9X5IEQWCTAEAILs4chpimrVlbJmZXeYZAIRO64SSTETjGR5uVXB/JUlRBRKR6jwFsbJDuCLBajA2Nn65JEkQr/UZF4lkTElUf2U6Jqj/2/Wt//uURNyABe5aTf5jAACtCYm/zGAADTU1O/2WgAGgpSf/stABFakn1KSbW5zf1Xqd19lq+rvrVdnd06CS2UZGxsySNKQLChNdwhIbEgnUoZdJlSo9m+g2h7eDwywblIWTkYyKzU1J48TUkCikxUOYHGOAYI2Nn8vEkW1qq1JGRu6y637VsyP36zRlnUGp2RST+jpup93vZLZe1aGzPZr1Z16CC6RliheqcqgtqjcjZb4LFfBTIcXoCO/NhcCeAIzgM+JOWLjUCqNEXXeIAeP/HGFv23ZZGiTCkNcRegYI4CQWh3OgPFJVXegU7DFLWGVgALvYy9GJ7/4VzoLkCTlKDgCYOCpK3//8jlm6yJY7/wMFUpj6CguKjx3Mmxj4uqfojmtKJeuVAytWEqnaB4oMAiCSggLz6FN/3//t/Icy/6FJKncgAw5jV5h0rIVWAUa0gULgYCVUJ0qEmVgKmM5rNoCeuBH8a87SHVrqKgOIYYIZVSw85TgPnKdYS9rW5OXx//uUZGwAAw480+nsE0w6IhpsMCwxj0jvIA1xKYkOFmiw9hxuBNDsVHCcPYVPLLQx0V0mSIKgtM86Bw61FVCQxpKDMSyhQDLN1GpspNrk7i7m8l9WfvzuzL7K3MMOyL98Ey0nEw4xGphRqmJEJ2EbNEhyVg6jNYW0jS5lblAJE1jYdb5l0VjT3zEW/VejGECbe+/RLbpFh/cIFClvhtIquGdnIKow3QCFLwNOCEIBgg6JTqTNFAznoFHd4hEIRdyH/l9yfvy6XwS+KfoUQVg4RGJM96EZr7tcdc/1Ld73uu0dvJ08/D0U2uPB4sRs1Zuyzvt3iaeykVXwOB8/51/8x1s9tCxQuve3emwu+sXv1b1uwbMgqUa0CNtImBCUCUGb6gcS+HcASxrfXrj7mr/ONt/IaA0NGYJzGyez1u1QMHpndr9GP59XPN/kGhpuhCf/fscFepSVnFHUcEjmQGqmGHuYjgXLVKSBnCe4QGx5qLlzD6zkSh2BZXB6OhghAHB0//uUZG0OA2sxyItbYeJERintMCWXDpzxIAztiUkTGCiw8IqmSgp1aFfHjDNkF9dTVTkVGmnEesb5fPeu2hIYNRs64fiW7DtYPcWY3u0SgDkQCv2+K7322cDhAgZpFtprT9z6OfOznwP052l+Evs2K7Sdgrr1Ugo3FWXs8B+pMkJ5JmyK60smNFtI0xGObcElh7HQhSRWSFHNo4NnucpepBnH3gFt///BJBr7M4xFlKlJaN/hq5UAhAQAEgxNKMLLQwAT9LWhwWgAb1TNhRicMk3J5Y4jr35TditJVe+AAaBAY86JCCf3stVXvV123LHdxhIeVPybzk5q2Vp8LNXu+9PVW3+2feyQCrhPZ1/t29M7SoWgQeMI+5yVURY5WQXNvZORNE1ZFSyUD7BNtTtz4hY10nLFCXVZXaGnniSmM4opm2dpkRJ5jJHJTwgNslf//ShWDH///KHGZzS//7/Y0KCC7sw4AeIlm0wnrV3BhcFkoj/scawz2HXns9f2dpvc//uUZGUCA6VIyUtsLOA2BipcMCKpjoDzIC0kdwj+mCZo9ZVgqkERFvIg7DSIw8cLl9PYzOJn5oWuC4NA2SkIstSK1lju1ZoFwLolQL55to3IbUKAeJjZzoy0aMxdUKgcOAFASyGCW4JGhuq6imQigN+fE43f7bZz0BosTJYBdDckh4rKWon7RaFl2vc11ovPe8Vk06rGViFfQtnSUyknbIi5jID0/91dv/840Yan+l9Cdr1Mhdx/B9UAAMAABwAOHakNMrXRnRIlChFSGDA4kUp+LzsYlUNyOlpqrwKAqb0ssymDxOpScUCranZkDOMxjJG3XYsxiKPKkR+Cfrcza2jbL2GmnWiZy7ar6mrGI9En5kP5SfHPakHKsYuQvL5/0mhA0LsiloY1QxAsACEURC44Zg8NDbpHWExSbm4U9XYY//+9nVrt/za/yHBA8il6d/HON/+q9r1K8eTc5RsBCUBDZDK4W6kK8ZKRRhrS25Ua9uIbywiWFpokXiXbH6GB//uUZGOCA1ZIScspHVA6RepMMSU5i1inJww8ywDch6Ywx5iY+FyW382gokQx2J1tJ2LCSBelPidKVggl5o0bqc/K3PRcDbiIP8pwSFTQlDKDotdLlyhtcJt9uVYsACCpEqgFhAAeVVpuqoslYdis8yrV2CrUi184aH7ufPBC4Exzhij/b9bD/0b//QhaSjVV9lS60gMEgAIyjRDLxO+l6WfWqzpwW0NRo3RSiGKr+XmBTMbGxmwZxJD9kgZhPYHtCtDkexn1Xl8bxGpV9Pen3NEzJPnyZpfd9Y9ft5KQWg3s3odDpHTO0vP0qErX79rv/qmwZJgARGDBV2CvZkak1O9jofuDpW3vi9k92wsBxYwUGH2WAIeuk1ZY4WFeqGXqF2Pegsm1P9n0xbQXPZ56ETociDEDskATJgHazcl6BtRZhABrT7ZS1yPKuThsRIEawsBAaLaDBaAwAoFGAa2CKLIBhoyQUamUheQr6QHPWpgOpmeiYyaPpljO83R+59Ex//uUZHYAAvQmycVh4AI8Ibl5p7AAGEEtM/msgAJVpad/MRAASxQYClHlzD2OrnK7NI/bRQFjUEqtzGW7yCajf2NMQeRTd++z9HEWwKXPFGXzlUhclwaWy1uH37sfLOQKzKffqTQ7ld3K7NPnOzViNxvcsqcpJrWWc/LZnL/3//v/1eyt6sV6e/SYTM52aldv6Ptxf0Mp1BQwQYMiQYaCgyO0ywAIlqTJdDx3KhR1AgajeueITilrEmSIkSd1Q8wWkl4PQHIFBATpdHENAUoeAUoWgibR/FwHTcQUDVhMjlDmlwmCuiZDRJ88aGdZuxbSOrNTx5TF8v5FisWSLE0YlBBNa1y+Xy4g0vl0cxOeRdA1Z2RoG6amQeeS/sxgtr6dzq1KZCnZal1TFhM/AEACA3ZgBBRAv8+KvnwYs/EZUBQUlVA0oG/WFfnheKjM+OYZQKwPIvGJxTqSqSRRMloM7qrQOoomJiSJeSS/SLzpstL1JqMlVpGySWklTR93qWoz//uUZCsAAyMzzm9hoAA2AonN54gAC4zxQ4wksvjYCun09gx+DUkXUPfrvI+s0JViSVLBexgEAEBEOBONRfTJHoHYT9sQ9pU82qiCwfYLB2DgAEGL+mgq8j1i4NeWWdXgIiFf//Qj7f1xpyBtOqlYEuJCsGL7Vm3a5GXjgxUUc/qeirr0xXuT17GiZuQpImSmm9hHfTp6UXrLiFAKl5dy3JVh0aNOPuMahniYCOTW7s+7JSSqa20M7HZWNxtmcDP1rX1HAPZI+1nI4mkUwMgJYGYbhbbedcQerMjwH4YPz2raxHxqRHf7coo6AYViX7rxb9/6d395PQ3d2HppzsNuSyhAYdIxlmcfdtuttItMMBszwJjB1tbE/N3wGDYKZqhq2VtUPdWgnNogTurqky/3V1pdEZ6V+25P/bb3v///Mo2h7+6AJKAABcVOlogAdxkLM2dyOA4GXTTzMyYSs1eeuUYBtteARHSFjxoc1pl5MDNV4BH6GEKBSLJp0sTJyhp8//uUREKAAkJHUOMMEl5MQ3lpYSN0CbinPaekbuE1JOd08YqVnG6VCh5zmCnfZL/MAJ15BW6waOVyNpQAdiIDiS5dkQPUbzcZSex+WAy3tNRFpWgEeRSZR0mm9Z16bSXHw4bfL24xGVtqHAvaZdXIgvStKLj9luqtUPu6kSolvXImCpax2bE0SWwCiUCfP04ncE/mJ4c0H9CEKTrBie38BS3dUSivQJYGO8PIrcqymD3vJbkaR/utWt6s/deW/7JRemq+jp5lVJ3d72bwTx2qAZLACBoBRkhUV1sIKvOshyXVc91YDldKTTahKocRhkwSyGiy2J7Aj60VtvkJhCiMg4C0oYLnjgpmQj7ln6kW3DBVycUX2aqORc/abMAIe8OBZQkWpndTqnTQA7RpAEssGdiJ3JDQw8xKjkbOnX8LxILLnsfqAgELUxk0L2twIaIrVeMekKBL/UECjK4YSpD1bHOyYACAhAAAGDo1QYEDTPSBWg2dkcEShl1W3dGo0c44//uUZFiAAs0wyssJG7A5wZmcPYI4C3i7KYwYsUjuhyXw9iRgEtfXm4SRXDgQY+oMvIfKRRO3zZmxusVL1ku77s5pNGO9UeihwBjO3sPACEE7h22v/RrfNejX/j/+oFu5QSyAAoWiQVGBQF7IcPQNly1KJZ+A0nrohbMRIbSekzptyuxUeSi9oTAQnkml97abWMvp8VnttG5KT9nNP6IBQ0AGO9hZYaBRCDV+x9rZgktPdjfvQDJZUKJeXcFfDahXIENldTTMqtRIC2XsRRYdoSV+P7mJnj4tqGVyW/rIzSIshyBSwAw+ta45ggMkBKhm+0idPGGIIqDKGOlBTpCWvmYYTYDmGCYmkdB4phDIPB2ayWgqHduCiD+FZm/8Qjs/cmEAsFQi5Dyaf/+svsTV0WyiHIcxazhcBEsQgnINg8xL9001kfUeEtaGkeSHH7pvWK6LgQrLtHUu2IbsfPyz9amKBJAMONUc2cv28qSqHUnMRBZnmYpRyU177Ps08XAo//uUZHEAAvwvScMPQfA6Qum9PYMaC6jjLYwMuEDZF6YkxBWwfb28YE7Sk2mbmzDnkC1NhCdURtLOHCAFkAMjDUcgeDgenG02QczVFbvYwhaiK2q8vYXAgH503oiuowvjeuzU/t2f9P/1BCf0f/+FqgCQhEAAAOGdCAAXEKA0rG1fR13hh6Fu2/9MyJ6n+eqZ7bsWoNYXT3Zi5lU1Bi7xIQSbFlKC1jyxrZCop59MJTl7njRThKpApkYGaIjRQTJ/DwYRy0BhrHbYjNelhOHA4IwzpQAIASgJhgBGJgbNJSYfchCmwKvO04ORlT7+CYYe5LISDv6tWLvMUfEwrK0bNWnT5g8JhjmoTbllkYUjkmAYp8EeGnLbsObDj+OAzBx7NRgLyRmbrOw/MrgNkzxh/cL7pqFSCAX//7NAtJRemugXiXpqzUdLPkBFc842Cf1hCmUFKsle0y/ekxLe31+CDsKD7usD1EFJLpZIBIigxGuUBxwWSJIto5jUhzoKPvWn//uUZIgAAwYwSmMjLiA3QlmaMGZYC9kdR6wYUxDekqo08wlmugC7FNv/+hSGR2b1UyKyL/xn/z8ktvso//KmEa0dVRErK2SAm1LgXoaYAGP+/TDWbyVvYdwqU+hZvJh05TTtXy6UzuWa3n0kWovpZoU10//52tOnR28aVFyLKbLgZWLOeZznQwMpkTvlpfKbZshmVJtCn/nM7/fSRp//+eyBJdI6IjyzoQCkevErcUUGwwocg4ouaEUgQH3CiZoRMTZwcB9DQEIf2w56Kg2k7/fsYdqyViPp+v8ctDSlXu6gDIXLgHeoh3lhG8TM5jMQxOow5CbK5RK4qJlRpBVd0xJky914jl8blg2mc+ZnsXrX49yl/3LdPfiQAXXs/TZ3wZTHEuqLYj9ayH8j1fq2zp6avpPOVq6abMhYRmyzXSJlOJFIATpsGeJqMBoHZkTHC2nhHhUtbhOyOJV39SP+5rbbLLPTQIArcVaL37FOY1vr/7rf3dQB7WygAJpuUDyn//uUZJ6AAwVPTesPGnI5IZoPYSYYC/FBOaewTcjmkKn09ghm7WElrX2ItearHmbPHgAQaLhIeKGRGUq+IxQas+KmzYdD508L7//+6lQpeQoR10ZwxyEsludUOsrqQz9O1m7OlG89Z1f+tvr+6qWm/H6l2w3Jdr2i5EwmD9BCvXh4XHQIPPNh4XHugeLkAguQM2y2biNfVjlEzTXlv1UK/H0XYqkIKDv/lW+nyLGHj7nmgbH8oAJG5LQTRjSQsMl8oJcRrEB1YMZAbHAvc8wqjd7KrTBa2kVIjl9g/cCw1VqEXtyHu9eEbuPtVmjeHJgt1YKnWcKGM9U9c1cn/yRmonn0cc73e7qb/m90yGypCIglNCJkWrgD07TyUURhHOIaF4Aua7++rMv+XWGb4sVQVCmvOFiKGMjpziLo+/+tUvX9pkjTugnLekADG3JQAJy5AsYB9oSbI1wDgHDo8FIrEhyAycURfZJ5augIaOxfdPrQDMn599lqqdb45Zaf1sy5//uUZLMAIstNTesJEuI95BqNMSIdi0SJOawxC0jljqn4x4xeoDLfKbf1tqIvSMtcDgsTAMMlhRdpBtifxZzD6D6FLrrYS3QsHq9FgEhJOpSRnwQlAk4hvVhdtcrN8TzNjdou2HkO3RULkSYnqd6dTf+RFL7j661sZ00urIqtCbh4CV8SAJmvDW6Lm+W5Vo0cDpDBZj2J6c0yKqWrHQ6wLUhkUgWA8QhJG3lUhtcOh7TZ6IKLCZZAoATQkDQ5xhKmNLFhJU5Dq3uQdTHZ0fWy6eKb7Egau8M5iAt1t/EadiRAxkyHEKUcaaOKWsWQWnOOAjtbnn/xhpkXWutu/bgn/kCV5lV3an7LFo1ZQ6HxbPDJCN5tFVUCp3Z4YAFstv4Joz7LWngfKSKlS735b504i4/wOFUCAZPrTR9oQG0i1g2rQ8UCcdazI4gV6qoOaf+3XBU9Z9f+LneGdy5kTfmhnSj3/LMyF75nCBf4bnDO7A8VEZkAAHJRMEN2oyLNts9R//uUZMuAAsQlzensMfA/IzpMPCeFimBVMYw9gUEGEGg88w0QHGlgWbc1AAAlNLJvgvamdVKjMw474SULBBgValozFj5CSAymLO//017umEYhMPUXrLYwDplhDdww4CrIbMQDxIyCR7qrsaSwPTZ3Qk0ogUO8PDH2nfzgR2owAMVR2PiVc6jhLjDKXqpH0d70VWVDHeV3SZ1GmkMySbprd8tLo7//9ohzTjR+2asFrpcUDUwkK/UFzbYcZWvk4dNXwvVWlDqxgylS6O2o5LnUIa1Taq+6MnSR0Y9SddqHqf/0X+7ub7t+qvbTf/qiAAly1CUEnOMAAEopYFRCZymLbJHPOnM64lgkBY1CodR7Ibl/goQxvCtA2wcPFh4xUPtxhg9WLoSqjlEBV6Cl2Vndnsk27rlZWNbO7voipZj7OyEO/Zmsd7t6PJyJqVkcOB8agoB6NPQgc6kDijkZBRJIBjZCTZEElLE5K0WQutlZfKhOEwPRuzABQlnKLmr6yICB//uUZOMAAsZAz3sJGzJAYcnfPykSCtEhRY0YTzEgpCjxh4imFnU4dfKvbslMZTTL5ytejL9KUL0uldH6d/739+fWY5WHKGRn/1kFIAARyslwS7zZ0DkWC/LODuMQ5ZF98ZbPDcXrzBRrzgZQJwn5OENULm3ni1IYoiZm99Y3153Ts5803w/dxBxD/uIRZcDEZdRPduz8FevJ8x3aqw77f7u91+NVDr4WLewDALwTAABNJt0R9xhQ6+a0elld22TrbWogUkjPiG1AUBgGS6JeDEo7uHoKPaQ6uZrKLXYhFct5tNNtqWc7K8yXvVe/+jtl1rTa/6SbdSFPKKFmdmoAhysAAAgBQC+XaLwQh0WpI/RyMLkcZfb3sAXnPS914pTVmkv3L2tSZa8hi+ODCZg4iTfv2yP/8kCDQXz2MT3EWe853ay6kegcWbdq3pvqtU1u291S3V7V/7mIopHEpvH01zegrO024gKdrbcBwcJOeLWpCQnQlVEcphGsLWPh3pl2//uUZPUAAzJOy+sMKfJNaPn9PYJJjBCHJwy8x8lNJKZ9hJVhFA0sNBcL+lk9yPrcBYMIBjIsHf4IIdeUGAK0W0//9vJD1jZl6ZP/Cf877p4Prc7fiH7Zo/AAKOUgCStBMgEc4yLCSJc5S/HallMqXMinjC3H65MOHycjGypS/G45rUSAx5fQReyaK2s6A7CASKYgEPYhs3aZvn88PKPSKVvofn+hky8u/mGU+phJ7v78EkJywACKVhbQ+xbEssqo1TnYFUPLByVxLGxLOfYJI5k0SmGDna13gQ7TG/UjO04CAiIi7nJTPzwwDucm6rQLIFg+KGVvCzQC1hySs7jqxdqSdS3LLIQUkUSqlhjlIPIpSVE+KwV9Cy5nkfRxpeWh/Kp68YoMWgAH5gwJ4JDGz9L3GKSOEOuuaXcoaB+wiAxhRHERZhMcdchmYyUeyIXo3S/bdKfUlUHJJ///SCtfKgWiikoDuER1fJ14UE1YJDF6GQFZzgZo0qDQjpWEBin3//uUZO8AAxtJS2sGFTJTpGn/GeMNSxCpL4eUdklDkeXw9g0oTV/r7XXL7I7WAG+lsp6k5XVA6FIrbv/96b8svkqQzARKuruga2cKC4zfQBEkpKoSRo623xnFa2wNwfFyGcNCAkSYTlSsoeGCveiJJJE6nniCxgaOKj0D1XeVXiYuyjj7XK+xHWR3XUYglQgHIqa6ozu9HPVTuwkzGHwuvdf1f/+mgLW22MvdLFTbPr40RL9hMbR4hTBuSzC/FLufxNVB95TEZhE+toYdgR4Jz9HCUEWue0EbbiLGNAKyR0fONMsQqJiK7CZhEgfe3KsWtPEwdKEQA5glh4VHuQpJLGkmmQSaXZ2IfEH+hJdSvPwmCoTRhVTsVle+Vi2xtk5nxNuFnpjS18s4aRun//1wMUgpIgsSX0TUHX3sp0poCuswpf7gmAJ0F4UFyK7W229q06f+7/UC6YWABVKxiL6GsbIRwJQeQcOEs9QiJ8Lbi3XbznQ9kO0/n0Ny37q0RoDD//uUZOyAAs1GUWniLGxLiWndMMJJCyzjNUwwqxFdFKjxgSJm6j58RdwSBqi5UrNXdbMrgMsUtpTa4w8ao2OYXFLYvs+opAt54PsQgQgFJKEAEEhJQQgexIhuoSIwDYJQHIYpsqxDoiEn9Ib/VrL06pJmEn8bVg7xREGHWNI/G0Vh4KKkFRLLbLbRSkszsZzCz0J3bYULR1kUkp/FRoDA5Vqwi8utKppqlPPf6OpyfX2pRuJJMPFaKZaK+RCGEuG+dTgoYakmZXcFlUovIxc/SMZX1X72Zky5iW4M+ZLFZ7+s5269E4V/fJ9jelU00p/Tsu1KTVWkguaC5AQtyO1A1KChnEWWMmJBCfhDgRkTsktjfnEIH2nTkN2wXD1cBQ9W9sLGKFhCTYUv2i7TRcFlCP9zH7ZzMfMf43q0yQwa8sBVoTcsaMXDr3XhshF110bVpOOSMiaAEP5EiHD8OIaJYxgnLRFafExTiOVDjldFhHUDVK/Qgd0bR2ayJK7CYRNR//uUZO+AAsEw0WnmFTxThLl8PGWmDAzjM6eMtFE3p6m08YovGVWM23qqRn7Zl9/V9vT1Xse0y/T/ocl7aAi3VKckkYKSQIJpvErOsuQiZBTrCMjkEsfSMgmQdh8m8ZEsBTwNbuqWbMXWGe5IkqLqN/zcgxM1IWLTeJZyma67fbMLDYiDk416Dhj+zjzM017600ZUcgc2LiqiYZEqxlX/9cb9COTlIAhCZUwYhgC1F9UB5LlsR52oWoj8nQzJfsCKJhu6Hv0maayWRLL/qra11atz6bOQQU/bq/L0190/p0qQ5Kd70pUxymIaXdHwSCmnG0CWkSk8MiWAaYmsHOb9f0GqAtiXWyZwnqcBw3ij6iICkTRoyRp2Vbuj8UaFvnEDO0XW9vc8tGQkI9lOML9PdWiQpF0NmUrb97pRU2Vz12Qun3v9vhkPBj/8cb5N1blhoJpxpAMqtkejKPzRxIqFWbuLxkCY0AsOk7yCWArmMJsack0xcPrPqezUJ043qVJL//uUZPCAAqknUGHnM4xLqVn8PMKBzFTvQaeY9PEupia08wmgR6ou7qZFo6dGEBqCwSVKp32/8z/9/RS3Xb49OxTynZ5x4/mw3bbGUk0SSqsnJprRmNPOmk/QtFPlgzI2VtckEscO0oeRk0YobNiSPVe3JMq+/l1VwSJ4PSkUgw6urGPOOvbVjaljGRkfe/ysERiTXdDqVoIWhqJk2v2pnbGU///d5qkOWyxp3crGsIajRKoWqSFC0qJaiF3ou3ShjLSC24cLTm/QGLLnQsz+xWFhChRx54JmABEWR/QKkyXeknWj/SHPkgWZABBIBVZqAvjgmMhBKhGkiMU60GkSlG4PXVRoWrLHOEm2BCTgDRKNkStp0dR45NiOJ7UuglTZ4EDjztJ55dCxKlo7cklYzg5M4hhJA9KVra03ILjRYaBAq1jxrVAssok9/9idKppVKMezdAbLbTgOI7C5osao9SnPAZKjBI4SpDh40SkoUThnQv5UEL3CEiwcQiookaFD//uUZPcAAxRGTesJE9RTCTmcYSVqi/EZQ6wkT3EEDmjw8YnWIQKOYHQkQFguNOTBdi7CIXt+l8+uOjYQQmk2bV/+BIpJN4omrEwNnDi0TGV+MUfphzD2uy9w3gkJkxvEqXSHGmZ1Bj2UYebpLbRVRrOg2NC3VzPFGLA0BarT1gMoHEyKhSMPVoSxwqlsqWXeY1mzv/2aaka2QIBJKVTICoEZFQLmfhJR8i0FIny/x0q4KFAqHss6yy2qKUqWFIZISxCLnl5nvSh1gKLop+5bnB9D5fM2T7yr+++e73HWo8zNjGSK6EFP///pJZbjQKRSScAUKGfQWU8JbS+cGKKeZinU1ZjaonrDYAENJlnU86DZ1BpEKUa2Gjl2nC93Wej5QUjA7jBiCpzvVWRD3TEB9CFZUNZmq61ZuzsbbelHlqLmsIVeRGsuokIjQc5Fe8PuPhsbu4AIRac4MICBIDlcDb1yJIxilYFxEU5AjIOQwWVPqlUl0oFBUWpaaSFVssWs//uUZPoAA0MnytMPSORLYpmdPSNGCuh/N0wkrVFMIKWo8YpqWOqtAlAu0DFSAhHhM0XWXUXtF61NNpqHGAmtdtkxY3/+kNqOtklJEgmD0JckYOonhtklYA6ThNdWxycli3ZPNOGTXgoC5cX7V7YUdepoIS3YpjmywIO8NVgVYfdtLVkHQjntI9OrkJ+lS7ZlnKxRdeyOj01iXUWvOuZ/1agrndsUWWUFAmGAbkRC2ElhprE48vrEQHkIrGLTpZKivjHNW+BMSJYORe7tUhXRTbBdasj8wtMinonqmpny9P/83Vauup9mVpkCtQUnG0QSSAAYIROUq9v1Z2gsUlrBa6716N5KYFpHxnJm/IUgkgjmQVooDSna+P8ma821bPAgoIY8qqXZumXfx5jiv6JBMMjEOh3lOhmu1z6O1d/bbU6oCQ6PNM4X9GjgCx60AuaSEkokkBSO2C4K85ykDiIkkyeNdQ0LYqiTFvtbiSPgTt76bti6sW+yq2lvRLGYyg4l//uUZPkAAzZLzWsJK1ZQwbl9YeYqC2kXP6eYUTEoo2b09gjlj0q6E0JotKA2Wmvv03P23qnZyb6o7BRgEWdK/+/voDNYAAAgp8BYEgh1m7gq40AWy5ClEdaMLJMMKdmc3UVQQXFDDJepl4rK4DqCDb+RCGmKNAelVUKQHaDMSZiUaUFAYhCwY6GfJam0I3CkbcJBow+XdyVT1bpdDmd/mDKDjwPRv/pWy1DkaGMq3YAEQSlQrS7zwp1CSAaeLQS09Vwu1DLANicNSolhRZj6R2EuajmI7tBKcGdmZkPtKvRf02I2irN1Sjnd58h1sZaVfn56Sae21tKyhDizIRWrQRJJScDjylQhLxXK/V/rtts7YE74YD+QQQGQVoR0os7Q8S7S00OI+QXVD23h+1yiTcxQfewy1VSaUzOzp+WSZnlWc/h0gQvURQxpvizXxRWiEIvTyL00IvzBCQJP1/3bxCW1gACnCnQ4HAIAGEtK5EqxFKNrYoUKAg1KmuQvsI5b//uUZPiAAxFMTmsGFExRqOndPGJ5jSkhJ0w8ZcFDI+W1hgkgOc0XTfBsRGbysuJdV7vkEEBri5/Rer6++ayEuV39O6/P29tNY7ij7bnKr8kiSSRIIoiBHl7M3ZMgolTSIehzHVZ2/DiNBI0wohWDdK4nqHy0ue+sIzEEH8qylrCyjlGZWHKklTuvum6tmqrmEMHEnVkp0ZbranZcEgNWEZcwmq//9nrqYsT1IBLN4FQBCGoLtS8bE/jEnPhp1qSB25VWnTd1mJdGeHO+XCccivvCkcrkXz7kSyzsq2i9nQ0t3ke1T0pQnGefo3i2stat1rtVnd0LT1EqCe6EkklJ0hQCYT0nwQEsZOCKFwLVGA4JCYlGAlD0bCwh7ZxSaWiLtwU9APWq5FUXV/t/ZH4r2VcHu5a5bH2ZvtXj50O/fRIW7Ia8uHK1y9/fmRGNSHhhM0IcdnEhq//9q3tYpibVAUtxMIlEgvBElycy8j8Lk3KVBP0KbFptvBhx6Q7/73JO//uUZO+AAxJHTFMMGuRKCPmNPGJ6S1jxQawkTXFBpiXxgYn483ymoQ0TpAAYzNvSlUubB9B/+RmIt55A//PLv4IcjErg4LE8NxdsmK2RJpFAAqK0UkcSNAVAPxYl2ISWAMdWtUx+l/WlIa4IaisDUeLRwoDOKJ6Rr3zVl4n/6IdpIK14wf6xGGRme1vQKrmRBbxmOvZrMPJjVX3Z2OzIr3f55sqMochr//0gSJpoAAAAp+nYXpusYJjrkmHaaWySJCdDETkxGQkA2euFaU/kqL0y07YMpY7oCquMLLy289QtanYff8ivPwSEidLQ0WytQu5aABbQSAzjLwD/9v9dACYAAAAF4DAmbiZRCJCTKymTvY3Bg8DLFf2B4/hBv0Eo61I+JQbIw0IJjbxAAE2OgwTJsxQNWkkzFdYVJFZiM0+a67aJicOoxiVrL9q/qSKboEEF36RNI6yd70zOXdU00MZpqKj3urV7uUcwQ7v/f+kCGZEgIAPBuLwj45i5jPLj//uUZPGAAzE1TFHsMlRHqOmNPCOuS/UfP6eYT7FRFqV1hI1gGPJRk4Li/iOGVZlyClagjX3GIEo+lZNoT2ZaxW+hGzvZWUi22R00ujlLbf/dnv92OOdkZSy7q9ftPVALLhKABBKToRQqyBIgyiXHOckBwNxViMASgpV6uzRogSHL8dGm0u2MwZdQ2wzkMYby3S+1b/bx/lU1Hdmz/x2L21t1GKH123OzNmJ4CzwFadKCZvExuLMWwGwoCBwyX/69kwIopSkEAUHOFyaYjhzixQiwKI52BMqCMYzrwLM4fERYXGDnadys1HQpGpypl40URmrxFCXEyAKVGa/WnkL2qRHV2orLaxEdEdjGkqdNWM6qNSpxweFRMZ36FU45C7FAW7WkSiS25ICDDrkigW0TmNwjNKgJAbukRIBHDKJDxBFt9bWbEqZnEiBNqzrLTP2lAWFvU372UVoQAL+MFYNDEKRGYOlUjEpdFX1YcxOCJ2VRNlr/NdjbhvPE4lffSP35//uUZO6AA4hISdMpFHBKh3lsPGJ6TFzPM7TzABFjpCY2nlAAuHLT+55QHHmvbryK3AMPw9GePJL93aKV0callv19SrGrfprEYxovs0cml12J5z2UltQxKLdyn7uMXZRv7VrPKimqSTW6Ts9Ys85Mz+GONNYysXmfVBEyKzzBKZoatVCm27HdsAf2SDgpKBIkRgAa1iEdOLwFgbd4mhJfdaxbwILmwJMnLcxPwspCbEnulv1bpbcMu9mVLVi8GyCCKLdiHFkN0cnj+T1in2+VmtOWP5rtDf13GYormpuzOclkspbNm7lZxs1pmIXLcozxykNrCzTXKan+zKsN73nKsKlihp8+Ybq5Xvsbq3s/jFh/eSyV7ldz5DKYrP1NXLt4xZW2wAAAAAJKC3IlJJLmGBukOAITHADSBIChCSF3y4ptpCEQPtFDACS4qHN7SEArKcs4QIkHa+wcOvSgZ4GSus3MOC4Do2gS9FZ5ocVRirdeYv4/9HlSUsZsVNP3SS1p//uUZOCABc1LSe5rAACpiVl/zWAAFn0rNbmcgAJCJac3HvAAkflkN0EfL4uPFsotH860Xpo1Vf5yZyGIdoY9XkmEbz3ZkdTT38ilKxOvLJ+DL28IzVnaGM02q13lq787+Hxjmd+/l/473bp+zVXC/+se50vatL0z/+lSEBAAAA24FtxSW33+ANNDBulhCIQhQCAqsT86BPgH4emAX+GpAMhBQPbMExkqBFjpcyNxGgmEY18wUE9mNJaZt2raN67/jQaRsL7NjcVvgwsQ3D7nmzerBHtmaWRxzjEGsFwv40amCDw1ypldelGGJJLN7fP1S2fiHdWzSxmTNffGN7zbHx4EGO8jyxIkdP71ZQ4QkElOYUIW8Bzi/UbRsepp88MsBX4cRpX+3JZNqfFeBVok0kTxBS6XSiIIpk6dKhoRQghuaKWiZIKQRZEuLQQL6JmjUxgpG9ze/rrWcWrWkb0SLk63rUgqtkn21abqUo05rplx6R56jV0rJIrkXJ9T+3/y//uUZDqAA7JITVdiIARJJZpM7CgBi3EbTawYUXEVE2Z08woIBC21qNU06rCM5fEmwHhf9DlOvHDzdlI3orLbe6bxBAJko8Ih8ScjNNqp71s8xkzNH5jXRev/5rt/Ko/LpYSptfK6UU06Fklh0ElSuMauJJNFJ5NNiRtCNJjynTAFH8HSe70kJbPNzbjH7rsaRIBs78luMixhhNPf37rUrxYE8d1HLi5yarGvwYsSreikQVL+VZWY7mroD71enZz650qiVVhx7///1vJgWhIAAhOBPBaC8k3VCqQKUjFEndkySylYFVJKmLQU/RqBtZqlTvWzMI0I1yr9L3el3MQcymN/jTK1d9+Y0AabpklAJaptsmxJKyLAfpBD9EoJVUE0Jq4HqSbJSBqxvORlOHOnCXMKXy3D3uFyRJSrNeWxc6Sw9XFBgwd/s2ExDX3PuYXtHVBGv1hLcBG+5Ul29Fd8b/+uf89FDjaXibZWWD8YB5gq1EuXsBiTD59gRFMsClXL//uUZDgAAqsnUOHmHE5GiWo8PwJFjLTzN6eYcVEOkec08YngMBqccG4V/z5LuinSR3QZbXf+lnQlGLfvujUKrcl23ddpl68v/+7L8tGz8E4RhLRJSJSTeCkXABCT1jKMDiO1ShLQlDpcF8ONHPYMJHXqBmIITC0gvg4vWKJnynW8lvOQPaXPVRmnbPzUGvl0eAXcSZFqMY9D0ieEqJJl/YX53HI8WKgIqxpQzOh4+3/1LoSvftybpmSbYUduAC0qicnga0ydyojLNbdpICeywqKkQPgXCIjy0ePsz1vQmQxP+rOUHFDDvv2LH8TWixV7m97b1h9IinYM1TIAAAADJlAhBBwIAXsVBW8VXZYqqNq1nygaBUUxGXHEj7ww2rYWQ20FWWpoirMOY1k6mZLcywCR6KoBC6Sj00Oq2cKECdqvY5BHsc2UWJJRzGuYUflYROrGCcXRpeoBWWL///5oKtrQoFAJt0DRoEgIiWJlS20Ep1FGJ42VmMUuRDkQOPOo//uUZEMAAyY2yTspK3A+YZmtPYMMCoz9MSwkTRkNFGX08YmwKpNCwLJUl6/9qXHAieaBKnyKrtu/0e+wNsh1QkUqxqgirRHw7N6XJbuCwaWNaZEyXOKrMaaGGjDZ0dSOlDElmYyOZK3zOBKIS6ImDMCRotYu9FUAHAaBD9h2KV8rIrtRXL7rZve+tuJZdIsnCPTnSmIwHGKCCgAERALqV4hpLnE6RSSkVpb0ZpNIaW8GquFRmdDKEdBCTBPyvCshZBleE2r0kVlfa/LseIWHDvNe44jI//kHORlOhIIJScZiSjS+Ijs1QiWfLoPX1D+mhNIgBAHDI8hQp9ujJWzCSBTMf93b/u103XepOSrgk6XDLdjJ0azT0fSZ006te51SMxhpN0X97av1aLEqzvresn/9YChFyARKRbouOQm6iGVBEVMrDWRJTCQnLRUfGS/IX1Y8S5JlL5EQnb3NUvMNhWBHMv8f09L3zqbk12/PxECpDG0QSQABApD+FjIUENLk//uUZFOAAstFzNMJE2Q9BAmtPYMaC6z/O6eYcTDvDKdw9gh0ELIKh4CgXrrovyqPotr1yYkSweBUdbMxmZ04JAC3vyY+mkKwNeawzEnFu4W/8zufnxk8n+/n5pUrS49Mu9zpl7coJ5JjMAO/q97Pd6wR7pKXCtcPF0SGD1ByHMlkIPXIY4Fz8watfmcWysVX9yablsMlWn4yXokb2ex/3vaUeE0Xi4AYRF6E0tXASQAAJwSMP8hZMAZo5TyPkVyKaS4OBkOkfkKXuCrKNYkUIEI86TK8rI5h8hJmmpbLGJpwK+Mt2yNaVSisoMY9vfP61RtSEp5sGDINhJkFWrMqjdK1xQu74iesNWXaTWQ/+sFujG+hh+tinLiAQzP2vY0ZMVNkrNBuxn/bPpdTsDJLtRfatKmgCHXv77Ytt9fvqT1Ld0AzUgQASUqDHBmC0K8TU4zLBGDGMsiTwSCwbjxhYXlVgTWyeVxElESmw02lH63fNZ63bhatw29eK4xGdCya//uUZGmAAt8zSsnpE3A5wxqMPSI5i2kdMUeYUVD2BWa0x6UApfplPut30fdJlbrtadqtVkPXdbG7VBMb//X4v3MUBWtrCESU1KKKAkKpuTEXHKkMuOFRwRFXhclmPCQ4VKJBFpAsxxIsu+854qyq8yLvdeETVXq6Vtiq6V6BQioEqKskoC3gAYLyWCOkidlQWw5hb0JPhUkpVj2Eu5BLRHZBHLpI22VWlgj34vutdDkmuyKcf82u9cVpHDeku9qeg7ZNwJTRWXNCxIQEUTopoUhJFEiAgCTKKkcAFQcASEIYwgdJzbag2HwKgxEXvOW3WgkCRyk96U/T/6diU251+v13/+uHOdbZFRu7r0VF7QdyQSekJIIKUBhlxLC8DoDOOyGT0HIkJCra3R+IZgwU3o3mKP+6w5MEwfJgKw4UPZarZgokCCiZj0GkRRzBKIA0LtLiMLqtizkjQ+OINzAXJzZ8IPMCIJ2Qn///9UQUdkZRQYcwBcqIFCP9yfo7wPr0//uUZIAAAoMrzGHmQ6BA6TmdMGJmC1x5MUewTVDvhmb09gg4MfTxxq6hpk0LodI3UD0za+yiw2HVCj/9zEgBH5h0aBKTL6hQyMjGkSwtkIAASgGYDpVHWeO0z1K1rLq0zK3QBIdiao1QvXUjXScO/KWDKdWCNtDGNs6k9PD1yziuCEPezOPC5bw3NSZxYGDAiMuH6KvjGIr41wPXruFexbtsNOcSbCqjlhTaCLnEaCujzOImY3NDOuemPKF7havZRBiyP2ZGez0kN//9ZusyKrt/39/m11f/q2mk3+tJHEC7agALgAACAYVClprP4411jMJHkwCq1gqPiQeFBYkJBOWwl7GY4FZxMnqwiHgFV+NbSpuJZmNNDow76/L+I+8553fVGUaMAbDRAWQBVIFHRVJ5vp9xw25JLrZFscDdOYcRAF4CgkJCXtAmJRs9zOuYd6MZ6lMtv9kb///dHHWRUv//33937/7tatd99nrowMmNM0UDMFhTYAIIAbgQ8kAj//uUZJoAIs4zS1MMGuI8SNnNPMIqCoyxKywxB4DxIymw9Ih2wmgjRfCeMI9BhhxhBEuu36sZlOhS6MalUk2sogNyXumosjcJbGu/Kx9TMgiezvN9M0r0YzWogEKhfR2ameNx10I07gTrVK25r+6zIEhTCpCAmUAxRi5un+fR8t6gEXDiIeii0gEo7PnxuuD91o8fPL+SUxdA1LtUsU3TKPO/7k6NJaxd//xAYlTaSA7NYiC2VJwSklyfQZloabLKUBxNqTLB0EoLqAkMIJx0aKwl7Xt/EDGpqVT6oV3QyWIiU/bIRNjFUYzOgsbp+7qzKJKR3SrFRtdv9tKysd1HMBXpUSCYKxvhum9VDjLafSRZTpZBa3yvncpWIBATHlIFHceqAwtyzGWa95GZYHupGsa24SOIoRf29hjf3NjlrQQgnGAASASuJmB9K0WgV1Xnqa5CVpXnCeqkP4oGCjbmKpQs0pNAq30EbJgwOnpniuU7XLgZSV13yv1dVNGyvfli//uUZLSAAsM1THnpLDI+4om/PYNWCi0jN6eYrsD8iiYw8JnYKLD7FSYgCTHClJprbjrwP3dVBH///1gxGGogBIBKhIH4PcSiBN4sJChKwPbqYhCbXb3GENmyXn/ayrlvkqJMqZ7EfDK/zzmKZd9BJWc8js9hv1uCYaPpAO5AAFsBRlNWeQQ15xh/EMxK5QC0eRWTTs5PY1UNNKhwvQIHnRokduiBceKwvdRdQ5bavcIRMLU3217Ww5oe4gsFp1ysvxu8tZtYxk8tqgI1fVu/b9vVAFMaQAABY5F4JQL0lT5XFIilWQkthzMnTAQPvJ2YSllCt1YtcU/rIhf5i6JvocBQMq0jYmiRfI+ypUBxlHYq/VwJUiu/6STKbgMlmLovKdtDuH2hUNGlyHxFPFPpRvPNZuuEKaXNmtIS7SeNbva0Kt3/tz1sRmuusf+r20MDAVfY7Gp6ihK6QR1hkmS+KE7q5xZkJuduJU6rkqtJQ9HEuWIsB3qMFEvi02iESTqB//uUZM6AAr0qy2npQ0BAZKmdPSMsCuCpLSwxBckKkiWw8wmQ1V+yO73oFpOxKb0dZZbEzpcrAjgGRO5IKoWuxl5hyc926l0Gwsc9S7VgqRMkAEgEqcDETLGArCJ8s/W0uYDciUSjcgDgoKKg8PnOSY1M7rkWgiZM6RuC6qGP7lrokLHIViGXcA8MgAuCzxCg856qqkLWgoXUlKvaGBCJCX/9BacaiRBRJTnJ6K+AdhVDjQkvp6iAKNRr7JcHxWFDb1SNRVjCZHTIjTcxPOqQjcETz6mvF3LeacLPWtF79VqDx3HC6yVBJ540LB96kEf//6Iqvqikkk5lFXsWsq5abeUzAGihckGhBQccGHAyAQQmorDAIo6cYKwjtxChi8CxuVLop8TlMCQGA47xuPmMY6mf8X+UjCqjLk4AffMaGFWDRinzKl0s5/n5X+W3tIeXT0+UzMibEuNAbd/+3WGYmUUBUAiQCHWbmFermSIuybn/Iwpg4UupShAOzmAyTEiJ//uUZOOAAn40T1HmFFREpInMPMJHCqBnLawwxwE9jGX09I1YdqjrSsQAEFXGGBJxYMRRiT2VJhp5/dEHO0GXvIpWZcUcbIE2EUo7xIgMpmIpEpEAGIbQeTTCpF9PgoI0dwSBYIiKJInzMI0Td0doLyjlgRFa8ASKdZheQBu/bvL4pmYl9O02uSd+pdakcJCR59EcQsRcEToRM0HxJHrDGcTQqsaIf//+0XRrpAEtlFQMdAYRjjyUMBAu0mXqyrPlXndmq+PRBmE6Yt4rRm1YaDYSNPnQbe4iu1rQivFRtobfecd33zIUA8oJydZI+EysBgILVQNMAAJJJUUSVY3iAhdq9U6i6MGrsFk8OE3EgYSFYbCNwBxxaoFVUFWFQSiCNIhoKIUYcPaooQHNbD/gz/ZcsyPN1HUSFFChAP3ScOgAm0WW4rJmACxsCq7XHP/o7NAJJMQAAlXDrDgAUDADAMwPiALgVERu4LH4fIo+BW7YQDPLaJaW2UkemmI3ps3J//uUZPUAA1tHTlMmHTRLofmcGekPC3CJPaw8yPEqi6c08IocryGcME8w4uKiwrQ58LClrBRgzXi+7V3mR6SySR0lIqIlKwACDFpMoTMc5pDLSKqHsYaiDkD/UT5Kq+p1RVSzhBxACKHfe7MMKNf81u7NuvW2BtyyTaeWQ5WhPzl2XQzSl2/zKChtBwE59yZmleHuCmRfT8qfzMENtqSkA2CMBBIkN0KxFF6YC+uwzhDlIZJ9TXRLWtwXbBEyWaxo2hSu++bd367U/SGqK6PaiduzW9///ZPtjozOn7s7KfzrsZ/xH43tn9deZXMEkgkqnGXpFKAJirVVInoqOUNkVdTCQwBINnJoQJWmwvMSuTrEjM4I11SqbqarChiCK1jx6BSc2ndrM/7UsLJxpaW+VL+HT//JXlyyWlCI0Iy+X/1QEBhYDIu/U5zCsnRXeoAiKqvlAUEYTDigOhkVggWmIlDgQYX1qVyS6hGg+BYrQ5xQc/MMFOY9ynPiFS1f+sZr//uUZPQAAu8qS9MPGqRLJDlsPYMOC7kXOYeYcXEvGqY09AnpTKukuI96j5DO9Fc0RcUc57H81+Pvb+IupLtzhgkkdkjSTSJVDTWmEEUFuEpHiLg3kHHkrRkMo5j3hKkLIVmUnDeJGJ4YKOrODxjJ7V3PTo4Q6TjN2wPG0UslMp+cOfoa/uDOx6HWouKGxZgwY1ph4sO4Nl9N/6AcNIEACCjyt6RCOKtkSYs5A9iBQ1QxeWW1dXrrj2Ne5aCsnURsIIcYSM+ZSuFOMfITfO0GTHTT9yUXx/TE9kdf/l/KznfejLb/ZduRGP8E6bsrr2mqrwFEjYkccKHHkCmr8Stub7Q+6jZIu4LdNFmkcTHGrg0oCSu6xdFY5qRpxnoEHW8ZjqwOatzP0WerdzTIESqIjr21n6XTaPdSRkFJqtW18aolSMASiQX29VGGbLA1WPE+jSY09j9YxOtAMBrJnAFdYXJtZf87U2bhj2Uez/UOX8SsgWRp4q4Xuj9HdbuzvIWt//uUZPiAAxpHSztJGuRN5TkhYYMey1TNRaekbrFFDSTlhgzpVPTvo++ht2e3TtbWbNe7AymFuUxn95omraSSkknTTWXiVi0QXE1JQDlXJMytQg6TUJREeRF5XyKvDTzm0keGL68Xs7isy1f/J+O9lFYqzOTpISdKeZhzJGd2w+yTfKNX7VyDHxF5vdWYyVhFZqeMaJpV5sSAQ1//XfFhruIJJJJdiCbB/j0i4qMnpCFoRk4mxcrkXwoc21PXniqJjkqFSxHeGozvZkNNvrlTRVu1TNVvtTVvZ1VLdZ62m2t/N37+zOqsJiYDQLjCl+7PepItNyVxpJEgmnBDslBR5DYoKexlE8878NtJIda2wbcWLiCKxCIiUYQoot5iXqKbaz/u1mQ905t7sm94cExMHZUjDHQX6pGjSkXFiOQ2+vk36veMFur0fQpHSrranEW/9ejJtQfskkAFQcEj2vJwy7FkOyQMIT4zSoi8T1x23Qb0VWxQzwRiURwQqLU1CIZb//uUZPgAAqBGTcsGE8ZXCQlKYMJuDHDbM0w8xZFNpGYo9gmSXaglCdL9A5RtFKI9JVwkMEF/JtdQ94spdpCEzLjRssXX9+z9YKiTbSKJAAMLA10rVZsyFmjSUDnzddntlx2mQaGGCUUFhRjpJIGaaNLzpHvVYeYexJfOIrmwbpOUSCBYux5nO7FFNF7Ps4xFngipBdi9WF+5HGzIs9//9p5GY7QCQ8a8uLM/q9EGxKgoLAsU0NUUfdw5Q0Nrz0wJLHab6F4TInqThqPwN2a66IzdnsODnOqUuyF5Gqdnd2rXv/VejhzAB4MQdQRj7IUXYl7kTy4jBYybGErqS++ZFJFJ45nRzZs6SLTrKCJ4JIK7g91mDYw2pTHJawCS3NXJCmkOVm+pMoVr9Zy2ff6RuqlDsZ9zKvXbrkp66oussGlykR0F9mMt3K7b58MMUKBcPISsS6n6s1FDz/KGwAF9VOqVrYqkqWRBFIpJPAmgDApV8Y0pKzrK8t4ipJxYMDwD//uUZPUAAxNI0GsJK9xQA5lnYYMojJEXO6wkbXE6mOXlgwliCPJVB3t2RQg1JNgBlEMFjjh7nBsb1xXPqRjGjFlIt/fQ4tn41J+7o/2+7WJ/9qEmW87KgziZ308UjXdSzfUokElPH9CJiPKMiJoOEUMainAoAyZXEHLANODZMQzEs0Tg2RSSxzepiBKeLznbUr8NPwckl8kCkznKijKdFHT58ywrILWQstqXdDdpmd/VHauR1Vm2u81is45iP/+zX8KO/VIxRtKIcSQIAACDGADhMxxGNpbxP1Sq6VG60WMWTMTD1c6ksePbalnL9ur8mX6RY6ua6kKDfrVyxqPDh/EZp00cpqXoh9Q89LtFsLK0v/8/v/P7z+3LWzOaMo+D5fpT1SGupqJrEFXKYcEGaHaX6/yYE71zx6DoPS8PyZASTxokDo9mq56Fqfj6uJsVMJ6c+XjNpbaV27yszPJHDKPuEF5gIku30wRHPTnPPL8i8JSKhChaHV+oMlS79EJL//uUZPAAA0AyzVMME+RXqXm9PeIMjQlPMUwkTZFtJCTxhg15ccSaYScoGAIUa5xlyT4zm8nY8FARIE/GCUuRMZabwlRtYZWqgooNmgg3U9x7dv0uaYse9yDhGpk8ZNVPo2olBBJASpIbUksSqlSDGiEYEMG+Q8m6fejoQJ4nicja9jpaSNjGIcUG1BCHEMSKLT3o6ghQgNMwasSgINuJemGO9YVlVOgyHyiu+TIs7CtupuCFG563j6/7/7SHlY6uUIq2C/KAAAwhwiyAAnA6Ww76HGiUedr9XKmIuXseZecDDOJSpYc+ziOp8hIt5sNKq0Y1OQoMklPM+u7SlbXR/7I1fVdnk3leVbrZSOFU0/PX7joBmYQAVhKgSaAQtxfppsUSKZU9L9uU5MOvDLotdTlk960sLB4DwgIKzdmKRMcvCEkMld0L3ZJGsJf+3SUR8frV+suOE62O0eZWfzNbekqorJSjX1/9hCygtgshkBu2lJEwGkUk6BhDpMMkJymG//uUZNwAAtE3TUsMGvRFwwmtPMMqC2EpQYeMU/k/I+Uk8YoZTQyuDVYamJnhRA4YYFTohzITSQMjyp81GhruErUgq664i2XlXf0Yhvn66WLhcXYZAwlgEaFJZJE0kyiVjNsQq+wuRJC2BgkgQJOEMYGi7ijUgYMj9wTrmJX42V2ZLrRb3ZzveAMGZHAXUxXf+z5tCFDC2aMvt9S0fUbpa1XbaKYr3CSAQAI16f2+kx10ABBgtgCmK+ji9msznOZScRItKl8FjXRsbjGqmXwZc1dBnYEERGeXK3ikcOLOpW4ull7DTD36EzCykq1T0L7qZVKBRVUCnSSEk03i2BoSOEbLKAYl50WWJAgyYD0JtMiYZjmnfYYAcYgohAzWXIpBbLsZm1R1B1mNfAK9V/mXwzL58zMBKNgzICxedWUhQ4ofRZi739QMSaSRVZaBOjbAEj0iAS4ipWkMXB/lgMlmRirV86wzO1Dhy6GFz1NztGm5uySV7YR/GsUjlMQIwcuK//uUZOSAAulNS0sIFHREQVmtPYkYCsT9R6eYTzEWiuVk9I0YOMVg4Lzw5pwnYxaGG2uQOCbTJysXdqzIBRLBpqRwr4EAMPBYqmyuC5Ke0wviZdVTCNrDyh2JJQM4gtrxRhh5ZpHC0QuVGykYhnI/kVk0gcOHM/hiA/i1mxi2/KeWUgg8p8LdgbOtFDXhA0FEHejuDzni51FDFqTMgEIxAAAAAFddKAIQDYCHoGg3JQemCEXhWD4yX+VR4A0zSjZsMggVBgmTGoYFCgJk3qcnHE4vPPiKxYvfatq+DImYMNx0ifcHyAIfATVf/+0qvtUUkk3xpzG2/Xmoor1AA/TivxRKKQE8i0Ao8Vo0Y09UxF8sWsyPlCbsAKGitGFsA8dOghbAIiYUG2baqF4MLLzIqqYdZqErGQ7IVrm/9x8GWPHcwXDM8cvBI4UTK3TzGJ/IxNCz/9ACBiSJKIJRXKdDkGHWBCOZRYYlYXsI+oi/sSJhyLCRZBB9dMsx8pv/ZwmF//uUZPOAAocszGMMGjBUhKmMPGKGi7jrOWwYcPE/h+U1hgxo/iWia73N92r2aIK9ezdVe7bJ8r425ssqChyHb2v2+nW1kJ7c52+A5///LAGAAAR54IGIoGAVFBCKGEASMqLSfLatJY3HmgyOjh2HajpTlWltQ9A9JJI1KK0XsS74hct0MguXKugc1BBoTTyQQA6486qNhDPFM/xzIp2ZFLfFucBlvpPYipDreM82W0Tf+sooizLW3XUd0aTQWZP0R9SjOGhBCAZxc0uBPA6TSBilI5L2yTKwnl6HxW8A4oD3VWCvSPKQi1IkTc5emUPzK7hG2Dhn+N+J/H085WL8HBfhLRdcty3XRjZgfuG7e/v+CnfkPQBlEAYFohEKoFITKFNsSA2tKBQ2j65LP3RfoPUYjVPx/FC9qLkx7c7huw3VkAFIBbZjIjsIy4RlAQjdIKdMe/9zXrSCQSwyO0ocpTEdy8+caEkQWfnz/L/4WGlScu7PgdvtklckYSY1zwPo//uUZPgAA0dITVMJG0RWCgl9PMV4jrk/IQ2YWQlAj+Ug9g1LnXMNHgRlyxuK5bFcGtUL3KhSouVqXp2PoVV0re/M5VrCHvZ1///17Z+9KU//bt7ejxL9QcxECAB/SKhPIlgxcEABGqmgqiEUArTFYWXuzDilbcH/hsG6U1B8tG30I6AMkxqkIR4c0ZKfmJAUXOXPZjZ2EtYbqozi8Z+4lrdpjIF65q7bM3yWKe7O+7tLVbhreb3j+JC6CkUy7sxvU/1rk6mFDnQn0N6+EtT+LQIrcCBJDKSgJoT5FISz0WRGz6NZrQbIjt1xxQGhVoQgqk2TylRhlZe/ae/A5DhksGHghrRLj1WMFHouKi8X2EutfaYUKMKKIAIiRRkwJCVS+EoskczjAC7czYcF4nKN+YEAceOKmDKDPyI6Z4wn/ATLkKUCSt6UZdFtmCiSFZY67EfWcyqOI/tDhy+hxkNC/Lb5MvZfFnUXo0x8YAqxFpMbzh994vDdBVlkp3BUMzFS//uUZOOCAvtGScNsG0ZBiQp9PGJpjsjtHrW2AAkZDuX2njAAeqzzvz+pXP9knJiktWKSco4rUlNWkicr1Kfd+RUFWNymmrUeGdfO1/9/HOgllyHeVqGgmXWlM1doNapMN449t/3+WK97VviUkAEMq8gpM5gkIhIlIABnVaEB5cEtMGIW3JWLxFmZEVSCcMOUz7r3TlaAK3KiZ+ivKIdxootZZ0hLi325d2nfCMWLKmr2SeNM8gCFUtnnYcw7AmV7HCtna1O50+Uro+U+dvG1e+1dtUN7Dv2ft55flyvjfufY1vu69XedJrPCfp8Md1P/WPNX7/e/l92l/C5Xxz5u3hnnOguO//S3GhJCS27LHJbJJJrh8YsnKx0ECSBekt8DVZi3ZQRMPsSiLC81iUCCT4AUjSyYoMWyHCLGemVJ5DyyjhfONStGONwC7QwI7aIEDPo5jYI2qJCqDWYghYBg2rvE9m/l2W3GjytywxfVAI21LEocm4k3KA6WrLY7hTRC//uUZOKABYlJy35rAACbCTk9zGQAF+UrQ7msgBI7pab3NPAAH4k/MMWJdTT9qvGpfnes09DWo7i76CtXrX47LZD+fbNym5YzuW8rVLnBk7SRiNX3KYfY/GQY/d1cpv+9TU1PRVNSqe/v+jyIFggEAyZJyRORy7Cw5IEzhseCF80My6TXV8ymEOKFSiIK7ovLWoSogJIMl5RCjWlKPOi+2WUpWH1U6dQMnhDVNpXrWOn7oiN29WK7LCM0tiqkj1xJZ7qassm6z4vRxxnVKSf23qka1U2rtUj4/e3+vm3ga/3bXpvP37QMQby6xi8GNJfUL3j13Dlg/q/PVRY0FI6442mufDoMxdJQkKZILFJSDML0ZTNlSsGApJI8giiZ7B7o9Lx0zGy1V10D8H39fX0qQtnj01XTbh9v6d/zXzrdXKtxt853apwzNpCTFTQIKDgPDh4uEbr//7hNMHodXI44wzOYKhiO5cPo7ObwY6pVMW8BseJytKVepwiWAOJXH2lH//uUZEMAAu4x0+89YAxHYhoN56QBC2DbS6eYVnEFBacw8InEHlhYWCRUyZchSySh1BvUgo11+/3PUaDxa5r0L2XXyZccGtccjSSeP8ySGnQdQ+Xg4jdRot8YxkqOFhgKc4HNmPhLb9YEmrLzlS9fgxJNJkLQiVinLOwUe8661qivyJpQ90N3Z76CMLRnsEKMlUG3F23pIjtUYb//b/eFkxeQWrmxebjdQwRgqoTC0sw9FZl9OMr15Yw2hzSobU8gCa3h9guPRbNIDCwCDinxeltWw/s3kCLqV21n0nGtTWpSWDW2SSNpzglHYhxeBenGaAkJuSwk8sAPUHiReb4uBZpa520m6rbvOutZiIqGYEjM5GCoIc7ysrKkxMgJFbvma13bmuunvwTNpKIgwbLey0/4sUymD//QE1B2oW62wFYjTzVh9yaYFe7Ff2aDKYOxyIZzhvnfIBjpa1Rjdn/r3W10PdWO5bEXnNTxR/r//f51TT5f591SPG/18cOSDaSS//uUZFAAAr9IVGnsEfxDYWnMPSNRSpjpTaekTzFLJSaw9I1VONpPjzoWAnhbBijKOqc7Dr5lESjjmPxpQ45G5zL5xQLB5JKLBVWe2V23eDOa4Jh6Kzq6s6llcO5LXHLBMCtWjS0bMCoX1b4+EGpU32gL/+sRiCWmXTLgfxqmiNw44kJoMt8IZGbODQMoy6EgR1+iQTgdg1XVi3pFabvtUM2i8bUvLd9GX1mZ5t4XJ/V79zQyO/5515M6/z/sOc4d8ullWocJag3arbbJI2m+T5YGZCHY9CHHOhwqw5JzMUCtmetTVIxq1PNiRvFa3j8eU6MHAaCvm5hpRWXIIYoQHrLnrZkrepJjnM/bYtUt3tqv3cWphnohm3QeIo6v//z4VjMls/6SBe/p0D4vKoSUYP9RlWo7DbWDgo8WDTiXVHiXInPf5BxjFEp6xKxZVLsWaltz12+//+XtGgMQEQ5MdK222kU8GkJsExSkx4noYmhKSwUzWvirSr+w8Py1eUjb//uUZFyAAsU8U+njFTw9YvpMMMNnjhkzRaykcXEGjOd1hg1UskQokqOKJXBNq7JMtFTUBPJnEpJJb1l93Ik7p3f1SUiiryc1cfZjoMO+b1O3QADZ/96eoMNcOWc3UvyuDMAxYIgrzuYM4HL8jJKWh6+oCXSzxRRNEwCBVaJdeSVSllL4uQrBdQ3Rcs+JkzL0JwtvzcVGnwuaRuLhTlZ+9dbLm9qbddL7u1Kr/6QgFouHzwMVBYVAgEEk8/fDAUIXYkauhIBbgwZnBfMoKH9iAKZmLiZCNR3WUfao43e6o9J/Q7uZsMjij56OQsyqhiBhsTDIiQ6fSzuIx5qPb+3n8BdT8uUgQ8JlmzZe/U0LsrFG/0xEtyA6AQkAAiYaSalEMEm6KUyiYlhtKfcRJNTc86hhIfNHrJGmSSylwMMewTH7T1l58AkrE9j762AQDBZLuz/7jTiS5kPrAwQSACecNUxk43hwXlCP6LLc3Ket/iINU0agajdbKSFkIPQMMYkc//uUZGYAAxc4StNsGtRCIrlpaeMMC0kpKu4YT5D1C2VhpgzgSGCelyfGdT+6IMYkm5PsWKgrC2r32CP2d31d3SrtXIvo1HZLaZbeup+UE9smtkdH/jf6ADLAAI5MJa6SStKsWLgtLFgEAgXGOlyFhS1kY/xVC6t0jjKEIogdwRmaBQ4/UysZbIS160wvGV39X/zX2oUAhQAASSTz6UfNVBgWZywaz3TKAA77c1zDQNnlg+CKOgrgfMFyEu1dA/rkS+rjfaapI50q/c9K2KzpCn8KeGgMikMQLDTRAiw4znC0rpnuVTswy2jk3KmxtzGFlaP//xofuf/uttgYgm8eKCCa1FSf70a4ZFclVVY21UMTmouaULANB8upl6r7Guo/9H//8TCuYaQXfeABQABGu0odRFp98RHBg1UoeBpn48JFdcWGr6g0FwK4qDJdhEse7JwR2og0/F6ikOkgJzcGbUyUIwpjPi0H/P99Xmbi17//+6e/9u1gUi2y/ati1STf//uUZHWAAv85StOMGtQ1gcrtPSIri5y5JQ5hIUjmi2Yw9Ixwf7x+v/xQ+gAAl5ILClhRi5IIsKoEub4uLdAzjYIInTVAq0KF5k/e0Pw3CDvCwTcUv09iLd36Uef+lP9Z9qUKdyYJvLXtOptFwGpxQFIGHvU1liTL4y3iqrTbDfPtTjiBGqCZR65tnZfsPm6G9DCvOk0gPP0Wdiodituzxcyt8sxaCSXG3nRKp6TViAVWp0mcqLuvDSSXlnVAAIVtABUcNfgSQWnTiW8eh0SSeO2C+Nk4qUk50ZsZVhYu1X0dSKVlUXCwjsFRKtTBIlSzKEuvVw3f/bc38sjjDigauCAARoUtH5CAlcggDgusOAgEXa9FYtMykBQ3qSyC8w0ujbJh+6rOk7x6uzmGqXbxvF1m2btue8uwUwgOdlAJAPU1JDYP0zs5G/J96fSOpOmscFYgNib4Rky0doSl/Ul6VhFKIcIEGVOCmGdRZGyqWyaUycSRQrhgsve7F/xcnm8x//uUZI2AArsiTutpK1hDYtlsZYUeDATfJK4wa8DsDWWlliCoO/dMXF48fYV94FCUtdkoWS9Nnt+dQT7f+l/RAEZAQAIPktzthMLiCqqsaH5bB51IrER5fB3XXjjaOvFB9sDJlYUlgukTxIK2UETnmTZ1I7tIJR3VrmQUlHYUad3boeqKxE1O9W5tDHEoSqmYqpWi0kf7dtv4hVADxXlQ4YLOimi2FAY9EQWtve1uGFuEH7azUSbyNlLrrV7rdbUNEggdu/AgmDI37VII5b/yhzW1sjrWHAFzhFd2VxyROA+jOCHuibSEjCQwycqk0ltgaEFEI0ZUi8imR9Jl2ySQ09BajEeHE4WaziEvXMt+SUciPXr12a0US2Eg7fZhcjzIjtQP0gZmSysYuKTgBIlzOUcw2AKW6zHUwlaZU6wbX9soiwCjs0UeiShIy9mt6IYKSUz5e1+nkDIkk+dk496ywoXmxR21Mt1/9mmKesqqAAwAAAYOnrwwyFgwkjgASca0//uUZKCAAvxGSktpE8Q3gtntYMUrC5TrO+08ZeDqC6WhpIy4rY2V6pW3WtJYFepTRkHrJiSbyE842CJc6cjDpCVwQV8P1NrxRqbbf656hAh9fsHw3hnmagfSf/itE35f+7XUk+b1UZldm1lbcSKQBjqEd7Sfg3mBkmgKrn4e6GSoZcGTjb3oxhINevdGtZX9jvCmBh1bksdf+r0+zxdqF1Xk0YuHAAUYD0cUBACAlsJfAdBSKTLmrJ6M2ZQ8LdInXjkrmtSMwuAkwyjBMk052NQdV5sFTGovm8pPsruEdLiJnCM7FSrD3ICVWCuIXr2pZZ2U1Ua89WrtmV6eQdqAJJgAEHCpmAAVbqfCxoqrx/zTAkxAFlz0/EKoweCIUaHg3ctQoJDW7bkUihdaFs7mp2L9PXQ9aoC9orUIhgAAFw2paDJA9NMvU0R4Syo2dy82BseDQlBMKrNVZ9dEjL+Qnewwm+E9jGDWW3CGtthocHxgEsbfnN7tm34Qm8RkmyYl//uUZLcAAqkbSkuJG0I9A5ptPMJ1i00dKs4YURjrhWWloyTI+1+BZN4x35W/39Qn+imDWZAAkgKA7E0EDC1d9EVkLmR8DAQkFCKthJAEOg/esnhwjb/snX41dTMaihuOeqNWSCK385+5GjtNORm3puNGhwhAgBEcPFyYZkeXllA6khAkGxB2GsuVGGtRWWO9Lr0Wwg4GQGJIgE9gAaecNafxRSeW5iSYGOkdNLVJ6RLKfGyIeZXTLzvW/PuXmPWs3li+UUF2PNhljiA8XAvzWGWIaZf9frrbIG2DylI5bMBIDAbCMA1ZDtFhu6sAA+nkxwQcFGGrdLQqao4gxCi2qSk4SCbP/SlfXX70/YgA5ABABg+KYjgA4N4yBSrlh2GN4thwwJErQOgsTk7SHZPYLTkrowwmZ3JVes7DHTfQiAz3QQUJ+9dVsAn8Hbx8wtaT3dO3WF5b5vln2Zv7+froRSCLICogwcih6ZHCkAzFYW/f9dEIg+IwbLoAfGlkdqfI//uUZNEAAqIYSkuYSDJA41lqaMM6C/DFJw7gwcDpjGr09gxugRyGUFXNoIgaGQU7rWa7zzsNMbeyc3szOmrChRzFkQ0aJWBZWfetLA3VqJjjlx+uMUSAKwvEgACQi4DFJUOrFQWFQ8BVUnoVgpgKDUOixEIYYHpJoxZSXrL/uuXXdb9mmYhjE2uYSQqUMERVZ9PrU8q9qU567MaGVbbOq1dPwR0zlIR20te+WsiDKAjdbVE3EkoDJJDwFHsl8AN2fiAmuMukEGzMufYEGiDjCykie4kdgKeWarMQTtT1pCkprGu6yj8x15TRqqWSe59nVkbVzWnRVqZknPK6KVqOBP7aEa3/Erp0qgOUUAAABPNu78+w2zBpIBQfEnrEBsQMEKJbg0Z5mnUkeKcBQPg4LNc4ImSYHA2Phxhu03x1UrDSkmLGhsXUFybyZTceFFKakoZIqTtDKCRUH2GG0n31RyWf//QgK77WWRyNpwDTOE8plGfqlQkW4hSaTBsFZJg7//uUROeIApsWykuYSDJUw9lJdwYMCrEjLa4wR8FnIuc1owm9SWWXGqaOZZGaVr5ohQh2d5lNJ760v/s3fT9SaaVb6+r1o+21PPMglnEwV/wAGVgAkglc5MdsWREwyAhdyWzMrC0lgI0rY16mcd/ZLR1pC6cjlr/y6KECNsmmUwVXW77nai509CSrxT1eHwsSNC7lmW5SaM351qyLZne2pHUZP44gwePPKF9y32NF3f//MAFIoAKMC5AhQTTL+uQygSB5KYQsA4mDhYZNJmmNUdA1ryISMvFPI0Iu6BT/L6RnP7kV5xV2Xx6Vh0itq/dxxhwuWOwMxulcCvnw/QxpQAIIJXMMYaMzgmJiQYEQBCYFhA0aaVJSt0aa/N1nYJggzyUyXSVLY98xGURzZSk5Wd7L5+qnhBbcmtlV8vzwMGELb8syf6b25b9Zen5HXtDErzxwuGnsnExWcGiV3//yIRQgAEbPOC8JEZQCExlolRaEYZEkMWBW6UxX7iFG1ZQw//uUZOmAAt4bSlOYQMRHiOqNPWJJzDTpK06YstErFOUllgyoWJsYgUx07j5qCyaU71rW+tpn+bNoaxSbX3MQcJIR9NI+FWNtF9UqCwpnCYJVoAkJErnz66bEC6KCwqS8UUteZwGvMTfpzZxucPPlJwocCFC5NkmZGEHTa9m/JiLO5ct1gUROvOw3oJ4DrGTGMQOku5UECtyCj5Z422L0bC9Fz12VAsSR//9IBuwAChB8MSCsAIPW6EpTricp+jiTKfZToOUMiBHEBlZOF7+5TcsYExzQsZmEKMmGZyPHzrUSWgfq7qbPn9NzK3QWagSUAJBJJXOgZgNXIWAyZbLBAA1FoZLVppqXoZqffh7THFZJr0oKQOYGACDgfyCUFWRXexn5njSHsxYcR6mF3NVwsopXXZ24xDDkVbsiNVluPpsvV1UX46Y1oLHAMCL///qUwAgEHlugdSBQWCCapZwGweD2CAeCGRzcw09VGqAdFVXERWqKHYkAzCporQjlp5Y5//uUZO+AAxM2StOpG2RLxBk4aYgqC3C7L04Yb1EMCKVlt5hgATvHji58IIqLqQUezm631LgMtrARNPwitoWJgsKyiSik+ax4JnAfhgHg58F7qxO4wFPYmD0uCVEQBcZQo10trU5zA8mHuApuL1r69G9kUz/lVBgypFOgpDgkZdHQdqjecGjOjfVtkm7kcRq96UcmrXs9O5Oyo5VRx2///SABChTH1fmaoM5EDXlZ/Mua5NuDFWxgCI7l+x5vmTrdfZtsj9Z2JrR92aasxGtBIEBEulrUA2YOlGc4/SlGcpsoa8h38P9FNQAAQTz2SMDMUEw4ck+hoAw4Ck+1hKxAA4QM0EmYLohSqSmY/EpO8qQ3Xjsci+ItBMALB4RRqHw5N0kziT5RE0+cqEF1nyne14+zLSZyCTrWlqN5klxs9tKb+lW1ZuPvxis90ZPN6OnJZRAwn+rt/NBgSAQAGPYdAsFlE0vqpc2yoEp6yBAXwAJwBgeInYRNPxBiap50S8y3//uUZPcAAwo6y1OJK1RL45lGcYMaDCkhM04wS5EcDSVhxg1gu2RPlX1ztGtDJ4EI72DiensaYv1pRzvg1fzf/vWmLl3P8xsTunhn/j/kg0FVJBJJXP4lczsDC+AMAQYD3hCAHJmokABXNGaRtaELAmRpMGWD4sQD5JpDhM5EotPTzG5foipn08Rh4fJJc6XxTbnnALMs9KKKZ3z2kBXVIa9WU8/xr3P9PXPxzAc///SGB0HrEGaBsQuO/xfJNSJxuMvEv7AzGgD+FJ0L4s9++tyqQO8GsumQExYCigMsIx6GCuIKBVoRMgmXUSqepLrqd5BqmGtJ9u/6WyICAAAAA5zheZ0QV5s5CGsuOZ9JEY7jTRBaPiIcxYBEVnIKgCiWPgbGBYD/wZEpPGh6HdD186Hh1QTLHS+FJq+mFtM8lbOyU+bfRIprWSX7UNphfZ/WHePbnlZx11+715v0sUykw1sjif77LV/puYXLczMbu7zFR7TsJAFn/r/gkSRLIAJI//uUZPoAA4ZAyRusMsRRgzk4cWZATD0XLU4kbVErCiVhthlIJ5gedmcguQBkwYD34c5HHIMwkBXSUHcrmRDXFsSeVQdbyA69qbYdj6gUUXC6mWdpwMJiHzY7lISOfDu0H5kFcKKFtJRdfvLLXCi38+pOQNHMhooCQACefIrZkIDhgdAwbY6WqFgS6tRLUoCL8QNADA5ncId2AK5/PP4WND9INN6kywdiflBZKTmtMPFl+0e39ddC1IzLpPqW4xLq/JdCyn21jO7tEIOEGjyf05SM5/+g3//7haUYaWaDsQ0emAIJWYwOyYvGzV5VOY+XprzmmkfA84YIfXqc6r9nDyFsy+0bMoRTg5yND7sCZ/WYq/12Mm5aMyhG+io2yfbsO3rOlgaiNNbT/CR3OicG1TRtgAS/MCg4wEAF2hweMMA8ui8sDkwFr0DVIEZUxZQRBQgtJgklcZLqL6k0PFyFS9JIRPeQli4rROWlDbx6KPoLL+iOeyaAxWfCkrQSwZSg//uUZPCIA/0+Rzu5YFRX5WlaceMujDkhJu4YcRFcqCXlpIl6QU9Fbg937wmMSIQ500xObgs4SIeeTQyj6Fhy8vAKr7ZZNu2B76WcRomkNPH1wQAaoROPxfE6YL3IZPPfvC/0VqoRX9kVZS6ptsqVnenbetG5UToYmwKu9tNmbSaeXeHxYDXqewBqDLURQ64ANGTQEhu6hKKxVIR/adKZdLUkt6eZNiUMhIMwVQywWzI+UJrjq0YnjjrN4T9cnavpedtC9Jx9Hfi+dsV2l3vio1WORBg4Iho9o5tKSQRLCi2SEcyB0vYNiyYylXhpl/RqorP35MBugBBVg46OFjgAlhlur9324sJfWGHUWVLFZAU6tShPFcvq2WARYYOA5Uc8FaeNWlALBwjufU8LCAOtsZS9TGsC4XYpDC1qJuQx6NJZNkkAiDKCkyAQBwmDR4IT0uENyoGStB1ZTCKlWlgZwWpRd329izvu82kDxxyhMPCpAVFTIk8GTblUMmBDSYoZ//uUZNYPA2ZEyAOMG+JMxenMZYIvDNkXIA2wbcEyB+VlvCQgRaRB4nA5AHt7KZPSkhwV1nKJg6rSTPL1DkiFuQ7AfTxmN58kXSTJ1W3KZ25MNPj25570zUa2UM/xJCBAAArBkOGkQnq0KCnmOn3D5LjZozFb0k1DLhZX7ZC1nhknJIW0+62TXTe939/16t2eZHI6Xqh7aNvSEvGyI6lVB6j8VQIBABxeXm4yieTrtEkrtAVWY0Sqw8p92sLzYGWEwdLA9MDAnBUxTEkwsgUQdCydpCx0RVrck3TUdTIott3FzG5ifqUqSdLuju5C5SnBpm9j9q2qX+7VlJ3Pv+4pSjaYAXQo+nj+VviMfT/5AAFRJABgYN7WEIMsqrEmZIWiZByeHYgCSBADg/JIkI7WLzD32XI64YjktmAii1IVLOdUmORRDTJpM4h1sd9vrtesrdXe3yaddURdF1S5snur//6ClQQAABBh71RpmDRfJM0vCKAqEA1baSvFg0QdOGoY//uUZM2DA8NBxwNpNSJGB3lZbSIuDVjpIQ5hIUlQp6UxpgjwoXed+y48gJiNMSmSITFDo82kbxWmsP3qCW7Fjq3UaMQtuU5rJ/OyKJfq5QG9KvlbP/IUTlXax/bMzpWllf/6t/5TTrAiBIAYcVaBKRSUAKI6xciSAuPzT6AfEUtWoGFjgHgewkShUTPERRxAGSRGIQ82XMNQJ0X06etznLyvqtOs6yHoURwATzsWTMbhwSI6G1GDgeoewCEtxbA7a3GJ05IOguk2FD0DpQfejECYAG+ihVrgx8W54iCPIIIdgBBCDDsIvFg+rk1WAB4KCpxpgUUIlDAw9bZsUBlph6d8+8UERD/0fxYBBR4pokBJ0HsWiMtFpZUoRk+wKEo4hOkhpOntJMqWoQQ6TZ+9/yGPP2f/3stMrdxnviiZBKFjgAmlQLFlFSgBZgEGPIloxOD0C0/WdJNqAq9WurAu9NQazsjxEglhaAtEDywQPUpENEszdAkhi6CGA5bmjSZx//uUZLyKAxxHyLOpHFA/AWmNPYYWDBCpJu4kbND7iyZ1lIx4sF4z0L0BCz3f2gzp3/37n9lwBgzSM299SjlP8qNLZbI1G0imA8V1Ujvoi4HJ8enSI4Um4uHVcR3/gYajcImP3r9lo56yuYooIoQEIMmVACoMmLVnAfMrDy7ipk+7keNraJaCp5Nrx4RUCPJgEcIrmCATpBwmTFRhoepar9rcpFAijgJCkXIZZEYnGLWjyHlSRZLlyK5Z5O842oisx0GI3wwNKbx2FABnihb09JgYI2pnGiFNgoKRtsn0EScwshQ9E9iYjnD73pQuYU0Y1PTbd6SSRpMJnZ5pmhgxZiTL7YzJFwVBooYzZI2GROnekwJdIMi6czTSQNAEctldnR2l0bXfZxXu5MCaqgBqwQFLDIbgF1Ilg4AeKA5K4bwR5+4PgWgfyPTNAFFEbIQTHPZzuPUVVmIs7skmtlc3YnRnSK8girjRp8RhBAaH+YucukeNjyKWNnbBEgTDREE3//uUZMmAApsVysOMMpZPA0nNYYMfDCUZIi2wadjvBmp08KXG5MBpVIgqjNg78RsA5R5VbJK4sONMc2FuBSOVKY1VqBJQ6OKUQOQhLnKjis5TDxQmqmutb3dmuzPMhpNlSqKiuQ7UP2GFJ8lRHuSKoMjWZJsuXARIisyQ1BhS+ySO2SXaWz6f7a74ANa0IlJUyBZQiAECwRn02yIGo6y9Fd10HFnIKS4TdNgShHga3QwvDiPshkYejTOziaEEEARKcVT9Tqw/0e6Q9CVCG01G7B2i5tl/R6gZOwAeT2UoFcKxl8Ll1pPeM8eRbtheTEMl2wr3ak9Nil3xwHi4MzTmC+a024275/mk9MR9Z+nHLG5v6Qv7Svat0L01/L4Dc4b0BGSBVFmMwC2X3SVua7XOQ7wngtxBRoa8taMkCFVrdw5mm6j+XXW+aIkbUQhZIiIaDYPSHTJSK3LXQojJ4IqV/ilzVJWNLLAlKAEBO1hj/N9JYBXexZIhg6NKdIB07CwK//uURNeAAqInSs1ooABXxplsrJQAFFkjWbmnkBsnrSa/NYAA5Gckp2OyqH5fGKSkmXFlvuxK7vbXJp3q8bkfb2GrkVfa7VpeXrd/WV6lysVJfR09FzGo89LWzuZ385fS2al2k7UzryvKb+b3+q+uXLtSpayq9s1K1yrq/d/n///////Zx53/////+zu5lQADABBRKgPapEoLBXLXWkGvzEZHtgus/HgMtV8pw+tEsD4GeFAyAPHSmVOcO1d6y3MNWh6Mb+kY4h2r3WluiZOMN97Nl67ZgmHfnPsZ/7v3QdV/qfQ4lv8lqQQQBGAC1WHVJpIvEyR+kS1mxUZHtyi62Vuyl9qkDyAdBWGIxdHNDAoB6CdiAkRMUTV7U9r1OpbuiZJtbe+ooJwj66oWeLdF/92hZ3V2L5vPUAq7yCLdX99+P9hu93UXAGQF2kSiUDACSCEakm2RFgdrnWKtBoYirDbU1bxN1sdRVFwWkaQrGmr0lZSfPOZnS9hT0RGF2bEh//uURHkAAtAiS1dpYAJfQ0l87UgATF0hMawkcKFypCWph4y4VncuCzDL6zqmJdZsop2y9Qf5vPDF0ypOVLodQef70sLnZCTIgozrhVVUgAEgSiSngavGmZfJYzaQOQTAEESqfLqeMU6WNcqtkSZws+aRBh01GqJTehgy8MNvK+DBB1YgZTKLtzjEV/lpEVMvfl7f0s+lmZYLNl2lWe+HzyP0k72tWTGtexRhOBLAzGIi+L4mBwMYDHBnRakiHcJA03Bga113C+0qVKpg+yL8Ifmda3DoyAd6kf6VoT4CFgUcRkAygaCTCcTY/szI9iIPiFChh8KYKrFZ59SaXWWtphDEhWhNfnWtUp6r8k9qV+VVBOPW1dNvWLtpeTbMeHqStuanScSSp1FuVv/pYc14sQaadLA7uzW0vGSJOtD11PXbMknp+WUW50K4w9mR7KR6ZFLmUqyezMpCovelMlu/dCK7v/clV/9NfrjhO/w3KGQWIstFqg5yR/MSBkmA4cBe//uUZGqPA9k8xwOaSXJIyLo8PYIvjdz9Hg3gx8j0jujw9hTehuT/r7dRjLM3yfqmlUEPcxeXskYYqi4Fh83adCHYzSxnq0wElJeybKReWJo8YZKlurX/XEoaVJhm86mEm1ZN4ROSrSvbvtwqsYxLn2nlVFZaMvu5uxzq7/WIiMI7dElDsbVfQI6imEHUdZdCbn9udGdiwdUNFpjndVQy8rlYqrI6alMZERz9Cgk2Rve7QeVecDdXsYj//6vkGSoAjYM2M7C4tqrYLBAwECVijAHIBW0hYscElALQqWG0ZXPkSQlHo7LX346LWoU/RsGCxAth3oMBD0X9HtZjIgwofGRAwQxbDVVJR2ABR4eMY0IMImhkNRR0zO75zI/yeZfq0zekBNiVocI4YNv/TUgIB2Mf4BKPBWDRZZAYwgJi2px3FbvW+ZdMW0NbmMHLMiit1JBmiJ4MQZCg9qbe3ehz7i1VDspZLsaMaHnCgmZWeBVmfDYOJgEQjAWqK62yailo//uUZGCPA1VJyIuMGtZEQ8n5MCOTivzPJA2waZknpOXlgYph5kIEDaw+KEizBG5GdsD09zpXPHJo9YasBA0N7Y+sQcEeqyEhFxxckCTdG+bIzJeGMvfkQ8EpuZ0lGN+Lc/+TxfuxNwGZxBm/xZWcK3rEfJ5YXLWYRl2tRSdmZBdi0al83flMuxw6oDCLZ2QrFufOrk8pSbrXy3dt7uWCXJq2/ra/6aza9U7/972b1Uz2xNLFBAAAEHSyZKHofrNQrBwEghfEQDSc8odmGXGfmBZbJ2tRR4zwhJBOEFGI1QwcMHTiZ6ZRTVLWf31GDCNr1DHtmo+dtY1vYyLaey2WFv6qNztPy/5qdgREok4Ws5+zM8iQjIzIuZIcBket8UFAGxgzLHhbhhGAMgBSzi6JW+2W1witVedbry1yXmM4rH0RGaY1MU++w5CrrXZP9nioZkS6DqeeHgH4M6LON3MEoCit0YMguk2Pg+AebO1JVLLLhgWFpeLTZUVFkulg4OpA//uUZGYGA1JGSLNpHHI4ohmcPYMOC7zDIgywyUj7COcwwI4EBhkTF7AFuLkkTNQTiazq1B0//nDvsLp10U9utJH/viLPja7KRx2mogMDo5TKfs/XjRnf3+gxDvoX0sOB2qjD005gzdxWBYmi4SmCXZ5P1s5Q56mQ7ngAGCjkw+URvRccZTqGsUQCc3ZMfm1ksps1HVn/4YUEABHvZgQ2lWOlxoYNAFVYkQhWxQQ05icjj9E/D+Vqselsnos3h1R7rhx8JlDp49kqqsRLKbF/xCtpzg3x4MbEaqijTB17wjLTz1jfNFuv0kyHUy74wZO54uy0cCyW1JJG0XKCHcAzxSTuvrWjrxYoGoaPI0gpqWgLf/3ZTmRGBgUERUZePJvYdtqV1MN4a7qtH/6RQTs/cawfgDjwMjeeUIPjKBaTXWItBIYPm4bVNVAxohuFaE5Jy1enpEW6UzoYvpLD15nKcvvbb//d/IcNT6tpLuFmFH3H88QzjzFpSSxbPYjJ+P/r//uUZHQLAuw8SStGHTA7AtosZMI5ioB7Jq3hgNjsh2WZhJiY/d/vKaA8AAGD6oZRLaSuO3gRIHIKH8HBEkiBwUEulCTaY0LvBA8TZAbhjh6h8kdtZRNcUpUyv//3a3mW2ARAohUAAJh8siBQyoj4WzUqdZlUSgFlS6RSyNgQGCByRonkIBUSrNQWRNd1RSe1t0+nJDhy2GQYljkVppCsVzzZStfS7rdGvtoqKjb285Ff5v/9SBTmDBr9aK0Mzw7LZbG3ANYjUU0zl2lyOoHqyTAnmeHbBCdWj6IYa7GV0K5qdr3RwhwPlWBYNbkAQ431fv1p7djO3PpiQWzJxAm13+bijjTYFPJmKXQGgqu93HXZG6rAdn7AVlomji8OlXtdfjcn3mGwwysUfMtwrqO5NYhoZpXTxcOmFBEYww6LDhc2rQMYuhBISPFow/Eg+NHS/66UAJuOtFApsPk9dKWrUUv2SkwEUwMiEWYzgZICOhC4g8mBbZYfGLQl2gmp2q2o//uUZI4AArNHyjNpEuBCwvrPYYI5isCRPawwaWEOhyYxlhigLKUt6bGtfQPP/b7OlIu/icq4+xB7QJoAt/WJJypJMHYaFrQuZFB0FZyNZgQg5JbrTUSlO6wgowt4Bgo+VBMSuCMIpmgEGEb4JdRIBkIp+f+NPyKy+ZmR2Yc8k0w+Iz5G1tKjKMDpc064CK+WatAXf8E01h6eJILGk0FAHHhOOBsVA0uJSBEmaIkaJlS0rrrdwC0cEryQwaXLPA+5I0a0+0p0tWs3LiDJ+n0qNF+1VmtYCUgICrBveNmbwoEBlVz2F+FB3kXxttn2jD8N+CCO3qP6JNcei2nQdkQcOhFDc7UtBN52GDhoWVBlhs3YE80A0jAUKViB4TWMNAIHhexVdwFM09ySgAAQBcdEiXpZChYBhoVjz+2zjVNNaomxI4xgj6aFmZqWtwpPMrRKFShdSNwuw5+myFqxVB7V/UyhCYp8pQFXgB4x9bBJTgUCI9LCNlYq7wLEsZFOIwHc//uUZKKAArcuTmtMGHhCwnmJZSIeCpiVKy4kbMDxC6WhpIxolHYcls/iJECFGIZPtp4pX1y0RmE9UOjbq56uyx+Z0gzMf/NvpWZmnXPzfvNJ5hVCjCF+rcdFYONGvaFTIZQUkmkoD8J7RzTESih4Ng4KxL1oCOKqPG8tPUpoYKkAZMiEUWG0hd5HedNVZoqyad1vq2KZ/+7Q8rFANO0A0CWwEAbIQyMBgzWszNTFItXC71/koGYNkQlFlIFCc3brsiIZnBVYjqjt9mIOV0xrNgngN2odiPRsjbXBkUY+2nhQigDgYSn0AENmBgobKxwFusJtM9mVfVkQ09I2wEk2BAd2t+uFFIdvWIImB6aaeYELLqMHPiZr9jQsLiUAAQfJxzGWiYrI2a1tuk////oRJK8itQBW0oAQs2HReIUly33V2sh7mIMngKIuzH421h4SJx0iJUUZHSQ6espgWbsHmUV99t2bdwWdIv+zZvOfxjkDMrNMIPhgcHTIAJ4gWqdV//uUZLsCAqM0SsOMGfBBQdmtYekACxyfJw4wacDoBqb1hhhQcg5rnQ89ko+RDERAAhVgP0ohAykZywMhjNA2TpLClRvmETeNyWUVcE4u9c2kqBxZoWWoOpSERQuuDYzre2bcprlzpLrJFl/VmNbMoPBCMTAAKLQcrMCqpchEyDkr3Oxhxjo4hoGcYlmBZM4Sk+Xlo9YgMnzxwIKwnIqcLK3IHKuIHhwOImU7v5KFzW74/ao0/2s8Vfcf9j2zX+/d/5Zy2a3/sh+QABFg4rmvA4t2WC1RFJ6oMD8IjkvHwDCkUpeCSEMasSRxybWwG8m08Vj0uBkwErn0v1ruFkqahj6HfY22tQoHvixxBTAAAAMOYXww0JQ9TZlyti54IX4/74uycJBKcOxarSH5tJ8uRqvgLt7ck1UtlxuO23b/3ds1DDgvMWGRXIpzXIbjbFsaPDSK7FSTzekJIihqj9/h+X+Xf///m7xFDPlhHPoyUkgEIx3C2QVcZA8HYMQCLyOV//uUZNSAArAty2NJGzBDgclIYekGCsBtK40waUkRBuWllhiYYQkOC1qA+oYKAMEWmVXLZbvvI6i4w6SfYJ1hqg0x7pDadYZdUVaeEaVzg0OxRNFZOJEAIAJABHOcMbCMxEQQwDoARwFFALWGYi2Jv2XC4lkQrFYc2iMfkwSgoDs4Hbk4AQ7CUqjabVN0tyzb7HaG7UaxWExqKwZVENxx9xISMUTgE9h8uardcGF7F2LPtTmuc9ipQ7D0mv7N2XF+8BAKMAAINgqBpC7AzrcmmIpQNyiOLDEdZIFwfFqiyqxpa5AZOAqWWQAwmaaE4ADYooQheJV0xQdWR7LJB59Ijb5ompaEY4nAQeBtADAAQjhjJNOEsvWLAJX6AxOqSULwfvkJ8nvxGidhCLChlONABigTj5b4SqNd0DJyIu8j+kiJYBJEDlzTOIUKBK9ahIYOgMefAY/ubS9LasJHgwTdC7wE3LIl1KACwW8ZiqyEYmAM88Vol50qhRrT1iesjHxd//uUZOiCAvxJSctsGvBJAwmMYYMbDSEhIw4wa8EpBqVxh7CY39hsXChxYu8MxOMkx50x71XN5R57nAJQsUBE4TehQtJDAJ7iYuMWmalE1F1pKjYAY6iQQwllwH/nUvlvR9VJiYmGsICw8gDpW0pRkwD5VEMklw6xxlc+9Cw8tRyxDfMNgdExUgyGFKFbi6TGUm1ZAn7aH96fcuPudxR1lZBaTU+8XyIjYuOcsz5g9JqOAhkABjakQyKIqYKCx0Dh6PoZEzA5LBKW0aUku+k590O7SOdVhNli74EhguxC1vmVkblw3Vpr/+yvp5P5/qEL+a2qNDTtPft6fzu4KgwAAfESHdphElBAKYANiwI+bFHfa0zdMF+mdriazKLcNwzLZU2pfuqAyM7AdDYmeUdRE1E9SahvXQWaUXS0eBRiyClcC2HCmYQYndI/WNNyiFg3uczo9ihs8ghzBwjLkTMWH0z329SKFPACpI4QQEUSBblAJE04OBqMhYAMgGDcd1Dv//uUZOiAAq4dScOMMdBNI7nsMeg5i+UfJK4wacEsjCThhgyxiiZRaZ7nW6NGEGtCEMKIeMB5MQpB4CAvGFHnXFGbkPqQ3dRckmwey0e44aMAA4gI6iMBAKuoQgq2EoVBXqfF/HHYwWAhAoeE4gHA+MIhCqIpXlIei8jFv46H8Lt/aZ7iWYtgWy6ECDpjG4XJ09mIi2MjIQ+mcBgGldZcI/WIzB3Xx1jfXu3vZ1JhnyAgAITVFQS2TXVjOOHZLEEPwlB4hnigrlV8wdcHl/0P0d+1o4NBkNQ4EcoCOQxbUdLBEttWHKyEK8hmZ815PRSozkt7kSyP9ze/u8m9FoDXXzoAIAACOSnsz0KxYXA4BiMBIIV7tMfxy2kQlBwETospRz5UNAGS+VjhahE92BDWkw9VchQ4lviakb77J7+3BUWKBjC+DuCYLT8GpxSK0dWjud80B0sxM06hEt7cHXJfedcqmcX0/iCD6uv2PoABKEkKLLBGEomIg1+lPEPQCFAq//uUZO+CAz8/yCtpHLJHgulZZYMYC+inJQ2wa4lDoySVlgi7k0KDUystvji05fipDh5b2A4ga2RDjuDnl/SEe0d/PR/Vo6MtfpBRqX/1S0e5y3prtquN3/v9gI0BpIAEHcQZgomAQhK1UiOr+LkTnbkOYel8tik5BsdtvmaYLR/KDxkduokzS+8SlwJJ7GOlMLKV+16aKYYEoNkzReodMEM/5U889KWszQRDax9bw29i1a0i8GTJVDwaFFdoeHVpbpG4K9gcGmZlOYtEWPEl5dnA9yXLKamaFggzwLZPn+sRRqhC6XJlJFFHcTCbqL0qyqzaNr69qe30fc/0V6L7/T39z2GNLRkAMapEBg+m+naxt6ErmWs5ZtzwrVA0XBBAKyc+eByKBYSIrejpC9to9XRwjBYKZwiT1fS/FDdrmoOvM60NhOStCSHrvXpppwz59LL1YVURzLcqpeli0OPjbAI12wShiM6XnS3Ww+bKm7xGPNqVieQ2B9PD+AkpA4Xq//uUZO2CA1pGyMOMGvJL4klcZYMeS7zRJS2waYEupCr88ZYnFji6FqN7VypBLOrI1BlSFSQZUQHQprgyOR1D/o+HHPNwQrbJ4nXzEnw5FqqMtcm51dnPZk8jCUSYdEeLymskICCiwBxseDGhQmHmPuvLxcHJlCE62JUIOC4nIz44eZURokT2Jlbfyy9nPKbyz4m41LhKPXah3c6UPASntlgzY/InPig4x7EP1bnWn/3OuV19y1O/pg65giiABg7vE15MxwwUBJ6q8ZneZRFYdeMWoaFT8DR0eEUZHUVlLTLqNP15dkRtL7d6O6xF4RaXptf3rE/s5pGHtMN/5+JM/+rlfumeGgoezt5baTSEi4/iAG0AUVoNRqHta5CsFDbcWrNefaPOM/Q6BArXNhl7iBRJYXVo55OnlfrID9+pPBUdxNvedLLVptSgb9zYst58VPhw7cvMkFczsFy/eZWQp/yBLjMCmQRuABBBSoGagOPNZVyGYzAKE0QrCo5XKSEg//uUROsCIs5FSStpGnZaBdkRaYNcS5EZIC2ka9lZnKSlpI1xFosm7btHIEHYCoZReUiJJBRyO1hhD5SqxIDpZ+k2vtyOVtJS0kneTwpECJyn6PnGvz5/29oXzjgkIxlAIAEZWA0ysOYxSk1qr/fsNqj6RDoVMFA+hQ1g7OA3PzSFOuXkNof08LKUGB0twAgLhWDMBEHoC5dxvA5jGMaE6UN3crezEH8Z2IyIxwBy9CubDuKg4F9dturoTAAVoAAEAYOUNABQoErGaA+rpp7Jps6K5JHBAH8/F5ZfaOmjsnlj2ce3lTKHyk32CN7XWhW4yidOGi+TleGURiOEaYbDmjl7yJKakUmaXL/8yT1Dx7lpw9flpxv3FmIqCwAR27SYSRgYuX6g+XIVOxFky4i8JYkphGEgOx0OhUFbI+8yFRUgPBzKdzk6ucmBnrbcG2zp0eIgECMRg1NRSoDCmjg4JAWYYCIRsDINKwU3dK+QRt8Sh9eG8TttCF1mv39fs8c+//uUROWAAqhByktJGuBViPlaaYMeS2zPIKywZ8l5o+Sxpg05P/DAEKzQABZTLwDEWB4DUdiNezZ6hiCYHHMVqMLtGe8jlLugpKaHr6/nkTT0QraG/P/q//69+pSeT7ZxJ58uIABgSDiTOQdAQJIAIiqmg05sDd1bGgrusxyarP/KHCZU7cXgCA4++zkQu6DLAwQxEThxERFPDAa1mXbMMjOmRrxaErGKEjoI9HgOYMx/IVC80cbNjdNDrDrFBGXTkGDikgc0NDtL3/IBT93xlJxJKAG/CQTEZBeURgggNeg81XQvFdh6gMqfc86XYTduqBQRMgyfICdkH0MtZH5G4crPmUOStSyaB5gal0XurUJZQXQldQFgAAI4CcAIkEDoFBg4FHhQeB1EAVDWbA2FQfFZcgnack2MF4cr1qJCTBDZQ8TkFYg2MLTHpFXJFuIGNqoSfdqZWopq/jTpoYhe0niKhM3iexPxTustCH7Hc5+LnatfiO0n6KGkaIAJaSUB//uUZOECA0VFyKtsGnQ8SNmdMCJ8DRD3IK4YdIEkC2c09gzkGBiZMVtn24yFXtC9MviUurPzUpL6KrYZ2WdxTMSRsMEGx48kMGWRRSS5vG1smHllZkI8hvLU5PcQEyDYUYg5irTVA1CAWYrA4OCgQBC3id6BkYUpisBQ6zpwL76s2fyJLLLiSJ/G3cqMSt+gaJnPDlA02owWXHm+onej5aTIdMhhO1q3cpOtZTi1xdIcrkymX+GbyrXxwqEK+Y75eZCLNsO14v9m+P/FXjYfXpdBrmj5CpFVEAtCCA8xcBaRdAEiaIj4iRAIq2nsTuUuRDaQnKoFiBMpNBwNE8FQPclbCK75wwKk7GucEx4wN1rLLlVpFG/AQHWdqYQVCgAAEHfAQKTTEQRAYhMWertj7DYYkz1wSzp+rzd61itNsotSGtnatw1ZCAWSSRx0qJ6q+hKqfvNevrAba2EBq4lwr9kWko7puYiNjOzJaylKiUK9VWtNq/r5kGOpB2qNCD21//uUZOOGAxYxyUNsQXJGQqmNYCN2DmkPHg4k1IkcBSZw9iRM2pd+zYQAmNAMAYTgwH5kKCo3ggunTaxEpv5Ami0KiVLgydRBDIG2rXV6kFEuqRahN+zHHKQLM9ogAlVzHkAAZ5eiBCMOCnzQkQ2KCSwz6NPUyZuW0xW25MXJCTzZBxAdUuknE80KPsLN01sM62qGJ8i4zySBOcCSeJFR9IBPKCBL29AjXzSlKMiXvSNNeTmm4sIJkteu1kZW+ME7d624ajX+ZMu6/ll/UtJRkV4INQoADRQKFW0m+zpwHPb5yXMsc0XYeBXLwwwzXxSXIMqXZ5bWKxJt6vR9KopV7vZ0vHZgFEhQuWNf67SshKmRWWfrqUWWAgAAGHjshzIcAbTQZoy9v4B3CHdlDBJWg0HtxTAhoL+FkzWFMIYFHs/ghw/NOUtw7qO0OXhgjSYPTyYrtAyFoCZ1bbWDwCw0x5sshpk/9pAXChkCKM0EFFJtUFzNMkCIN4+lSjVYaIDC//uUZN+CAwZKSTNmFTI/IbncMYMNDiEBHq28y4kUFCWlgwkoyy1mBzb2gwN9aBXINwgLhw4fGa0LilyG40XLioNCkzv621pZ7k9c+ZKgd7ihBH4bRiI2FQwwwAFgZTVCsuwullMhaYsWjisRiMP2pRLwuClAsv+QYSqjo0eAnOV1GiwP8CApuydXcisuFtF6RcUhSDCyYo1CSMNaQ1MFPqcmiReEgVgQvUvy5QdtzoXl+dZU59nBMM1y0ff+vu0W2W8fNe7TdmAqjPDsSGONtwBIiJ6GdunvhWPodry0OBtxP91ciDgy/NV7/wzdD1yCTOZVDLaokjI1jN3subJ5fnrXUJcbVFgYvfxZuQaMsWhNCAARvfRMxBRcdDqXjQpK1rrOakVbV+cLUoGx0tO8nCBeIIcDzLx8Q5VlJSNqcslV0xBEoUH1+2FITvb2e777nXyzs3vld83kZ3PElQ/6jBRXRc3QGrWqz25n+f3+EO/h4SHbbrG03EgUKUUpTF4H//uUZOKKApgnybNMG0BCoemdPMJGDtkhHq2w1IklGie8wYosUf5fENMlfopy6ElO5le1mttqsinl6ZlYBCZFmNGBZq7F7RknR7P/cjp/PE1UJIhgPTzTHQkFBLVmPDwNF1NkqGBwy4UXpXYgWOWo3FHofRGxnJMCPm17hOqEWN6uYjytWUVqCDnhNkEyagNkej1TOYYgmvSGVSS+jplIkpOUjRrPK8rY6TZ6u/uA3f8587YXjl3yuqkFO7lFn9+MgKzRCKZpRlJMA9awt2hazJY60sZGAADuhUDRG5aaZkJAU5b1/yfqUs8SOJHMYOTTX5McpOLyMWQos5lgtvvHoYKUNTMAc7PJBAAR4qdGCQ0Y7EaOSQ6BiIOCfV11XToobb136OApEzmH2vBwRS4XrD8k+ldqKzbNIxGr1Rg5tE3rkInKIkTCBo9ZcsfiMJ9RcTAEu4RCzn3SngPlsW20qcZKdQ3xmeImkM6D2zZHv1/9xjnyW/sZMLADogBEsFmK//uUZOWGAwsuSStPMtI8Q5pdPCWHjajtIK280kkgjCd9hIzk0rFgYowKkAVgOEhcQiA7OOU0bEptNDmRni70Izsq0Vkryaq/ZPRHalnyMz2//6O1m/fjUzpZeffP2Pb6OMfxUSBiSYNAqCYvozhMhf8DslgZ55S88fiUVl8qkMdQNYK1uWyyUy1mkvijmO5RyHTUB6pJddsoNaUSOIK1OOo8zrpEDCejhEw8oK0jWAyHfCT7PIoJRCB46J7aFtMVZsPNXogBACgbQAEBHADELAI0Uyw3lyFzSoSUZqzIdRFVY1rMMTHymYzGU9gh3LZLqsvn//szq3uuqN6Xoqv8v9/2Mte3ro/VEq4oQaUCAAHET4JFdi4FAQcFpWlrWeF9kI1hFY3SguGInOv3EHpTpLgs8jjVa9JHH+jt1pdSFWt4EkvcIVpoVfdnKcDGrWYmss7SiA3YENFIT1yKxZRY/5OIBJyjIThbyBWexDCd5eA/jus/xMAu9WkvmhgWuQ6j//uUZOoOA4RAyCuJNZJER1lpYSIeTLTzIg4kdpEZo+Ww9gjolLnsF0otolCDOCwUmBrLZzzz1tvdbHrGX0cyMrCiiQyv6hG0RzKniiQBKnCKM4z9JfVWtJiOFBcn0kSkCCtAakJxoBm4MHYk8TOP0sBoJCyGq9VPVOoVYY6HTKhgw9VbnFUpIbwpm1TLXXDuxvWW8Izyk8xtw8r6iwCsJJQ55yCoQTF7QYFDCA4KyjF1u1rKuGnvuHgMtCMwFcEAhJchp1As6CEx2unZyJocEkjHdEFcSlT9YGGWTeA9jHM7NxrieW67g+3JnM6Wx0HiMsogQzx80Mbn6Ln7miyyFZ/eVNF+4/axCjXYM4wOMrEBGHsJWITECgLOX9iSw70PqwF/67/TsFPAqRIMqgCqgLiQJSEXjJDA4W1y+/N1ZNlb9GCa+eUh1cFgHCaSYxeYKv7I52RnqqG0vZ7GH3yBbEvjUIrXJ/KfmS65V0HuqtTZbW5Wx4r47fTXIDPoQ1QC//uUZOeCA0M7SCuGHaJFYvm8PMNbCzyJJq08x8E/EeVhhg0qChiBiTTLgEJEopRzk7SsSIkjkRyEp+BHqNelqqulmP1CCkmPpJVObtIc7B9ZLD6onTPU2yIsnVXNa3b9v/eu9OzvbZerkTmSkg2fYVAgEfxqYQcZsQZEQTHgEHaQ5jD5thT8Nea6199ZyN6W3wFcrn5VOaiywxltOK5yZYstq0caYvr6XNMx5s2Vs+PrcDF/i3xG7TTvRPhGnquDtKVORKpFeqHtGOy//dz/gqzklkDA0WCcj1mWMizDBisrxEsm6ai3eLBuPHOCrgOGVsYZJH7yyXXDkTKlXWEWj2RpF3/Uaoej2t2NapVpNOROCsaGRSKRtq2QQJO+JhFujYBAsiLrCp5ZqiUHL0S1BxPlfKI7GCDLw0F0ADgmwjApYgadPwCUmP81lKqYpjroWMAwEwfnQeaKMs+zwH8nlYgAjBfFQI2VE0FqWnRts0m3HhwCkFOWw8EP05s87ZSB//uUZOmGA69Fx4NsNLBNqZmNPSJaDByLJRWngAjuBqYmniAAV7d0wKi5lw6epzof4Mr+8WBvPxCgw94jvPE22UUcZ2+cEOfXfwITHCcos9pH7JgharVERXPdSv8n//wI0JG5LV2NRsPVrbntGKfBAqsb4kjaDAT/iwtOUWVpgM3C4FPiWw0qqCSYQ7YWXjdWVJ1l9IszpdUKuOQgMLZlq5W4zrxVr8MIbgYCKEpYRDNZ/3hdaNQNEISAiI+QI69q9I4g88xKIrTVpZIWAL0g9/JBG69BWnozO1+c3MWbkglcOT9JDsbuW68t3Y3ZpZbTWKSrd3TZc7/aSkh/Gfs6nL1aU7xt2ZZhTcpc+fD8jimGPMeXafn////////////////2+VzWoolJKStOSW227bDCKSYkMGc7Z9SAKFmLUCiFPguKQgV9DoFTV9XPTcXEo8DnVYZnZQGxZS1lk1B1g3+kC1pnsZdG1AzwseQEgEijyAZOql/Cb/OV197Tqrty//uUROeABXdLTu5p4ATECymdzWAAFOkvTbmsABLGJic3M5AAbuzmmncu575vPPfew1llqGqGiw/Oz3HHf36uq/wY+sxVsU35fRb+tn++VMeU+8tV4ZgLtyzKpVer6/9Ut/V38b1nOrT/Ww/t5n/0AG2gAg0muSO3bbbXbgFx3aIx4ydYjRUOQUHVXLQo5pODKLB0vUayoA1dFw95H1Rlh1W0N4Hi0RU1b+MKvdC4hdoRHx13XbhdEx8zqzZmCFBGIWuWjDJfFVWdh6G4rus2dQ1eS0VNY9Yr09LWq0eMtjF7TjTuNSjl29Wd0vcpT9XuesbsxJaSZnK9LbvV7P49sZW5/mNekv26tWtupytTWJbq72/Yw+5O017P7nNbwz+e0gk1ACSAAm3ATFl65khVEy9UDl4V+MNXwuq1PLikwqIOSPkECCegtphIdDpqHAfzWzXFe3H+WBRQ2WA9gRs1xbOHkfWN79fXoo9WrWcbhQ9Wz/jwoz1QmEc5eTmfvvPB//uUZC+ABAxJUG9h4ARP5lod5iwADeiTJ009MMEDI+p08I6+mQ5IqRXub343rMSeFn+vtfX1jP+v/n7///39/4lw9hBUnvGkKABBAtvGFFxIODw4Q1MEvdOtNAmpJJCiYdrCdLYb047F9dZsaw6a///NH1HzDqx3DaSl0TpKUPtdz8brRJBHJsXxzTkzQlm9G0Owur/9QIQAAAAAHHnjmNDNdJAECJ1LAOsxhuz6vBaWibw+piZtmHeEJigyOKRL+XVrUcjpDWOZ/HgyxleoKkzEJTijcEDgfDdwLjfRCIXQvMVJmfOqEyGFa/h0JAiIMOmQeLmUlEn1qUaAQQChlHMfi7f/QZNvFbW3G0mI+EEqCwsowS9DaN43jzePy+8/XNhUpeYk7h4kuKzB7yCTbbAFl7AI+ebw///JnH/L5f//1//y0d0VUbkQsjSSRSxxA3IMEWXSwd1hbJGrTjpLozsJ1Zs9xd9XlIA44MR7FYWVRqRba+btO+VLsilbV8Ji//uUZByAAtVG0esJFExLaNo8MGKNjejxJu3gw9EVDCr08ZoeWH70JzvYqXAkXV/UQ6dvV6dF152KjWnsbQ9mMbjmf//8kW3Ki3OrlixaAI+DWTFeOJ2FAM5uSsKiN1mNlnFLBEHxqeeZQzvihbdKf87PsTChCZjpoEu6MZXeTnqSrfuvp00/ZNLf0BzaC6L7bQgAAEknngJINAEIQaDojMEGhCxFLkWWfrpibOTUGMk1BKap6emkkt3es/gFGV/FxIhaR1mAdprBmQTmhEH2RTRZKa4kZ/QGx2vhaKdb7lUrmTDEnWllkWZ32S1Jf6l2/z1iJ8iLhJLY1hpgp6Uf+s3W7L62WSNwPNkqSVi9K2KhDQjH0iGDB8nXL+dBSkxtPxzZ8/JD2ZYkVEJPfFR6kCU3FaVmtnomLnddpEnoO6wwNYvlHKUAjCWzrnisABTwGbCzxYZPhBMiqSgWCu3DgCFKVCNsLEnedCBAwFSgmmhibYWbXHSxMuI6dJSch9lS//uUZB0AA41HyAtJHFRIiQmKPYIeCvCHQawwyPExiSe0wRmNweVxAgXaCTdhL6+phg3hVx5kmQxT2q6Ds2gILGuz6FFBNlSP6waRaxHFbLVCDGKpGqCAaAIq/0ANJ8gSknAQFNnghAsXLj0pKR1efhIHG5ooRxcyvdFsKz6HL10DUYdCkMtPZ+zLdlfqv2b/P/tb9GIjHe7NWZT9ur3V1cW1fuWGlWk20kkQTVOYkODZGsxTN52br7VKlHFFAILQdRCSYfwbuLDpuaUgZzOro+fmrYVVmGB4zTy2BjUr7rVI0ycMAmA7ClccmwxXtQ4CA+loNE6lKXz5r/yYgW2D0qUccgJ5kA0Fd14pjgUSeIPHGwrGsGCXxp+Vl0jp1MP8BnJHfLRj3RsfLsLO3AvB3956jvk3BvzZ+u2PYRi+ct7dKD3f/79TXBawQUuvBiNcK/0HSRjCLaKaPk6TKJGhzAqn5Z0clpueOb6qjMdHJxw5KQ5zpv5h7vi4LAF8uEo6//uUZBwAAowhy8svGPJB4xlsYYMqCyyJL0ywZ5ETBye0xhics5OifPX+DlH8TsGOrc+7h/fmN9Z/3Z56/7AhJAIAAo4OjUCeli6mz3lEnFdIhBUvfTQQczA/TKK4mF9qaVDzrBfopbMnlZMhW3MkMcDqNfq1oivcruC+h1yQFSACkIQKJKpLMaQyQhjAMlX2pjNiwB50ACQxJBe6AZlZEvX+thPPbfYV68CuAiWodzP57PkHihQRzmgZgYUA7nDgiFxQ8YHGiaDgmKucLOKhS1sjTxVzIz//qCcu1tjacaUC8jAYCkIC2yAoDgnm7lAmUgwISUrKy2c4+wAPJgZlJ5b6RZrXAqLswPoKrpvFk7H7SS39Gl2SYGGyYq/etQtKCrh62n5CCjRQ1HAuyWMxCnN9ApdToSwdII6J2ZevS9FsYcSLceaZFKEA+RoCL7EMod2ZX728Mos8ts/plafAlRhrGJmJcUI9XFAdUHwJnvlECA5D3BTro2owpCwtMqiF//uUZDEAAnYwS7MvGWQ84jmpPSMtCkxpJk2kzokOjOWk9JlI0cnibHJNod7Tw7TuLQa5iw6DDEjGfaSbZ0MtFLmUKbdJX7LbU8m0BUzYAg4c8hmIC6R7XUxXXRvR/j7TGrMGm42zWB3ejSAmWUgTjqwe7plyjPkm/z6cRGuRY+ubAY+6BaKbBmZW2nf/J9+bv3WfZb/ze0ef/NZfX+EquwI5GkZLAaIZwQ8lptOjkkRpdmc1yEebRnWeLg5+xPMdOC5NKTyC/mlsg/timUXLuSh6ho5QA66aK/+zD6FEJla+ICFaDGqFaUED7I0IlXW4kqh0FQNktx0qjJx2gKaH6koZuga0YD+uKNSOIwXCRlmZmFuy8gcxj8zHW8Oe179K8og3GN/H1Ds+3uV9XXlRc4jQAKNNgUIAktsz1r6JwyAg0GRPKTp2RgejYJhrOTujKRtEvZdLpYQvKtc8RzTdFYck4wFYFiQQp9X0r7jRVfd//7n73j972siv3n6UBAAK//uURE4AAmEYTEsMGVpM4ulpYYYmSZEbKMywSsktI6VZgwnpuHs8PeJuK0siZiokgFgtYkqaAJxTDheINnC21vKeFgEgwIAwTidENZyENa6XRz1Zpa3RINqUf7c7Ntobc2q60fVKf16O1uuIOiAlIquJkhkHYRAQHQWpQwVOhukHQ5ehmMR19eFWKXISQsra8IFOhPx6em5RePW69HUEiNOlqpTTrvNKvT7du7912brM//J5WTV1FFYADQCgSkoDnuWdMUQnKyvyu2xQSdi+TvwxDISKZXIIr14s7YPem1SMX6ovulSpRKFJZ2P90Tevuyb2rJR0dfPL3Wi7suztb7k96gmsIEwBAAEoqA/S0elBGzsBkMfn1uy9zt3X4ug55UAx2LTPhOni9vKxWujnQ1hN0e7trZ77wcimpsQuqTquzgzjHNmXPtgYkSzUBlysiVhpLAirYcieoKslV6mU2N2mFANNy8CBBH1DTYT3Ukdcfdfgs+1YPdDcSSFXQ+uh//uURGOAAmZGS1NGE1JNBdlqZMJoCcR5Ks2wZ4kvjqUJtg0oS0xWlEYjOU3P/DT9Vc/ejI/3tACXb8gpzndf9qna/YwqAIMHNqokEKlL4pFL7l66ok12IWD26fHRITvlxGcuK7es6zS7MUOS5ooCbwOypRzYwhA0hcuaDBVDyh8sHkuNUONH4cutXc2hT7aGKia/u/IHgHcVZCRYQjQDIai8qhSfGJHw2VB7UkimXLUuEYI+Z2K/wMgoS7n9o2JyERQ2cpvDvf/Bz2CW/o/v+926eD6ub54sPmEWUFEnAUqiExrid0ZWQIgtI5mlHUSqrjB5U+uSCojrzYK2M46FmjEJe/duHri29NRzag2zBTX6PXZfl3//+/tILYjek9+ffMpozAQ0QABEqA/U3GBNri8nnJgNKExRKS4STAQxEHG76NTRlMlkXJbcPTLRjNCu9I/uOUH3C584GxqigoXFVyMhQ0mEzPFrtoiMpbi5ETFUiz3bb2y2RuAx+aKFpxHj//uURHeAAkQPT0NMMN5KQsl3bYMqSaxtK02wZYEulmr09ImmvPJNqctT/OROUC6ps2SPVBIFZK7DxzC+5iK47jWzMw6vByoeRJGk5GPpma/koDtWtl70uXetmMvU9VTwSgAiCMACSiZAc4SNHa24LPUZFeuyKlhPSMWBTqNERYJ0spvKQ7KODAXMzYiChmmmQ55wZu0zN5w0om5MQQvIjM7kXK/S405pPX/8p3PolN5GDuN7ygVi72gAC2KQgiOBUHQRxpMSplwhz5OKd8dDIIoeRNXq6nTg1wNnhsVuukrVwSYkmJ0ony0eMY3nSytrTSc+XExEUAUblrLTaJUB+Ukgn2tBOlpjkuWOQTkgtvIxJWEQtGBMtGcrK6BSNRyBvLzBIemwMz39RDNgfq+d0nnGf7vt/N7h4HbiKfu+n/zlE+zf5rmI64iQAGkBCWgd6aWlmwdNSTarBzkLJodh1rhAtKrP1/4DAIhmMCi3hIvRAhNkgwYe4oxcgsVPnxpJ//uUZI+AAr4+S2tvGcI+AWmcPCZhCmBxOawwZ2klCaVlh4yoaij6mjHovWmzbtAfMbyaUFyeIA3LLNXJY45Ac7KoVH2FMhd1xXLlT+SF3Zc20ulksrONSsQPTq4fFnLfzjdpuq/8NqhKNnKJVQ25rjRwYkwJ+5MVVNLYk+2TWI3sF3j1rtqBBGwREsHGneBxQBWJrxWXKrQlrk3IKSgga33NAkWCEz5a/uvzbbnHtOGFLdRe1xMTuPdNtC1LqvXZcUtTixENjWCqrHpIirHGlOLNG2xjSa3W63W67W7ainrHFpTeBEURbi3o0vAxcFAjDpCZWYcIt9pBcUyYdVCFF7FajINKDm7jRK2GhtkM4ZAgOAnsKsWLKueaUNGJECiFrIXCLLS2TQe+8nBwaAxYigDAl/L6rtyzhcDQbyVQ4gW5SmzFJPCo+nLBtLGbv2n+kXPydikgdnFFK88HDiMlaTGoTYxeuelUZ3nekj8a5u9Naltqmp8Iesy3VLUrTfc6//uUZKSAAnoZz+1kwAhLItlZrBgAGJkrSbmsgBIOn+m3HvADOgguRQ9RS+3MUlm1urLYa5rl7hP/9CI0uI6Xs7ecznu+utAB1Wxlk2CXJ0f7mBVFESgmFKtVBTVl6r8JI+jRGsWMhKHAxYuz/SxDXjJ+3Tpx7EbGzWPry3xBr4jts8PcS029xYNrfsjU4vGzqOJDpEq7kYi3Ro8ztUukIQ50+cW5niYcM5V93nxL90tjd6z4o9NlmMzS8/ELahEogqr30uF2BAgH4Mg6CRGUc4tpPEUelmIRIkv2ICA1R5pGwlULSSglhHHssuF4+uko6qz1KrPVFx7pMfYzmLOhf2u/WupGmts0To6mpaq6lV93dX/+tle5qXicyJsmqpdM0UQAMAhYEAAFCFYTlzewYznDeGkczcuhntFappPoiBukyAlQw2FXk//v+ZVRSZqhWPczIMepFaCo4xI45I2kwHysA/0YAvHfOL0mwwFCdtNGKJmr2ckWBeo+Aaq3Kh4y//uUZGEAAxtM0Wc9oAw2Ywms56QBC3UZSbTzgDDaDGe2mFAEYOqOKSG0TDI+QMHJAqe5iOjiZiKmZbYhLIjV2OK1OKaHrT3/c59zrNVmqZNrt/+painEQDBZsdSkSJgA82hpxYftwS/im8j6vNmxqCp4YNI5TnV26dWexJgDzJiqi+ucpR7Ht/+tH9R0Bdpmo3CPXNm5JLbZfsPhMmYcGU4zFDwi03BBgQCoSHFFimTAEgZmjLEny6I4HRpLRo8o6SJZo4UWqMYYEJjrQUNchsTQ8ZUyhnRjjH6O46BZUSnIxDEsVws37RUDW21H6driGrTLxeBvI3LG9QZIkwMy7Ept0CAlgtXLG3SY24bf93aS00VmDuv5fq5R2QUU3fa/P4S//p2PRaPS+5f1x3f1Gv7+6aWWOZ1KTn6ZFGb13U1S739XD/vdzx//////////u3DR5ulNMhxsCs2SJFGJVHSWhd1IUQlWyqlokrVsL9Qt5YkWFOKTFjEJROxNiiAF//uUZHkABjhS1m5rIASTqlp9x8AAzdj5O125gAjRiubjsvAAAywoAQFDHA6pwmhikDDI43A/YP1ADiJj5DlCCEXJQiAsgupE0LWSR7HyTKEuGmaEqJSEES4jSKJrrN1M5VIGiqZlwirdaNZCiUy2xm83IkbJfR/rQTeZk0Xz5fOJte6Fva1d00//Mk4RghgNAKb4AQGZsulBWUNngNNduVEY9PsTYa7qfM1KVbgtEAUMDbwHHgDSSClgvalAPEHrESWmjoj5FKlEiJq2rSUXjZAyNkvykTySSlPf9Fm2qQcyRbqpoUqFrI1rSUipbVU0nM4vuq9Hc7a/sWjiGSBgEIh2Hm3lOcogGjesRSJww7SvLflt0uBxGkLGA0o2LJf96PgsU/1rOMWe3mq0XbQMRQABg3LyUgFPl/iyZYYKiJNgZoQJQMp44acSULhGGQYwhzGlmESsWuWyCR+NJVi8ZHeNfJyF54whn9ZbjKZNKq352zW/31PVyrW8+236WnNp//uUZCgKA3knyRM8YkI5AgoNPMtjDbC5JM5ky0DSheakzCUARy87DfIs7r2EibD3/b5OQczPkp48X/8Y2PdWfqGz8w4DEhMiLaagb+LGGu55wqmxZDPSr3R/GlLBokFS1v1c4/QgFZIA2/ZqEr0Gn226uhBzPNWv+67i1KEYM/NoHMEEBQw+ACIjGEwcYKAYCDSJpa0xFcRoVp6sZMgNUUYjQNLKBX7MLMWJgWlrRqXSiVwLvnLuAShPnOz5OXiJgCnbkIQ87CjeXpmNrxjdNI3IvvB+ZjqefbNz89euwP1MMG2BcsEhOBtoghgit3hIsFA0KHcQzJkCIbNK0IiIYjeYKXvYOAt+LjmPRETkOY42fNqql2v2/2kfY+3+69UAAAA81umgNvTCAABobHg6EAgDBUaHrKVOipZg5LMXZkaSONbkJiTDWxQbr0VeU59pYEdKHr1PO4hDdB6wfUgoQQ0eNNYoYgSieDGMklbom5+rJUgavuTENZMxU3ET3H0q//uUZDCLE6w+yBuYQtRFA9qtJeMbjWTjIQ48y1EQDmg89gx0vv12Nn6vnZIHvCpEynIGiLPp/27hPMtZLJLI3BGqGgbRDYbkJcsBf5lIrEIs01Eq4I+4vyjx4yn59bt++RZYHAcKPWiXUOMsIbDaOxymEbbt6PIyijKPfLkyAM4VE1sIUQBwBICWtGEQEomQg1xTC71RyEyKwCClzPTYJCTYixaoQqmFzUsBleLLl4NSz40lHmze0j5mncUPAQKJrs1Jrpvrk4hXePq3Ytm5Hf23Gh9/d2rc3y8lzplqiYESp7lHRUDlv1Ahm4PTK0skN/k0LuP/VWjiqHTQSL4VHxZTFaF0M3obQ3PbNEdZhmvl9v+COAJAqWVdRjdBJfOpYyphwoYy/5Jhj74mSgEQMeioBgOYSAGCjxjIakIWsKgOzZpRlEYjmrYpej0o1AbxbpLDQpTPxANEyAKWLlLK3r9y4yvEo16rIQq8o6pd+9sT0Pfpto7wyK1qX6Rz/7nZ//uUZCmIAzpEyStpHMZColm8PSNXDw0zHA4kc0DmCmj09himlkfntkaG08vOp1lFY+WAbBfAd/6CAIVjX2q4ZZ3hxF7WosJ5HXYV+FkOix85sVKdipnsp7NgzPMOnpgmXYk+H2EC+Mll6d1BZyda8yuK7McmkodZ75Iic0WQwQwsFTAIpMQANI9W8yKAmLIfGZU2TEiJqhbu8rN5c/6mUy6cWdtuYVmjQC5gjJaLEo4gO8yQEJ04vAjiPw0RXqiqNLqlm7P2kwnDYYo9N9AcH3FUGtQU8JhgKnXGc86dNnjmVeoSvczJljCiamU4VoS8zwoci6mXI440iCgsuhJBAwh5A3y6MiqTJ9a0Q6/Kiri/VLv72s2lDsJpgJ0IsZ2VGiCGUW/+79Gl6UJ8WgAFwxkcWERUCBCIGsEscGh0JIjAGeaIJpe0JvxWGrIgTBocmgIW2nCtZplbMSN00QWaEtlGPmOKaN3YlIp9u/nv+Vb9il4t/cc7mUK7/xCyy8/I//uUZCmAAuRGyZNJG0I6oTmdPekkC1SdJS0wbQjpCmXlh4xg+3Kl4gf3MNxvoNUhMglFINUIkngsCHEL0XcWxfYzLoWEbKCkFJELkB+k7BUmgexFxBCGKe6ylH/9RRov7e+y7B4aPxMEAIBAAIdacaAAYo+1xbaFoWDhUBBbJTNmbEGvK1UDR4Yh0erHz2qp9xspOHsVtYcoGjM4nzlKslfsRJOc3M1GhUyGPDsZsEgCv5fHgX6tXbvtZNu9/0+89v9QIBhOFRxkDgBBHXKZXryHODMUi0zPX9Cj6g8nWbGaGlsMnBkGFTQymTUx49iD/osYhfT/f+/ohVbzySiPqLKBIIOJWLae9sEsFgztmOGOdwIOhIBh0Ug0CYCpjSHwFRCdUhMhzu1ycTV2IG5oRaCYIhXO2Z0QTRAAWmBrSbGMWkk0PERichbv5uGxKTShSRUhccQHKjI43G0CEA861WSw2xgFAK23gMsMELGuYRzYWNmHlg09Z5qpkky0Ccar//uUZEGAAsEkSYtJGtA4wXpdCegFiyCZLywwylDxjem09gye/3OZRT//QPTtGpaeMC5AggtMrBNA8BUi6bIoHcWJq6aqge/gTHbA5lAkkUxEqPvcEnLD7PFJhxvNSh6c9A4v5tzO9kCsnt2EOZk+cxITBxY1yiiwX6MJpxUqBaDN9ccwO5YqIiVr1t1Rxy1uRoJBgfm8ZRQnLSAaQSIrz0Ld0hsQBOkNll8Lcu6f1bm5uKdgw+MCCDos7+v/yTrg+zohGEv2Z6oAAAiAAkBIwHjhjx97w4cg8o+vdLpTlVcugscPAwFBufH6o9Kl2rqGJhUsAQDbmtz3aX5KwUf7SM/PX6Xv5Q1ZQWBxUoOOoNJOBYMjHiKVFOPpt4dGkPzxJIKJQERexBC5oseB/hOS8KQciKH7NrVbf6BuEeIdm3CyYoD0erenHQ6KHCYRiMSuSvZjlOWejO6LJq33fIc6POojjUhoQiIkbmBgoQFoOJbiEFTRBR2sIhq7MlZpG5sk//uUZFwIAskpSutMGsA+o1msPYMdC4jbIg2kbwDijSo1hIyuRsKpaOIBWEEJssjIkxKlqUSjmpwOalgACYPDpDPjsjjqL5Oz3ReYylycIyyJbyzL8UmRD0IAM4MQM01tskkttckjYTAiKXMYmlavSKCohIkULWHQ+hbHU0MSdpf9L65/4LiwqSUe4RBucujvm+n//fpu4v9FQY0/UBNEJJEOMQFQtQkmTCpsBANhpIGbHEnTc6LRyBcocBLBR4IaBEeFGC1hRHydDlH/Ht8s7dmEiRhWuEMQvq8xuazvR1ZmSjK5pWt/fRksLEBrc1Q2l6FJdJrY5IwggiYiFqA0VKjyMnchzylNKAS9qtTXSnizFs617DBXOMqEIqRRdoE6f/o2VX///3JBAB7QHfAFlzOnBJzGcv0hYAlEQl/qzs6b2AwbOiEneITAleHufEwpkoK8pDllPR1p0IJS7KNqM7V02xatLK9RrdT5DJWey1GJ1c79XqFSwzwk5+v1b8uX//uUZHOIAsY8SQtGFEA2gqqNPSJJi5iNIozhIwjihia0lKSYsy42FToAAFWMpJJp0IgGIRsjRNKJaeJYvEoiUHVk1nNcNmbWQhYPMakYbTOCu56VZmT3f2OMf71oUh61PKo6dQaQW0NdY1QwQGBgUjQQYkUAATBAUhEUancU4dyniUsZ8/s7Dm26zNi4YDKwtQdLop75Kk2NxlnifqlapLM7g3XgN1Y90Jo+HPDkcBdid5AaVbBflbd/8bJ/m1cc8wsDBsHRHgKwIyOURw8TSG5DsnqhEjRZhtInpCy/ui5Ot6a3Nye4QUU0UtYTEi1FbrFewX//9vSLrvQEZ0CP6XghKSleAvMBUJIgYCUQYpiLkVGRUVM4iFIcEaE3ZgVz6RVaJ1jTSeTYu8uS9pY5wzWMLTypL4onM55hEsASqVthLvXEx703Pd3nGLdNyaldv+aEiITnZTgIKE68HUHwDNhMQYjGquaOvOZRlgtJzyORDW/9T0vka09M5NAgsBg6//uUZI8DAwEyyAMmHTI4YzloPMNUCvidJKwkb0j4Dmc09gx8z9Ox4z/6+HrVVpt48M7hxioIgFH+diAYYsAw4BHXId5LcIEIOEwFRxAUVh9H5M8fqT1bZCeVlKG/4ldoCaig1ayGaSGEj07lN5BBtckONCxJRFBEbFAMGkBMPweGiry3c70MBQRSlhFNqaaYGDAOhQDYBMYdNsCxUcv4AoY19SZJkCxyWhIE6ZGMerrlio59H2rIo01uFV3pawV9bTqyLWsHJAAAgYDKjYEphJYNAEt0A6Hi42rkEdA5Fy0wBdIJBPOKFsLHjbN4gWYSST2lFwdRxuef+86gpYJtcTrEawuQDHfODXpGD3vckus+MP4sb3yxxAwAbcGlWxf1MISQ8WjwQCeAIVieuJTg2WlZ7jzQfS1zpO0G8Ic7a1DhSMIMKicgG3NJPeBhUfCq++5dBLuptFVxeYM3lyOyA0UEhSgz9zZFBAxcsRJLWTCcJUoWASOSgbHASWSYSXzC//uUZKWAApAhSatMGfA/wjlVYYMaCkhpK40wxwEejWWlhgxwAyWxe424pvb0J51/IoPZZ6iYhkDPCorwmnrs1CzyShDI+OarwnD2fn98+xJ2Ugft9K+iWftfJ4QqqmTEbAT369oybA6HFoIkRys98TXiSYNPMmB1ffoNs7ml6suXpnlsSGCOsWZQz1teyiXbt/RcnDaRQVAwzEA6ljgkJNtZBoPGk44kBSLsdpoKsIfA4OsJRPiuU5cXiMuWOmTVXYrGpBKQRzhq8SMifaqloWhGfSXgfZjybyLUss4XNLrD78l8+ayfJ+mfPnjGsGdUpGzE67+lhlN4uKtHCWOp2ByVWNki0N2/Bcii/Rrb1MTa5GqwVdKShZLzApge56mKpHJp/V6a/iEPpSCFVJDSSgMZRoSyldLnXQ1N7mHL8c2Ntfi1NUgSGpifltSrXvWe455WtZ6x2mwAbVTkJDY0G0wLJFG07nZpQg+FKfyAf28jAfzd+jNrVo/RO8PwwJkb//uUZL2OArk5SQssGuI7ZGliPYMaCw0hJEwwbQDnjKkw9IjuRs+oQBIIuBiiJzrhtCyImeCi2tliZ8cTZgRMG8stdLfaO8HfFizJg4WFBo1Dd7K0y5T2Me+bpdcrqudAAbCAbJgEDwYibBCFQkEWdHjlclx24qrtNf5mDHAtVlxQ+er4DCLzM+au8/BToT8JRVEpaR04zOm0woNBlypl5sRxSLvIZUzNUy8j6Vkfi//c///5D7LZlUIeRARg3jYAAlREzimBFoodCFZgVCr0Tmr2FUO5wxj0QMShKYMuJKWfNOWSuIWBNSWvDJjN+mbqQduq+zumBlQ/JACMFVMuWMQACi5BIm4AnhlwZAIRCI3mmw6FE1EdStQyzDK+5S7cy3WG422JybTKaSQUkWl8wAhyNUigWf3kQQeTdTMdReIZi7GYW2FFl52xHP4wUK33nN/2GgvdmZvsY6Weey/kUr00/uc7JA9EzSYdL+wMEAwy3MEBKBFgKUnQOlFKAk5W//uUZNkAAphGy9MBFeJDo0msMeM3C0kjJCywbRjsB2Xk9gxoOresiks/N68AOQnynd3oU1gtwPFjwJUHQOhobx7STD4WDT5ANDL+hMa25xcXRZUQRe0TSC0KArCqIPBTwLaqmVgd52KYmBayHoUktWdEvxepsKTahJVrr1gbldSFIDwgMi1qkeaZ2sb/mgY9G6/TTQzyn68/PG3DdUy/xooNRTHtZaIox72ra9VJCAAAAD0ejBkGKMaluWGpp9pGpC1H0lENX3ccSGFnEIMETWhV5qjzvjXN98uK5rrI6dzTHOXjVPKuZHp6UXzjW9LJPJZlX//zjZp/ef3/KQEtVpT+XCdW6jF9Q+OFAoGWmLJFQCZYSZASkekMJBwYCjTW3XiDGXKNiEGZdAmKzkqB8oSOLfsPB2XfbQobTkEeoLQxAMGjkIHHiODBGSLOtOPSBif1shFLvlqHdPyjcv5GicRJfiP/yk9+mz/lq8nr1QGCQIiThfMaGAmqtnXkYcyt//uUZPEKA5ZAx4tYMfJIArmcMeMNCpzlJKywa1FfpmSdgw3apLfPyBwpHZDyDTqOMvsSDbondQrsEHTPtUrIzvSxSR7cRfGlbFnAOok9JpRA443gNJwmunkEs1SiKDxQc8+eMGKAYtgqAZcqdYVBIpC7gRyeEDJQVLtKZRKeYkZMV6wrlw1sUzG/Q2BLFmDV6zj5G84D902Qo3521fP5nY4ozCQEV2lzIRsHl23mB8Kz72ULUKF3Wi8aiNRAETYCWBfEoHQeKvDPQ4L8W1UI52zA65N8JnGWHP0kkYzQVDBk2YvOOHQ2SABBj3CC2kc/Q9zED84rW3bXwK4w50Z1mjYIg280GiNS0xhjLAQGGwVJzEBQMHi3gOamvqqJ7sCQiY88zpLLmU9FA37BJdUvD4OQjls+JZYlcjoflNXAjpY+aJj+lYkcSOU7EZJysORBh4jghDTAy2QQLQMOMYhBSsXHZAwr4qUn2DN9/84GtHxfReHcvXx/6jM+doaMLzN0//uUZOsPE0gySANMG8JQBAk2YSNYCxClJA08Z8ESCuUU8w2Quj7W0wMKkCQSSeAEjb09Z1WODn3SAgdyatJNyRqzTYvpxOKKrdFntzcbfTagxGnneXMir+dPMjn0ElbKnOHEytTxRTGE9xC1dS8/yTZChjLi3ca1ImaGJmhIKgJloUIjAMFn4HjKJFxjAwZ1y6UZfhUkAxKAVFYdrOtkvprtFyYbERzD00Q3zi1HIKx0eyRLTDAzBHpazBSZRhxqFc1PoiUr/7xqEFtFvEHYyJbY5j5cM8Hy2ef0d8Rsb8ZtX+2EBb0ecK3NSwtga5eqvBQ6UbX1HJCSykmw1wZFhgO1IAK1IqMiS1rwcGLjw+CQbGC2dHoaouVdQ2q6dVovWvOsuaDtkv3MUOW2BAABvaBggJjk4jAkIELDAEKEAoCh0R34ZyGAmBKcutDTcJU1BwB0SjyBtBSAsZIgnq+daZZi9+ir5vGx5s9FG0++4upBGyu+bAWCg1fdY69Gpv4U//uUZO4PBB9MRoNsNLBOhFlqYSN0jq0LHg2w1IkABiZlhJhYI2WINQZOGr/u0LQjoxlQZFdDItGDyCgIAABpywUwIIXnbNHSJiE+dYMo/GZEhwgOHUDMpM1BsyokVDBUnvGNYTCJx33eL+DVRX/9AclaHzJrpNXmA2b2XGvhb93H1P7tRRRrdhqp6SxkVAOvAFeYA8aYSggLRu+JCjDAkBSSJfhRdLSPTjcoNjK7LkEPkmFB0XQgisTSbc2YKlESE7a7hwSHzqWr62r9VOMTQwnRjZPrMShL6GdhDOtdSePjZyaKJXKeecZxJw7RXv2/49Udu7GmgI04lUEEgNRxalhJxg7mSV1ayJIEAJfU66x3KNXp73kRpqGIldOumzXlfu0lDrpfX2q5LJlRdK1eSn/IjppYznuYuwWrPI2IQwRlS7CA6XjcHzSGW8VAySal7xuVAlpajwxhuzws6lN4DoEicuJm2jTkbNlHa+ckkDf2lO0Y9Wjc+oJDCgw6+DjR//uUZNgHQ1Y6SCtMHFBO4zlIYMNUzWkLIA0kc0kYJaWlgwlZUlWPxWn5T973tIZwnBpjzRQVjr0mUk9SIAQEUYEUmYxQ+qFq2BqxRkcdg7iSiLAiZdXqWVDfTTW2i25VLjI/jQIyGKVV6S2GRAjyJ6RGunX5nPMJ70F9RE8Oae/l1jPmEuP6P5+fjQHVHne/Rdq0pJEgGCBFxHyCrAAAmyHC9DUDpIeqwmFtLYQE4lVI5IjR3WfMiUKIRUGCs0bnJztNyMlJ0/SKsQmjFXNmCySuDoIF3PmeNnTbuWfS76ESV7+MeTj5DNNJ/O3y2SmNX//UJX7CrYnGymIboHOQUxBjwVYHASlMqS0FNkSsXAhxZVvkZr0i9YkODgKJig8UE94UAySrREJlHIUDT7qbdQd9EkgeHVVqTSs3qkby6gAgAAAGD2hMhGOrDkSwqKXiWol657WXITVvu44FQcFhIrRCMO8ECE6qK4L6Sncv/402F6WfSHSoeY4S9Em9Pv3u//uUZNCAAuo0SINJHMBUJbklPYNMTFEhOUeka/EuCOd08JmMFVVZJhf8qKZ/M3Ay0H7kk1QK17sejhgP///v/rI1XbElEgCC4ymwS4igx0EdxeUQONViwjdaGw6lYoHin8afcrFE78LShr5lm1NfcT3PHLMKaWw6+8aHwPEJQba4ODJusu4o82lGJrV3lyKKxjv9XS5AAcMn91hMIuYkW1FBGozAjtqAy127D6UMtjRUeAcEQEQuDwgJxhkGhMamKplGGXPKLQTEjFvIzjGdkWptz0aucz3ZlMe7ur1fVLdqZ5lHzkP9eibJObljoZEa79kSiQDhd3oqhDBESSivosXhOzUhF7ZUq0L66GQJBo4qPsWUhi04iIzCnnNOcszofU5zlW7nqY76LRrPYxt+m6uxtGRkdbt22RfVvbrtSvlgUCp7/8NVQCAciJtsltuuksltvHAaZvAycFw5QnYOEIQgCCADOwgazwdCvvCmvGBIl5zeLCjx2GiTIB/LAXIC//uURM6EAu8bSUssMzJUJEnaPGaji0EZJlWTgAldoueqnnAGWEBQwsYgBeLoFkP9QqR6yqgxjlXllmUpYLKi5LT+RxEITFIpHuaoVR9OCugKluTjO8q5mV8WbHkWE8iZZ59yS6mXT59JFw7bMTbq/jahfcSDAlreO3w4Mx+MT+JuJ97WNOUjqNB3vLO9q3Xc3+ojlv//UABLozGAiqsaLJ9fbXJJAAtdmyXxMHO66JhUYArAxURAQQHBidTQY2IgBkSkX0gTsKensb54sCBmciCnKeSkebeMKFWRaUWrQer1i8VhXDYpS8CgbzocXaKSDCr86tSCxdILaiNG+3ULw6Odu+7HDgT/Lk8xGlxF3Dk3Cjv54NJtW+s+keDCpDvvMXf9PrU1tV194zaa6bOsMBtgFt6Qp7WN9OSegzlAwYczxlUaKjypGgESXpHrYCRHKxg4fVMEHERYm3MiMozOFLAP4EQODQN9jsblSjwmpuDdQ0UMAI6Uj33CoHVpR2HK//uUZMqABV1Ly+5p4ACXaSl/zbwAWNFLObmsAAJAqWm3MPACCWtfQGPlPcUTcuHf41htVhVfmEqI7OFxp7dR9ATVewDTTVNCudhiKS+afxgCml2K4QVyKxL/5WlX1VINyZI5cIaWtdWBh27OWXe1b1nnN85vedfvJfG4vII3KZTzK7urO1pn8d83/9/r9zcXwl/4f///xna/KSEkSYmm25LXJG5LrDDMx5LaNqmol73HUxq3Ny+lmL1o/zTerp0fQIMGGFfZMjqaCAkM+Iiq/m9Yk1Y7qm6myrpo2TpVr+RXvoirjq4vohpw5cX/jMMuIm/dklxmVec3kLU+tPvhRsmt9/eh/Z///z/n/+mt/7piemN//H1mmc5+M29eyM+7y7pPSn/kyV9KAAAAPMCApTyU4IQGAQCkAIjoHpfL8BgQmO8pGIIbGIIMA4JmRO3BScoGFMClADRQGZ6BjYiAkxiVWHJEBQJARVmJmRUolBFAvuOSsqu5kmX1pGxNCyi6//uUZCuOBGBLSR92gARH4gmt7LAAD4TLJAzxiYkPiWp9h6ROZGZwwRosieYxRZl1LZms6tr2SWhW6VkEl73Wm9VnTSWp0FIKZl/17J2Wo8zLdlMeDkLE/+8xKjEAAkptwLYg2FIVQ92XWSzbJBHG6zTExFwXtedYdAeBiotWd2ocnoalCAuYkamHetyLG0RKup5dxtTXN7kN1Kae655dOhApKrw2wQNsjyYwo8sdYaORi1mII6AhwHA5VBUlVxo8OglgqVgyAhYAP+wZOdqT/2YYWGhdPKI5isWdEzp+rXrCeI4/xrm1Y9AHNiQJDjlNbtPWhmzCyByfzp3Lvt4wspWftSZjfnvYzbAM4nYn3D9/cmX7ZO5/eX7/r9XlNkgQVTQo000mJJHD6dapWvSVk6zVNEk2Yk778AdZDaJBkM4gNt70F0LZfkVOvmA/DNCQ/WMZ6U9zFSf/lmquQTsHHidtIBUhe8rhb6GXwa4prPuCNAkCxeoL/NPTkTGaay9W//uUZA+AA6AwzmMvNFo/5OrNZEmnjXEvJi2kVNjsh2Ylhj1YF4o6uxm6RUM1hml+dM0qdZGJ87Q2dt1KhN9ZonVS+cvGtITpCBNjKgsudONMyapFR1HgIi77H/dkq158+tyVdNDJ82HPcz2XXu9Q7fTMvFSc6YrbXdXUF5NI5GAApmzZfWGG6BoW/3/PvR2UU3zFeI2ajcjlnQNs36oUSbmd0CtsDG/DEMKXrWmiElKCaSpE3VxZo9QBAYqMEAwcfmBgwoEAUPMFAkhTIykycAEM6BA8IGV9FnmcRuKMyIRJUatq1WvPs68joY1KiVS7gk9ddhFt1TZwe1TrS1pK42+vid0VgbOQfKYu69GQ5SqSv0T00NqduqN2dU3fM9pKtPIUdICAYAACZOvupasE11y4Lg5fLqBYUbLOAeusJTA9NVu4hh9P7sq3WkGBdgKGqWD02pq9X+RO6WI1//uchTVECAA49lphgsYKMmDAqKxhXGPGoW3jDycAHLlhac1p//uUZBAPA5Y1x4N4SsJA4vliPeMuDTjFIA0kdIj9B+XYx6DYmDcRmCkmqAojIG/Xc9tmhnEub6QPmoxDZTFIQJBKYdNAyjci84SUihkmtJDuWo/wxjpIdprEfn3zvynvll1do/utXjTGZYShjXTUqma5cd0G3+xkVxMFSA1IFTNL0/hPEaAJ2BWqVtb+8rCis0SeUSWmoWK/epIJYmwZiS4YQBXixzR+tKmrO3/7Ub6LrvcJQBFRGCWprUycQFImCOIVAY4hobp4ArJx9YIDrDK1qVMbZ69CqLgxdMBwIChxXMjm5bFiAukCAWmQKD9aoZlF4kZVYdzlYQ7DiwMY47mm7UKpfKeyCSLC8QraDwRVT0IJwAR4y1za3ohkV02kM6YhCZMENokCwPyK8YE+S4NhpkZVw3qODwbRFMCp3XNYbGmMXPuNPuJA6IzJyLivBtbnMQ/dZo//ZsFwKKqopLoSgAlIBTJbkEgTClwUTAxQ0NxcppHCsDavCxZ8GlOJ//uUZA+PAvIqyINMNJI6QVmZPYYkC2CfIgy8ywDgjKr08whuJF1NqgEg9zYu+M+ZfKqdVrnEbfKvSX0E9pw6Zk7HqMIZqX22b/l5rVu5Syn/kB7/1HmPcvHc3s6ZNCSSyX/8YACwVqrBeC5KpCTofa+DfRnErOmEHlGrDBwJnWEDppA0keIDd5NRIWZmzqz9YiilFStOjV2fRLW7noOV4xwgsLuHDidewDHmv6DrjP9SPBPFKPlfN4fAuop4nhhJtUPjRbJ1HOs4xN32gdMs4HQaSYDp4Q5tNJ7uPqnJ+zqz3m19WwqCyUQ+w09ybwGSPbjkDoU8hRk7LErbJZa4wwLkIULSW8kg0vkYicw8g6B5j3t0ZIk+t0LdFcTLvU2cYz9i97blliwkUqRT//m6PKUydMSnio9GQlBMRQ+SHM7EMIMKIwxQw50WGKEULMpE76kGTqlU7+Qa5F5WDQoBgyP8GE1HWu8fTGz1tkF0mkvyZfzpZy5+/YfTbx4Sf/qQ//uUZCeMAxguyINZMXA24drNPMIpi2ErKGwMU0DUBmVE95goj2DXkzZgYaDCAcrc9E+4dAFz0RbTxyYTW2yBsEcBpNSiBvGkAgI6A9tFBKDdUxIoOg0YsS0AweZIH9iWnLifP5i+p//xvsFf7r/SAASTAdGG2iZgjArGgFFNuiFIMkBo5G0KFvxFYJltJS0z+tPj1RxgRoHoN8BcWwJntZxwASyRELkyPXBUjnTOUnysCvcq92XWmRnXWib0u9DdFXX86XCspAzwbAE8ARxgklEDfJVPIYyGq8ev0MC1ml3In84GAtHWIHhQ4CaA6MjaN96xydqOwUuX//t+3vbcAI5qQwo9zi5hhgIKTjBUBBjCkguDDBDkgdJYUmaIwGQ9AqQRsIKoyJJ68e9VYpQlk05ekWVkU8BFQnK2TGW6uxa8N7fqFUkw+nHhm1cnPln3y9511+FP8//luWqNiCGIDkdrU0bEAcKNkhI8orT+hYg00ATd+ThwaD4ANnu8yWup//uUZECOAvRISQtMGuQ5oWoNCeYBC0CvJA0wbQjliukw8I4OAag+HAICzXVrJ0JdfJWKilf//rWRcTUdxaBVT/DgBg4qHMSCTfEBcBAi/aYrFmdLpFxuiIYKKx21WjJgrXHtE/xMqieDEBMuYSBICBvYyQcjpI5mNqCSFCIjl4RXoK3/ros2FXyT9iz99zPX37zL+6EW2nJUv+liE5iAC0ml36qZdqmd4tsFGBnu8u/fI3UxKEfXBjxQyQC04uhCEXsWSjkOZY/Z7etntR66CKJKoGwoI5K3Yt+zpYFrLA39Xq3ucieSkkMgHFi4MJFpOgr7przZxsu1YyWVkqsY8lgNllVNpj7Oja2opT0V8j+mTZX+qWPMZ0T6acIMdgyhEazH9D/jggQgJNuFtPAeLjSyA9I4Cw4GTzuDoMOS4IU8zGUnKoQo0UpVHQeqK5zuq9fIqaKvVXt0rur1f6X//9arU03bpnQEqii4kk4AmS2mrpjrSZut5niZEoU2fqSv//uUZFiMArlIyxsGE8JDqQmdMGJeCy0nLmwYT0j4jKn08o3WrRSzKTigRkI9XW0xRDmYQR5zVJTvqaMpEG+7NdGTORDIzs11POa7PdH20TbSeq2fSQx1tTpQu1ldLC5S2UHnwV5G86crmkljkbSYU6mPovp0pAkJuKqCoX6HJk6pWbdo+0rBDY6SnGn/7SBkYaUxqWs2jBV+y9dLrZZchZq/3veGKgEQwupiBpKor2ZCFBTbPFlsQdsJGgLhMMNofC0t1TSSVEg3MvNGnUr0/7BqaUUHpFJ9cXi7UPCwrCedZr2KMC9Ec5/CZ5CbhC4qWWCAAYYNksQl0yPQ1g2BzEyHexJlvV6RhMCCwUpREKejOY+wiY1D3JBoyXY6tXoyGL0Zn0eyWozjExqiwg0D54D01RVNZilt7xIgBpu1qKbbarvfSwErJ2ZE5NzgVpJiRqcmj9EYmAtyXF88x0TFuHpyOZKNWlivDqqSuVHqr6rWpO5n2d69Fb3+yM6mhVV9//uURG0AAlseShMJGsBNZSlWPMJoCWTrR4eYTTEvj6ZlhgxsOHUev2dLgk1SEiAeFTqCA6yz2BIZjsOgkBGE6oBR2PCQyjZhTEM8JTB2gWVECQRLkm3DkVE6XNfBDnnlC9zDIQwouJzSHFHgykJvQwXDqUOt6nFC9aIIWVLSOjCOMMUyZFkyWACwI5YKAViQfr21WI8q37dmZKQRnlL+EpGdIaihzLDuPKzdr5V8R/NKIXO+YzzaV+PAsLdyvl/6b0X/Xy7ACBJFksPodJ4TcqkChZYRdFOPE/zJS6w+OCw5IKEWT5NyXTQVnY2U8uft62uUaBspSC0oxyRZ4OGigqgWegvd2OkN8g2tiRdDbHDJFIgCGBg42K2DVy3IOCvFikAlqIaII5hMPRTJYtP1jeIh8onAyJcCCmBjUPkEJqRFaXst1prfb7IFKCqDD5BKRZNfVU2Lw+RkrRu1qTwQAgAA0GbMggTfXqBl4khWNjQkjmBY6Maj4hrD9MtIvMuM//uURIMAAloZTEMsGdpM4wlpZeYKCZyLKMwwaQEvICVZlgh5zXa4ylcjDmM6o6oz60g+x1VFaiG9lpd+r6du2/fI9W033uYol228ltrfBgCOSNUcxEJoMGEgEChAOWRXov+DoLZcgJUGl8qnl/Q478qSqiuJq20PH6TdusIyISJXYswgENwdxROkDdVM/HIqxDzMiWmS+2D5vz4MI5IcLnzV9S2Ys5LmA+LEICqpYQwGoH4XctzM/HWH6UsNxFlOfLHWKuL1M/2PdGnf27uwgqaPHhdgxvCi3Ju0af9FT75ne7PrLf/pJMklYtTbuTQV00h61CHKfZ1lsTzw5zmx5O3vj5p/h/4Syine2A1sb6bcvY3C1Ipnxju729ut++29KWL7HMZkuyMSquVHkzokdeZVbNXQescVwN//bcTrUARgDA7rqwbhwqyqDi5hYVRE1Du9DFSdk8RHwkyNWF0AEo2Gjp3a1NziJXftWrZu+Kmvdv/a6gAkgICKLgPu8jCj//uUZJiAAtMzSQtpHJQ6oxl5PYM2C1UhQUeYszDXieaw9gwYqJiGr/NIzxYTNYy7k7JarE9VU5YGdnFyb4TkzZzsdIYQhqDL4Xr6yxMdQQU184hH19pz7o2h/ds/Hz9/t9fv+dXvTWbZ/7dUDCsEgAAYDzSJBfjzr3hh9HvaQ4x5MP9+n52RoNF6nudnW4UaGZvg2bnZ5XUeKM2Nu3XvZRu1Ie2c/3yzv3i0uAIQQpL4fjZbZlyAdSpk6w0XA0oEUazIZpAqLA+HdbEEcX7RGdGWQ3Shg8D4RV6V1suEbP7M4o8k8c2qnd+T378+Zf2Zf3PfztTF1cy0L1XGTX7mAESABIwfCC82mwOCZ2WywDIUmjyNuMbFIwuuY0s/xhFLNF5lAe9r0v4X//8mtlDwocUtAbkz61idKNFP79pXZFDv5hctFTqebkckSb56mKwNZLgtPpmAuvCXBNQ9K6OwsMyzG7ChAs/ZxivsA3CQdXBmj/D3AGues4YdaeoeUo45//uUZLOAAqgwS1NPGXI9o7lWZSNICmjzLSywZ4kUEWWllgx4A1MNuZYTbZdcVOWEQ3FRUu9sIEUIjqFDP/9JBCIJXThi5CWjcmAhzKEQBIBSxp55q7UXomPJD6TtqnuNsEhIiiEN4NzQudZF2UxtmTMvfi/7PpYLO+xKRAAAAgrit0agNg4iAAKRwDQZY/ywCnhsH4TZKqZeubzGxyIwW11MWpBtIO3oTauezt/raUimgfW7s90TXJaHXCkVW1t5BYjtG7abysqdpXS83SLjWuDZA5+rQU3G5E++lg761iKghxrtbUPvk5KoRHLB+aUfcL7Kf0GYqfRLT4DIqMmYpYIRdhKYjN2r9r4E+r6gNu9KhKgOuzWySSNtvjdXvRKc1kSMjkQlqypH5bZh0PxoPJgVg2qRBaa+vlUxEE0Yru6I0rSpZlOr1oZbXROzeqfenSqyjKpExECgFAEMiUyCY+OZcq2xGSSz/qC7dtsf/w4cMNdabfJsF8aCUI0FAbaM//uUZMwAArUjzussGlg8oVmZZSYSC0x9KO28w8D1iKkxkw0eiILEghRIL1el75pJ4cWS8sy7RIngeGXevX//f/zBYvCx9AQAAAEEkrnI3iYbUAAAZICQgHlpkTGTt4X1MIgVLt4pdTvs78CO4/cJfZ7ZuMNIJMSMGyp9HXz+0ZhXFTjIonAgFYGEBPCUf5w5nMPihhVHL5NW1HKpZxQ4oDjh9Ri3GZC1aQVO9mI+yLs1X6rd/yyC35YgKISSkB9hqakhITWRIoQ+TBaavQuOSwdzRpnk7j+lKmQVFh3cz1jI3L5RjQntUeKjkk114o5rjJkz2ytiBaBxCMEegYo6+VDSewCSkk+fDkRpQGp+kIOLUFzBICrTC4ATxMPClUYxDsoiSFMY5KtIKEkXqv9v2EBSczU4oddaOPvUIMTqCLysaIUCyGFzSqWRhDcBOBEE/zbQvPkrH5nWyILU/Msz6eF5QoMfD//p/jgoKOSQCk0nge3VQkZVwEgy1KK0se54//uUZOSAArQzVGsJKzw4oYpsYMkhjl01KU4ksxEijeYdtgiwWHPFFY2xBniFywGwCGNLGwhWNTYRGm3rD80F56aCJmLZf7H13obppPn9Ns2G6QIAEgkvm5OCZID6HJOcRgFmzd1VUHmCmDRUhceCcRSM8doJ4dDWOMC531ylYeGuUyou5S0leHRMvJCZhMRMHGFGd2L2313FKK5Dq52b2ejohXvq6NR0d+prb/voLlYPFs//9AE1gByOOUHRcfxobUMsT1+e5pqwXdZi1XnSvwk766Zq9w5mZgbwQpXZGQhAtpMHw5cxh18yYeHh7BdzzBeu9wtqYLnIOW3EwOHlmH01AAAAPM6omNjQZMrxDBwbmBYGg0CjCYGwIBLwFuTE4jSYEgMCaZrstVfdIVe7LWNqkT2bPJmOsCX6zlyQTRwASwEOQQLj0jB0RuJwYFy8huB1gdWRdItG/Mn4m5YSjru+nJrsnup63dGIqjSshuyccy0y/SL5uYVXRy/SLtld//uUZO0AAz5HS7uMGsREQXmdcwMKDGUhKu4wq1ExDmapl4gw9qnbSnWALf//YFGA5LE3G0oD8dFhVApS/Fm2w4KICINMIkl66BhSJBdMUnh8ytoTTlw9HO1Dzwr5OkeIGZb9lXn4b3/z12M1ry+//3/LW9T/hOsslXw2Y5HYcUDMY+h+YGgEFQVTHMDwVWGSyVwDQ2TqTIkx8CTKxEFU4L70Q+Ia5MTcH+ha0lHbyG+lA0oZZJeAhYCHYvDHhZ7JqINrvvpc+qn1vhN8nlLZMnByZIlJhgNGYo4lOiiKtAosE3S2xpyyNwANFiqv4XeI4BzsYtBQuCrVS88hyB4FXFpCCE9PLLpxMoKLOGdkNrLM8db8+3PEtEbYGWpLEGQ2eeVX1oSVqg8dUgUKPVKnggoAAAA8yVmM0YCowBIgwHAQwZAQQA+o0xBg4GAcwIClH2AV+P860qaXQQfBCgCBhftuC0i3LuPxMDA+sONNvm9RZrcfagEsKc72k9rF0MLZ//uUZO0OBD1HRxupNSRNQnndZMM9TLypIg68y0E+k2e1l4xswMTwgahmgw+6vWcmv4Bh3QxypGnwsZ2LGK0Zkat9mVs//86KwfhwIGP930JKRbtjV/SwGPSsZ5D4l1ySsJRIMgJgQMA585b+Cgy7fOHlJBTmbiiaAxOkQOeJK2vaKS4nXshx+LPbov1nn93kjb0nEhRQABAC5qRe5pSNoOOkSBMAA8WQEYGioJOKYLiuY8h+CQSCjxcQGguZLyEPKl4sxWwvMhMZSpcswMIWBqTkMvn3oclAGylU7bwxLZfLN36NsEtXO0/Kk44sL6o8MDECaEio+6f0KhYXtPJVxNA2Ag5RrLWxGcT9/+lpfWbA5ndfb53718cvkNr5NZ74r9Tvc/JmnemULUFZ65pQ/7yKFgH5D9YMADvAjNjSgMrVPNBdCihWu3ohGKkpgd/Gsxa6w1WtJA8pFaB5qbEU9k9N72c+fP/e/90D6VpA8dEWwGHCD9nFwTKGgaEdpiKl//uUZNWMA9pJSBupHTRFYvo8YYMdlCkvJG7hi9FzJaf1gwo1aU1sT9jn9/f/7vOiXVTvnkDiyT0AAAAcxfHw6Ze8yjAEwbA0UAJBoICRWMwKAEYHMy1C4ZAFQ6fXoYCgDBqRzRkFbFpCap9iKPqrlAjB8DGzxSVUc+kNaHoEpWMPVfbXl3iqrNU5XH9upJPUhOOTGYW9jJTZM9tZWEjdswnKu8dVymgLPk7LVw6kPEdnKLAlcnma+5leH2D6Vc1O19g/1E0Q04Xj3snHfHiOgyJBuJolgzNPcUrx44GPLI7viYJdRUflMT57D3siyG52hJ2KSpRVWqiFDzULm90No+OGvNnZOnK372X/Un/b9P/23SzVlHoOUc6zNTeDflGDF0RzGINjBQGTA4GogMgyIgDERDmxoQDhBTEXORVU++kGNXlD6uiz51IbbKvFQWMFkh0KhCPoPoYdcqTioQnSJR0OLXamXVW0ynrr8M/pLXRlj58p+9GZsda3tfH51mjD//uUZKQPBK1LyJusNaRL6Wn9YYUfDwT5Ig7lCUkUh+axt5hglWV8mZa6nTQo9TKVIgH986+IBJEBIqqrDFq4BMySbhInUySQYnwT8FxUjNi9jY461u+/kwEVc8WUWB8HHPbfIDFAYasv9hDnp99VatHKU9SmlCQoWCepAgABj3YRlsCJjwH4YPCRICDJCSX2VWCgZkRQs7C+RC2H4RTwnWFoYaFEzD2JioFaa8+Vm8GFCEMUB4DszdRkDgCqMwMcJfpxSuUKlfHljrO8OQKxDy5RHL4X2UrJtWLmVlMvpGXkZPdforeyaAAAqBBP5Yb/Q94XFh1O2tTSX8RKEwOCXPN2g0XJNGBtblWCR9ISbDD32AFZA+XVFHW2KT/+jq7Km8WaXoBAAAPM8ZjN6DcMCgvAQVCMAi8Q6BSe4BABjYkRqdbB6dNN9mzzjvQ7OthLPTSDw0AEdhMxVAGpJ+zji0iDYUhw7LhjuetRGnrKwxp9VWRtapsZtSu5Vm/Sb2Uq//uUZIKIA2ZMSauvGtA8wZmsZegIDb05Jm6YVFEIi6n1hg1OUZOvdslWZGRnHVkZHgrAxo/i4PjltUkkbSQR1nOWgd1k8FzFDBakqE8OiwK0iJxxvyUlTBAbZWlLY2NGNWMGuVPmWxele+pm4mWr5Xooq97dFyGY9QCMC7KM5hPMEAoL1CoEAoBDAwDX7MU8CjAMlWGRLIbjOQzOSB33KTlTGUIDCVcxbKio3Sg69alFCpHu4G6KQIKCDS4cHTY7GFDmmZ/EVJAuZ8BDQuYIiOf/cEPFSy1jQQUfHiahNwu0W+otGlxSyRptxMJADYSY3DtOGszUMEg6GK9yRAkHJ9GnXhtiVBsksJCyliFToHYgo6pH3tHCx8n/8veb+ru+kio6QEAAE86ZaTKizDlCCgCUBIt4BQEAgoHC8qhRCFOBuKuIc5Djbw9dn4myVUiywPEjAbRK43p2PUDvCGUNb0wZhhiNnC1ypp04j3y+R9VhNOr5f+3z5/O0jcBQPJlU//uUZIOIA180yQu5GfA+AZptQeYJjbkVJu4kclDeDOo08wi2Nn0dM+BIsGJxogRadCL/+LqUpS6RyRtJAyxH1aPAMI4fRd56HJKIjixxaTWRMzb27bTH+kfXFU02xeyTUhNbL/oGgekZvu3VKiQAgxdjjTBSCwPEABRDAwULQp4LuGAUCgm1FqsnhmP08Jnrd2ONxTlDAOoXEMpqSEqK01mc1IlBWfHKWEA/ck6sKu1FRpsvM14y+cvzkt+0v/Omg6NXULC8XXBc/2f+tm24RMm0ASBbQJXHEoBJjuCyiCMmNsYyZMaSg02H8zeJ6htltPZaO1xZrm3Jofeko2npY/+XNrmUgEIVVYEHCZ4GA4LKwdhceTYSGBmFaKJwSMBkpI7syMDdVvDkV08fGVKxECpkrjozBwKIDAlEni/Q+Gqm3NF7Ka1jU1Kkdb6h+XKCFXOfPYlEFv5cvyw/rlaIsnBjgQPASSRAgGQBBLBUdENAYPD4pqUIUrT1XWxtFhCX//uUZImEAyc0ShOGHTI0YSoNYSYBC4EZLs28Z5DjiKZowJnAbIy0bQ9hmOGBRtZhxlzB241+xn///ywQqAUS1QEAQDgHsRukTsuhJCFlvoxNQfZOJUTPEtJmPy3VWAKG2+DK12o5kQAjW71YKj3yTRDeXqXbFXSOS5jp5lenBsvy1vvNc7orXV0TNeWpVyk9V3TRNXtboqKqJ3bFyZpbeJZK4HALkFCUgZqLhLSDwM0OhJWtKuWVzhNSQ5lIDraKG6XxUAQ0WDzWP9rT2gAf/YTwAEOhKBQAKABASw6oijUpUJTm4QdQYAkqlMcphqAE6UZiI1OKhdJOZifIUxBQSSvcJ5r9XkOkEqSbsQgubrsT1izb1PjHemXdMBSUvu3/uy29r/L938Vkyjyny4fi7gEACNtRiCiokRc8ncVNNwWuKnnmgOG+42xnTTpbAqBZCHtY9lbFVEBjxIWTkHLHGwhc2NrBB4r5SrU2kfXcji93+0uqSgAAQAOftZ5lMAJh//uUZKAAAuJLyjOGLTA6wkoNMCODCyRHKS5h4IkMCqUVhJWQqNmBAAVQMDhjJ0mFBC5CUxtE6ep903M7jMkWY5AJw31BhvXF8Ys4zdcNRKn5UiyEyKKPOFoaMFAV3Guuct73TteZUMdKMWG0ECcyLKnDtQtQt13//5sDAiJQccTgDdR5QJsyAo6uPvZWIlDGQvbmkHhhg4ys+MSErmpYyN/2xV6da9n2aaXP3tsSxvnlUAAJh/EGdSjhggNAYBAUBr3qOJ0lvxINYeijJO3bGxSoYrEPPEdwdztXp1gbZZcR+ryYErKOqrhRRyqcB6E5JfF4IOakX823xgilD73MkIX9yTH0lQ+W6m4qaxMdB82LLHb23JG04EpsrmctHxiBASE21owXTlE+tN0lqrM9Ag4cEDosbA5ZFHFrD9DKPq6oesoMkGXIYyJypOoATQAWmm6DXsWAg2MIgNiLMUijPQSXNE4oRDlCpWF2n2ZvY5nJmNwILUZUfV4dQHwtSYBY//uUZLQAAxAwSbuPGtQ34Zl2aekCC8jXJE28a0Dshap09gB2oY/ZVPyIla8ZtH2IjuXMbZi5URygR+4XWHPzGo9b+zl6QbeYXd3//R/QYHEeHQPGEeygWp90mAP2Cp6ZE9etK52/HRKUTAD2rTkJmx8s9xqKEENA6wXbnhUWHGCwaft3oZ2gSaTooR3qfDqABkAupbD4x5MoAweAqvkDkuCABPA19Thp7+iaDEkSgrh3oScb2XCDMGTSqUeXu1lfITnJ3XdP/45f9qwmGGtvRHOkHQoZnrFUpUUmI/JG2CU4+0QHEDhcCEkdZcjkkbKYAWXZJkfUOrMqB8mTIYrCyph+rUQFSiO6ba+Lpl5c4sAxx0ql6FoUolkQ4cCjmptTlXJ/qxdZKfXRMLFaAoIx4uKGYCAAgCLANi66kt2bNZYe/LXoaZZHQ24RptnBxY0iUEyEJ0J2JwvYxm/MgczjkagSBw8CRZIGQs2qhdFD5QAWD9tBQZIh6RLhiLhMk8MJ//uUZMgAAuwry9OPGWJCAylVaYM2CuiHLy4kysETCae9h6A0i1L01h4QgLKmGEgb6Bm9dx2m3TSZBZeBYeCxKRXI24cITNrzA4QlwbUs0DojPmR7V3ptF2iX3sUbacQvb//OGXfucQRhlDgcIgcSHRDklYEWBjlMVYHoT5WHahVbBgViZN5d6kNceBkPLmWVNWl6+SyGeo53T9IDgOuAUqmPeRYRER1oFWgg2WJ0hNwswNEYq1a03LkTf3B1oEywuqckrLYK5rRgUYJOojw0hz2uciBnNBJMjkEMBQiNDYwo1ohYUTEiShNI87KtwC6vW2z0rcGu2KtklfzY2jeEdN/KkcCJgoDF7zAgiMTgtcRe4RBhSZWAFBywBXyZJGcWlPLBkRch4y6tC8r6siimnEjNNc+LnZINpL6ZpAOTRP50hZ9omW1IoTDiFQweoIujook4JMxwUzDv4tTSVldiZ4h0gEkveiULXqnNWygM89xuz8MRYu3uZjbZA1HoAqnJ//uUZNgIAr8cSiuJG0A+wZl5YMNGCthvJi5hIUD8hef0x4yUPqlDTzR75UOJ+CDfJlJFaZNvPOF6t3d/0MpE4feEzIoECq2SJVgP8UwhiJhyt9AvcoIB56mJXbQTpCAEHD/LoiGxIfL2hABIk+UnCzDJWfONFmRszikre5IGTBODCEBi13NAoGXmL95kn1Vrsg5WLzIWdZCsr0Yzslu1Trr0rKtyaq3T0Rkk83177oXfMoK8aCGYgKRKRUB2DA0GX0RDXGgl9l9ln1TCxEGCMOro5pzOnWpOxNjaAdVWFiUR7DeWmRwy9u/YbXscoefvM1sqd3uAvQSTcnou7557wKREVVU/TcQcTjFAKMAAAVCDWwwTtQFAKnOYEAC9CQAPJF1br1LKZXAcJrrrjS63rzIahZy89bdavV7l7rNDmKJ70XmyhRg2MYwHlgK+llzzY3Z0Ht1L5UzU0hHK4Q96MRls837W6aVmIbUKBLIFJFKA5ZWyr/ctdrzEyAEQRRMs//uUZO+EA85IR4OJHaJDAslBYeYKCvUnKM2kTwk3F+WppI0oKJUjfMbBjIYGoZXRaoiOVmR30MSdrEM7Orh32I+pJ/8iddfrrrRGpr3kfr5+tn7qqPujsHNH6VAEAAc+pDgSHAxCapJYOstop+upRdTcOQLIahSzFmFszkcJoCNBpF5gWhUJaqE8UUZXfDSu0atk12a3vE0J4ZcBhY6NMPhYHHAoeOAiTBAILArwKkPlTJdBpTGvKE3iE+8gn//9AFiJFXVh1Qz9IpLtk92SOPQO5EsBIwKpdlWbL6uFU9fLUox97bkVLyaGaalSwoyXYkAvDHUKIyVS76lJ2Mf+Rjcel51KIAAAAAOcFquYFgqtMfGFjP0PEctkyJSlESWpCWukgOAcLg0JAqDJYAQSWHRXSrUFn+1bMT3X1dKW68Uxl+0jjB5YMQkYGCQ+FcPnEKAh0WCIs5ZoIiAcArCjJ2KF2MTd//+bBEJUOJRZbbHIBDFoJgSCJhhmEJxu11YC//uUZOwGAzI2SAOMHLJNaWl6ZSIqDJhvI05hgdEREOYlgw1QLSNJRFbvXMuTJLgjhY0BQTLSo0kyvWTBIjc8KI7jjlhfxMhaafeLItHgNh9FNASGI4x4H/AqqNYZ9ba1lhiyBIBg8haY6k629HMDxQJCjgAUosRIIkUlVJ58jOi0/hl2ybEpyMRyWrbln9hkb/kl8yPPTelscOZm4euWJhw0+cC+YctqgEUkAIKdKXDwZMCBBGGRRL9sQUGURbYPWnCqYNHxkWjtzGGDzGVDjk0tIMpaMwTmbdbHo+iEMXadOnTsqwiD0HqSZI3YTVUesfQExhgzFBYYxpXU+BoGABHNvaa2ApwGoumtGBhChFItHcv+rpQRd8sZ+1+EO0CyKQrFI+RkoqSSSAoSTekmgyrlj1nXVVK9dWzkjlS2VOc+k74ey8I0gzbtfq2oTHVVne2jjyCzAjr7Uv2ITDzzD3wMsCQubu8kHqaO4ztdF6BUwGx0QdgMCJD2xOoLKKpo//uUZOuEAxgfSTu4SFRHQsoPMSNHCqDrKKwYb1FWD+Shlg1gxwQgMgEXAs5XkRqR5vxL2GE1g09wKucaJMteZdOAJkEFvWtZ66acwLmIyRs4kYQAigqACYLApmUGA0tdO1hRfNHSHn3ZK7ihDSm7P08UNyKHHpibNYrIIAAkwgwmmLuCiGB7ugg0wtwZgBCwOKkyDQUzQ8QoGL67JZjMaFEEkvgamdLRjMUYU8SZXwCGqpvphJJhACJhpVLVFC7KKDW5cvNwFviyFsxUjSsr2MwjePAzEJKriyPiJCkepwi2ZELZ0Nd7SrRq2QGRfp3zWqi0JYy3ctN9dn+bp6bKIMCHL+fXKQCX8dboEAR54r1saczDVMXdTnbV3QFWQxKJB+jXxnT67brH2eLoMlKakWZ/U/emCYrNxSqcEnAIRZnWGvwuhiFFD8vBZWER5YPPDZNQwm5AaslWLKOjpMAAqAoMzVgLAA3VJaPJ2VGk6OhYai4nwhQI95wwokzdbNAG//uUZO8Cgv4iSKuZSGZK5HkgaSNWDTzxHg3kZck9pCTZhgkg3mquGUwJAIgkKmQRU174lGFHREprQufC1ALRMxpdGs0hqRq2lglQLUo9hGVO61MyoKjAQzMCCZJdToxCAAqCk4ACDx0FNxL4qYLSXZuXsOj7tPzG2wOLFm7zTSXsYI+sf1ynex0JvYKlV1BpHKDM6IFJxpRCu1FVJDmgIhKyR3WcA1keYMWHFC1QukWLQchYNuuEeUE+hDxPq/lYfzPLpwWfvQXMhZ1WyxyRtJCNlZUou5vdjRxf58gGVxEjb6nWMTuVM9P84JtaM+oI7CQvUrqC3tIgWwFDMHViik9MdDClGefPXdZGpDCX7QCNtkTLVUoRAqFNCJQICAskGaAqhqgRAmYr6fNQmxcUyhL+GwOx9l0CJnP86E8tOTfEmRQRUgp/yO9RKGYvED9Sxti3AYIcWJZ695V+4xdPLPLfW+1n/7MvTYWUh0TGMhKComvF0O+C0uVlLI4r+lhc//uUZOsIArowyqtMGvRPAql8PMNkDx0jHg4kdsEXkin09I0mkokkvELEAu6rWXJWNg/lWXBwT3RaWNFk9qWVLIx7UKJ96NVraFuGXp08j0X///bv0r//9Osx0BKdcchosQAAHllx0NFUHBkQWxaAOelQhWqRhTdlFXugWISKJyZuq5FFEvWBXXupDCRskglC0zGQHJKcg/JGlOfSo1k4lyrEoQcjCpEX8drb7ZdM2zEbjPlZ9S0U2xpg5XaDOsR9dJE8moC/////FLtlmgdcjaTEfDIcxG4w1BGS0FAK7xMOAvUMtfz/QruCad+0qn36pGyKg6toHFnNaoumx6EVMV1215GrYHFiQclNitw6kMqmGDgoBgYSCCcw0DasDgxCtjsnbipOla64LyQ673X0tw+ioAUFz0a4jNBV5KXl0W2HRr3jC1BrAZNCMEDjrCKhEHB/Uz6pBDHMtI+vglXQ7f+/De+CveoZbd/1N6QACQBEVgTSwwOG3V2pfAjauAy4//uUZOaOA0s0yItvMfA/yTpMYMJljbTNIA5gxckMjqo09gimTCYVBEChIbQwtJ7F0MzZW3ynFeI40OJ16RnMGRDqEkVooniQqLmw/e283SQiR1BxE1gdXPnSyT1QoTcdIlGRjJlQq7zMwgeIR4DLaC5kOHGL4AYiOSbUHJ7wA6zcJbHmyjonlBJWnQm5AagGLSCWRGIBkyRPQKx7nkJ1RrDhJZSDlIgXGrFQppI6FFmhZV3GfXHcdr8rFxXH4w4yX1MCusJ41/0o518hwDgEVaD+dKdxHPR5a+mHVMBxCfjTriwSz0PDW1nBkP3R7RCDgOpnGiixToScAAxdj3F9Jz9dWc4j31xilqa9NQAAABzYF7NqmNkIQE0LExiboFwXkByjMBA9GVFhkDU5a9yXwyGduQC0oomHGTWc2vEAfBQNKF2GgrQseCk8/scQ46UKNVeS1HbPBUi1io/T4Neu3R6O1l1dEjm/Z46v7ipGigZi4tMWpMB1ZcPi87//7gta//uUZOePAwAqyQNsHKJOI4lZYSNKDYzlIA3hB4j/hmXlhhiglnNbtJJAUqqaFYu2p0ZR+rJGvCAGDlua5m2LWxIp/pqblBNswDkBMKlsUBC+XKeo3PXx5p6FugIXIDWgVyWObdfqPMNh4ERgOFxoxiIEKSyhQE0ng5D7EkVoBIT8OxIFcdSuYU6+TGzRF3BwnIoKPXMATqqmHx9RB5BMBhgxXQuzMyEJPQzMMc75GLbTpvVuzpW5upEyHdZmGWuX7u+7VgcGQrKWGtDrJLKgWJKBJcA+AkTlrmpA4FhbeLAoMRH9o04EiQMEmOCoa2kUCBhJUXZY6577XTf+6Xuax/dsIkYvQxiFAAAAHPLIMwaDTCACSXMJB8EAk7LUDa2FUgkoRNWNSxzGPPI1l2n7lddQUQFQSQUt2IU9ccEhxgmVs32UjeeZZqL2FcPUPp7kmqsjbvPJvs0ZGrD9vtd36sW/mXXd91Vz3HbWxU5w85LmiBLZQXk///6BKpJmWzIm//uUZOgMA5A4yBuYQXRHgyoNPMNLC0zzKE48pckOhuXlhhiglAXk4SBHia5YXkyQQ0/Uc8a1uG1bKrDsCHkl62/a8ALDoJGLHLgso4+hUANIk4Pevboe92NFjqra0qKKWNKNVFXCN5uzaVRmAqIgYkRsCA4LhiD6mNwLgnRe2AXRnZS9H2esU0ENApCxukNTK4OdV5peGFskQsFRMhgTGJ6749J5R5OUemltJuW+fAydafroAU/nqC68/Efc/nuiLs5+/vVhywQmwFM1gQ+QTAwg5OLFsQA4aOxzEA2TrWzlt7OiPzG6r4qYmlDGCQKLK25ACvhAYGydtdVMnvI30r27Si7kYg7AmgCH0Y0MCMnDi240FlzlgS84IBAQHpMl90HYHdpr9LGYHgSJU8QQYLwotOtJ9vqrInUaT1FqNmmpVQB2E0PtaZtDkPf9YaU2HLskOTdT8gbViSqXoSUzX91/Mumf8L6ve7GykhByoe9zjegABh0QBuwwCCL9SVX8//uUZOoOA4w5yBuYWXRH40ndPGJ1DDiRIi28yZkGC+Xk9hR41tB9QgRAxZB9lWn2NLwdO3rydIC6DBHG0qvK5Chri8JDjobg3oEIEURmPXh0/zYjMpC145fT4dLiqXl6X3/7T1/uRQ8u9pfq55kDFJbtYgEDKrhyJAcNamyNfysiJfUgkgHQsvzC54vRqW07j5kmDofI2a5RdM22K3W76YsgAgdTWErnKD0zu3ya6tCr0Ih19+ect8sjJ9p7UZut01r3/dJQkND8omIxVVoOKovSXXU6gJshRG8QlF8vVArCmEI9WrtSVXHReLHOnsABTrP+zBqC3Bi3wfjydJ2JTL3L5nfM6WK2OT9n67vmzUseLUAEimVmf3T0ae12AAGAhaL+B6eWXZeDBAkEMKEX4zNxC7JbyQlmhqvUDKmmlIpBkGGOjtb7PI9OvruUgXm4wC9uawEuaLLaWGN0Zr4A2uGM/Uok1x9ixO6dMrYx5/bIv3fl32yhB41+s3PSQSCE//uUROmEA01HyAtpHTZcSMkiaeNKSvEhLS0wrdFJlSXZlgzz8D4xGQ5TCKxdjx0M0M/aqyeDCu7K5iotrrukcOG1shJ8xpvYtraCL2zk5/6qVLLmaGUAc6B4FITEpAuWABgEjDBgTAEwEFkotEu+NIdOAIcQheljzs1Z1v4Hf1SLfx+1GpfEa+NqoKEBcDjMO8Aqk3IkxpIRObKP+Njo7s7csbG9HZfnIU+eYau8+1ajk/6u0MJxhIyYidSZOfKi70dZVRTUTSYUqUVM7HAay61C87NYFKJJa2NJsc0QExq/XdkBkol4NbMlWM8GABHOH9IkWoyARzjpCfBtwtJ2UJcZSLcYl4lDyS9YfjkJsM2Eww0FEImTmHx8DiWn2RBQBAsqgNynvTqTMZU/T+RmPUj2xR8k75W7UetzZ4EEGwekegW7u1WXeGxJAvUlETHTz7BSv22CbpTSr0fp+dImQ1zmefMvy6UTXPnT6bXcGRvlrUIpCIKBBzINYRPZ41FY//uUZN+OAtEdSRNPMsI8Y1oNMEZ5DRzZHg1gx8Etjuc1hIkcIgHofhiWFBCdeJRUW3bWVas9Ew32yliK6GKLelhllTcFoUSwJiFTFON6VdbX77d11X72tIoA80fMkjSMAMiIAAVncJgcF0At7FVgVYAwziA4SgsBUUCP+/VK8b9UKwBgNTQEQdFic2IFXQwQlcu1MsR1QiBYUq6IKrhr1SEmZi66iFpDOv7G33Fz181xjpkhZeNFOx1zXIcWq3CMzhUumikMu15IEVWfhoEFRPcbrRjngtWEd4xyGd3hBZXKQg5GWxXho5Yr9OPPKjMA3WvgO5z9hR9VTXsV9aggMAiQUWHaBAAQACedAzGvOY0arJASKuoFEokBOUWuVTZcFA0MAE55AkTLm4qW2HZuuEsMIZEqELEJmUS4tTSpmrlljQwKjClsQaNWrW0tR6VZzD0gViepV61n4morruYJjqYmLW+LnW67KLGdSLCz++krNr5KKLX35dCES/yYYwfN//uUZOgPAzFDyAOGHTJGIvlZYYMuDEzdIg3hA8EXkyVJhg0QVvLTK1WC2HcxthVtjYrztaYbv7rmBcoh2niMnDJE5oUph/ERhJw4TRCMJswozE2I++rJa/7zWcxvm/1/6e7MY6weJnBkksjkbiaR4nwN4dT4Y0U0hkgQTKzYfJwH2hqmfgaCVORwkutNR7EnaTUfVeoUToYxnR5hQNpQrBhWpf7rmN8xaAThRKSo7J/CiV1FSwVOuEpZYU/b/WdyvDWoIGb6bi6OsIFYGwDhKjrBcBINh2uST2zMS7Fy6M5MYpmrufkIwqYcTCieFCFycT/94wEQNHqiwNZ49Ugl//yNTEFNRTMuOTkuNVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV//uUZOuAA+RLx7tpRKRCCVmGPCJ+y2jjM6eYTzD7lKU0wI6kVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVMQU1FMy45OS41VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV//uUZFGP8AAAaQAAAAgAAA0gAAABAAABpAAAACAAADSAAAAEVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV';

            self.eventHandlers = {};
            self.verto = {};
            self.call = null;
            self.socket = null;
            self.initCompletionPromise = null;
        }   

        init () {
            let self = this;

            if (!self.initCompletionPromise) self.initCompletionPromise = new Promise(function(resolve, reject){
                if (self.user) return resolve();

                self.api = new CliqueAPI({ baseURL: self.baseURL, sessionToken: self.sessionToken });

                self.api.init()
                    .then( () => {
                        self.user = self.api.user;
                        resolve();
                    } )
                    .catch( (error) => {
                        return reject({code:'http-error', data: error});    
                    } );
                
            });

            return self.initCompletionPromise;
        }

        on (event_name, event_handler) {
            let self = this;
            if (typeof event_name !== 'string' || typeof event_handler !== 'function') throw new Error('Invalid function params');

            if (!self.eventHandlers[event_name]) self.eventHandlers[event_name] = [];
            self.eventHandlers[event_name].push(event_handler);
        }

        off (event_name) {
            let self = this;
            self.eventHandlers[event_name] = null;
        }

        socketInit_ (p2pSessionId) {
            let self = this;

            if (!self.socket) {
                let query = 'token=' + self.sessionToken;
                if (p2pSessionId) query = query + '&p2pSessionId=' + p2pSessionId;
                self.socket = io.connect(self.websocketEndpoint, { query: query });
                self.handleEvents_();
            }
        }

        socketSubscribe_ (id) {
            let self = this;
            self.socket.emit('join', id);
        }

        socketClose_ () {
            let self = this;
            self.socket.close();
            self.socket = null;
        }

        trigger_ (event_name, event_data) {
            let self = this;
            if (!self.eventHandlers[event_name]) return;

            self.eventHandlers[event_name].forEach( (event_handler) => {
                event_handler(event_data);
            });
        }

        cleanup_ () {

        }
 
     }

    /*
    
    CliqueBase is a base class for all voice-related objects. Extend it if you need voice functions.

    We do not expose it to users as an independent entity. However, keep noted, that its methods will be
    visible to all its children.

    */

    class CliqueBase extends CliqueAbstract {
        handleEvents_ () {

        }

        audioElementCreate_ (id) {
            function guid() {
                function s4() {
                    return Math.floor((1 + Math.random()) * 0x10000)
                        .toString(16)
                        .substring(1);
                }
                return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
            }
            var audioElement = document.createElement('audio');
            if (!id) id = guid();
            audioElement.id = id;
            audioElement.autoplay = true;
            audioElement.style.display = 'none';
            document.body.appendChild(audioElement);

            return id;
        }
       
        localVolume (volume) {
            var v = document.getElementById(this.number || this.conference.id);
            if (!v) throw new Error('Audio element not found.');

            if (volume) v.volume = volume/100;

            return v.volume*100;
        }

        initUA_ (node_domain) {
            let self = this;
            return new Promise( function(resolve, reject) {

                var vertoCallbacks = {
                    onWSLogin: onWSLogin,
                    onWSClose: onWSClose,
                    onDialogState: onDialogState
                };

                const confId = self.conference ? self.conference.id : null;

                let audio_id = self.audioElementCreate_(confId);

                self.socketInit_();

                self.verto = new Verto({
                    socketUrl: 'wss://' + node_domain + ':' + self.wssPort,
                    login: self.user.uuid,
                    passwd: self.user.sip_pwd,
                    iceServers: self.iceServers,
                    tag: audio_id,
                    keepalive: 1000 * 60 * 3, // 3 mins
                }, vertoCallbacks);

                self.trackVertoStats_(self.verto);

                function onWSLogin(verto, success) {
                    success ? resolve(self.verto) : reject('Signaling connection failed');
                }

                function onWSClose(verto, success) {
                }

                function onDialogState(d) {

                    self.vertoDialogState_(d);
 
                }
                
            }); // end of promise
        }

        vertoDialogState_(d) {

        }

        isNetworkConnectionLost_(data) {
            let connectionLost = false;
            if (!self.webrtcStats) {
                self.webrtcStats = {
                    audio: {
                        packetsSent: 0,
                        sentLost: 0,
                        packetsReceived: 0,
                        receivedLost: 0,
                    },
                    video: {
                        packetsSent: 0,
                        sentLost: 0,
                        packetsReceived: 0,
                        receivedLost: 0,
                    },
                };
            }
            if (data.packetsSent) {
                const sentPacketsDelta = Math.abs((+data.packetsSent || 0) - self.webrtcStats[data.mediaType].packetsSent);
                const sentLostDelta = Math.abs((+data.packetsLost || 0) - self.webrtcStats[data.mediaType].sentLost);
                connectionLost = Math.abs((sentPacketsDelta - sentLostDelta) / sentPacketsDelta) <= 0.7;
                self.webrtcStats[data.mediaType].packetsSent = +data.packetsSent || 0;
                self.webrtcStats[data.mediaType].sentLost = +data.packetsLost || 0;
            }
            if (data.packetsReceived) {
                const receivedPacketsDelta = Math.abs((+data.packetsReceived || 0) - self.webrtcStats[data.mediaType].packetsReceived);
                const receivedLostDelta = Math.abs((+data.packetsLost || 0) - self.webrtcStats[data.mediaType].receivedLost);
                connectionLost = Math.abs((receivedPacketsDelta - receivedLostDelta) / receivedPacketsDelta) <= 0.7;
                self.webrtcStats[data.mediaType].packetsReceived = +data.packetsReceived || 0;
                self.webrtcStats[data.mediaType].receivedLost = +data.packetsLost || 0;
            }

            return connectionLost;
        }

        sendNetworkConnectionEvent_(status) {
            if (!this.networkStatusTimestamp) this.networkStatusTimestamp = {};
            if (this.networkStatusTimestamp[status] && (this.networkStatusTimestamp[status] - Date.now()) < 15000) {
                this.trigger_('network-status', { status: status });
            }
            this.networkStatusTimestamp[status] = Date.now();

        }

        trackVertoStats_ (verto, options = {}) {
            let self = this;
            const repeatInterval = options.repeatInterval || 3000;
            self.removeVertoStats_();
            if (!verto.dialogs) throw { code: 'not-supported', data: 'WebRTC stats is not supported' }


            self.repeatVertoStatsInterval = setInterval(() => {
                Object.keys(verto.dialogs).forEach(dialogId => {
                    if (!verto.dialogs[dialogId].rtc.peer) return;
                    self.getPeerStats_(verto.dialogs[dialogId].rtc.peer.peer)
                    .catch(err => console.log('WebRTC stats', err));
                });
            }, repeatInterval);
        }

        trackPeerjsStats_ (peer, options = {}) {
            let self = this;
            const repeatInterval = options.repeatInterval || 3000;
            self.removePeerjsStats_();
            if (!peer.connections) throw { code: 'not-supported', data: 'WebRTC stats is not supported' }

            self.repeatPeerjsStatsInterval = setInterval(() => {
                Object.keys(peer.connections).forEach(connection => {
                    const mediaConnection = peer.connections[connection][1];
                    if (!mediaConnection || mediaConnection && !mediaConnection.pc) return self.removePeerjsStats_();
                    self.getPeerStats_(mediaConnection.pc)
                    .catch(err => console.log('WebRTC stats', err));
                });
            }, repeatInterval);
        }

        removePeerjsStats_ () {
            if (this.repeatPeerjsStatsInterval) clearInterval(this.repeatPeerjsStatsInterval);
        }

        removeVertoStats_ () {
            if (this.repeatVertoStatsInterval) clearInterval(this.repeatVertoStatsInterval);
        }

        getPeerStats_ (peer) {
            let self = this;
            if (!peer.getStats) return Promise.reject({ code: 'not-supported', data: 'WebRTC stats is not supported' });


            if (!!navigator.mozGetUserMedia) {
                return peer.getStats()
                .then(stats => {
                    stats.forEach(result => {
                        if (result.type === 'outbound-rtp' || result.type === 'inbound-rtp') {
                            if (self.isNetworkConnectionLost_(result)) self.sendNetworkConnectionEvent_('packets_lost');
                        }
                    });
                    return Promise.resolve({
                        success: true,
                    });
                });
            } else {
                return new Promise((resolve, reject) => {
                    peer.getStats(stats => {
                        const statsList = stats.result();
                        let ssrcStat = statsList.find(stat => (stat.type === 'ssrc'));
                        if (ssrcStat) {
                            var ssrc = {};
                            ssrcStat.names().forEach(name => {
                                ssrc[name] = ssrcStat.stat(name);
                            });
                            ssrc.id = ssrcStat.id;
                            ssrc.type = ssrcStat.type;
                            ssrc.timestamp = ssrcStat.timestamp;

                            if (self.isNetworkConnectionLost_(ssrc)) {
                                self.sendNetworkConnectionEvent_('packets_lost');
                            }
                        }
                        resolve({
                            success: true,
                        });
                    });
                });
            }
        }


    }

    /*

    CliqueConference is the high-level object represents a Clique Conference.

    */

    class CliqueConference extends CliqueBase {

        /* Public Interface */

        id () {
            let self = this;
            if (!self.conference) throw {code: 'not-connected', data: 'Instance is not connected to the conference'}
            return self.conference.id;
        }
        
        create (options) {
            let self = this;
            return self.actionRun_('create', options).catch((data)=>{
                if (self.debug) console.error(data);
                throw {code:'api-error', data: data}
            });
        }
        
        join (id) {
            let self = this;
            return self.actionRun_('join', id).catch((data)=>{
                if (self.debug) console.error(data);
                throw {code:'api-error', data: data}
            });
        }

        leave () {
            let self = this;
            return new Promise((resolve, reject) => {
                if (!self.conference) return reject({code: 'not-connected', data: 'Instance is not connected to the conference'})
                
                if (self.conference.ss_calls) {
                    for (var call in self.conference.ss_calls){
                        self.conference.ss_calls[call].close();
                    }
                }
                
                if (self.isP2P){
                    for (var call in self.conference.calls){
                        var id = self.conference.calls[call].audio_id;
                        if (self.conference.calls[call]) self.conference.calls[call].close();
                        var audio_el = document.getElementById(id);
                        if (audio_el) document.body.removeChild(audio_el);
                    }
                    self.p2pUnbindMicManager_();
                } else {
                    self.call && self.call.hangup();    
                    var audio_el = document.getElementById(self.conference.id);
                    if (audio_el) document.body.removeChild(audio_el);
                }

                self.cleanup_();
                resolve();
            });
        }

        users() {
            let self = this;
            if (!self.conference) throw {code: 'not-connected', data: 'Instance is not connected to the conference'}
            return self.conference.users || [];
        }

        checkModerator() {
            let self = this;
            if (!self.conference) throw {code: 'not-connected', data: 'Instance is not connected to the conference'}
            if (!self.conference.users) return [];

            return Object.keys(self.conference.users).filter(uuid => (self.conference.users[uuid].meta.is_moderator));
        }

        mohPlay() {
            let self = this;
            if (!self.conference) throw {code: 'not-connected', data: 'Instance is not connected to the conference'}

            const audioFile = self.musicOnHoldUrl;
            self.conference.audioEl = document.getElementById(self.conference.id) || document.getElementById(self.audioElementCreate_(self.conference.id));
            self.conference.audioEl.setAttribute('src', audioFile);
            self.conference.audioEl.loop = true;
            self.conference.audioEl.play();
        }

        mohStop() {
            let self = this;
            if (!self.conference) throw {code: 'not-connected', data: 'Instance is not connected to the conference'}

            if (self.conference.audioEl) {
                self.conference.audioEl.loop = false;
                self.conference.audioEl.pause();
            }
        }

        invite (data, options) {
            let self = this;
            if (!self.conference) throw {code: 'not-connected', data: 'Instance is not connected to the conference'}

            return self.init()
            .then(function() {
                return self.api.conferenceSettings(self.conference.id);
            })
            .then(function(conferenceSettings) {
                if (!conferenceSettings) return Promise.reject('Incorrect conference id');

                return self.prepareInvitation_(conferenceSettings, data, options);
            })
            .then(function(inviteData) {
                return self.api.conferenceInvite(self.conference.id, inviteData);
            });
        }

        lock () {
            let self = this;
            if (!self.conference) throw {code: 'not-connected', data: 'Instance is not connected to the conference'}

            if (self.isP2P && self.conference.creator_uuid === self.user.uuid){
                var lockEvent = {
                    Action: 'lock',
                    Room: self.conference.id,
                    'Caller-Caller-ID-Number': self.user.uuid,
                    User: self.user,
                    Time: (new Date()).toISOString(),
                };
                self.socket.emit('inject', lockEvent);
            }
            self.socket.emit('lock', self.conference.id);
        }

        setGrace (period) {
            let self = this;
            if (!self.conference) throw {code: 'not-connected', data: 'Instance is not connected to the conference'}
            self.socket.emit('setgrace', period);
        }

        unlock () {
            let self = this;
            if (!self.conference) throw {code: 'not-connected', data: 'Instance is not connected to the conference'}

            if (self.isP2P && self.conference.creator_uuid === self.user.uuid){
                var lockEvent = {
                    Action: 'unlock',
                    Room: self.conference.id,
                    'Caller-Caller-ID-Number': self.user.uuid,
                    User: self.user,
                    Time: (new Date()).toISOString(),
                };
                self.socket.emit('inject', lockEvent);
            }
            self.socket.emit('unlock', this.conference.id);
        }

        userMute (uuid) {
            let self = this;
            if (!self.conference) throw { code: 'not-connected', data: 'Instance is not connected to the conference' }
            self.socket.emit('mute-local', uuid);
        }

        userUnmute (uuid) {
            let self = this;
            if (!self.conference) throw { code: 'not-connected', data: 'Instance is not connected to the conference' }
            self.socket.emit('unmute-local', uuid);
        }

        muteUser (uuid) {
            console.warn("muteUser is deprecated, please use userMute instead");
            this.userMute(uuid);
        }

        unmuteUser (uuid) {
            console.warn("unmuteUser is deprecated, please use userUnmute instead");
            this.userUnmute(uuid);
        }


        recordingStart (options) {
            let self = this;
            if (!self.conference) throw { code: 'not-connected', data: 'Instance is not connected to the conference' }

            self.socket.emit('recording-start', options);
        }

        recordingStop () {
            let self = this;
            if (!self.conference) throw { code: 'not-connected', data: 'Instance is not connected to the conference' }

            self.socket.emit('recording-stop');
        }

        getRecordingById (recordingId) {
            let self = this;
            if (!self.api) return Promise.reject({ code: 'not-ready', data: 'Instance is not ready' });

            return self.api.recordingById(recordingId)
                .then(recording => ({
                    status: recording.status,
                    conference_session_id: recording.session_id,
                    url: recording.url,
                }));
        }

        getRecordingBySessionId (sessionId) {
            let self = this;
            if (!self.api) return Promise.reject({ code: 'not-ready', data: 'Instance is not ready' });

            return self.api.recordingBySessionId(sessionId)
                .then(recording => ({
                    status: recording.status,
                    conference_session_id: recording.session_id,
                    url: recording.url,
                }));
        }
        
        localMute () {
            let self = this;
            if (!self.conference) throw { code: 'not-connected', data: 'Instance is not connected to the conference' }

            self.socket.emit('mute-local', self.user.uuid);
        }

        localUnmute () {
            let self = this;
            if (!self.conference) throw { code: 'not-connected', data: 'Instance is not connected to the conference' }

            self.socket.emit('unmute-local', self.user.uuid);
        }

        isMuted () {
            let self = this;
            if (!self.conference) throw { code: 'not-connected', data: 'Instance is not connected to the conference' }
            if (self.conference.node_domain === 'p2p') return self.p2pMuted;

            return self.call.getMute();
        }

        screenShareStart (appId, options = {}){
            let self = this;
            if (!self.conference) return Promise.reject({ code: 'not-connected', data: 'Instance is not connected to the conference' });
            if (!window.chrome) return Promise.reject({code:'not-supported', data: 'Browser is not supported'});
            if (self.ssStream) return Promise.reject({code:'in-use', data: 'Screen sharing is active'});

            var chromeMediaSource = 'screen';
            var screen_constraints = {
                mandatory: {
                    chromeMediaSource: chromeMediaSource,
                    maxWidth: screen.width > 1920 ? screen.width : 1920,
                    maxHeight: screen.height > 1080 ? screen.height : 1080
                },
                optional: []
            };

            if (!self.screenShareSource || self.screenShareSource.disconnected) {
                if(self.screenShareSource) self.screenShareSource.destroy();
                self.screenShareSource = self.p2pScreenShareInit_();
                self.screenShareSource.on('error', (err) => {
                    if(self.debug) console.error('PeerJS error occured:', err);
                    self.screenShareSource.destroy();
                    self.screenShareSource = self.p2pScreenShareInit_();
                    self.trackPeerjsStats_(self.screenShareSource);
                });
                self.screenShareSource.on('disconnected', () => {
                    self.screenShareSource.destroy();
                    self.screenShareSource = self.p2pScreenShareInit_();
                    self.trackPeerjsStats_(self.screenShareSource);
                });
                self.trackPeerjsStats_(self.screenShareSource);
                if (self.debug) console.log("Screen Share Broadcast object initialized", self.screenShareSource);
            }

            const request = { action: 'capture_screen', sources: options.sources || ['screen', 'window', 'tab'] };
            return new Promise((resolve, reject) => {
                window.chrome.runtime.sendMessage(appId, request, response => {
                    if (response && response.error) return reject(response.error);
                    if (!response || response && !response.sourceId) return reject('PermissionDeniedError')
                    return resolve(response.sourceId);
                });
            })
            .then(sourceId => {
                return setSource(sourceId);
            })
            .catch(err => Promise.reject({ code:'mediastream', data: err }));

            function setSource(sourceId) {
                return new Promise((resolve, reject)=>{
                    screen_constraints.mandatory.chromeMediaSource = 'desktop';
                    screen_constraints.mandatory.chromeMediaSourceId = sourceId;

                    navigator.getUserMedia({
                        video: screen_constraints,
                        audio: false
                    }, (stream) => {
                        self.ssStream = stream;
                        self.ssStream.getVideoTracks()[0].onended = () => {
                            var currentTime = (new Date()).toISOString();
                            var screenShareStopEvent = {
                                Action: 'screen-share-stop',
                                Room: self.conference.id,
                                'Caller-Caller-ID-Number': self.user.uuid,
                                User: self.user,
                                Time: currentTime,
                            };
                            self.socket.emit('inject', screenShareStopEvent);     
                            self.screenShareSource.destroy();
                            self.ssStream = null;
                            self.screenShareSource = null;
                            self.removePeerjsStats_();
                        };

                        var currentTime = (new Date()).toISOString();
                        var screenShareStartEvent = {
                            Action: 'screen-share-start',
                            Room: self.conference.id,
                            'Caller-Caller-ID-Number': self.user.uuid,
                            User: self.user,
                            Time: currentTime,
                        };
                        self.socket.emit('inject', screenShareStartEvent);   
                        resolve();
                    },
                    (error) => {
                        reject({code: 'screen-share-failed', data: error});
                    });
                });
            }
        }

        screenShareStop (){
            let self = this;
            if (!self.conference) throw { code: 'not-connected', data: 'Instance is not connected to the conference' }
            if (!self.ssStream) throw {code:'not-in-use', data:'Screen is not shared'}

            // Stop all tracks of the share stream
            self.ssStream.getTracks().forEach(track => track.stop());
            self.ssStream = null;

            // Disconnect and remove PeerJS connection
            self.screenShareSource = null;
            self.removePeerjsStats_();

            // Send screen-share-stop
            var currentTime = (new Date()).toISOString();
            var screenShareStopEvent = {
                Action: 'screen-share-stop',
                Room: self.conference.id,
                'Caller-Caller-ID-Number': self.user.uuid,
                // ConferenceUniqueID: event['Conference-Unique-ID'],
                User: self.user,
                Time: currentTime,
            };             
            self.socket.emit('inject', screenShareStopEvent);
        }

        screenShareViewStart (uuid, domElement){
            let self = this;
            if (!domElement) return Promise.reject({ code: 'dom-element-missed', data: 'DOM element is not exist' });
            if (!self.conference) return Promise.reject({ code: 'not-connected', data: 'Instance is not connected to the conference' });

            const peerCallId = 'ss-' + uuid + self.id();

            self.conference.domElements[peerCallId] = domElement;
            if (!self.screenShareViewer || self.screenShareViewer.disconnected) {
                if(self.screenShareViewer) self.screenShareViewer.destroy();

                self.screenShareViewer = self.p2pScreenShareInit_(self.user.p2pSessionId);
                self.screenShareViewer.on('error', (err) => {
                    if(self.debug) console.error('PeerJS error occured:', err);
                    self.screenShareViewer.destroy();
                    self.screenShareViewer = self.p2pScreenShareInit_(self.user.p2pSessionId);
                    self.trackPeerjsStats_(self.screenShareViewer);
                });
                self.screenShareViewer.on('disconnected', () => {
                    if(self.screenShareViewer) self.screenShareViewer.reconnect();
                });

                self.trackPeerjsStats_(self.screenShareViewer);
            }


            var currentTime = (new Date()).toISOString();
            var callMeEvent = {
                Action: 'call-me',
                Room: self.conference.id,
                'Caller-Caller-ID-Number': self.user.uuid,
                'Callee-Callee-ID-Number': uuid,
                User: self.user,
                Time: currentTime,
            };

            self.socket.emit('broadcast', callMeEvent);
            self.ssViewSessionsCount = self.ssViewSessionsCount + 1;
        }

        screenShareViewStop (uuid){
            let self = this;
            const peerCallId = 'ss-' + uuid + self.id();

            if (!self.conference) throw { code: 'not-connected', data: 'Instance is not connected to the conference' }
            if (!self.conference.ss_calls[peerCallId]) throw { code: 'no-session', data: 'There is no screen share session for user with UUID = ' + uuid };

            self.conference.ss_calls[peerCallId].close();
            delete self.conference.ss_calls[peerCallId];

            self.conference.domElements[peerCallId].srcObject = null;
            self.removePeerjsStats_();
            self.ssViewSessionsCount = self.ssViewSessionsCount - 1;
            if (self.ssViewSessionsCount <= 0) {
                self.screenShareViewer.destroy();
                self.screenShareViewer = null;
            }
        }

        userKick (uuid) {
            console.warn("kickUser is deprecated, please use userKick instead");
            this.kickUser(uuid);
        }

        kickUser (uuid) {
            if (!this.conference) throw { code: 'not-connected', data: 'Instance is not connected to the conference' }

            let self = this;
            if (self.isP2P &&
                self.conference.creator_uuid === self.user.uuid){
                var userKickSocketEvent = {
                    Action: 'kick-member',
                    Room: self.conference.id,
                    'Caller-Caller-ID-Number': uuid,
                    Time: (new Date()).toISOString(),
                };
                self.socket.emit('inject', userKickSocketEvent);
            }
            self.socket.emit('kick', uuid);
        }

        /* End of Public interface */

        cleanup_ () {
            let self = this;
            self.mohStop();
            self.socketClose_();
            if(self.screenShareSource) self.screenShareSource.destroy();
            self.screenShareSource = null;
            if(self.screenShareViewer) self.screenShareViewer.destroy();
            self.screenShareViewer = null;

            self.call = null;
            self.conference = null;
        }

        prepareConference_(conference) {
            let self = this;
            let c = Object.assign({}, conference);
            c.calls = {};
            c.users = {};
            c.ss_calls = {};
            c.domElements = {};
            return c;            
        }

        initUA_ (conference) {
            let self = this;

            self.ssViewSessionsCount = 0;
            self.user.p2pSessionId = Date.now();

            if (conference.node_domain === 'p2p') {
                self.isP2P = true;

                self.p2pInit_(conference.id);
                return Promise.resolve(); 
            }
            else {
                return super.initUA_(conference.node_domain);
            }
        }

        vertoDialogState_(d) {
            let self = this;
            
            switch (d.state.name) {
            case 'trying':
                break;
            case 'answering':
               break;
            case 'active':
                if (self.conference && self.conference.Settings && self.conference.Settings.e1_joins_muted) self.call && self.call.setMute('off');
                self.trigger_('session-connect', { });
                break;
            case 'hangup':
                var error;
                if (d.cause !== 'NORMAL_CLEARING') { error = d.cause; }
                self.trigger_('session-disconnect', { });
                break;
            case 'destroy':
                // cleanup...
                self.cleanup_();
                break;
            }            
        }

        handleEvents_ () {
            let self = this;

            self.socket.on('reconnect', function(event){
                if (self.conference && self.conference.id) {
                    self.actionRun_('reconnect', self.conference.id);
                }
            });

            self.socket.on('event', function(event){

                switch (event.Action) {

                case 'add-member':
                    const sameUser = event.User.uuid === self.user.uuid;

                    const response = {
                        user: event.User,
                        is_moderator: event.User.meta.is_moderator,
                        p2p_session_id: event.User.p2pSessionId,
                    };
                    
                    // Our own add-member received, so we have a participant list attached
                    if (self.user.uuid === event['Caller-Caller-ID-Number']) {
                        
                        self.conference.users = Object.assign({}, self.conference.users, event.ListUsersInConference);
                        response.listUsersInConference = Object.assign({}, self.conference.users);
                    }

                    let userAlreadyInList = self.conference.users[event.User.uuid];
                    if (self.debug) console.log('userAlreadyInList',userAlreadyInList);

                    if (self.isP2P) {
                        self.conference.users[event.User.uuid] = event.User;

                        const conferenceHasModerator = self.checkModerator().length;
                        const numberUsersInConference = Object.keys(self.conference.users).length;
                        const waitForModerator = self.conference.Settings.wait_for_moderator;
                        const musicOnHold = self.conference.Settings.music_on_hold;

                        if (userAlreadyInList){
                            // Kill media for the same user we probably have connected
                            let peerCallId = userAlreadyInList.uuid + self.conference.id + userAlreadyInList.p2pSessionId;
                            if (self.conference.calls[peerCallId]) {
                                self.conference.calls[peerCallId].close();
                                var audio_el = document.getElementById(self.conference.calls[peerCallId].audio_id);
                                if (audio_el) document.body.removeChild(audio_el);
                                delete self.conference.calls[peerCallId];
                            }

                            if ((userAlreadyInList.uuid == self.user.uuid) && (userAlreadyInList.p2pSessionId == self.user.p2pSessionId)){
                                // We've got add-member for the same user we're running
                                // but having different p2pSessionId
                                let res = {
                                    newUser: event.User,
                                    oldUser: userAlreadyInList
                                };
                                self.leave();
                                self.trigger_('device-switch', res);
                                return;
                            }
                        }

                
                        if (musicOnHold && sameUser && (!conferenceHasModerator && waitForModerator || numberUsersInConference === 1)) {
                            self.mohPlay();
                        }
                        if (musicOnHold && numberUsersInConference !== 1 && (conferenceHasModerator && waitForModerator || !waitForModerator)) {
                            self.mohStop();
                        }
                        if (!waitForModerator || conferenceHasModerator && waitForModerator) {

                            // It's time to place calls

                            Object.keys(self.conference.users || {}).forEach(uuid => {

                                // Calling everybody but the user himself and users we already have a call with

                                let peerCallId = uuid + self.conference.id + self.conference.users[uuid].p2pSessionId || "";
                                if (uuid !== self.user.uuid && !self.conference.calls[peerCallId]) { 
                                    if (self.debug) console.log('Call', uuid, self.conference.users[uuid].p2pSessionId);
                                    self.p2pCall_(self.conference.id, uuid, self.conference.users[uuid].p2pSessionId)
                                    .then((call, id) => {
                                        if (self.debug) console.log('Call', uuid, self.conference.users[uuid].p2pSessionId, 'ready');
                                        self.conference.calls[peerCallId] = call;
                                        self.conference.calls[peerCallId].audio_id = id;
                                    })
                                    .catch(error => {
                                        if (self.debug) console.log('Call', uuid, self.conference.users[uuid].p2pSessionId, 'failed');
                                        if (self.debug) console.error('P2P call error:', error);
                                    });
                                }
                            }); // forEach
                        }
                    } else {
                        // Server-based conferences
                        self.conference.users[event.User.uuid] = event.User;
                    }

                    self.trigger_('add-member', response);
                    break;

                case 'del-member':
                    if (self.conference && self.conference.users){
                        let user = self.conference.users[event['Caller-Caller-ID-Number']];
                        if ((user && (user.p2pSessionId == event.User.p2pSessionId)) || !self.isP2P){
                            if (self.debug) console.log('del-member:', user);
                            delete self.conference.users[event['Caller-Caller-ID-Number']];
                        }
                    }
                    self.trigger_('del-member', { uuid: event['Caller-Caller-ID-Number'], p2p_session_id: event.User.p2pSessionId });
                    break;

                case 'start-talking':
                    self.trigger_('start-talking', {uuid: event['Caller-Caller-ID-Number']});
                    break;

                case 'stop-talking':
                    self.trigger_('stop-talking', {uuid: event['Caller-Caller-ID-Number']});
                    break;

                case 'mute-member':
                    if (self.user.uuid === event['Caller-Caller-ID-Number']) {
                        if (self.isP2P){
                            self.p2pChangeMuteState_(true);
                        } else {
                            self.call && self.call.setMute('off');
                        }
                    }
                    self.trigger_('mute-member', {uuid: event['Caller-Caller-ID-Number']});
                    break;

                case 'unmute-member':
                    if (self.user.uuid === event['Caller-Caller-ID-Number']) {
                        if (self.isP2P){
                            self.p2pChangeMuteState_(false);
                        } else {
                            self.call && self.call.setMute('on');
                        }
                    }
                    self.trigger_('unmute-member', {uuid: event['Caller-Caller-ID-Number']});
                    break;

                case 'lock':
                    self.trigger_('lock', { });
                    break;

                case 'unlock':
                    self.trigger_('unlock', { });
                    break;

                case 'kick-member':
                    if (self.isP2P) {
                        self.p2pKick_(event['Caller-Caller-ID-Number'], event.User.p2pSessionId);
                    }
                    self.trigger_('kick-member', {uuid: event['Caller-Caller-ID-Number'], p2p_session_id: event.User.p2pSessionId});
                    break;

                case 'conference-create':
                    self.trigger_('conference-create', { conference_session_id: event['ConferenceUniqueID'] });
                    break;

                case 'conference-destroy':
                    self.trigger_('conference-destroy', { conference_session_id: event['ConferenceUniqueID'] });
                    break;

                case 'call-status-amd':
                    self.trigger_('call-status-amd', {uuid: event['Caller-Caller-ID-Number'], status: event['Status']});
                    break;

                case 'call-status':
                    self.trigger_('call-status', {uuid: event['Caller-Callee-ID-Number'], status: event['Status'], number: event['Number']});
                    break;

                case 'setgrace':
                    self.trigger_('setgrace', { });
                    break;

                case 'start-recording':
                    self.trigger_('start-recording', { recordingID: event.Record });
                    break;

                case 'stop-recording':
                    self.trigger_('stop-recording', { recordingID: event.Record });
                    break;

                case 'recording-completed':
                    self.trigger_('recording-completed', { recordingID: event.Record, url: event.url });
                    break;
                case 'screen-share-start':
                    self.trigger_('screen-share-start', { uuid: event['Caller-Caller-ID-Number'] });
                    break;
                case 'screen-share-stop':
                    self.trigger_('screen-share-stop', { uuid: event['Caller-Caller-ID-Number'] });
                    break;
                case 'call-me':
                if (self.debug) console.log(event)
                    if ((event['Callee-Callee-ID-Number'] == self.user.uuid) && self.ssStream){
                        if (!self.screenShareSource || self.screenShareSource.disconnected) {
                            self.screenShareSource = self.p2pScreenShareInit_();
                            self.screenShareSource.on('error', (err) => {
                                if(self.debug) console.error('PeerJS error occured:', err);
                                self.screenShareSource.destroy();
                                self.screenShareSource = self.p2pScreenShareInit_();
                                self.trackPeerjsStats_(self.screenShareSource);
                            });
                            self.screenShareSource.on('disconnected', () => {
                                if(self.screenShareSource) self.screenShareSource.reconnect();
                            });
                            self.trackPeerjsStats_(self.screenShareSource);
                        }
                        var call;
                        let tries = 1;
                        let timer = setInterval(() => {
                            call = self.screenShareSource.call('ss-' + event['Caller-Caller-ID-Number'] + self.id() + (event.User.p2pSessionId||""), self.ssStream);
                            if (self.debug) console.log(`Attempt ${tries}: calling ss-` + event['Caller-Caller-ID-Number'] + self.id() + (event.User.p2pSessionId||""));
                            if (call) {
                                call.on('stream', (remoteStream) => {
                                }); 
                            } else {
                                tries = tries + 1;
                            }
                            if (call || (tries > 5)) {
                                clearInterval(timer);
                                if (self.debug && !call) console.error(`Calling ss-` + event['Caller-Caller-ID-Number'] + self.id() + (event.User.p2pSessionId||"") + ' failed');
                            }
                        }, 1000);

                    }
                    break;
                }
            });
        } // handleEvents_

        prepareInvitation_ (conferenceSettings, data, options) {
            let self = this;

            if (!options || options && !options.conference_url) return Promise.reject('Missing conference url');
            if (!data) return Promise.reject('Missing contacts information');
            var inviteData = { invited_by_uuid: this.user.uuid };
            var contacts = [];

            data.forEach(function(data) { return contacts.push(new CliqueContact(data)) });

            var emails = contacts.filter(function (el) { return el.isEmail() }).map(function(el) { return { contact: el.contact } });
            var phones = contacts.filter(function (el) { return el.isSms() }).map(function(el) { return { contact: el.contact } });
            var calls = contacts.filter(function (el) { return el.isCall() }).map(function(el) { return { contact: el.contact } });

            if (emails.length) inviteData.email = {
                contacts: emails,
                template_name: options.email_template,
                template_params: {
                    name: options.sender_name || options.sender_email || 'Your contact',
                    conference_url: options.conference_url,
                    defaultLogoUrl: options.defaultLogoUrl,
                    phone: conferenceSettings.phone_number,
                },
            };

            if (phones.length) inviteData.sms = {
                contacts: phones,
                template_name: options.sms_template,
                template_params: {
                    name: options.sender_name || options.sender_email || 'Your contact',
                    conference_url: options.conference_url,
                    phone: conferenceSettings.phone_number,
                },
            };

            if (calls.length && self.conference.node_domain !== 'p2p') inviteData.call = {
                contacts: calls,
            };

            return Promise.resolve(inviteData);
        }

        actionRun_ (action, data) {
            let self = this;

            if (self.call && action !== 'reconnect') return Promise.reject({code: 'already-connected', data: 'Instance is already connected to the conference'});

            return new Promise( (resolve, reject) => {
                self.init()
                    .then( () => {
                        let whitelist = Object.assign({}, data.whitelist||{});
                        if(data.whitelist) { 
                            delete data.whitelist; 
                        }
                        let conferencePromise = (action == 'create'? self.api.conferenceCreate(data): self.api.conferenceFetch(data, true));

                        if(Object.keys(whitelist).length) {
                            let wl_array = Object.values(whitelist);
                            if(!wl_array.includes(self.user.uuid)) {
                                wl_array.push(self.user.uuid);
                            }
                            return new Promise( (resolve_, reject_) => {
                                conferencePromise.then( (conference) => {
                                    self.api.whitelistCreate(conference.id, wl_array);
                                    resolve_(conference);
                                });
                            } );
                        }
                        else return conferencePromise;
                    } )
                    .then( (conference) => {
                        self.conference = self.prepareConference_(conference);
                        self.p2pMuted = conference.Settings.e1_joins_muted;
                        if (conference.locked && self.user.uuid !== conference.creator_uuid) {
                            return Promise.reject({code: 'conference-locked', data: 'Conference is locked'});
                        }

                        return self.initUA_(conference);
                    } )
                    .then(function() {
                        self.socketSubscribe_(self.conference.id);

                        if (self.isP2P){
                            var currentTime = (new Date()).toISOString();
                            self.user.meta.is_moderator = self.conference.creator_uuid === self.user.uuid;
                            var userAddSocketEvent = {
                                Action: 'add-member',
                                Room: self.conference.id,
                                'Caller-Caller-ID-Number': self.user.uuid,
                                // ConferenceUniqueID: event['Conference-Unique-ID'],
                                User: self.user,
                                Time: currentTime,
                            };
                            if (self.debug) console.log('P2P add-member', self.user);

                            self.socket.emit('inject', userAddSocketEvent);
                        }
                        else if (action !== 'reconnect') {
                            self.call = self.verto.newCall({destination_number:'conference_' + self.conference.id, caller_id_number:self.user.uuid});
                        }
                        resolve(self.conference);
                    } )
                    .catch( (data) => {
                        reject(data);
                    } );            
            } );
        }

        p2pChangeMuteState_ (isMuted){
            let self = this;
            self.p2pMuted = isMuted;
            if (self.conference.calls) {
                Object.keys(self.conference.calls).forEach(peerCallId => {
                    self.conference.calls[peerCallId].localStream.getAudioTracks()[0].enabled = !isMuted;
                });
            }
        }

        // P2P routines
        p2pBindMicManager_ (stream, threshold){
            let self = this;
            if (self.micContext) return; // We don't wanna make too much microphone managers
            self.micContext = new (window.AudioContext || window.webkitAudioContext)();
            let micSource = self.micContext.createMediaStreamSource(stream);
            let micProcessor = self.micContext.createScriptProcessor(2048, 1, 1);
            let micAnalyser = self.micContext.createAnalyser();
            micProcessor.connect(self.micContext.destination);
            micSource.connect(micAnalyser);

            // Now we should have an AudioContext associated with the local microphone
            micAnalyser.fftSize = 64;
            var data = new Uint8Array(micAnalyser.frequencyBinCount);
            var talkStatus = false;
            var talkCount = 0;
            micProcessor.onaudioprocess = function (){
                micAnalyser.getByteFrequencyData(data);
                var sum = 0;
                for (let item of data){
                    if (item) sum += item;
                }
                if (sum > threshold) {
                    if (!talkStatus && talkCount>10) {
                        talkStatus = true;
                        let currentTime = (new Date()).toISOString();
                        if (self.conference && self.conference.p2p) {
                            let startTalkingEvent = {
                                Action: 'start-talking',
                                Room: self.conference.id,
                                'Caller-Caller-ID-Number': self.user.uuid,
                                // ConferenceUniqueID: event['Conference-Unique-ID'],
                                User: self.user,
                                Time: currentTime,
                            };
                            self.socket.emit('inject', startTalkingEvent);
                        }
                    }
                    if (!talkStatus) talkCount++;
                } else {
                    if (talkStatus && talkCount<=0) {
                        talkStatus = false;
                        let currentTime = (new Date()).toISOString();
                        if (self.conference && self.conference.p2p) {
                            let stopTalkingEvent = {
                                Action: 'stop-talking',
                                Room: self.conference.id,
                                'Caller-Caller-ID-Number': self.user.uuid,
                                // ConferenceUniqueID: event['Conference-Unique-ID'],
                                User: self.user,
                                Time: currentTime,
                            };
                            self.socket.emit('inject', stopTalkingEvent);
                        }
                    }
                    if (talkStatus) talkCount--;
                }
            }
        };

        p2pUnbindMicManager_ (){
            let self = this;
            if (!self.micContext) return;
            self.micContext.close();
            delete self.micContext;
        }

        p2pScreenShareInit_ (p2pSessionId){
            let self = this;

            p2pSessionId = (p2pSessionId?p2pSessionId:"");
            var location = new URL(self.peerManager);
            var port = location.port || (location.protocol == 'https:' ? '443' : '80');
            var ssPeer = new Peer('ss-' + self.user.uuid + self.id() + p2pSessionId, {
                debug: 3,
                secure: location.protocol == 'https:',
                host: location.hostname,
                port: port,
                path: location.pathname,
                authParams: { conference_id: self.id(), session_token: self.sessionToken },
                config: {iceServers: self.iceServers}
            });
            ssPeer.on('call', function (call) {
                if (self.debug) console.log(call.peer)
                self.conference.ss_calls[call.peer] = call;
                self.conference.ss_calls[call.peer].answer(null);
                self.conference.ss_calls[call.peer].on('stream', function(remoteStream) {
                    self.conference.domElements[call.peer].srcObject = remoteStream;
                    let p = self.conference.domElements[call.peer].play();
                    if (p) p.then(()=>{}).catch(()=>{});
                });
                self.conference.ss_calls[call.peer].on('error', function(err){
                    if (self.debug) console.error(err);
                });
                self.conference.ss_calls[call.peer].on('close', function(){
                    if (self.debug) console.log('Close connection');
                    delete self.conference.ss_calls[call.peer];
                });
            });
            return ssPeer;
        }

        p2pInit_ (conference_id){
            let self = this;
            if (self.p2pObj) return;
            self.p2pMicThreshold = self.p2pMicThreshold || 800;

            self.socketInit_(self.user.p2pSessionId);

            var location = new URL(self.peerManager);
            var port = location.port || (location.protocol == 'https:' ? '443' : '80');
            if (self.debug) console.log("Init P2P",self.user.uuid, self.id(), self.user.p2pSessionId)
            self.p2pObj = new Peer(self.user.uuid + self.id() + self.user.p2pSessionId, {
                debug: 3,
                secure: location.protocol == 'https:',
                host: location.hostname,
                port: port,
                path: location.pathname,
                authParams: { conference_id: self.id(), session_token: self.sessionToken },
                config: {iceServers: self.iceServers}
            });
            self.p2pObj.on('call', function(call){
                // call is a mediaConnection instance
                if (!self.conference) return;
                navigator.getUserMedia({video: false, audio:true}, function(stream){
                    self.micStream = stream;
                    self.p2pBindMicManager_(stream, self.p2pMicThreshold);
                    // TODO: Add conference id check, participant number limit
                    if (!self.conference.calls) self.conference.calls = {};
                    self.conference.p2p = true;
                    self.conference.calls[call.peer] = call;
                    call.answer(stream);
                    call.on('stream', function(remoteStream){
                        var id = self.audioElementCreate_(call.peer);
                        document.getElementById(id).srcObject = remoteStream;
                        self.conference.calls[call.peer].audio_id = id;
                        if (self.p2pMuted) {
                            self.conference.calls[call.peer].localStream.getAudioTracks()[0].enabled = false;
                        }
                    });
                }, function(error){
                    if (self.debug) console.error('FATAL: Can\'t getUserMedia', error);
                    throw error;
                });
            });
        }

        p2pCall_ (conference_id, user_id, p2pSessionId){
            let self = this;
            if (self.debug) console.log('P2P Call:', conference_id, user_id, p2pSessionId);
            return new Promise((resolve, reject) => {
                var peerCallId = user_id + conference_id + p2pSessionId;
                navigator.getUserMedia({video: false, audio:true}, function(stream){
                    if (!self.conference.users[user_id]) {
                        // No such user in the conference
                        return reject({code: 'no-user'});
                    };
                    self.micStream = stream;
                    let call = self.p2pObj.call(peerCallId, stream);
                    if (!call) {
                        // Call failed
                        return reject({code: 'no-call'})
                    }
                    if (self.debug) console.log("P2P Call Stage #2");

                    self.p2pBindMicManager_(stream, self.p2pMicThreshold);

                    var id = self.audioElementCreate_(peerCallId);
                    call.on('stream', function(remoteStream){
                        if (self.debug) console.log("P2P Call Stream Ready", remoteStream);
                        document.getElementById(id).srcObject = remoteStream;

                        if (self.p2pMuted) {
                            call.localStream.getAudioTracks()[0].enabled = false;
                        }
                    });
                    return resolve(call, id);
                }, function(error){
                    if (self.debug) console.error('FATAL: Can\'t getUserMedia', error);
                    return reject({code:'webrtc-error',error:error});
                });
            });
        }

        p2pKick_ (uuid, p2pSessionId){
            let self = this;
            return new Promise((resolve, reject) => {
                if (!self.conference) return reject({code: 'not-connected', data: 'Instance is not connected to the conference'})
                if (uuid !== self.user.uuid) {
                    const peerCallId = uuid + self.conference.id + (p2pSessionId||"")
                    if (self.conference.calls && self.conference.calls[peerCallId]) {
                        self.conference.calls[peerCallId].close();
                    }
                } else {
                    if (p2pSessionId == self.user.p2pSessionId){
                        self.leave();
                    }
                }

                resolve();
            });
        }
    }


    /*

    CliqueSimpleCall provides an interface to usual two-side calls via Clique servers.

    PSTN numbers, SIP URIs are supported. 

    */
 
    class CliqueSimpleCall extends CliqueBase {

        id () {
            let self = this;
            return self.number;
        }

        initUA_ () {
            let self = this;

            self.socketInit_();

            return AjaxRequest({
                url : self.apiEndpoint + '/nodes',
                dataType : 'JSON',
                bearer: self.sessionToken
            })
            .then( (data) => {

            //TODO: remove this logic from frontend. We need API method to return domain with the least cpu load
                var minLoad = 101;
                var minIndex = 0;

                for (var i in data.result){
                    if (data.result[i].cpu_load < minLoad) {
                        minLoad = data.result[i].cpu_load;
                        minIndex = i;
                    }
                }

                var node = data.result[minIndex];
                if (!node) throw new Error('Node is undefined');

                return super.initUA_(node.domain_name);
            })
            .then( () => {
                return self.verto
            });
        }

        vertoDialogState_(d) {
            let self = this;
            switch (d.state.name) {
            case 'trying':
                break;
            case 'answering':
                self.trigger_('call-answer', { });
               break;
            case 'active':
                self.trigger_('call-active', { });
                break;
            case 'hangup':
                var error;
                if (d.cause !== 'NORMAL_CLEARING') { error = d.cause; }
                self.trigger_('call-hangup', { });
                break;
            case 'destroy':
                // cleanup...
                self.cleanup_();
                break;
            }            
        }

        handleEvents_ () {
            let self = this;

           self.socket.on('event', function(event){

                switch (event.Action) {

                default:
                    break;
                }
            });
        } // handleEvents_

        dial (number, options) {
            let self = this;

            self.init()
                .then( () => {
                    return self.initUA_()
                } )
                .then(function (ua) {
                    var vars = {};

                    if (options && (options.detect_machine || options.detectMachine)) vars.Live_Answer = true;
                    if (options && (options.detect_beep || options.detectBeep)) vars.Detect_Beep = true;
                    if (options && (options.connect_on_live || options.connectOnLive)) {
                        vars.Live_Answer = true;
                        vars.Connect_On_Live = true;
                    }
                    if (!vars.Live_Answer) {
                        vars.Detect_Beep = false;
                    }

                    self.call = ua.newCall({ destination_number:number, userVariables: vars, caller_id_number: options && options.source || self.user.uuid });
                    self.number = number;
                    return Promise.resolve(self.call);
                })
                .catch(function(err) {
                    throw {code: 'http-error', data: err}
                });
        }
        
        hangup () {
            this.call && this.call.hangup();
        }

        cleanup_ () {
            let self = this;
            self.call = null;
            self.number = null;
        }
    }

    class CliqueUser extends CliqueAbstract {
        getConferenceSettings() {
            return this.init()
            .then(() => {
                return this.api.userFetch();
            })
            .then(user => user.settings);
        }

        updateConferenceSettings(props) {
            return this.init()
            .then(() => {
                return this.api.userUpdate({ settings: props });
            })
            .then(user => user.settings);
        }
    }

    class CliqueChat extends CliqueAbstract {
        constructor(options) {
            super(options);
            this.doneTypingInterval = 5000;
            this.subscribedChats = {};
            this.typingTimer = [];
            this.socketInit_();
        }

        create(data) {
            let self = this;

            return self.init()
            .then(() => {
                return self.api.chatCreate(data);
            })
            .then(chatInfo => {
                self.subscribedChats[chatInfo.chat.chat_id] = {};
                self.subscribe_(chatInfo.chat.chat_id);
                return chatInfo.chat;
            });
        }

        join(chatId) {
            let self = this;

            return self.init()
            .then(() => {
                return self.api.chatJoin(chatId);
            })
            .then(chatInfo => {
                self.subscribedChats[chatInfo.chat.chat_id] = {};
                self.subscribe_(chatInfo.chat.chat_id);
                return chatInfo.chat;
            });
        }

        leave(chatId) {
            let self = this;

            return self.init()
            .then(() => {
                return self.api.chatLeave(chatId);
            })
            .then(chatInfo => {
                delete self.subscribedChats[chatId];
                self.unsubscribe_(chatId);
                return chatInfo.chat;
            });
        }

        send(chat_id, message) {
            let self = this;

            if (!chat_id) return Promise.reject({code: 'not-connected', data: 'Chat id missed'})

            self.socket.emit('message', {
                chat_id: chat_id,
                message: message,
                timestamp: (new Date()).toISOString(),
            });
        }

        fetchMessageHistory(chat_id, options = {}) {
            let self = this;

            return self.init()
            .then(() => {
                self.socket.emit('message-history', {
                    chat_id: chat_id,
                    options: options,
                });
            });
        }

        markMessagesAsRead(chat_id, timestamp) {
            this.socket.emit('message-marked-read', {
                chat_id: chat_id,
            });
        }

        subscribe_(chat_id) {
            this.socket.emit('subscribe', {
                channel: chat_id,
                type: 'chat',
            });
        }

        unsubscribe_(chat_id) {
            this.socket.emit('unsubscribe', {
                channel: chat_id,
                type: 'chat',
            });
        }

        handleEvents_ () {
            let self = this;

            self.socket.on('reconnect', function(event){
                if (Object.keys(self.subscribedChats).length) {
                    self.actionRun_('reconnect', Object.keys(self.subscribedChats));
                }
            });

            self.socket.on('event', function(event){

                switch (event.Action) {
                case 'message':
                    self.trigger_('message', { uuid: event['Caller-Caller-ID-Number'], message: event.message, chat_id: event.chat_id, timestamp: event.timestamp });
                    break;
                case 'message-history':
                    self.trigger_('message-history', { uuid: event['Caller-Caller-ID-Number'], messages: event.messages, total: event.total, chat_id: event.chat_id });
                    break;
                case 'message-start-typing':
                    self.trigger_('message-start-typing', { uuid: event['Caller-Caller-ID-Number'], chat_id: event.chat_id });
                    break;
                case 'subscribe':
                    self.trigger_('subscribe', { uuid: event['Caller-Caller-ID-Number'], chat_id: event.channel, unread_message_count: event.unread_message_count });
                    break;
                case 'unsubscribe':
                    self.trigger_('unsubscribe', { uuid: event['Caller-Caller-ID-Number'], chat_id: event.channel });
                    break;
                case 'message-stop-typing':
                    self.trigger_('message-stop-typing', { uuid: event['Caller-Caller-ID-Number'], chat_id: event.chat_id });
                    break;
                }
            });
        } // handleEvents_


        actionRun_ (action, chatList = []) {
            let self = this;

            self.socketInit_();
            return Promise.all(chatList.map((chatId) => self.join(chatId)));
        }
    }


return { Conference: CliqueConference, User: CliqueUser, SimpleCall: CliqueSimpleCall, Chat: CliqueChat }

}));