/*
 *  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
/* eslint-env node */

'use strict';

(function (global, factory) {
    /*global define*/
    if (typeof define === 'function' && define.amd) {
        define(['./utils', './shims/chrome_shim', './shims/edge_shim', './shims/firefox_shim', './shims/safari_shim'], factory); // AMD
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('./utils'), require('./shims/chrome_shim'), require('./shims/edge_shim'), require('./shims/firefox_shim'), require('./shims/safari_shim')); // commonjs
    } else {
        global.adapter = factory(global.utils, global.chrome_shim, global.edge_shim, global.firefox_shim, global.safari_shim); // Browser
    }
}(this, function(Utils, chrome_shim, edge_shim, firefox_shim, safari_shim) {

// Utils.
    var utils = Utils;
    var logging = utils.log;
    var browserDetails = utils.detectBrowser(window);

    // Export to the adapter global object visible in the browser.
    var adapter = {
        browserDetails: browserDetails,
        extractVersion: utils.extractVersion,
        disableLog: utils.disableLog,
        disableWarnings: utils.disableWarnings
    };

    // Uncomment the line below if you want logging to occur, including logging
    // for the switch statement below. Can also be turned on in the browser via
    // adapter.disableLog(false), but then logging from the switch statement below
    // will not appear.
    // require('./utils').disableLog(false);

    // Browser shims.
    var chromeShim = chrome_shim || null;
    var edgeShim = edge_shim || null;
    var firefoxShim = firefox_shim || null;
    var safariShim = safari_shim || null;

    // Shim browser if found.
    switch (browserDetails.browser) {
    case 'chrome':
        if (!chromeShim || !chromeShim.shimPeerConnection) {
            logging('Chrome shim is not included in this adapter release.');
            return adapter;
        }
        logging('adapter.js shimming chrome.');
        // Export to the adapter global object visible in the browser.
        adapter.browserShim = chromeShim;

        chromeShim.shimGetUserMedia(window);
        chromeShim.shimMediaStream(window);
        utils.shimCreateObjectURL(window);
        chromeShim.shimSourceObject(window);
        chromeShim.shimPeerConnection(window);
        chromeShim.shimOnTrack(window);
        chromeShim.shimGetSendersWithDtmf(window);
        break;
    case 'firefox':
        if (!firefoxShim || !firefoxShim.shimPeerConnection) {
            logging('Firefox shim is not included in this adapter release.');
            return adapter;
        }
        logging('adapter.js shimming firefox.');
        // Export to the adapter global object visible in the browser.
        adapter.browserShim = firefoxShim;

        firefoxShim.shimGetUserMedia(window);
        utils.shimCreateObjectURL(window);
        firefoxShim.shimSourceObject(window);
        firefoxShim.shimPeerConnection(window);
        firefoxShim.shimOnTrack(window);
        break;
    case 'edge':
        if (!edgeShim || !edgeShim.shimPeerConnection) {
            logging('MS edge shim is not included in this adapter release.');
            return adapter;
        }
        logging('adapter.js shimming edge.');
        // Export to the adapter global object visible in the browser.
        adapter.browserShim = edgeShim;

        edgeShim.shimGetUserMedia(window);
        utils.shimCreateObjectURL(window);
        edgeShim.shimPeerConnection(window);
        edgeShim.shimReplaceTrack(window);
        break;
    case 'safari':
        if (!safariShim) {
            logging('Safari shim is not included in this adapter release.');
            return adapter;
        }
        logging('adapter.js shimming safari.');
        // Export to the adapter global object visible in the browser.
        adapter.browserShim = safariShim;
        // shim window.URL.createObjectURL Safari (technical preview)
        utils.shimCreateObjectURL(window);
        safariShim.shimRTCIceServerUrls(window);
        safariShim.shimCallbacksAPI(window);
        safariShim.shimLocalStreamsAPI(window);
        safariShim.shimRemoteStreamsAPI(window);
        safariShim.shimGetUserMedia(window);
        break;
    default:
        logging('Unsupported browser!');
        break;
    }

    return adapter;

}));