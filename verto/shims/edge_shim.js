/*
 *  Copyright (c) 2016 The WebRTC project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a BSD-style license
 *  that can be found in the LICENSE file in the root of the source
 *  tree.
 */
/* eslint-env node */
'use strict';

(function (global, factory) {
    /*global define*/
    if (typeof define === 'function' && define.amd) {
        define(['./../utils', './../getusermedia_edge', './../rtcpeerconnection_shim'], factory); // AMD
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory(require('./../utils'), require('./../getusermedia_edge'), require('./../rtcpeerconnection_shim')); // commonjs
    } else {
        global.edge_shim = factory(global.utils, global.getusermedia_edge, global.rtcpeerconnection_shim); // Browser
    }
}(this, function(Utils, getUserMedia, rtcpeerconnection_shim) {

    var utils = Utils;
    var shimRTCPeerConnection = rtcpeerconnection_shim;

    return {
        shimGetUserMedia: getUserMedia,
        shimPeerConnection: function(window) {
            var browserDetails = utils.detectBrowser(window);

            if (window.RTCIceGatherer) {
                // ORTC defines an RTCIceCandidate object but no constructor.
                // Not implemented in Edge.
                if (!window.RTCIceCandidate) {
                    window.RTCIceCandidate = function(args) {
                        return args;
                    };
                }
                // ORTC does not have a session description object but
                // other browsers (i.e. Chrome) that will support both PC and ORTC
                // in the future might have this defined already.
                if (!window.RTCSessionDescription) {
                    window.RTCSessionDescription = function(args) {
                        return args;
                    };
                }
                // this adds an additional event listener to MediaStrackTrack that signals
                // when a tracks enabled property was changed. Workaround for a bug in
                // addStream, see below. No longer required in 15025+
                if (browserDetails.version < 15025) {
                    var origMSTEnabled = Object.getOwnPropertyDescriptor(
                        window.MediaStreamTrack.prototype, 'enabled');
                    Object.defineProperty(window.MediaStreamTrack.prototype, 'enabled', {
                        set: function(value) {
                            origMSTEnabled.set.call(this, value);
                            var ev = new Event('enabled');
                            ev.enabled = value;
                            this.dispatchEvent(ev);
                        }
                    });
                }
            }
            window.RTCPeerConnection =
        shimRTCPeerConnection(window, browserDetails.version);
        },
        shimReplaceTrack: function(window) {
            // ORTC has replaceTrack -- https://github.com/w3c/ortc/issues/614
            if (window.RTCRtpSender &&
        !('replaceTrack' in window.RTCRtpSender.prototype)) {
                window.RTCRtpSender.prototype.replaceTrack =
          window.RTCRtpSender.prototype.setTrack;
            }
        }
    };

}));