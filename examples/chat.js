(async() => {
    const BASE_URL = '<BASE_URL>';
    const API_KEY = '<API_KEY>';

    let chat,
    chatData;

    //get clique user info
    const fetchData = {
        method: 'POST',
        headers: new Headers({
            'Authorization': 'Bearer ' + API_KEY,
        }),
        mode: 'cors',
    };
    const data = await fetch(BASE_URL + '/api/v2/users', fetchData);
    const userRes = await data.json();
    const user = userRes.user;

    //create an instance of chat client
    chat = new CliqueClient.Chat({
        base_url: BASE_URL,
        session_token: user.token,
    });


    //update elements on page
    const id = (id) => document.getElementById(id);
    //determine the user ID
    id('id_profile').textContent = user.uuid;


    //create new conference with custom settings
    id('createChat')
        .addEventListener('click', async() => {
            let ids = id('allowedUserIds').value;
            if (ids) ids = ids.split(',');
            chatData = await chat.create({
                allowed_user_uuids: ids,
            });
            prepareChatInfo();
        });

    //join existing conference
    id('join')
        .addEventListener('click', async() => {
            chatData = {
                chat_id: id('joinChatId').value,
            };

            prepareChatInfo();
            try {
                chatData =  await chat.join(chatData.chat_id);
                console.log('chatData', chatData);
                const messages = chat.callMessageHistory(chatData.chat_id, {
                    from: 0,
                });

            } catch (e) {
                console.error('join error', e);
            }
        });

    id('leave')
        .addEventListener('click', async() => {
            chatData = {
                chat_id: id('joinChatId').value,
            };

            try {
                chatData =  await chat.leave(chatData.chat_id);
                console.log('chatData', chatData);

            } catch (e) {
                console.error('join error', e);
            }
        });

    //show conference information
    const prepareChatInfo = () => {
        id('id_chat').textContent = chatData.chat_id;
    };


    id('sendMessage')
        .addEventListener('click', async e => {
            const message = id('emojionearea1').value;
            if (!message) return;
            var el = $('#emojionearea1').emojioneArea();
            el[0].emojioneArea.setText('');
            chat.send(chatData.chat_id, message);
        });

    const linkify = (inputText) => {
        var replacedText, replacePattern1, replacePattern2, replacePattern3;

        //URLs starting with http://, https://, or ftp://
        replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
        replacedText = inputText.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

        //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
        replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
        replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

        //Change email addresses to mailto:: links.
        replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
        replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

        return replacedText;
    }

    const addChatMessage = (sender, text) => {
        $('#messages').append('<p>' + sender + ' : ' + window.emojione.toImage(linkify(text)) + '</p>')
    }

    //events
    const eventhandler = (event, name) => {
        console.log('event', name, event);
        id('currentAction').textContent = name;
    }

    chat.on('message', event => {
        eventhandler(event, 'message');
        addChatMessage(event.uuid, event.message);
    });
    chat.on('message-start-typing', event => eventhandler(event, 'message-start-typing'));
    chat.on('message-stop-typing', event => eventhandler(event, 'message-stop-typing'));
    chat.on('message-history', event => {
        eventhandler(event, 'message-history');
        event.messages.map(message => {
            addChatMessage(message.user_id, message.message);
        })
    });



    chat.on('error', event => eventhandler(event, 'error'));

    $('#emojionearea1').emojioneArea({
        pickerPosition: 'right',
        tonesStyle: 'bullet'
    });
})();