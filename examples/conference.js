(async() => {
    const BASE_URL = '<BASE_URL>';
    const API_KEY = '<API_KEY>';

    let conference,
    client,
    api,
    recordingId,
    members = {},
    mute = false,
    recording = false,
    lock = false;
    let settings = {
        wait_for_moderator: false,
        e1_joins_muted: false,
        join_in_watch_mode: false,
        music_on_hold: false,
        assign_phone_number: false,
        record_all_conferences: false,
    };

    //get clique user info
    const fetchData = {
        method: 'POST',
        headers: new Headers({
            'Authorization': 'Bearer ' + API_KEY,
        }),
        mode: 'cors',
    };
    const data = await fetch(BASE_URL + '/api/v2/users', fetchData);
    const userRes = await data.json();
    const user = userRes.user;

    //create an instance of conference client
    client = new CliqueClient.Conference({
        base_url: BASE_URL,
        session_token: user.token,
    });

    //update elements on page
    const id = (id) => document.getElementById(id);
    //determine the user ID
    id('id_profile').textContent = user.uuid;

    //actions allowed to made for moderator by participants
    const actions = (uuid) => {
        return `
            <button data-userid="${uuid}" class="btn btn-danger kick-user"      onClick="window.kickUser('${uuid}')">Kick</button>
            <button data-userid="${uuid}" class="btn btn-default mute-user"      onClick="window.muteUser('${uuid}')">Mute</button>
            <button data-userid="${uuid}" class="btn btn-default unmute-user"    onClick="window.unmuteUser('${uuid}')">Unmute</button>
        `;
    }

    //show participant list
    const show_members = () => {
        var s = '<ul>';
        for (let i in members) {
            s = s + '<li>' + members[i].uuid + actions(members[i].uuid);
        }
        id('member_list').innerHTML = s;
    };

    //create new conference with custom settings
    id('createConference')
        .addEventListener('click', async() => {
            const keys = Object.keys(settings);
            keys.forEach(setting => {
                const el = document.getElementsByClassName('user_' + setting);
                if (el[0]) {
                    settings[setting] = el[0].checked;
                }
            });
            conference = await client.create({ conference_settings: settings }) || conference;
            disableElements(false);
            prepareConfInfo();
        });

    //join existing conference
    id('join')
        .addEventListener('click', async() => {
            conference = {
                id: id('joinConfId').value,
            };
            conference = await client.join(conference.id);
            const confUserList = await client.users();
            members = Object.assign({}, confUserList);
            show_members();
            disableElements(false);
            prepareConfInfo();
        });

    //show conference information
    const prepareConfInfo = () => {
        id('id_conference').textContent = conference.id;
        id('invite_link').textContent =  'https://yourdomain/conference/' + conference.id;
        const keys = Object.keys(conference.Settings);
        keys.forEach(setting => {
            const el = document.getElementsByClassName('conference_' + setting);
            if (el[0] && conference.Settings[setting]) {
                el[0].setAttribute('checked', true);
            } else if (el[0]) {
                el[0].removeAttribute('checked');
            }
        });
    };

    let elMute = id('mute');
    let elLock = id('lock');
    let elRecording = id('recording');
    const unmuteHandler = () => {
        elMute.textContent = 'mute'
        elMute.classList.remove('btn-success');
        elMute.classList.add('btn-danger');
        mute = false;
    };

    const unlockHandler = () => {
        elLock.textContent = 'lock'
        elLock.classList.remove('btn-success');
        elLock.classList.add('btn-danger');
        lock = false;
    };

    const recordingOffHandler = () => {
        elRecording.textContent = 'Start Recording'
        elRecording.classList.remove('btn-success');
        elRecording.classList.add('btn-danger');
        recording = false;
    };
    //disable buttons if it not available
    const disableElements = flag => {
        const els = ['invite', 'setgrace', 'mute', 'lock', 'recording', 'hangup'];

        if (flag) {
            $('#volumeWrapper').hide();
            els.forEach(elName => {
                id(elName).setAttribute("disabled", true);
            });
            unmuteHandler();
            unlockHandler();
            recordingOffHandler();
        } else {
            $('#volumeWrapper').show();
            els.forEach(elName => {
                id(elName).removeAttribute("disabled");
            });
        }
    };
    disableElements(true);

    const displayRecordLink = link => {
        if (link) {
            id('recording_audio_player').src = link;
            id('recording_audio').style.display = 'block';
        } else {
            id('recording_audio').style.display = 'none';
        }
    }
    displayRecordLink();


    //invite options
    const getInviteOptions = () => ({
        // sender_name: newUser.display_name,
        conference_url: 'https://yourdomain/conference/' + conference.id,
    });

    //prepare contact data for invitation
    const getContacts = () => {
        const inviteList = [];

        if (id('sms').value) {
            inviteList.push({
                type: 'sms',
                contact: id('sms').value,
            });
        }

        if (id('email').value) {
            inviteList.push({
                type: 'email',
                contact: id('email').value,
            });
        }

        if (id('call').value) {
            inviteList.push({
                type: 'call',
                contact: id('call').value,
            });
        }

        return inviteList;
    }
    //invite to conference
    id('invite')
        .addEventListener('click', async () => {
            const invite = await client.invite(getContacts(), getInviteOptions())
            console.log('invite', invite);
        });

    //hangup conference
    id('hangup')
        .addEventListener('click', () => {
            client.leave();
            members = {};
            show_members();
            disableElements(true);
        });

    //audil slider initialization
    $('#slider').slider({
        value: 100,
        min: 0,
        max: 100,
        step: 10,
        slide: (event, ui) => {
            client.localVolume(ui.value);
        },
    });

    // get conf user list
    id('confUserList')
        .addEventListener('click', async () => {
            const confUserList = await client.users(conference.id);
            console.log('confUserList', confUserList);
        });

    // set grace period
    id('setgrace')
        .addEventListener('click', () => {
            client.setGrace(30)
        });

    //add handler to mute/unmute the conference
    elMute
        .addEventListener('click', e => {
            if (!mute) {
                client.localMute();
                elMute.textContent = 'unmute';
                elMute.classList.remove('btn-danger');
                elMute.classList.add('btn-success');
                mute = true;
            } else {
                client.localUnmute();
                unmuteHandler()
            }
        });


    //add handler to lock/unlock the conference
    elLock
        .addEventListener('click', e => {
            if (!lock) {
                client.lock();
                elLock.textContent = 'unlock';
                elLock.classList.remove('btn-danger');
                elLock.classList.add('btn-success');
                lock = true;
            } else {
                client.unlock();
                unlockHandler();
            }
        });

    //add handler to start/stop recording the conference
    elRecording
        .addEventListener('click', e => {
            if (!recording) {
                client.recordingStart({
                    // put your callback url if you wish to get webhook call
                    // callback_url: 'localost:3000/test',
                });
                elRecording.textContent = 'Stop Recording';
                elRecording.classList.remove('btn-danger');
                elRecording.classList.add('btn-success');
                recording = true;
            } else {
                client.recordingStop();
                recordingOffHandler();
            }
        });

    id('getRecording')
        .addEventListener('click', async e => {
            const rec = await client.getRecordingById(recordingId);
            console.log('recording', rec);
        });

    //events
    const eventhandler = (event, name) => {
        console.log('event', name, event);
        id('currentAction').textContent = name;
    }

    client.on('init', event => {
        id('currentAction').textContent = 'init';
    });
    client.on('add-member', event => {
        eventhandler(event, 'add-member')
        members[event.user.uuid] = event.user;
        show_members();
    });
    client.on('del-member', event => {
        eventhandler(event, 'del-member')
        delete members[event.uuid];
        show_members();
    });
    client.on('start-talking', event => eventhandler(event, 'start-talking'));
    client.on('stop-talking', event => eventhandler(event, 'stop-talking'));
    client.on('mute-member', event => eventhandler(event, 'mute-member'));
    client.on('unmute-member', event => eventhandler(event, 'unmute-member'));
    client.on('lock', event => eventhandler(event, 'lock'));
    client.on('unlock', event => eventhandler(event, 'unlock'));
    client.on('kick-member', event => eventhandler(event, 'kick-member'));
    client.on('conference-create', event => {
        eventhandler(event, 'conference-create');
        displayRecordLink();
    });
    client.on('conference-destroy', event => {
        eventhandler(event, 'conference-destroy');
        disableElements(true);
    });
    client.on('call-status-amd', event => eventhandler(event, 'call-status-amd'));
    client.on('call-status', event => eventhandler(event, 'call-status'));
    client.on('setgrace', event => eventhandler(event, 'setgrace'));
    client.on('start-recording', event => {
        eventhandler(event, 'start-recording')
        recordingId = event.recordingID;
    });
    client.on('stop-recording', event => eventhandler(event, 'stop-recording'));
    client.on('recording-completed', event => {
        eventhandler(event, 'recording-completed');
        displayRecordLink(event.url);
    });
    client.on('connected', event => eventhandler(event, 'connected'));
    client.on('disconnected', event => eventhandler(event, 'disconnected'));
    client.on('conference-join', event => eventhandler(event, 'conference-join'));
    client.on('conference-leave', event => eventhandler(event, 'conference-leave'));
    client.on('error', event => eventhandler(event, 'error'));

    //kick user from conference
    window.kickUser = (uuid) => {
        client.kickUser(uuid);
    };
    //mute user in conference
    window.muteUser = (uuid) => {
        client.muteUser(uuid);
    };
    //unmute user in conference
    window.unmuteUser = (uuid) => {
        client.unmuteUser(uuid);
    };
})();