(async() => {

    //set up variables to define dial settings
    let client, mute = false;
    const BASE_URL = '<BASE_URL>';
    const API_KEY = '<API_KEY>';

    //get clique user info
    const fetchData = {
        method: 'POST',
        headers: new Headers({
            'Authorization': 'Bearer ' + API_KEY,
        }),
        mode: 'cors',
    };

    const data = await fetch(BASE_URL + '/api/v2/users', fetchData);
    const userRes = await data.json();
    const user = userRes.user;
    //update elements on page
    const id = (id) => document.getElementById(id);

    //init dial client
    client = new CliqueClient.SimpleCall({
        base_url: BASE_URL,
        session_token: user.token,
    });

    //start dial
    id('dial')
        .addEventListener('click', async () => {
            const number = id('phoneNumber').value;

            if (number) {
                await client.dial(number);
            }
        });

    //hangup
    id('hangup')
        .addEventListener('click', () => {
            client.hangup()
        });
})();