(async() => {
    const BASE_URL = '<BASE_URL>';
    const API_KEY = '<API_KEY>';
    const SCREENSHARE_APP_ID = 'poeimciighedmedbnjlckohmddlpikci';

    let conference,
    client,
    api,
    recordingId,
    members = {},
    mute = false,
    screensareStatus = false,
    lock = false;
    let settings = {
        wait_for_moderator: false,
        e1_joins_muted: false,
        join_in_watch_mode: false,
        music_on_hold: false,
        assign_phone_number: false,
        record_all_conferences: false,
    };

    //get clique user info
    const fetchData = {
        method: 'POST',
        headers: new Headers({
            'Authorization': 'Bearer ' + API_KEY,
        }),
        mode: 'cors',
    };
    const data = await fetch(BASE_URL + '/api/v2/users', fetchData);
    const userRes = await data.json();
    const user = userRes.user;

    //create an instance of conference client
    client = new CliqueClient.Conference({
        base_url: BASE_URL,
        session_token: user.token,
    });

    //update elements on page
    const id = (id) => document.getElementById(id);
    //determine the user ID
    id('id_profile').textContent = user.uuid;

    //actions allowed to made for moderator by participants
    const actions = (uuid, appendNode) => {
        const userIdEl = document.createElement('SPAN');
        userIdEl.innerHTML = uuid;

        const kickNode = document.createElement('BUTTON');
        kickNode.className = 'btn btn-danger kick-user';
        kickNode.onclick = () => client.kickUser(uuid);
        kickNode.setAttribute('userid', uuid);
        kickNode.innerHTML = 'kick';

        const muteNode = document.createElement('BUTTON');
        muteNode.className = 'btn btn-default mute-user';
        muteNode.onclick = () => client.muteUser(uuid);
        muteNode.setAttribute('userid', uuid);
        muteNode.innerHTML = 'mute';

        const unmuteNode = document.createElement('BUTTON');
        unmuteNode.className = 'btn btn-default unmute-user';
        unmuteNode.onclick = () => client.unmuteUser(uuid);
        unmuteNode.setAttribute('userid', uuid);
        unmuteNode.innerHTML = 'unmute';

        const ssElement = document.createElement('VIDEO');
        ssElement.id = 'video-' + uuid;
        ssElement.className = 'screenshareEl';



        appendNode
            .appendChild(userIdEl);
        appendNode
            .appendChild(kickNode);
        appendNode
            .appendChild(muteNode);
        appendNode
            .appendChild(unmuteNode);
        appendNode
            .appendChild(ssElement);
    }

    //show participant list
    const addUser = (uuid) => {
        if (id('user-element-' + uuid)) return;
        const node = document.createElement('LI');
        node.id = 'user-element-' + uuid;
        actions(uuid, node);
        id('member_list').appendChild(node);
    }

    const show_members = () => {
        for (let i in members) {
            addUser(members[i].uuid);
        }
    };

    //create new conference with custom settings
    id('createConference')
        .addEventListener('click', async() => {
            const keys = Object.keys(settings);
            keys.forEach(setting => {
                const el = document.getElementsByClassName('user_' + setting);
                if (el[0]) {
                    settings[setting] = el[0].checked;
                }
            });
            conference = await client.create({ conference_settings: settings }) || conference;
            disableElements(false);
            prepareConfInfo();
        });

    //join existing conference
    id('join')
        .addEventListener('click', async() => {
            conference = {
                id: id('joinConfId').value,
            };
            conference = await client.join(conference.id);
            disableElements(false);
            prepareConfInfo();
        });

    //show conference information
    const prepareConfInfo = () => {
        id('id_conference').textContent = conference.id;
        id('invite_link').textContent =  'https://yourdomain/conference/' + conference.id;
        const keys = Object.keys(conference.Settings);
        keys.forEach(setting => {
            const el = document.getElementsByClassName('conference_' + setting);
            if (el[0] && conference.Settings[setting]) {
                el[0].setAttribute('checked', true);
            } else if (el[0]) {
                el[0].removeAttribute('checked');
            }
        });
    };

    let elMute = id('mute');
    let elLock = id('lock');
    let elScreenshare = id('screenshare');
    const unmuteHandler = () => {
        elMute.textContent = 'mute'
        elMute.classList.remove('btn-success');
        elMute.classList.add('btn-danger');
        mute = false;
    };

    const unlockHandler = () => {
        elLock.textContent = 'lock'
        elLock.classList.remove('btn-success');
        elLock.classList.add('btn-danger');
        lock = false;
    };

    //disable buttons if it not available
    const disableElements = flag => {
        const els = ['mute', 'lock', 'hangup', 'screenshare'];

        if (flag) {
            els.forEach(elName => {
                id(elName).setAttribute("disabled", true);
            });
            unmuteHandler();
            unlockHandler();
        } else {
            els.forEach(elName => {
                id(elName).removeAttribute("disabled");
            });
        }
    };
    disableElements(true);

    //invite options
    const getInviteOptions = () => ({
        // sender_name: newUser.display_name,
        conference_url: 'https://yourdomain/conference/' + conference.id,
    });

    //hangup conference
    id('hangup')
        .addEventListener('click', () => {
            client.leave();
            members = {};
            show_members();
            disableElements(true);
        });

    //add handler to mute/unmute the conference
    elMute
        .addEventListener('click', e => {
            if (!mute) {
                client.localMute();
                elMute.textContent = 'unmute';
                elMute.classList.remove('btn-danger');
                elMute.classList.add('btn-success');
                mute = true;
            } else {
                client.localUnmute();
                unmuteHandler()
            }
        });


    //add handler to lock/unlock the conference
    elLock
        .addEventListener('click', e => {
            if (!lock) {
                client.lock();
                elLock.textContent = 'unlock';
                elLock.classList.remove('btn-danger');
                elLock.classList.add('btn-success');
                lock = true;
            } else {
                client.unlock();
                unlockHandler();
            }
        });

    //add handler to start/stop screenshare
    elScreenshare
        .addEventListener('click', e => {
            if (!screensareStatus) {
                client.screenShareStart(SCREENSHARE_APP_ID);
                elScreenshare.textContent = 'screenshare stop';
                elScreenshare.classList.remove('btn-success');
                elScreenshare.classList.add('btn-danger');
                screensareStatus = true;
            } else {
                client.screenShareStop();
                elScreenshare.textContent = 'screenshare start';
                elScreenshare.classList.remove('btn-danger');
                elScreenshare.classList.add('btn-success');
                screensareStatus = false;
            }
        });

    //events
    const eventhandler = (event, name) => {
        console.log('event', name, event);
        id('currentAction').textContent = name;
    }

    client.on('init', event => {
        id('currentAction').textContent = 'init';
    });
    client.on('add-member', event => {
        eventhandler(event, 'add-member')
        if (event.listUsersInConference) members = event.listUsersInConference;
        members[event.user.uuid] = event.user;
        show_members();
        setTimeout(() => {
            if (event.listUsersInConference) {
                Object.keys(event.listUsersInConference).forEach(uuid => {
                    if (event.listUsersInConference[uuid].screen_shared) {
                        client.screenShareViewStart(uuid, document.getElementById('video-' + uuid));
                    }
                });
            }
        })
    });
    client.on('del-member', event => {
        eventhandler(event, 'del-member')
        delete members[event.uuid];
        if (id('user-element-' + event.uuid)) id('user-element-' + event.uuid).remove();
    });
    client.on('start-talking', event => eventhandler(event, 'start-talking'));
    client.on('stop-talking', event => eventhandler(event, 'stop-talking'));
    client.on('mute-member', event => eventhandler(event, 'mute-member'));
    client.on('unmute-member', event => eventhandler(event, 'unmute-member'));
    client.on('lock', event => eventhandler(event, 'lock'));
    client.on('unlock', event => eventhandler(event, 'unlock'));
    client.on('kick-member', event => eventhandler(event, 'kick-member'));
    client.on('conference-create', event => {
        eventhandler(event, 'conference-create');
    });
    client.on('conference-destroy', event => {
        eventhandler(event, 'conference-destroy');
        disableElements(true);
    });
    client.on('call-status-amd', event => eventhandler(event, 'call-status-amd'));
    client.on('call-status', event => eventhandler(event, 'call-status'));
    client.on('setgrace', event => eventhandler(event, 'setgrace'));
    client.on('start-recording', event => {
        eventhandler(event, 'start-recording')
        recordingId = event.recordingID;
    });
    client.on('stop-recording', event => eventhandler(event, 'stop-recording'));
    client.on('recording-completed', event => {
        eventhandler(event, 'recording-completed');
    });
    client.on('connected', event => eventhandler(event, 'connected'));
    client.on('disconnected', event => eventhandler(event, 'disconnected'));
    client.on('conference-join', event => eventhandler(event, 'conference-join'));
    client.on('conference-leave', event => eventhandler(event, 'conference-leave'));
    client.on('message', event => {
        eventhandler(event, 'message');
    });
    client.on('screen-share-start', event => {
        eventhandler(event, 'screen-share-start');
        if (user.uuid === event.uuid) return;
        client.screenShareViewStart(event.uuid, document.getElementById('video-' + event.uuid));
    });
    client.on('screen-share-stop', event => {
        if (user.uuid === event.uuid) {
            elScreenshare.textContent = 'screenshare start';
            elScreenshare.classList.remove('btn-danger');
            elScreenshare.classList.add('btn-success');
            screensareStatus = false;
      }
        eventhandler(event, 'screen-share-stop');
        client.screenShareViewStop(event.uuid);
    });

    client.on('error', event => eventhandler(event, 'error'));
})();