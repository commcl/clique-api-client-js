﻿chrome.runtime.onMessageExternal.addListener((message, sender, sendResponse) => {
    if (message && message.action === 'capture_screen') {
        const sources = message.sources;
        const tab = sender.tab;
        chrome.desktopCapture.chooseDesktopMedia(sources, tab, (streamId) => {
            if (!streamId) {
                sendResponse({
                    type: 'error',
                    error: 'Failed to get stream ID'
                });
            } else {
                sendResponse({
                    type: 'success',
                    sourceId: streamId
                });
            }
        });
    } else {
        sendResponse({
            type: 'success',
            extension_status: 'active'
        });
    }
    return true;
});
